import React, {useEffect} from "react";
import {
    Row,
    Col,
    Container,
} from "react-bootstrap";
import AOS from 'aos'

const SupportSystem = () => {
    useEffect(() => {
        AOS.init({
            duration: 1200,
        })
    })
    return (
        <div className="supports-system">
            <Container>
                <h1 className="text-center mb-4" data-aos="zoom-in">
                    <b>Supporting your raising money endeavours from begin to wrap up!</b>
                </h1>
                <h5 className="text-center mb-5" data-aos="zoom-in">Our Arrangements</h5>
                <Row>
                    <Col lg={3} md={6}>
                        <div className="supportSystemBox" data-aos="zoom-in">
                            <img className="mb-4" src="/images/memberShipDues.png" />
                            <h5><b>Membership Dues</b></h5>
                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a to='/'>Read More</a>
                        </div>
                    </Col>
                    <Col lg={3} md={6}>
                        <div className="supportSystemBox" data-aos="zoom-in">
                            <img className="mb-4" src="/images/mutualAid.png" />
                            <h5><b>Mutual Aid</b></h5>
                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a to='/'>Read More</a>
                        </div>
                    </Col>
                    <Col lg={3} md={6}>
                        <div className="supportSystemBox" data-aos="zoom-in">
                            <img className="mb-4" src="/images/spriritualCenters.png" />
                            <h5><b>Spriritual Centers</b></h5>
                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a to='/'>Read More</a>
                        </div>
                    </Col>
                    <Col lg={3} md={6}>
                        <div className="supportSystemBox" data-aos="zoom-in">
                            <img className="mb-4" src="/images/ticketedEvent.png" />
                            <h5><b>Ticketed Event</b></h5>
                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <a to='/'>Read More</a>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
export default SupportSystem;
