import React, { useEffect } from "react";
import {
    Row,
    Col,
    Button,
    Container,
} from "react-bootstrap";
import Slider from "react-slick";
import { AiOutlineEllipsis } from "react-icons/ai";
import AOS from "aos"

const FeatureEvent = () => {
    const blogSlider = {
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    };
    useEffect(() => {
        AOS.init({
            duration: 1200,
        })
    })
    return (
        <div className="feature-events">
            <Container>
                <Row>
                    <Col md={8} data-aos="fade-right">
                        <h1 className="mb-4">
                            <b>Supporting your raising money endeavours from begin to wrap up!</b>
                        </h1>
                        <h5>Featured Events</h5>
                    </Col>
                    <Col md={4}>
                        <Button className="ms-auto d-block" color="primary">See All</Button>
                    </Col>
                </Row>
                <Slider {...blogSlider} className="feature-slider">
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <span className="ads-tag">
                                #Mutual Aid
                            </span>
                            <div className="blog-box-content">
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                        <div className="content">
                            <h3 className="mb-3"><b>Fundraiser For the Tigray Sharia Islamic Council</b></h3>
                            <h5 className="mb-4">Zelle: 2034458696060 (AI Najashi Community Center Seattle ) EIN #: 86-349484949 EIN #: 86-349484949 EIN #: 86-349484949</h5>
                            <a to='/'>Donate Now</a>
                        </div>
                    </div>
                </Slider>
            </Container>
        </div>
    );
}
export default FeatureEvent;
