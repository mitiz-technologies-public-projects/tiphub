import React, { useEffect } from "react";
import {
    Button,
    Container,
} from "react-bootstrap"
import Slider from "react-slick"
import { AiOutlineEllipsis } from "react-icons/ai"
import AOS from 'aos'

const Blog = () => {
    const blogSlider = {
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        focusOnSelect: true,

        responsive: [
            // {
            //     breakpoint: 1024,
            //     settings: {
            //       slidesToShow: 2,
            //     }
            // },
            {
                breakpoint: 767,
                settings: {
                    focusOnSelect: true,
                    slidesToShow: 1,
                }
            }
        ]
    };
    useEffect(() => {
        AOS.init({
            duration: 1200,
        })
    })
    return (
        <div className="small-efforts">
            <Container>
                <h1 className="text-center mb-4" data-aos="zoom-in">
                    <b>Small efforts, make big change!</b>
                </h1>
                <Button className="m-auto d-block" color="primary">See All Blogs</Button>
                <Slider {...blogSlider} className="blog-slider">
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Letraset sheets containing Lorem</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Many desktop publishing sit packages</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Various versions have evolved over the years</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Many desktop publishing sit packages</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Letraset sheets containing Lorem</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className="blogs-box" data-aos="zoom-in">
                        <div className="blogs-box-bg">
                            <img className="img-fluid" src="/images/banner.jpg" alt="Image" />
                            <div className="blog-box-content">
                                <h1 className="mb-4">Various versions have evolved over the years</h1>
                                <a>
                                    <AiOutlineEllipsis />
                                </a>
                            </div>
                        </div>
                    </div>
                </Slider>
            </Container>
        </div>
    );
}
export default Blog;
