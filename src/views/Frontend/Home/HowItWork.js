import React, { useEffect } from "react"
import {
    Row,
    Col,
    Container,
    Button
} from "react-bootstrap"
import { BsFillArrowDownRightCircleFill } from "react-icons/bs"
import AOS from "aos"

const HowItWork = () => {
    useEffect(() => {
        AOS.init({
            duration: 1200,
        })
    })
    return (
        <div className="how-it-works">
            <Container>
                <Row className="mb-4 mt-2">
                    <Col lg={5} md={12}>
                        <div className="tiphub-banner-intro" data-aos="fade-left">
                            <h5>How it Works?</h5>
                            <h1>
                                Raise funds superior, quicker, and for free
                            </h1>
                            <h5 className="mt-4 mb-5 pb-3">Publishing software like Aldus PageMaker including versions of Lorem Ipsum. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h5>
                            <Button color="primary">Get Inspired</Button>
                        </div>
                    </Col>
                    <Col lg={7} md={12}>
                        <aside data-aos="zoom-out">
                            <span className="rotate-down-left">
                                <BsFillArrowDownRightCircleFill />
                            </span>
                            <img className="img-fluid" src="/images/h1-banner05.png" />
                        </aside>
                    </Col>
                </Row>
                <Container>
                    <div className="supports-system">
                        <Row>
                            <Col lg={4} md={6}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/images/signInIcon.png" />
                                    <h4>Sign In</h4>
                                    <h5 className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                                    <a to='/'>Read More</a>
                                </div>
                            </Col>
                            <Col lg={4} md={6}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/images/createProfile.png" />
                                    <h4>Create Profile</h4>
                                    <h5 className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                                    <a to='/'>Read More</a>
                                </div>
                            </Col>
                            <Col lg={4} md={12}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/images/createAnEvent.png" />
                                    <h4>Create An Event</h4>
                                    <h5 className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h5>
                                    <a to='/'>Read More</a>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Container>
            </Container>
        </div>
    );
}
export default HowItWork;
