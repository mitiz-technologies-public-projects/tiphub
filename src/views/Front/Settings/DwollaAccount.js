import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  Col,
  Row,
  Spinner, Table
} from "react-bootstrap";
import user from "../../../services/user";
import moment from "moment";

export class DwollaAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader:true,
      balance:0,
      error:false,
      transactions:[],
      pageNo:1,
      pageSize:20
    };
  }
  
  componentDidMount () {
    user.getDwollaAccount().then((res) => {
        if (res.data.success) {
          this.setState({loader:false,transactions:res.data.transactions, balance:parseFloat(res.data.balance.value)})
        }
        else if (res.data.error) {
            this.setState({error:true, loader:false})
        }
    });
  }
  
  render() {
    return (
      <>
        <Card className='modified-card'>
          <Card.Header>
            <Row>
              <Col><strong>Dwolla Account</strong></Col>
              <Col className="text-end">
                <small><strong>Balance </strong></small> <span style={{fontSize:25}} className="text-success">${this.state.balance.toFixed(2)}</span>
              </Col>
            </Row>
          </Card.Header>
          <Card.Body style={{minHeight:350}} className="modal-body">
              {this.state.loader && <p className="text-center p-5"><Spinner size="lg"/></p>}
              {this.state.error && <p className="text-center p-5"><Link to="/settings/dwolla-profile" className="btn btn-success btn-lg text-white">Please verfify your account</Link></p>}
              {!this.state.loader && !this.state.error && <Table className="table table-responsive customise_table table">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Amount</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.transactions.length > 0 ? (
                    this.state.transactions.map((ele, index) => (
                        <tr key={index}>
                          <td>{this.state.pageNo === 1
                              ? index + 1
                              : (this.state.pageNo - 1) * this.state.pageSize + index + 1}
                              . </td>
                          <td>{moment(ele.created).format("MMMM DD, YYYY")}</td>
                          <td>{ele.customer.first_name}</td>
                          <td>{ele.customer.last_name}</td>
                          <td>{ele.customer.email}</td>
                          <td>{ele.amount.currency} {ele.amount.value}</td>
                          <td>{ele.status}</td>
                        </tr>
                    ))
                    ) : (
                    <tr>
                        <td colSpan="10" className="text-center" style={{height:300}}>
                            Record not found.
                        </td>
                    </tr>
                    )}
                    {/* <tr>
                        <td colSpan="2"><Button type="button" color="danger" disabled={this.state.selectedRecords.length === 0} onClick={this.deleteMultipleRecords}><Badge color="primary" style={{width:'auto',overflow:'auto'}}>{this.state.selectedRecords.length}</Badge> Delete</Button></td>
                        <td colSpan="10">
                          <Pagination
                            activePage={this.state.pageNo}
                            itemsCountPerPage={30}
                            totalItemsCount={
                              this.state.pageDetails.totalCount?parseInt(this.state.pageDetails.totalCount):0
                            }
                            pageRangeDisplayed={5}
                            onChange={(e) => this.handlePageChange(e)}
                          />
                        </td>
                      </tr> */}
                </tbody>
            </Table>}
          </Card.Body>
        </Card>
      </>
    );
  }
}

export default DwollaAccount;
