import React, { Component } from "react";
import { Card, Col, Row, Form } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import DwollaPersonal from "./DwollaPersonal";
import DwollaBusiness from "./DwollaBusiness";

export class Dwolla extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { cType: "P" },
    };
  }

  handleChange = (e, field) => {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  };

  render() {
    return (
      <>
        <Card className="modified-card">
          <Card.Header>
            <Row>
              <Col>
                <h5>Dwolla Beneficial Details</h5>
              </Col>
              <Col className="text-right">
                <Form.Group check inline>
                  <Form.Label check>
                    <Form.Control
                      type="radio"
                      name="cType"
                      value="P"
                      checked={this.state.fields.cType === "P"}
                      onChange={(event) => this.handleChange(event, "cType")}
                    />{" "}
                    Personal
                  </Form.Label>
                </Form.Group>
                <Form.Group check inline>
                  <Form.Label check>
                    <Form.Control
                      type="radio"
                      name="cType"
                      value="B"
                      checked={this.state.fields.cType === "B"}
                      onChange={(event) => this.handleChange(event, "cType")}
                    />{" "}
                    Business
                  </Form.Label>
                </Form.Group>
              </Col>
            </Row>
          </Card.Header>
          <Card.Body>
            {this.state.fields.cType === "P" ? (
              <DwollaPersonal />
            ) : (
              <DwollaBusiness />
            )}
          </Card.Body>
        </Card>
      </>
    );
  }
}

export default Dwolla;
