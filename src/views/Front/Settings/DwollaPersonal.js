import React, { Component } from "react";
import { Button, Col, Row, Form, Spinner, Label, Card } from "react-bootstrap";
import { toast } from "react-toastify";
import LoadingOverlay from "react-loading-overlay";
import user from "../../../services/user";
import common from "../../../services/common";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export class DwollaPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: { type: "personal" },
      errors: {},
      loader: false,
      submitted: false,
      states: [],
      countries: [],
      containerLoading: true,
      showForm: true,
      iavLoader: false,
      iavContainer: false,
      dwollaCustomerUrl: null,
    };
  }
  getStates = () => {
    this.setState({ teamListLoader: true });
    common.getStates().then((res) => {
      let states = [];
      if (res.data.success) {
        res.data.states.forEach((s, i) => {
          states[i] = { label: s.state, value: s.state_code };
        });
        this.setState({ states, containerLoading: false }, () => {
          user.getBeneficial().then((res) => {
            if (res.data.success) {
              if (res.data.beneficial !== null) {
                let fields = res.data.beneficial;
                fields["dateOfBirth"] = new Date(
                  moment(res.data.beneficial.dateOfBirth)
                );
                fields["stateProvinceRegion"] = {
                  label: res.data.beneficial.state.state,
                  value: res.data.beneficial.state.state_code,
                };
                if (res.data.beneficial.ccountry !== null) {
                  fields["cCountry"] = {
                    label: res.data.beneficial.ccountry.name,
                    value: res.data.beneficial.ccountry.code,
                  };
                }
                if (res.data.beneficial.cstate !== null) {
                  fields["cStateProvinceRegion"] = {
                    label: res.data.beneficial.cstate.state_name,
                    value: res.data.beneficial.cstate.state_code,
                  };
                }
                this.setState({ fields });
              }
            }
          });
        });
      }
    });
  };
  componentDidMount = () => {
    this.getStates();
  };
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validateForm()) {
      this.setState({ submitted: true });
      let fields = this.state.fields;
      user.saveBeneficial({ fields }).then((res) => {
        this.setState({ submitted: false });
        if (res.data.success) {
          toast.success(res.data.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
          if (res.data.addBank) {
            this.setState(
              {
                dwollaCustomerUrl: res.data.dwollaCustomerUrl,
                iavLoader: true,
              },
              () => {
                this.getIavToken();
              }
            );
          }
        } else {
          if (res.data.error) {
            this.setState({ errors: res.data.message });
            toast.error(res.data.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        }
      });
    }
  };
  handleChange = (e, field) => {
    let fields = this.state.fields;
    if (e.target.type === "checkbox") {
      if (e.target.checked) {
        fields[field] = 1;
      } else {
        fields[field] = 0;
      }
    } else {
      fields[field] = e.target.value;
    }
    this.setState({ fields });
  };
  handleStateChange = (state) => {
    let fields = this.state.fields;
    fields["stateProvinceRegion"] = state;
    this.setState({ fields });
  };
  validateForm = () => {
    let formIsValid = true;
    const errors = {};
    if (!this.state.fields.firstName) {
      errors["firstName"] = "First Name can not be empty.";
      formIsValid = false;
    }
    if (!this.state.fields.lastName) {
      errors["lastName"] = "Last Name can not be empty.";
      formIsValid = false;
    }
    if (!this.state.fields.email) {
      errors["email"] = "Email can not be empty.";
      formIsValid = false;
    }
    if (!this.state.fields.dateOfBirth) {
      errors["dateOfBirth"] = "Please enter date of birth.";
      formIsValid = false;
    }
    if (!this.state.fields.address1) {
      errors["address1"] = "Address can not be blank.";
      formIsValid = false;
    }
    if (!this.state.fields.city) {
      errors["city"] = "City can not be blank.";
      formIsValid = false;
    }
    if (!this.state.fields.stateProvinceRegion) {
      errors["stateProvinceRegion"] = "Please select state.";
      formIsValid = false;
    }
    if (!this.state.fields.postalCode) {
      errors["postalCode"] = "Please enter postal code.";
      formIsValid = false;
    }
    if (!this.state.fields.ssn) {
      errors["ssn"] = "Please enter ssn.";
      formIsValid = false;
    }
    if (!this.state.fields.terms_accepted) {
      errors["term_accepted"] = "Please accept terms.";
      formIsValid = false;
    }
    if (!this.state.fields.declaration) {
      errors["declaration"] =
        "Please clarify that information you entered is correct.";
      formIsValid = false;
    }

    this.setState({ errors });
    return formIsValid;
  };
  handleDateOfBirth = (dob) => {
    let fields = this.state.fields;
    fields["dateOfBirth"] = dob;
    this.setState({ fields });
  };
  getIavToken = () => {
    let that = this;
    this.setState({ showForm: false, iavContainer: true });
    let params = {
      dwollaCustomerUrl: this.state.dwollaCustomerUrl,
    };
    common
      .getIavToken(params)
      .then((res) => {
        if (res.data.success) {
          /* microDeposits: 'true',
            fallbackToMicroDeposits: 'true' */
          this.setState({ iavLoader: false }, () => {
            var iavToken = res.data.iavToken;
            const dwolla = window.dwolla;
            dwolla.configure("sandbox");
            dwolla.iav.start(
              iavToken,
              {
                container: "iavContainer",
                stylesheets: [
                  "https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext",
                ],
              },
              function (err, res) {
                console.log(
                  "Error: " +
                    JSON.stringify(err) +
                    " -- Response: " +
                    JSON.stringify(res)
                );
                user
                  .saveFundingSourceUrl({ fundingSourceUrl: res })
                  .then((res) => {
                    if (res.data.success) {
                      toast.success(res.data.message, {
                        position: toast.POSITION.TOP_RIGHT,
                      });
                    } else if (res.data.error) {
                      toast.error(res.data.message, {
                        position: toast.POSITION.TOP_RIGHT,
                      });
                    }
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              }
            );
          });
        } else if (res.data.error) {
          console.log(res.data.message);
        }
        this.setState({ loader: false });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  render() {
    return (
      <Card className="modified-card">
        <Card.Header>
          <strong>Dwolla Profile</strong>
        </Card.Header>
        <Card.Body style={{ minHeight: 400 }}>
          <LoadingOverlay
            active={this.state.containerLoading}
            spinner
            text="Loading..."
          >
            {this.state.showForm && (
              <Form onSubmit={this.handleSubmit} method="post">
                <Row>
                  <Col md={6} className="mb-2">
                    <Form.Label for="name">First Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="firstName"
                      id="firstName"
                      value={
                        this.state.fields["firstName"]
                          ? this.state.fields["firstName"]
                          : ""
                      }
                      onChange={(event) =>
                        this.handleChange(event, "firstName")
                      }
                      invalid={this.state.errors["firstName"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["firstName"]}
                    </Form.Control.Feedback>
                  </Col>

                  <Col md={6} className="mb-2">
                    <Form.Label for="lastName">Last Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="lastName"
                      id="lastName"
                      value={
                        this.state.fields["lastName"]
                          ? this.state.fields["lastName"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "lastName")}
                      invalid={this.state.errors["lastName"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["lastName"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="email">Email</Form.Label>
                    <Form.Control
                      type="text"
                      name="email"
                      id="email"
                      value={
                        this.state.fields["email"]
                          ? this.state.fields["email"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "email")}
                      invalid={this.state.errors["email"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["email"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="email">Phone</Form.Label>
                    <Form.Control
                      type="text"
                      name="phone"
                      id="phone"
                      value={
                        this.state.fields["phone"]
                          ? this.state.fields["phone"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "phone")}
                      invalid={this.state.errors["phone"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["email"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="ssn">SSN</Form.Label>
                    <Form.Control
                      type="number"
                      name="ssn"
                      id="ssn"
                      value={
                        this.state.fields["ssn"] ? this.state.fields["ssn"] : ""
                      }
                      onChange={(event) => this.handleChange(event, "ssn")}
                      invalid={this.state.errors["ssn"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["ssn"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="dateOfBirth">Date Of Birth</Form.Label>
                    <DatePicker
                      className="form-control"
                      style={{ width: 100 + "%", float: "left" }}
                      selected={
                        this.state.fields["dateOfBirth"]
                          ? this.state.fields["dateOfBirth"]
                          : ""
                      }
                      onChange={this.handleDate}
                      dateFormat="MM-dd-yyyy"
                      showMonthDropdown
                      showYearDropdown
                      id="dateOfBirth"
                      onChange={this.handleDateOfBirth}
                    />
                    <small className="text-danger">
                      {this.state.errors["dateOfBirth"]}
                    </small>
                  </Col>
                  <Col md={12} className="mb-2">
                    <h5 className="mb-1 mt-2">Address</h5>
                    <hr className="my-2" />
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="address1">Address 1</Form.Label>
                    <Form.Control
                      type="text"
                      name="address1"
                      id="address1"
                      value={
                        this.state.fields["address1"]
                          ? this.state.fields["address1"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "address1")}
                      invalid={this.state.errors["address1"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["address1"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={6} className="mb-2">
                    <Form.Label for="address2">Address 2</Form.Label>
                    <Form.Control
                      type="text"
                      name="address2"
                      id="address2"
                      value={
                        this.state.fields["address2"]
                          ? this.state.fields["address2"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "address2")}
                      invalid={this.state.errors["address2"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["address2"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={4} className="mb-2">
                    <Form.Label for="city">City</Form.Label>
                    <Form.Control
                      type="text"
                      name="city"
                      id="city"
                      value={
                        this.state.fields["city"]
                          ? this.state.fields["city"]
                          : ""
                      }
                      onChange={(event) => this.handleChange(event, "city")}
                      invalid={this.state.errors["city"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["city"]}
                    </Form.Control.Feedback>
                  </Col>
                  <Col md={4} className="mb-2">
                    <Form.Label for="stateProvinceRegion">State</Form.Label>
                    <Select
                      name="stateProvinceRegion"
                      placeholder="Choose State"
                      value={
                        this.state.fields["stateProvinceRegion"] &&
                        this.state.fields["stateProvinceRegion"]
                      }
                      options={this.state.states ? this.state.states : []}
                      classNamePrefix="select"
                      onChange={this.handleStateChange}
                    />
                    <small className="text-danger">
                      {this.state.errors["stateProvinceRegion"]}
                    </small>
                  </Col>
                  <Col md={4} className="mb-2">
                    <Form.Label for="postalCode">Postal Code</Form.Label>
                    <Form.Control
                      type="number"
                      name="postalCode"
                      id="postalCode"
                      value={
                        this.state.fields["postalCode"]
                          ? this.state.fields["postalCode"]
                          : ""
                      }
                      onChange={(event) =>
                        this.handleChange(event, "postalCode")
                      }
                      invalid={this.state.errors["postalCode"] ? true : false}
                    />
                    <Form.Control.Feedback>
                      {this.state.errors["postalCode"]}
                    </Form.Control.Feedback>
                  </Col>
                </Row>
                <Row>
                  <Col md={10}>
                    <Row>
                      <Col md={12}>
                        <Form.Group>                       
                            <Form.Check
                              type="checkbox"
                              name="term_accepted"
                              value="1"
                              onChange={(event) =>
                                this.handleChange(event, "terms_accepted")
                              }
                              label={
                                <span>By checking this box you agree to our partner 
                                    <a
                                    href="https://www.dwolla.com/legal/tos/"
                                    target="_blank"
                                  > Dwolla's Terms Of Services </a>
                                  and
                                  <a href="https://www.dwolla.com/legal/privacy/" target="_blank"> Privacy Policy </a>
                                </span>
                              }
                            />                          
                        </Form.Group>
                        <p className="text-danger">
                          {this.state.errors["term_accepted"]}
                        </p>
                      </Col>
                      <Col md={12}>
                        <Form.Group>
                            <Form.Check
                              type="checkbox"
                              name="declaration"
                              value="1"
                              onChange={(event) =>
                                this.handleChange(event, "declaration")
                              }
                              label={<span>I certify that the information entered for Sample
                                Company's Controller and Beneficial Ownership
                                Information is complete and correct.</span>}
                            /> 
                        </Form.Group>
                        <p className="text-danger">
                          {this.state.errors["declaration"]}
                        </p>
                      </Col>
                    </Row>
                  </Col>

                  <Col md={2} className="text-end">
                        <Button
                        color="success"
                        type="submit"
                        disabled={this.state.submitted}
                        className="btn-lg"
                        >
                        {this.state.submitted && (
                            <Spinner size="sm" color="#887d7d" className="mr-1" />
                        )}
                        Save
                        </Button>
                  </Col>
                </Row>
              </Form>
            )}
            {this.state.iavContainer && <div id="iavContainer"></div>}
            {this.state.iavLoader && (
              <div className="text-center" style={{ height: 400 }}>
                <Spinner size="lg" />
              </div>
            )}
          </LoadingOverlay>
        </Card.Body>
      </Card>
    );
  }
}

export default DwollaPersonal;
