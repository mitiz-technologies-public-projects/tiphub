import React, { Component } from "react";
import {
  Button,
  Col,
  Row,
  Form,
  Spinner,
} from "react-bootstrap";
import { toast } from "react-toastify";
import LoadingOverlay from "react-loading-overlay";
import user from "../../../services/user";
import common from "../../../services/common";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export class DwollaBusiness extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {businessType:'soleProprietorship',type:'business'},
            errors: {},
            loader: false,
            submitted: false,
            states: [],
            cStates: [],
            countries: [],
            showForm:true,
            iavLoader:false,
            iavContainer:false,
            dwollaCustomerUrl:null,
            containerLoading:true
        };
    }
    getCountries = () => {
        common.getCountries().then((res) => {
            let countries = [];
            if (res.data.success) {
                res.data.countries.forEach((c, i) => {
                    countries[i] = { label: c.name, value: c.code, states: c.states }
                })
                this.setState({ countries, containerLoading: false });
            }
        });
    };
    getStates = () => {
        common.getStates().then((res) => {
            let states = [];
            if (res.data.success) {
                res.data.states.forEach((s, i) => {
                    states[i] = { label: s.state, value: s.state_code }
                })
                this.setState({states, containerLoading: false}, ()=>{
                    user.getBeneficial().then((res) => {
                        if (res.data.success) {
                            if(res.data.beneficial !== null){
                                let fields = res.data.beneficial;
                                fields['dateOfBirth'] = new Date(moment(res.data.beneficial.dateOfBirth))
                                fields['stateProvinceRegion'] = {label:res.data.beneficial.state.state, value: res.data.beneficial.state.state_code};
                                if(res.data.beneficial.cDateOfBirth !== null){
                                    fields['cDateOfBirth'] = new Date(moment(res.data.beneficial.cDateOfBirth))
                                }
                                
                                if(res.data.beneficial.cstate !== null){
                                    fields['cStateProvinceRegion'] = {label:res.data.beneficial.cstate.state_name, value: res.data.beneficial.cstate.state_code};
                                }
                                if(res.data.beneficial.ccountry !== null){
                                    fields['cCountry'] = {label:res.data.beneficial.ccountry.name, value: res.data.beneficial.ccountry.code};
                                }
                                this.setState({ fields });
                            }
                        }
                    });
                });
            }
        });
    };
    componentDidMount = () => {
        this.getCountries();
        this.getStates();
    };
    handleSubmit = (e) => {
        e.preventDefault();
        if (this.validateForm()) {
          this.setState({ submitted: true });
          let fields = this.state.fields;
          user.saveBeneficial({ fields }).then((res) => {
            this.setState({ submitted: false });
            if (res.data.success) {
              toast.success(res.data.message, {
                position: toast.POSITION.TOP_RIGHT,
              });
              if(res.data.addBank){
                this.setState({dwollaCustomerUrl:res.data.dwollaCustomerUrl, iavLoader:true},()=>{
                    this.getIavToken();
                })
              }
            } else {
              if (res.data.error) {
                this.setState({ errors: res.data.message });
                toast.error(res.data.message, {
                  position: toast.POSITION.TOP_RIGHT,
                });
              }
            }
          });
        }
    };
    handleChange = (e, field) => {
        let fields = this.state.fields;
        if(e.target.type === 'checkbox'){
            if(e.target.checked){
                fields[field] = 1;
            }
            else{
                fields[field] = 0;
            }
        }
        else{
            fields[field] = e.target.value;
        }
        this.setState({ fields });
    };
    handleCountryChange = (country) => {
        let fields = this.state.fields;
        fields["cCountry"] = country;
        let cStates = [];
        this.setState({ cStates }, () => {
            if (country.states.length > 0) {
                country.states.forEach((s, i) => {
                    if (i > 0) {
                        cStates[i] = { label: s.state, value: s.state_code }
                    }
                });
            }
            this.setState({ fields, cStates });
        });
    };
    handlePassportCountryChange = (country) => {
        let fields = this.state.fields;
        fields["passport_country"] = country;
        this.setState({ fields });
    };
    handleStateChange = (state) => {
        let fields = this.state.fields;
        fields["stateProvinceRegion"] = state;
        this.setState({ fields });
    };
    handleControllerStateChange = (state) => {
        let fields = this.state.fields;
        fields["cStateProvinceRegion"] = state;
        this.setState({ fields });
    };
    validateForm = () => {
        let formIsValid = true;
        const errors = {};
        if (!this.state.fields.businessType) {
            errors["businessType"] = "Please select type of business.";
            formIsValid = false;
        }
        if (!this.state.fields.firstName) {
            errors["firstName"] = "First Name can not be empty.";
            formIsValid = false;
        }
        if (!this.state.fields.lastName) {
            errors["lastName"] = "Last Name can not be empty.";
            formIsValid = false;
        }
        if (!this.state.fields.email) {
            errors["email"] = "Email can not be empty.";
            formIsValid = false;
        }
        if (!this.state.fields.dateOfBirth) {
            errors["dateOfBirth"] = "Please enter date of birth.";
            formIsValid = false;
        }
        if (!this.state.fields.address1) {
            errors["address1"] = "Address can not be blank.";
            formIsValid = false;
        }
        if (!this.state.fields.city) {
            errors["city"] = "City can not be blank.";
            formIsValid = false;
        }
        if (!this.state.fields.stateProvinceRegion) {
            errors["stateProvinceRegion"] = "Please select state.";
            formIsValid = false;
        }
        if (!this.state.fields.postalCode) {
            errors["postalCode"] = "Please enter postal code.";
            formIsValid = false;
        }
        if (!this.state.fields.businessName) {
            errors["businessName"] = "Please enter business name.";
            formIsValid = false;
        }
        if(this.state.fields.businessType && this.state.fields.businessType !== 'soleProprietorship'){
            if (!this.state.fields.cFirstName) {
                errors["cFirstName"] = "First Name can not be empty.";
                formIsValid = false;
            }
            if (!this.state.fields.cLastName) {
                errors["cLastName"] = "Last Name can not be empty.";
                formIsValid = false;
            }
            if (!this.state.fields.title) {
                errors["title"] = "Title can not be blank.";
                formIsValid = false;
            }
            if (!this.state.fields.cDateOfBirth) {
                errors["cDateOfBirth"] = "Please enter date of birth.";
                formIsValid = false;
            }
            if (!this.state.fields.businessClassification) {
                errors["businessClassification"] = "Please enter business classification number.";
                formIsValid = false;
            }
            if (!this.state.fields.ein) {
                errors["ein"] = "Please enter ein number.";
                formIsValid = false;
            }
            if (this.state.fields.cCountry && parseInt(this.state.fields.cCountry.value) === 254) {
                if (!this.state.fields.sSsn) {
                    errors["sSsn"] = "Please enter ssn.";
                    formIsValid = false;
                }
                if (!this.state.fields.cPostalCode) {
                    errors["cPostalCode"] = "Please enter postal code.";
                    formIsValid = false;
                }
            }
            if (!this.state.fields.cAddress1) {
                errors["cAddress1"] = "Address can not be blank.";
                formIsValid = false;
            }
            if (!this.state.fields.cCity) {
                errors["cCity"] = "City can not be blank.";
                formIsValid = false;
            }
            if (!this.state.fields.cStateProvinceRegion	) {
                errors["cStateProvinceRegion"] = "Please choose state.";
                formIsValid = false;
            }
        }
        if (!this.state.fields.terms_accepted) {
            errors["term_accepted"] = "Please accept terms.";
            formIsValid = false;
        }
        if (!this.state.fields.declaration) {
            errors["declaration"] = "Please clarify that information you entered is correct.";
            formIsValid = false;
        }
        this.setState({ errors },()=>{
            console.log(errors);
        });
        return formIsValid;
    };
    handleDateOfBirth = (dob) => {
        let fields = this.state.fields;
        fields["dateOfBirth"] = dob;
        this.setState({ fields });
    }
    handleCdateOfBirth = (dob) => {
        let fields = this.state.fields;
        fields["cDateOfBirth"] = dob;
        this.setState({ fields });
    }
    getIavToken = () => {
        let that = this;
        this.setState({ showForm: false, iavContainer: true });
        let params = {
            dwollaCustomerUrl: this.state.dwollaCustomerUrl
        }
        common.getIavToken(params).then((res) => {
            if (res.data.success) {
                /* microDeposits: 'true',
                fallbackToMicroDeposits: 'true' */
                this.setState({ iavLoader: false }, () => {
                    var iavToken = res.data.iavToken;
                    const dwolla = window.dwolla;
                    dwolla.configure('sandbox');
                    dwolla.iav.start(iavToken, {
                        container: 'iavContainer',
                        stylesheets: [
                            'https://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext'
                        ],
                    }, function (err, res) {
                        console.log('Error: ' + JSON.stringify(err) + ' -- Response: ' + JSON.stringify(res));
                        user.saveFundingSourceUrl({ fundingSourceUrl: res }).then((res) => {
                            if (res.data.success) {
                                toast.success(res.data.message, {
                                    position: toast.POSITION.TOP_RIGHT,
                                });
                            } else if (res.data.error) {
                                toast.error(res.data.message, {
                                    position: toast.POSITION.TOP_RIGHT,
                                });
                            }
                        })
                            .catch((err) => {
                                console.log(err);
                            });
                    });
                });
            } else if (res.data.error) {
                console.log(res.data.message);
            }
            this.setState({ loader: false });
        }).catch((err) => {
            console.log(err);
        });
    };
    render() {
        return (
            <LoadingOverlay active={this.state.containerLoading} spinner text="Loading...">
                {this.state.showForm && <Form onSubmit={this.handleSubmit} method="post">
                    <Form.Group row>
                        <Col md={6} className="mb-2">
                            <Form.Label for="businessType">Business Type</Form.Label>
                            <Form.Label type="select" name="businessType" id="businessType" value={this.state.fields["businessType"] ? this.state.fields["businessType"] : ""} onChange={(event) => this.handleChange(event, "businessType")}>
                                <option value="">Select</option>
                                <option value="soleProprietorship">Sole Proprietorship</option>
                                <option value="corporation">Corporation</option>
                                <option value="llc">LLC</option>
                                <option value="partnership">Partnership</option>
                            </Form.Label>
                            <Form.Control.Feedback>{this.state.errors["firstName"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="name">First Name</Form.Label>
                            <Form.Label
                                type="text"
                                name="firstName"
                                id="firstName"
                                value={
                                    this.state.fields["firstName"] ? this.state.fields["firstName"] : ""
                                }
                                onChange={(event) => this.handleChange(event, "firstName")}
                                invalid={this.state.errors["firstName"] ? true : false}
                            />
                            <Form.Control.Feedback>{this.state.errors["firstName"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="lastName">Last Name</Form.Label>
                            <Form.Label
                                type="text"
                                name="lastName"
                                id="lastName"
                                value={
                                    this.state.fields["lastName"]
                                        ? this.state.fields["lastName"]
                                        : ""
                                }
                                onChange={(event) => this.handleChange(event, "lastName")}
                                invalid={this.state.errors["lastName"] ? true : false}
                            />
                            <Form.Control.Feedback>{this.state.errors["lastName"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="email">Email</Form.Label>
                            <Form.Label
                                type="text"
                                name="email"
                                id="email"
                                value={
                                    this.state.fields["email"]
                                    ? this.state.fields["email"]
                                    : ""
                                }
                                onChange={(event) => this.handleChange(event, "email")}
                                invalid={this.state.errors["email"] ? true : false}
                            />
                            <Form.Control.Feedback>{this.state.errors["email"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="phone">Phone</Form.Label>
                            <Form.Label
                                type="text"
                                name="phone"
                                id="phone"
                                value={
                                    this.state.fields["phone"]
                                    ? this.state.fields["phone"]
                                    : ""
                                }
                                onChange={(event) => this.handleChange(event, "phone")}
                                invalid={this.state.errors["phone"] ? true : false}
                            />
                            <Form.Control.Feedback>{this.state.errors["phone"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="ssn">SSN</Form.Label>
                            <Form.Label
                                type="text"
                                name="ssn"
                                id="ssn"
                                value={
                                    this.state.fields["ssn"]
                                        ? this.state.fields["ssn"]
                                        : ""
                                }
                                onChange={(event) => this.handleChange(event, "ssn")}
                                invalid={this.state.errors["ssn"] ? true : false}
                            />
                            <Form.Control.Feedback>{this.state.errors["ssn"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="dateOfBirth">Date Of Birth</Form.Label>
                            <DatePicker
                                className="form-control"
                                style={{ width: 100 + "%", float: "left" }}
                                selected={
                                    this.state.fields["dateOfBirth"]
                                        ? this.state.fields["dateOfBirth"]
                                        : ""
                                }
                                onChange={this.handleDate}
                                dateFormat="MM-dd-yyyy"
                                showMonthDropdown
                                showYearDropdown
                                id="dateOfBirth"
                                onChange={this.handleDateOfBirth}
                            />
                            <small className="text-danger">{this.state.errors["dateOfBirth"]}</small>
                        </Col>
                        
                        <Col md={6} className="mb-2">
                            <Form.Label for="address1">Address 1</Form.Label>
                            <Form.Label
                                type="text"
                                name="address1"
                                id="address1"
                                value={
                                    this.state.fields["address1"]
                                        ? this.state.fields["address1"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "address1")
                                }
                                invalid={
                                    this.state.errors["address1"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["address1"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="address2">Address 2</Form.Label>
                            <Form.Label
                                type="text"
                                name="address2"
                                id="address2"
                                value={
                                    this.state.fields["address2"]
                                        ? this.state.fields["address2"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "address2")
                                }
                                invalid={
                                    this.state.errors["address2"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["address2"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="city">City</Form.Label>
                            <Form.Label
                                type="text"
                                name="city"
                                id="city"
                                value={
                                    this.state.fields["city"]
                                        ? this.state.fields["city"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "city")
                                }
                                invalid={
                                    this.state.errors["city"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["city"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="stateProvinceRegion">State</Form.Label>
                            <Select
                                name="stateProvinceRegion"
                                placeholder='Choose State'
                                value={this.state.fields["stateProvinceRegion"] && this.state.fields["stateProvinceRegion"]}
                                options={this.state.states ? this.state.states : []}
                                classNamePrefix="select"
                                onChange={this.handleStateChange}
                            />
                            <small className="text-danger">{this.state.errors["stateProvinceRegion"]}</small>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="postalCode">Postal Code</Form.Label>
                            <Form.Label
                                type="text"
                                name="postalCode"
                                id="postalCode"
                                value={
                                    this.state.fields["postalCode"]
                                        ? this.state.fields["postalCode"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "postalCode")
                                }
                                invalid={
                                    this.state.errors["postalCode"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["postalCode"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="businessName">Business Name</Form.Label>
                            <Form.Label
                                type="text"
                                name="businessName"
                                id="businessName"
                                value={
                                    this.state.fields["businessName"]
                                        ? this.state.fields["businessName"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "businessName")
                                }
                                invalid={
                                    this.state.errors["businessName"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["businessName"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="doingBusinessAs">Doing Business As</Form.Label>
                            <Form.Label
                                type="text"
                                name="doingBusinessAs"
                                id="doingBusinessAs"
                                value={
                                    this.state.fields["doingBusinessAs"]
                                        ? this.state.fields["doingBusinessAs"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "doingBusinessAs")
                                }
                                invalid={
                                    this.state.errors["doingBusinessAs"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["doingBusinessAs"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="businessClassification">Business Classification</Form.Label>
                            <Form.Label
                                type="text"
                                name="businessClassification"
                                id="businessClassification"
                                value={
                                    this.state.fields["businessClassification"]
                                        ? this.state.fields["businessClassification"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "businessClassification")
                                }
                                invalid={
                                    this.state.errors["businessClassification"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["businessClassification"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="ein">EIN</Form.Label>
                            <Form.Label
                                type="text"
                                name="ein"
                                id="ein"
                                value={
                                    this.state.fields["ein"]
                                        ? this.state.fields["ein"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "ein")
                                }
                                invalid={
                                    this.state.errors["ein"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["ein"]}</Form.Control.Feedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Form.Label for="website">Website</Form.Label>
                            <Form.Label
                                type="text"
                                name="website"
                                id="website"
                                value={
                                    this.state.fields["website"]
                                        ? this.state.fields["website"]
                                        : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "website")
                                }
                                invalid={
                                    this.state.errors["website"] ? true : false
                                }
                            />
                            <Form.Control.Feedback>{this.state.errors["website"]}</Form.Control.Feedback>
                        </Col>

                        {this.state.fields.businessType && this.state.fields.businessType !== 'soleProprietorship' && <>
                            <Col md={12} className="mb-2"><h5 className="mb-1 mt-2">Controller</h5><hr className="my-2" /></Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cFirstName">First Name</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cFirstName"
                                    id="cFirstName"
                                    value={
                                        this.state.fields["cFirstName"] ? this.state.fields["cFirstName"] : ""
                                    }
                                    onChange={(event) => this.handleChange(event, "cFirstName")}
                                    invalid={this.state.errors["cFirstName"] ? true : false}
                                />
                                <Form.Control.Feedback>{this.state.errors["cFirstName"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cLastName">Last Name</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cLastName"
                                    id="cLastName"
                                    value={
                                        this.state.fields["cLastName"]
                                            ? this.state.fields["cLastName"]
                                            : ""
                                    }
                                    onChange={(event) => this.handleChange(event, "cLastName")}
                                    invalid={this.state.errors["cLastName"] ? true : false}
                                />
                                <Form.Control.Feedback>{this.state.errors["cLastName"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="title">Title</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="title"
                                    id="title"
                                    value={
                                        this.state.fields["title"]
                                        ? this.state.fields["title"]
                                        : ""
                                    }
                                    onChange={(event) => this.handleChange(event, "title")}
                                    invalid={this.state.errors["title"] ? true : false}
                                />
                                <Form.Control.Feedback>{this.state.errors["title"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cSsn">SSN</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cSsn"
                                    id="cSsn"
                                    value={
                                        this.state.fields["cSsn"]
                                            ? this.state.fields["cSsn"]
                                            : ""
                                    }
                                    onChange={(event) => this.handleChange(event, "cSsn")}
                                    invalid={this.state.errors["cSsn"] ? true : false}
                                />
                                <Form.Control.Feedback>{this.state.errors["cSsn"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cDateOfBirth">Date Of Birth</Form.Label>
                                <DatePicker
                                    className="form-control"
                                    style={{ width: 100 + "%", float: "left" }}
                                    selected={
                                        this.state.fields["cDateOfBirth"]
                                            ? this.state.fields["cDateOfBirth"]
                                            : ""
                                    }
                                    onChange={this.handleDate}
                                    dateFormat="MM-dd-yyyy"
                                    showMonthDropdown
                                    showYearDropdown
                                    id="cDateOfBirth"
                                    onChange={this.handleCdateOfBirth}
                                />
                                <small className="text-danger">{this.state.errors["cDateOfBirth"]}</small>
                            </Col>
                            
                            <Col md={6} className="mb-2">
                                <Form.Label for="cAddress1">Address 1</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cAddress1"
                                    id="cAddress1"
                                    value={
                                        this.state.fields["cAddress1"]
                                            ? this.state.fields["cAddress1"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "cAddress1")
                                    }
                                    invalid={
                                        this.state.errors["cAddress1"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["cAddress1"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cAddress2">Address 2</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cAddress2"
                                    id="cAddress2"
                                    value={
                                        this.state.fields["cAddress2"]
                                            ? this.state.fields["cAddress2"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "cAddress2")
                                    }
                                    invalid={
                                        this.state.errors["cAddress2"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["cAddress2"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cAddress3">Address 3</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cAddress3"
                                    id="cAddress3"
                                    value={
                                        this.state.fields["cAddress3"]
                                            ? this.state.fields["cAddress3"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "cAddress3")
                                    }
                                    invalid={
                                        this.state.errors["cAddress3"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["cAddress3"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cCity">City</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cCity"
                                    id="cCity"
                                    value={
                                        this.state.fields["cCity"]
                                            ? this.state.fields["cCity"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "cCity")
                                    }
                                    invalid={
                                        this.state.errors["cCity"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["cCity"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cPostalCode">Postal Code</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="cPostalCode"
                                    id="cPostalCode"
                                    value={
                                        this.state.fields["cPostalCode"]
                                            ? this.state.fields["cPostalCode"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "cPostalCode")
                                    }
                                    invalid={
                                        this.state.errors["cPostalCode"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["cPostalCode"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cCountry">Country</Form.Label>
                                <Select
                                    name="cCountry"
                                    placeholder='Choose Country'
                                    value={this.state.fields["cCountry"] && this.state.fields["cCountry"]}
                                    options={this.state.countries ? this.state.countries : []}
                                    classNamePrefix="select"
                                    onChange={this.handleCountryChange}
                                />
                                <small className="text-danger">{this.state.errors["cCountry"]}</small>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="cStateProvinceRegion">State</Form.Label>
                                <Select
                                    name="cStateProvinceRegion"
                                    placeholder='Choose State'
                                    value={this.state.fields["cStateProvinceRegion"] && this.state.fields["cStateProvinceRegion"]}
                                    options={this.state.cStates ? this.state.cStates : []}
                                    classNamePrefix="select"
                                    onChange={this.handleControllerStateChange}
                                />
                                <small className="text-danger">{this.state.errors["cStateProvinceRegion"]}</small>
                            </Col>
                        </>}
                        
                        {this.state.fields.cCountry && parseInt(this.state.fields.cCountry.value) !== 254 && <>
                            <Col md={12} className="mb-2"><h5 className="mb-1 mt-2">Passport</h5><hr className="my-2" /></Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="passport_number">Number</Form.Label>
                                <Form.Label
                                    type="text"
                                    name="passport_number"
                                    id="passport_number"
                                    value={
                                        this.state.fields["passport_number"]
                                            ? this.state.fields["passport_number"]
                                            : ""
                                    }
                                    onChange={(event) =>
                                        this.handleChange(event, "passport_number")
                                    }
                                    invalid={
                                        this.state.errors["passport_number"] ? true : false
                                    }
                                />
                                <Form.Control.Feedback>{this.state.errors["passport_number"]}</Form.Control.Feedback>
                            </Col>
                            <Col md={6} className="mb-2">
                                <Form.Label for="passport_country">Country</Form.Label>
                                <Select
                                    name="passport_country"
                                    placeholder='Choose Country'
                                    value={this.state.fields["passport_country"] && this.state.fields["passport_country"]}
                                    options={this.state.countries ? this.state.countries : []}
                                    classNamePrefix="select"
                                    onChange={this.handlePassportCountryChange}
                                />
                                <small className="text-danger">{this.state.errors["passport_country"]}</small>
                            </Col>
                        </>}
                        
                    </Form.Group>
                    <Row>
                        <Col md={10}>
                            <Row>
                                <Col md={12}>
                                    <Form.Group check inline>
                                        <Form.Label check>
                                            <Form.Label type="checkbox" name="term_accepted" value="1" onChange={(event)=>this.handleChange(event, 'terms_accepted')}/> By checking this box you agree to our partner <a href="https://www.dwolla.com/legal/tos/" target="_blank">Dwolla's Terms Of Services</a> and <a href="https://www.dwolla.com/legal/privacy/" target="_blank">Privacy Policy</a>
                                        </Form.Label>
                                    </Form.Group>
                                    <p className="text-danger">{this.state.errors["term_accepted"]}</p>
                                </Col>
                                <Col md={12}>
                                    <Form.Group check inline>
                                        <Form.Label check>
                                            <Form.Label type="checkbox" name="declaration" value="1" onChange={(event)=>this.handleChange(event, 'declaration')}/> I certify that the information entered for Sample Company's Controller and Beneficial Ownership Information is complete and correct.
                                        </Form.Label>
                                    </Form.Group>
                                    <p className="text-danger">{this.state.errors["declaration"]}</p>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={2} className="text-right">
                            <Button
                                color="success"
                                type="submit"
                                disabled={this.state.submitted}
                                className="btn-lg"
                            >
                                {this.state.submitted && (
                                <Spinner size="sm" color="#887d7d" className="mr-1" />
                                )}
                                Save
                            </Button>
                        </Col>
                    </Row>
                </Form>}
                {this.state.iavContainer && <div id="iavContainer"></div>}
                {this.state.iavLoader && <div className="text-center" style={{height:400}}><Spinner size="lg"/></div>}
            </LoadingOverlay>
        );
    }
}

export default DwollaBusiness;
