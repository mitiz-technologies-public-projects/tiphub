import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Form,
  FormFeedback,
  FormGroup,
  Input,
  Spinner,
  Label,
  Table,
} from "react-bootstrap";
import { toast } from "react-toastify";
import LoadingOverlay from "react-loading-overlay";
import user from "../../../services/user";
import common from "../../../services/common";
import Select from "react-select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export class Dwolla extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {},
      errors: {},
      loader: false,
      submitted: false,
      states:[],
      countries:[],
      containerLoading:true
    };
  }
  getCountries = () => {
    this.setState({ teamListLoader: true });
    common.getCountries().then((res) => {
        let countries = [];
      if (res.data.success) {
        res.data.countries.forEach((c,i)=>{
            countries[i] = {label:c.name, value:c.code, states:c.states}
        })
        this.setState({ countries, containerLoading:false });
      }
    });
  };
  componentDidMount = () => {
    this.getCountries();
  };
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validateForm()) {
      this.setState({ submitted: true });
      let fields = this.state.fields;
      user.saveBeneficialOwner({ fields }).then((res) => {
        this.setState({ submitted: false });
        if (res.data.success) {
          toast.success(res.data.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          if (res.data.error) {
            this.setState({ errors: res.data.message });
            toast.error(res.data.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
        }
      });
    }
  };
  handleChange = (e, field) => {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields });
  };
  handleCountryChange = (country) => {
    let fields = this.state.fields;
    fields["country"] = country;
    let states = [];
    this.setState({states}, ()=>{
        if(country.states.length > 0){
            country.states.forEach((s,i)=>{
                if(i > 0){
                    states[i] = {label:s.state, value:s.state_code}
                }
            });
        }
        this.setState({ fields, states });
    });
  };
  handlePassportCountryChange = (country) => {
    let fields = this.state.fields;
    fields["passport_country"] = country;
    this.setState({ fields });
  };
  handleStateChange = (state) => {
    let fields = this.state.fields;
    fields["stateProvinceRegion"] = state;
    this.setState({ fields });
  };
  validateForm = () => {
    let formIsValid = true;
    const errors = {};
    if (!this.state.fields.firstName) {
      errors["firstName"] = "First Name can not be empty.";
      formIsValid = false;
    }
    if (!this.state.fields.lastName) {
      errors["lastName"] = "Last Name can not be empty.";
      formIsValid = false;
    }
    if (!this.state.fields.dateOfBirth) {
      errors["dateOfBirth"] = "Please enter date of birth.";
      formIsValid = false;
    }
    if (!this.state.fields.address1) {
        errors["address1"] = "Address can not be blank.";
        formIsValid = false;
    }
    if (!this.state.fields.city) {
        errors["city"] = "City can not be blank.";
        formIsValid = false;
    }
    if (!this.state.fields.stateProvinceRegion) {
        errors["stateProvinceRegion"] = "Please select state.";
        formIsValid = false;
    }
    if (!this.state.fields.postalCode) {
        errors["postalCode"] = "Please enter postal code.";
        formIsValid = false;
    }
    if (!this.state.fields.country) {
        errors["country"] = "Please select country.";
        formIsValid = false;
    }
    if (this.state.fields.ssn) {
        if (!this.state.fields.passport_number) {
            errors["passport_number"] = "Please enter passport number.";
            formIsValid = false;
        }
        if (!this.state.fields.passport_country) {
            errors["passport_country"] = "Please select country.";
            formIsValid = false;
        }
    }
    this.setState({ errors });
    return formIsValid;
  };
  handleDateOfBirth = (dob) => {
    let fields = this.state.fields;
    fields["dateOfBirth"] = dob;
    this.setState({ fields });
  }
  render() {
    return (
      <>
        <Card>
          <CardHeader>
            <Row>
              <Col>Dwolla Beneficial Details<hr/></Col>
            </Row>
          </CardHeader>
          <CardBody>
                <LoadingOverlay active={this.state.containerLoading} spinner text="Loading...">
                    <Form onSubmit={this.handleSubmit} method="post">
                    <FormGroup row>
                        <Col md={6} className="mb-2">
                            <Label for="name">First Name</Label>
                            <Input
                                type="text"
                                name="firstName"
                                id="firstName"
                                value={
                                    this.state.fields["firstName"] ? this.state.fields["firstName"] : ""
                                }
                                onChange={(event) => this.handleChange(event, "firstName")}
                                invalid={this.state.errors["firstName"] ? true : false}
                            />
                            <FormFeedback>{this.state.errors["firstName"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="lastName">Last Name</Label>
                            <Input
                                type="text"
                                name="lastName"
                                id="lastName"
                                value={
                                    this.state.fields["lastName"]
                                    ? this.state.fields["lastName"]
                                    : ""
                                }
                                onChange={(event) => this.handleChange(event, "lastName")}
                                invalid={this.state.errors["lastName"] ? true : false}
                            />
                            <FormFeedback>{this.state.errors["lastName"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="ssn">SSN</Label>
                            <Input
                                type="text"
                                name="ssn"
                                id="ssn"
                                value={
                                    this.state.fields["ssn"]
                                    ? this.state.fields["ssn"]
                                    : ""
                                }
                                onChange={(event) => this.handleChange(event, "ssn")}
                                invalid={this.state.errors["ssn"] ? true : false}
                            />
                            <FormFeedback>{this.state.errors["ssn"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="dateOfBirth">Date Of Birth</Label>
                            <DatePicker
                                className="form-control"
                                style={{ width: 100 + "%", float: "left" }}
                                selected={
                                    this.state.fields["dateOfBirth"]
                                    ? this.state.fields["dateOfBirth"]
                                    : ""
                                }
                                onChange={this.handleDate}
                                dateFormat="MM-dd-yyyy"
                                showMonthDropdown
                                showYearDropdown
                                id="dateOfBirth"
                                onChange={this.handleDateOfBirth}
                            />
                            <small className="text-danger">{this.state.errors["dateOfBirth"]}</small>
                        </Col>
                        <Col md={12} className="mb-2"><h5 className="mb-1 mt-2">Address</h5><hr className="my-2"/></Col>
                        <Col md={6} className="mb-2">
                            <Label for="address1">Address 1</Label>
                            <Input
                                type="text"
                                name="address1"
                                id="address1"
                                value={
                                    this.state.fields["address1"]
                                    ? this.state.fields["address1"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "address1")
                                }
                                invalid={
                                    this.state.errors["address1"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["address1"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="address2">Address 2</Label>
                            <Input
                                type="text"
                                name="address2"
                                id="address2"
                                value={
                                    this.state.fields["address2"]
                                    ? this.state.fields["address2"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "address2")
                                }
                                invalid={
                                    this.state.errors["address2"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["address2"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="address3">Address 3</Label>
                            <Input
                                type="text"
                                name="address3"
                                id="address3"
                                value={
                                    this.state.fields["address3"]
                                    ? this.state.fields["address3"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "address3")
                                }
                                invalid={
                                    this.state.errors["address3"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["address3"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="city">City</Label>
                            <Input
                                type="text"
                                name="city"
                                id="city"
                                value={
                                    this.state.fields["city"]
                                    ? this.state.fields["city"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "city")
                                }
                                invalid={
                                    this.state.errors["city"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["city"]}</FormFeedback>
                        </Col>
                        <Col md={4} className="mb-2">
                            <Label for="country">Country</Label>
                            <Select
                                name="country"
                                placeholder='Choose Country'
                                value={this.state.fields["country"] && this.state.fields["country"]}
                                options={this.state.countries ? this.state.countries : []}
                                classNamePrefix="select"
                                onChange={this.handleCountryChange}
                            />
                            <small className="text-danger">{this.state.errors["country"]}</small>
                        </Col>
                        <Col md={4} className="mb-2">
                            <Label for="stateProvinceRegion">State</Label>
                            <Select
                                name="stateProvinceRegion"
                                placeholder='Choose State'
                                value={this.state.fields["stateProvinceRegion"] && this.state.fields["stateProvinceRegion"]}
                                options={this.state.states ? this.state.states : []}
                                classNamePrefix="select"
                                onChange={this.handleStateChange}
                            />
                            <small className="text-danger">{this.state.errors["stateProvinceRegion"]}</small>
                        </Col>
                        <Col md={4} className="mb-2">
                            <Label for="postalCode">Postal Code</Label>
                            <Input
                                type="text"
                                name="postalCode"
                                id="postalCode"
                                value={
                                    this.state.fields["postalCode"]
                                    ? this.state.fields["postalCode"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "postalCode")
                                }
                                invalid={
                                    this.state.errors["postalCode"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["postalCode"]}</FormFeedback>
                        </Col>
                        <Col md={12} className="mb-2"><h5 className="mb-1 mt-2">Passport</h5><hr className="my-2"/></Col>
                        <Col md={6} className="mb-2">
                            <Label for="passport_number">Number</Label>
                            <Input
                                type="text"
                                name="passport_number"
                                id="passport_number"
                                value={
                                    this.state.fields["passport_number"]
                                    ? this.state.fields["passport_number"]
                                    : ""
                                }
                                onChange={(event) =>
                                    this.handleChange(event, "passport_number")
                                }
                                invalid={
                                    this.state.errors["passport_number"] ? true : false
                                }
                            />
                            <FormFeedback>{this.state.errors["passport_number"]}</FormFeedback>
                        </Col>
                        <Col md={6} className="mb-2">
                            <Label for="passport_country">Country</Label>
                            <Select
                                name="passport_country"
                                placeholder='Choose Country'
                                value={this.state.fields["passport_country"] && this.state.fields["passport_country"]}
                                options={this.state.countries ? this.state.countries : []}
                                classNamePrefix="select"
                                onChange={this.handlePassportCountryChange}
                            />
                            <small className="text-danger">{this.state.errors["passport_country"]}</small>
                        </Col>
                    </FormGroup>
                    <Row>
                        <Col md={12} className="text-right">
                        <Button
                            color="success"
                            type="submit"
                            disabled={this.state.submitted}
                            className="btn-lg"
                        >
                            {this.state.submitted && (
                            <Spinner size="sm" color="#887d7d" className="mr-1" />
                            )}
                            Save
                        </Button>
                        </Col>
                    </Row>
                    </Form>
                </LoadingOverlay>
          </CardBody>
        </Card>
      </>
    );
  }
}

export default Dwolla;
