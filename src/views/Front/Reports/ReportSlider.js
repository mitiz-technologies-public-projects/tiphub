import React, { useState } from "react";
import Carousel from "react-elastic-carousel";
import {
  Row,
  Col
} from "react-bootstrap";

const breakPoints = [
  { width: 1, itemsToShow: 1 },
  { width: 550, itemsToShow: 2, itemsToScroll: 2 },
  { width: 768, itemsToShow: 3 },
  { width: 1200, itemsToShow: 4 }
];

function ReportSlider() {
  return (
    <>
      <Carousel breakPoints={breakPoints}>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/venmoIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>01</b></h4>
               <p>venmo</p>
            </Col>
          </Row>
        </div>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/cashAppIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>02</b></h4>
               <p>Cash App</p>
            </Col>
          </Row>
        </div>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/venmoIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>03</b></h4>
               <p>Zelle</p>
            </Col>
          </Row>
        </div>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/venmoIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>04</b></h4>
               <p>venmo</p>
            </Col>
          </Row>
        </div>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/venmoIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>05</b></h4>
               <p>venmo</p>
            </Col>
          </Row>
        </div>
        <div className="item">
          <Row>
            <Col sm={5}>
               <img src="/assets/venmoIcon.svg" />
            </Col>
            <Col sm={7}>
               <h4><b>06</b></h4>
               <p>venmo</p>
            </Col>
          </Row>
        </div>
      </Carousel>
    </>
  );
}


export default ReportSlider;
