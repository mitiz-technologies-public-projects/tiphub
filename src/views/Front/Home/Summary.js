import React, { Component } from "react";
import { Row, Col, Card, CardBody } from "react-bootstrap";
class Summary extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <Card className="homepage_tracker">
          <CardBody>
            <Row>
              <Col sm="3">
                <div className="tracker_box">
                  <img src="/assets/tracker_icon1.jpg" />
                  <h3>
                    <strong>950 +</strong>
                  </h3>
                  <p>
                    <strong>Projects</strong>
                  </p>
                </div>
              </Col>
              <Col sm="3">
                <div className="tracker_box">
                  <img src="/assets/tracker_icon2.jpg" />
                  <h3>
                    <strong>960 +</strong>
                  </h3>
                  <p>
                    <strong>Fund Raised</strong>
                  </p>
                </div>
              </Col>
              <Col sm="3">
                <div className="tracker_box">
                  <img src="/assets/tracker_icon3.jpg" />
                  <h3>
                    <strong>550 +</strong>
                  </h3>
                  <p>
                    <strong>Donations</strong>
                  </p>
                </div>
              </Col>
              <Col sm="3">
                <div className="tracker_box">
                  <img src="/assets/tracker_icon4.png" />
                  <h3>
                    <strong>440 +</strong>
                  </h3>
                  <p>
                    <strong>Volunteers</strong>
                  </p>
                </div>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default Summary;
