import React, { Component } from "react";
import { Row, Col, Button, Container } from "react-bootstrap";

class JoinCommunity extends Component {
  render() {
    return (
      <div className="joinCommunity">
        <Container>
          <Row>
            <Col md="12">
              <h2 className="text-center mb-3">
                <strong>Join our community today!</strong>
              </h2>
              <p className="text-center mb-4">
                Various versions have evolved over the years, sometimes by
                accident.
              </p>
              <Button className="mt-2" color="primary" type="submit">
                GET STARTED FOR FREE
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default JoinCommunity;
