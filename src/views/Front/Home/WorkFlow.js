import React, { Component } from "react";
import {
    Row,
    Col,
    Button
  } from "react-bootstrap";

class WorkFlow extends Component {
  render() {
    return (
      <div className="how_it_works">
        <Row>
          <Col md="12">
            <h2 className="text-center mb-4">
              <strong>How It Works?</strong>
            </h2>
          </Col>
        </Row>
        <Row>
          <Col md="4">
            <div className="how_it_works_box">
              <div className="how_it_works_box_image">
                <img src="/assets/homepage/how_it_work.png" />
                <span>01</span>
              </div>
              <h5>
                <b>Sign In</b>
              </h5>
              <p>Using gmail, twitter and facebook you can sign in easily.</p>
              <Button color="outline-primary" type="button" onClick={()=>document.getElementById('login-box').scrollIntoView({behavior: 'smooth'})}>
                See More
              </Button>
            </div>
          </Col>
          <Col md="4">
            <div className="how_it_works_box">
              <div className="how_it_works_box_image">
                <img src="/assets/homepage/how_it_work2.png" />
                <span className="light-orange">02</span>
              </div>
              <h5>
                <b>Create Profile</b>
              </h5>
              <p>Add an image, your cashapp, venmo, paypal and send your profile out into the world.</p>
              <Button color="outline-primary" type="button"  onClick={()=>document.getElementById('login-box').scrollIntoView({behavior: 'smooth'})}>
                See More
              </Button>
            </div>
          </Col>
          <Col md="4">
            <div className="how_it_works_box">
              <div className="how_it_works_box_image">
                <img src="/assets/homepage/how_it_work3.png" />
                <span className="light-green">03</span>
              </div>
              <h5>
                <b>Create An Event</b>
              </h5>
              <p>Create a fundraiser, team event or an activity that helps you raise money for a specific cause.</p>
              <Button color="outline-primary" type="button"  onClick={()=>document.getElementById('login-box').scrollIntoView({behavior: 'smooth'})}>
                See More
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default WorkFlow;
