import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Spinner,
  Container,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import user from "../../../services/user";
class FeaturedProfiles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      featuresList: [],
      loader: true,
    };
  }

  componentDidMount = () => {
    user
      .featuredProfile()
      .then((res) => {
        if (res.data.success) {
          this.setState({ loader: false, featuresList: res.data.users });
        } else {
          toast.error(res.data.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      })
      .catch((err) => {
        toast.error("Unexpected Error!", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  render() {
    return (
      <div className="featured-profiles">
        <Container>
          <h2 className="text-center mb-4">
            <strong>Featured Profiles</strong>
          </h2>
          {this.state.featuresList.length > 0 && (
            <div className="featured-Profile">
              {this.state.loader ? (
                <Spinner className="mt-4" />
              ) : (
                <Row>
                  {this.state.featuresList.map((ele, index) => (
                    <Col md="6" sm="12" key={`featured-profile-${index}`}>
                      <Card>
                        <Link
                          to={`/${ele.username}`}
                          target="_blank"
                          className="featured-profiles-anchor"
                        >
                          <CardBody>
                            <div className="profile">
                              <div className="row">
                                <div className="col-7">
                                  <div className="profile-details">
                                    <h4>{ele.name}</h4>
                                    <p className="mt-2 mb-4 text-muted">{ele.description}</p>
                                    <Row>
                                      <Col sm={12}>
                                        <h6>Tipping apps</h6>
                                      </Col>
                                      <Col sm={12}>
                                        {ele.services.length > 0 &&
                                          ele.services.map((element, i) => (
                                            <a
                                              href={element.url}
                                              target="_blank"
                                              rel="noopener noreferrer"
                                              key={`test-key-${i}`}
                                            >
                                              <img
                                                src={`/assets/${element.appname.icon}`}
                                                className="tipping-app"
                                              />
                                            </a>
                                          ))}
                                      </Col>
                                    </Row>
                                  </div>
                                </div>
                                <div className="col-5">
                                  <div className="profile-pic">
                                    <img
                                      src={`${this.props.apiUrl}/${
                                        ele.image !== null
                                          ? `/web/profile_images/${ele.image}`
                                          : `/assets/no-profile-image.png`
                                      }`}
                                      size="lg"
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </CardBody>
                        </Link>
                      </Card>
                    </Col>
                  ))}
                </Row>
              )}
            </div>
          )}
        </Container>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    apiUrl: state.apiUrl,
  };
};
export default connect(mapStateToProps)(FeaturedProfiles);
