import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Row, Col, Button, Container} from "react-bootstrap";
import axios from "axios";
import { toast } from "react-toastify";
import { BsPlus, BsFillArrowDownRightCircleFill, BsCheckSquareFill, BsShare, BsCurrencyDollar } from "react-icons/bs";
import {Link, Navigate} from 'react-router-dom'
import Slider from "react-slick";
import { AiOutlineEllipsis } from "react-icons/ai"
import FeaturedEvents from "./FeaturedEvents";
import Blogs from "./Blogs";
import Signup from "./Signup"



const Home = () => {
    const [showSignupModal, setShowSignupModal] = useState(false);
    const giversSlider = {
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
          {
              breakpoint: 1024,
              settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
              }
          },
          {
              breakpoint: 767,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
              }
          }
      ]
  };
  const blogSlider = {
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    focusOnSelect: true,

    responsive: [
        // {
        //     breakpoint: 767,
        //     settings: {
        //       slidesToShow: 2,
        //     }
        // },
        {
            breakpoint: 767,
            settings: {
                focusOnSelect: true,
                slidesToShow: 1,
            }
        }
    ]
  };
  const testimonialSlider = {
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 576,
            settings: {
                slidesToShow: 1,
            }
        }
    ]

};
  const apiUrl = useSelector((state) => state.apiUrl);
  const userId = useSelector((state) => state.userId);
  const userName = useSelector((state) => state.userName);
  const [isLoader, setLoader] = useState(false);
  const [buttonLoader, setButtonLoader] = useState(false);

  const [showModal, setLoginModal] = useState(false);
  const [showUpdateUsernameModal, setUpdateUsernameModal] = useState(false);
  const [dataToUpdate, setDataToUpdate] = useState(null);

  //const history = useHistory();

  const [errors, setErrors] = useState({});
  const [fields, setFields] = useState({});

  const [signupModal, setSignupModal] = useState(false);

  //const [showForgotPasswordModal, setForgotPasswordModal] = useState(false);
  const dispatch = useDispatch();

  const facebookId = "809703702992782";
  const googleClientId =
    "373805934341-lvhcmiccakdb9r6giag6i2f7dd90udn0.apps.googleusercontent.com";

  const responseTwitter = (response) => {
    setLoader(true);
    if (response.ok) {
      response.json().then((result) => {
        let image = "";
        if (result.profile_image_url) {
          image = result.profile_image_url;
        }
        let params = {
          fields: {
            name: result.name,
            email: result.email,
            image: image,
            twitter_id: result.id,
            twitter_username: result.screen_name,
            username: result.screen_name,
            description: result.description,
          },
        };

        axios
          .post(apiUrl + "/user/social-login", params)
          .then(function (response) {
            if (response.data.success) {
              localStorage.setItem("token", response.data.token);
              localStorage.setItem("userType", response.data.user_type);
              localStorage.setItem("userName", response.data.twitter_username);
              localStorage.setItem("userName", response.data.username);
              localStorage.setItem("uName", response.data.name);
              localStorage.setItem("userId", response.data.user_id);
              localStorage.setItem("userImage", response.data.image);
              localStorage.setItem("userEmail", response.data.email);
              localStorage.setItem(
                "userDescription",
                response.data.description
              );
              let payload = {
                token: response.data.token,
                authenticated: true,
                userName: response.data.username,
                uName: response.data.name,
                userId: response.data.user_id,
                userType: response.data.user_type,
                userImage: response.data.image,
                userDescription: response.data.description,
                userEmail: response.data.email,
              };
              dispatch({ type: "login", ...payload });
              setLoader(false);
            } else if (response.data.error) {
              if (response.data.message.username) {
                setUpdateUsernameModal(true);
                setErrors(response.data.message);
                setFields(response.data.userDetails);
                setLoader(false);
              }
            }
          })
          .catch(function (error) {
            console.log(error);
          });
      });
    } else {
      setLoader(false);
      toast.error(response, {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  };

  const responseGoogle = (response) => {
    setLoader(true);
    if (!response.error) {
      let params = {
        fields: {
          name: response.profileObj.name,
          google_username: `G${response.profileObj.googleId}`,
          username: `G${response.profileObj.googleId}`,
          email: response.profileObj.email,
          image: response.profileObj.imageUrl,
          google_id: response.profileObj.googleId,
        },
      };
      axios
        .post(apiUrl + "/user/social-login", params)
        .then(function (response) {
          if (response.data.success) {
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("userType", response.data.user_type);
            localStorage.setItem("userName", response.data.username);
            localStorage.setItem("userId", response.data.user_id);
            localStorage.setItem("userImage", response.data.image);
            localStorage.setItem("userEmail", response.data.email);
            localStorage.setItem("uName", response.data.name);

            let payload = {
              token: response.data.token,
              userName: response.data.username,
              uName: response.data.name,
              userId: response.data.user_id,
              userType: response.data.user_type,
              userImage: response.data.image,
              userEmail: response.data.email,
            };
            dispatch({ type: "login", ...payload });
            setLoader(false);
          } else if (response.data.error) {
            if (response.data.message.username) {
              setUpdateUsernameModal(true);
              setErrors(response.data.message);
              setFields(response.data.userDetails);
              setLoader(false);
            }
          }
        })
        .catch(function (error) {
          console.log(error);
          setLoader(false);
        });
    } else {
      setLoader(false);
      toast.error("There is some problem with the authorization.", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
  };

  const responseFacebook = (response) => {
    if (
      !response.status &&
      !response.error &&
      (response.userId !== undefined || response.userId !== null)
    ) {
      setLoader(true);
      let image = "";
      if (response.picture.data.url) {
        image = response.picture.data.url;
      }
      let params = {
        fields: {
          name: response.name,
          facebook_username: `F${response.userID}`,
          username: `F${response.userID}`,
          email: response.email,
          image: image,
          facebook_id: response.userID,
        },
      };

      axios
        .post(apiUrl + "/user/social-login", params)
        .then(function (response) {
          if (response.data.success) {
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("userType", response.data.user_type);
            localStorage.setItem("userName", response.data.username);
            localStorage.setItem("uName", response.data.name);
            localStorage.setItem("userId", response.data.user_id);
            localStorage.setItem("userImage", response.data.image);
            localStorage.setItem("userEmail", response.data.email);

            let payload = {
              token: response.data.token,
              userName: response.data.username,
              uName: response.data.name,
              userId: response.data.user_id,
              userType: response.data.user_type,
              userImage: response.data.image,
              userEmail: response.data.email,
            };
            dispatch({ type: "login", ...payload });
            setLoader(false);
          } else if (response.data.error) {
            if (response.data.message.username) {
              setUpdateUsernameModal(true);
              setErrors(response.data.message);
              setFields(response.data.userDetails);
              setLoader(false);
            }
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  const handleUpdateUsername = (e) => {
    e.preventDefault();
    if (validateForm()) {
      setButtonLoader(true);

      axios
        .post(apiUrl + "/user/update-username", { fields: fields })
        .then(function (response) {
          if (response.data.success) {
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("userType", response.data.user_type);
            localStorage.setItem("userName", response.data.username);
            localStorage.setItem("uName", response.data.name);
            localStorage.setItem("userId", response.data.user_id);
            localStorage.setItem("userImage", response.data.image);
            localStorage.setItem("userEmail", response.data.email);

            let payload = {
              token: response.data.token,
              userName: response.data.username,
              uName: response.data.name,
              userId: response.data.user_id,
              userType: response.data.user_type,
              userImage: response.data.image,
              userEmail: response.data.email,
            };
            dispatch({ type: "login", ...payload });
            setLoader(false);
            //history.push("/home");
          } else if (response.data.error) {
            setErrors({ ...response.data.message });
            setButtonLoader(false);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };
  const handleChange = (e, field) => {
    setFields({ ...fields, [field]: e.target.value });
  };

  const validateForm = () => {
    let formIsValid = true;
    const errors = {};
    if (!fields.username) {
      errors["username"] = "Username can not be empty.";
      formIsValid = false;
    }
    setErrors(errors);
    return formIsValid;
  };
  return (
    <div>
      {userId ? <Navigate to={`/${userName}`}/> : <><div className="tiphub-banner">
          <div className="tiphub-banner-bg"></div>
          <Container>
              <Row>
                  <Col sm={12}>
                      <div className="tiphub-banner-intro text-center" data-aos="zoom-in">
                          <h1>
                              Radical Generosity Unlocked
                          </h1>
                          <h5 className="mb-5">Make it as easy as possible for your supporters to send you money.</h5>
                          <Button color="primary" onClick={()=>setShowSignupModal(true)}>Sign up - it's free</Button>
                      </div>
                  </Col>

                  <Col sm={12}>
                      <div className="tiphub-banner-bottom text-center" data-aos="fade-up">
                          <span className="trangle"></span>
                          <img className="img-fluid" src="/images/Group 1222.png" alt="image" />
                      </div>
                  </Col>
              </Row>
          </Container>
          <Row>
              <Col sm={12}>
                  <div className="tiphub-banner-middle">
                      <Container className="d-flex">
                          <div className="left-align">
                              <span className="animation"></span>
                              <div className="content">
                                  <h4><b>Do something life changing donate</b></h4>
                                  <hr />
                                  <p>All your payment apps in one place.</p>
                              </div>
                          </div>
                          <div className="right-align ms-auto" data-aos="flip-up">
                              <span className="circle loader"></span>
                              <ul>
                                  <li>
                                      <img src="/assets/Image 7.png" alt="image" />
                                  </li>
                                  <li>
                                      <img src="/assets/Mask Group 2.png" alt="image" />
                                  </li>
                                  <li>
                                      <h6><b>Connect More People</b></h6>
                                  </li>
                              </ul>
                          </div>
                      </Container>
                  </div>
              </Col>
          </Row>
          <div className="share">
              <Link to=''>S H A R E <BsPlus /></Link>
          </div>
      </div>
      <div className="how-it-works">
            <Container>
                <Container>
                    <div className="supports-system">
                        <Row>
                            <Col md={4}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/assets/signInIcon.png" />
                                    <h4>Sign In</h4>
                                    <h5 className="mb-4">Using gmail, twitter and facebook you can sign in easily.</h5>
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/assets/createProfile.png" />
                                    <h4>Create Profile</h4>
                                    <h5 className="mb-4">Add an image, your cashapp, venmo, paypal and send your profile out into the world.</h5>
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className="supportSystemBox" data-aos="zoom-in">
                                    <img className="mb-4" src="/assets/createAnEvent.png" />
                                    <h4>Create An Event</h4>
                                    <h5 className="mb-4">Create a fundraiser, team event or an activity that helps you raise money for a specific cause.</h5>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Container>
                <Row>
                    <Col>
                        <div className="supports-system">
                            <Container>
                                <h1 className="text-center mb-4" data-aos="zoom-in">
                                    <b>Supporting your raising money endeavours from begin to wrap up!</b>
                                </h1>
                                <h5 className="text-center mb-5" data-aos="zoom-in">Our Arrangements</h5>
                                <Row>
                                    <Col lg={3} md={6}>
                                        <div className="supportSystemBox" data-aos="zoom-in">
                                            <img className="mb-4" src="/assets/memberShipDues.png" />
                                            <h5><b>Membership Dues</b></h5>
                                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <a to='/'>Read More</a>
                                        </div>
                                    </Col>
                                    <Col lg={3} md={6}>
                                        <div className="supportSystemBox" data-aos="zoom-in">
                                            <img className="mb-4" src="/assets/mutualAid.png" />
                                            <h5><b>Mutual Aid</b></h5>
                                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <a to='/'>Read More</a>
                                        </div>
                                    </Col>
                                    <Col lg={3} md={6}>
                                        <div className="supportSystemBox" data-aos="zoom-in">
                                            <img className="mb-4" src="/assets/spriritualCenters.png" />
                                            <h5><b>Spriritual Centers</b></h5>
                                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <a to='/'>Read More</a>
                                        </div>
                                    </Col>
                                    <Col lg={3} md={6}>
                                        <div className="supportSystemBox" data-aos="zoom-in">
                                            <img className="mb-4" src="/assets/ticketedEvent.png" />
                                            <h5><b>Ticketed Event</b></h5>
                                            <p className="mb-4">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <a to='/'>Read More</a>
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    </Col>
                </Row>
                <Row className="mb-4 mt-2">
                    <Col md={{ span: 5 }}>
                        <div className="tiphub-banner-intro" data-aos="fade-left">
                            <h5>How it Works?</h5>
                            <h1>
                                Raise funds superior, quicker, and for free
                            </h1>
                            <h5 className="mt-4 mb-5 pb-3">Publishing software like Aldus PageMaker including versions of Lorem Ipsum. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h5>
                            <Button color="primary">Get Inspired</Button>
                        </div>
                    </Col>
                    <Col md="7">
                        <aside data-aos="zoom-out">
                            <span className="rotate-down-left">
                                <BsFillArrowDownRightCircleFill />
                            </span>
                            <img className="img-fluid" src="/assets/h1-banner05.png" />
                        </aside>
                    </Col>
                </Row>
            </Container>
        </div>
        <div className="console-your-givers">
            <Container>
                <h1 className="text-center mb-4" data-aos="zoom-in">
                    <b>Console Your Givers</b>
                </h1>
                <h5 className="text-center mb-5" data-aos="zoom-in">Their blessing doesn't go to fees</h5>
                <div className="console-slider">
                    <Slider {...giversSlider} className="feature-slider">
                        <div className="givers-box" data-aos="zoom-in">
                            <div className="givers-box-align">
                                <h4>with Zelle</h4>
                                <h2>They give $100,<br /> you get $100</h2>
                            </div>
                        </div>
                        <div className="givers-box" data-aos="zoom-in">
                            <div className="givers-box-align">
                                <h4>with every other platform</h4>
                                <h2>They give $100,<br /> you get $96.80 or less</h2>
                            </div>
                        </div>
                        <div className="givers-box" data-aos="zoom-in">
                            <div className="givers-box-align">
                                <h4>with every other platform</h4>
                                <h2>They give $100,<br /> you get $96.80 or less</h2>
                            </div>
                        </div>
                        <div className="givers-box" data-aos="zoom-in">
                            <div className="givers-box-align">
                                <h4>with every other platform</h4>
                                <h2>They give $100,<br /> you get $96.80 or less</h2>
                            </div>
                        </div>
                        <div className="givers-box" data-aos="zoom-in">
                            <div className="givers-box-align">
                                <h4>with every other platform</h4>
                                <h2>They give $100,<br /> you get $96.80 or less</h2>
                            </div>
                        </div>
                    </Slider>
                    
                </div>
                <a to='/' className="text-center mt-5 d-block">How we do it?</a>
            </Container>
        </div>
        <Blogs />
        <FeaturedEvents />
        <div className="testimonial">
            <div className="testimonial-right-align">
                <div className="bg">
                    <span className="animation">
                        <img className="img-fluid" src="/images/Group 1224.png" alt="Image" />
                    </span>
                </div>
            </div>
            <Row>
                <Col sm={12}>
                    <div className="testimonial-top">
                        <div className="content-align">
                            <span className="rotate-down-left">
                                <BsFillArrowDownRightCircleFill />
                            </span>
                            <img className="img-fluid" src="/assets/Image-5.png" alt="Image" />
                            <div className="box" data-aos="fade-left">
                                <div className="content">
                                    <Row>
                                        <Col xs={4}>
                                            <img className="img-fluid" src="/assets/Ellipse 19.png" alt="Image" />
                                        </Col>
                                        <Col xs={6}>
                                            <div className="titleWrapper">
                                                <div className="title">
                                                    <h5><b>Michalle</b> <BsShare className="share" /></h5>
                                                    <p>2 week ago</p>
                                                    <h6><BsCurrencyDollar className="dollar" /> $ 600.00</h6>
                                                </div>
                                            </div>
                                        </Col>
                                        <Col xs={2}>
                                            <BsCheckSquareFill className="checkbox" />
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row className="testimonial-bottom-content">
                <Col md={4} data-aos="fade-right" style={{position: 'relative'}}>
                    <div className="testimonial-headline">
                        <h1 className="mb-4">
                            <b>Our customers love what we do!</b>
                        </h1>
                        <h6 className="mb-4">Testimonials</h6>
                        <h6>Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like</h6>
                    </div>
                </Col>
                <Col md={8}>
                    <Slider {...testimonialSlider} className="testimonial-slider">
                        <div className="testimonial_box" data-aos="zoom-in">
                            <div className="content">
                                <Row>
                                    <Col xs={3}>
                                        <img className="img-fluid" src="/assets/Ellipse 19.png" />
                                    </Col>
                                    <Col xs={9}>
                                        <div className="titleWrapper">
                                            <div className="title">
                                                <h4><b>Anne Doe</b></h4>
                                                <h6>Chief Executive Offier</h6>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <h6 className="mt-3">Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like</h6>
                            </div>
                        </div>
                        <div className="testimonial_box" data-aos="zoom-in">
                            <div className="content">
                                <Row>
                                    <Col xs={3}>
                                        <img className="img-fluid" src="/assets/Ellipse 35.png" />
                                    </Col>
                                    <Col xs={9}>
                                        <div className="titleWrapper">
                                            <div className="title">
                                                <h4><b>Anne Doe</b></h4>
                                                <h6>Chief Executive Offier</h6>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <h6 className="mt-3">Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including.</h6>
                            </div>
                        </div>
                        <div className="testimonial_box" data-aos="zoom-in">
                            <div className="content">
                                <Row>
                                    <Col xs={3}>
                                        <img className="img-fluid" src="/assets/Ellipse 35.png" alt="Image" />
                                    </Col>
                                    <Col xs={9}>
                                        <div className="titleWrapper">
                                            <div className="title">
                                                <h4><b>Anne Doe</b></h4>
                                                <h6>Chief Executive Offier</h6>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <h6 className="mt-3">Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including. Publishing software like Aldus PageMaker including versions of Lorem Ipsum.Publishing software like Aldus PageMaker including.</h6>
                            </div>
                        </div>
                    </Slider>
                </Col>
            </Row>
        </div >
      </>}
      {showSignupModal && <Signup 
        showModal={showSignupModal} 
        closeModal={()=>setShowSignupModal(false)}
      />}
    </div>
  );
};

export default Home;
