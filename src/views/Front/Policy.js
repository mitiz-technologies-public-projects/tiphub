import React from "react";
import { Container, Row, Col, Card } from "react-bootstrap";

const Policy = () => {
  return (
    <>
      <div className="defalut-overview tiphub-banner ">
      <div className="tiphub-banner-bg h-100"></div>
        <Container className="pb-5">
          <Card className="p-3">
            <Card.Body >
              <h1>Tiphub Acceptable Use Policy</h1>
                <hr className="mt-0" />
                <p>Please read this acceptable use policy ("policy", “AUP”) carefully before using [website] website (“website”, "service") operated by [name] ("us", 'we", "our").</p>
                <p>Services provided by us may only be used for lawful purposes. You agree to comply with all applicable laws, rules, and regulations in connection with your use of the services. Any material or conduct that in our judgment violates this policy in any manner may result in suspension or termination of the services or removal of user’s account with or without notice.</p>
                <h4>Prohibited use</h4>
                <p>You may not use the services to publish content or engage in activity that is illegal under applicable law, that is harmful to others, or that would subject us to liability, including, without limitation, in connection with any of the following, each of which is prohibited under this AUP:</p>
                <ul>
                  <li>Phishing or engaging in identity theft</li>
                  <li>Distributing computer viruses, worms, Trojan horses, or other malicious code</li>
                  <li>Distributing pornography or adult related content or offering any escort services</li>
                  <li>Promoting or facilitating violence or terrorist activities</li>
                  <li>Infringing the intellectual property or other proprietary rights of others</li>
                </ul>
                <h4>Not permitted businesses on platform</h4>
                <ul>
                  <li>MLM</li>
                  <li>Credit Repair</li>
                  <li>Nutraceuticals</li>
                  <li>Firearms</li>
                  <li>CBD/THC</li>
                </ul>
                <h4>Enforcement</h4>
                <p>Your services may be suspended or terminated with or without notice upon any violation of this policy. Any violations may result in the immediate suspension or termination of your account.</p>
                <h4>Reporting violations</h4>
                <p>To report a violation of this policy, please contact us.</p>
                <p>We reserve the right to change this policy at any given time, of which you will be promptly updated. If you want to make sure that you are up to date with the latest changes, we advise you to frequently visit this page.</p>
            </Card.Body>
          </Card>
        </Container>
      </div>
    </>
  );
};

export default Policy;
