/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import { FaTrashAlt } from 'react-icons/fa'

class ListPaymentApps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      
    }
  }

  
  render() {
    return (
        <li
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        ref={provided.innerRef}
      >
        {parseInt(ele.service_id) === 7 ||
        parseInt(ele.service_id) === 15 ||
        parseInt(ele.service_id) === 17 ||
        parseInt(ele.service_id) === 22 ||
        parseInt(ele.service_id) === 23 ||
        parseInt(ele.service_id) === 24 ? (
          <a href="javascript:void(0)">
            <div
              className="tipping-app-payment"
              onClick={() => {
                if (ele.phone){ 
                  toggleZelleModel(ele.phone);}
                else toggleZelleModel(ele.email);
              }}
            >
              <img src={`/assets/${ele.appname.icon}`} />
              <span>{ele.appname.name}</span>
            </div>
            <div className="payment-actions">
              <FaRegCopy
                className="copy"
                onClick={(e) =>
                  copyToClipBoard(
                    e,
                    `${ele.email ? ele.email : ele.url}`
                  )
                }
              />
              <FaTrashAlt
                className="delete"
                onClick={(e) => deleteService(e, ele.id)}
              />
            </div>
          </a>
        ) : (
          <a href={ele.url} target="_blank">
            <div className="tipping-app-payment">
              <img src={`/assets/${ele.appname.icon}`} />
              <span>{ele.appname.name}</span>
            </div>
            <div className="payment-actions">
              <FaRegCopy
                className="copy"
                onClick={(e) =>
                  copyToClipBoard(
                    e,
                    `${ele.email ? ele.email : ele.url}`
                  )
                }
              />
              <FaTrashAlt
                className="delete"
                onClick={(e) => deleteService(e, ele.id)}
              />
            </div>
          </a>
        )}
      </li>
    )
  }
}

export default ListPaymentApps
