import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, Form, Button, Input, FormGroup, Label, FormFeedback, Spinner, CardBody, Card, Collapse, Row, Col } from "react-bootstrap";
import { loadStripe } from '@stripe/stripe-js';
import { CardElement, Elements, ElementsConsumer, PaymentElement } from '@stripe/react-stripe-js';
import common from "../../../services/common";
import { toast } from "react-toastify";


//const stripePromise = loadStripe('pk_test_51L7nCvFzN10ERQjS769unzFhnQBPpd8jj2QOdTJZrJ1W6Dej2TB0TCPcSgUv8dOm1We2qBE2t75sl1GCKxpIonyc00pVu4H5nK');
const stripePromise = loadStripe('pk_live_51L7nCvFzN10ERQjSAQ5yJyP7Tyq7uqMz8IpfFgQkx3bKRTaf9WJHKK3FfBtANrTDwfIDGRQUh5XCOY1CkslR2dEB00HfMOsAOd');

class CheckoutForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {subscription: false},
            errors: {},
            loader: false,
            enableSubscription:false,
            recurringCycle:0,
        };
    }
    handleSubmit = async (event) => {
        event.preventDefault();
        this.setState({ loader: true })
        let fields = this.state.fields;
        let errors = {};
        let validForm = true;
        if(!fields.name){
            errors["name"] = "Please enter your name.";
            validForm = false;
        }
        if(!fields.email){
            errors["email"] = "Please enter your email.";
            validForm = false;
        }
        if(!fields.phone){
            errors["phone"] = "Please enter your phone.";
            validForm = false;
        }
        if(!fields.amount){
            errors["amount"] = "Please enter amount.";
            validForm = false;
        }
        if(!validForm){
            this.setState({errors, loader:false});
            return false;
        }

        const { stripe, elements, eventId } = this.props;

        if (!stripe || !elements) {
            return;
        }

        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: 'card',
            card: elements.getElement(CardElement),
        });
        if (!error) {
            try {
                common.createPayment({
                  event_id:eventId,
                  amount:this.state.fields.amount,
                  name:this.state.fields.name,
                  email:this.state.fields.email,
                  payment_method_id:paymentMethod.id,
                  subscription:this.state.fields.subscription,
                  recurringCycle:this.state.recurringCycle
                }).then((res) => {
                  if (res.data.success) {
                    if(res.data.payment.status === 'succeeded'){
                        toast.success(res.data.message, {
                            position: toast.POSITION.TOP_RIGHT,
                        });
                        this.setState({ loader: false }, ()=>{
                            this.props.closeModal();
                        });
                    }
                  } else if (res.data.error) {
                    console.log(res.data.message);
                    this.setState({ loader: false });
                  }
                }).catch((err) => {
                    this.setState({ loader: false });
                });
            }
            catch (error) {
                this.setState({ loader: false });
            }
        }
        else{
            toast.error(error.message, {
                position: toast.POSITION.TOP_RIGHT,
            });
            this.setState({loader:false});
        }
    };
    handleChange(field, e) {
        let fields = this.state.fields;
        if(field === 'subscription'){
            if(e.target.checked){
                fields[field] = true;
            }
            else{
                fields[field] = false;
            }
        }
        else{
            fields[field] = e.target.value;
        }
        this.setState({ fields });
        console.log(this.state.fields)
    }
    componentDidMount = () => {
        if(this.props.eventId){
            common.checkSubscriptionSetting({
                event_id: this.props.eventId,
            }).then((res) => {
            if (res.data.success) {
                this.setState({enableSubscription:true, recurringCycle: res.data.recurringCycle})
            } else if (res.data.error) {
                this.setState({enableSubscription:false})
            }}).catch((err) => {
                console.log(err);
            });
        }
    }
    render() {
        const { stripe } = this.props;
        const cardOptions = {
            iconStyle: 'solid',
            style:{
                base: {
                    iconColor: '#000000',
                    lineHeight: '2.5',
                    fontSize:'15px'
                }
            }
        };
        
        return (
            <Form onSubmit={this.handleSubmit}>
                <Card>
                    <CardBody className="bg-light">
                        <FormGroup>
                            <Label for="amount"><strong>Name</strong></Label>
                            <Input
                                type="text"
                                name="name"
                                id="name"
                                value={
                                    this.state.fields["name"]
                                        ? this.state.fields["name"]
                                        : ""
                                }
                                onChange={event =>
                                    this.handleChange("name", event)
                                }
                                invalid={
                                    this.state.errors["name"] ? true : false
                                }
                            />
                            {this.state.errors["name"] && (
                                <FormFeedback>
                                    {this.state.errors["name"]}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="email"><strong>Email</strong></Label>
                            <Input
                                type="text"
                                name="email"
                                id="email"
                                value={
                                    this.state.fields["email"]
                                        ? this.state.fields["email"]
                                        : ""
                                }
                                onChange={event =>
                                    this.handleChange("email", event)
                                }
                                invalid={
                                    this.state.errors["email"] ? true : false
                                }
                            />
                            {this.state.errors["email"] && (
                                <FormFeedback>
                                    {this.state.errors["email"]}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="phone"><strong>Phone</strong></Label>
                            <Input
                                type="text"
                                name="phone"
                                id="phone"
                                value={
                                    this.state.fields["phone"]
                                        ? this.state.fields["phone"]
                                        : ""
                                }
                                onChange={event =>
                                    this.handleChange("phone", event)
                                }
                                invalid={
                                    this.state.errors["phone"] ? true : false
                                }
                            />
                            {this.state.errors["phone"] && (
                                <FormFeedback>
                                    {this.state.errors["phone"]}
                                </FormFeedback>
                            )}
                        </FormGroup>
                        <FormGroup>
                            <Label for="amount"><strong>Amount($)</strong></Label>
                            <Input
                                type="number"
                                name="amount"
                                id="amount"
                                value={
                                    this.state.fields["amount"]
                                        ? this.state.fields["amount"]
                                        : ""
                                }
                                onChange={event =>
                                    this.handleChange("amount", event)
                                }
                                invalid={
                                    this.state.errors["amount"] ? true : false
                                }
                            />
                            {this.state.errors["amount"] && (
                                <FormFeedback>
                                    {this.state.errors["amount"]}
                                </FormFeedback>
                            )}
                        </FormGroup>
                    </CardBody>
                </Card>
                <Card className="bg-light mb-2">
                    <CardBody>
                        <CardElement options={cardOptions}/>
                    </CardBody>
                </Card>
                {this.state.enableSubscription && <Row>
                    <Col md={12}>
                        <FormGroup check>
                            <Label check>
                                <Input type="checkbox" id="subscription" onChange={(event) => this.handleChange("subscription", event)}/> I am agree to donate <strong>{parseInt(this.state.recurringCycle) === 1 ? 'Daily' : parseInt(this.state.recurringCycle) === 2 ? 'Weekly' : parseInt(this.state.recurringCycle) === 3 ? 'Monthly' : parseInt(this.state.recurringCycle) === 4 ? 'Yearly' : null}</strong>.
                            </Label>
                        </FormGroup>
                    </Col>
                </Row>}
                <Button type="submit" disabled={!stripe || this.state.loader} color="success" className="mt-2 btn-block" size="lg">
                {this.state.loader && (
                    <Spinner
                        size="sm"
                        color="#887d7d"
                        className="mr-1 mb-1"
                    />
                )}
                    Pay
                </Button>
            </Form>
        );
    }
}



export default class StripePaymentModel extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { stripe, eventId } = this.props;
        const options = {
            //clientSecret:'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'
        }
        return (
            <Modal isOpen={this.props.showModal} size="md">
                <ModalHeader toggle={() => this.props.closeModal()}>Transfer Fund</ModalHeader>
                <ModalBody>
                    <Elements stripe={stripePromise} options={options}>
                        <InjectedCheckoutForm eventId={eventId} closeModal={this.props.closeModal}/>
                    </Elements>
                </ModalBody>
            </Modal>
        );
    }
}
const InjectedCheckoutForm = (props) => {
    return <ElementsConsumer>
    {({ stripe, elements }) => (
        <CheckoutForm stripe={stripe} elements={elements} eventId={props.eventId} closeModal={props.closeModal}/>
    )}
</ElementsConsumer>
};

