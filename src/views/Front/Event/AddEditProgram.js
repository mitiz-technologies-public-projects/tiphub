/* eslint-disable react/prop-types */
import { Button, Modal, Form, Spinner, FloatingLabel } from "react-bootstrap";
import React, { Component } from "react";
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import events from "../../../services/events";
import { connect } from "react-redux";


class AddEditProgram extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loader: false,
            records: [],
            showModal: false,
            fields: { event_id: this.props.eventId, uploaded_image:"/assets/no-image.jpeg"},
            errors: {},
            submitted: false,
        };
    }
    handleChange = (e, field) => {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    };
    handleValidation = () => {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields["name"]) {
            formIsValid = false;
            errors["name"] = "Please enter name of the program.";
        }
        if (!fields["price"]) {
            formIsValid = false;
            errors["price"] = "Please enter price.";
        }
        if (!fields["description"]) {
            formIsValid = false;
            errors["description"] = "Please enter description.";
        }
        
        this.setState({ errors: errors });
        return formIsValid;
    };

    handleSubmit = e => {
        e.preventDefault();
        if (this.handleValidation()) {
            this.setState({ submitted: true });
            let params = { fields: this.state.fields };
            if (this.props.eventId) {
                events
                    .saveProgram(params)
                    .then((res) => {
                        if (res.data.success) {
                            this.setState({ submitted: false }, () => {
                                this.props.listRecords();
                                this.props.closeModal();
                                toast.success(res.data.message, {
                                    position: toast.POSITION.TOP_RIGHT,
                                });
                            });
                        } else if (res.data.error) {
                            this.setState({ errors: res.data.message, submitted: false });
                        }
                    })
                    .catch(() => {
                        toast.error("Unexpected Error !", {
                            position: toast.POSITION.TOP_RIGHT,
                        });
                    });
            }
            else {
                this.props.updateRecords(this.state.fields);
            }
        }
    };

    componentDidMount = () => {
        if (this.props.ticketId) {
            this.getProgramDetails();
        }
    }

    getProgramDetails = () => {
        this.setState({ loader: true });
        events
            .getProgramDetails({
                id: this.props.ticketId
            })
            .then((res) => {
                if (res.data.success) {
                    let fields = res.data.program;
                    fields['uploaded_image'] = this.props.baseUrl + "/api/web/events/" +res.data.program.image;
                    this.setState({ fields, loader: false });
                } else if (res.data.error) {
                    console.log(res.data.message);
                }
                this.setState({ loader: false });
            })
            .catch((err) => {
                console.log(err);
            });
    };
    uploadFile = (e) => {
        console.log(e.target)
        this.setState({ overLayLoading: true });
        const formData = new FormData();
        formData.append("image", e.target.files[0]);
        events
          .uploadEventImage(formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
              }
          })
          .then((res) => {
            if (res.data.success) {
              let fields = this.state.fields;
              fields["image"] = res.data.file;
              fields["uploaded_image"] = this.props.baseUrl + "/api/web/events/temp/" + res.data.file.file_name;
              this.setState({ fields });
            } else if (res.data.error) {
              toast.error(res.data.message, {
                position: toast.POSITION.TOP_RIGHT,
              });
            }
            this.setState({ overLayLoading: false });
          })
          .catch((err) => {
            this.setState({ overLayLoading: false });
            toast.error("Unexpected Error!", {
              position: toast.POSITION.TOP_RIGHT,
            });
          });
      };
    render() {
        return (
            <Modal 
                show={this.props.showModal}
                onHide={this.props.closeModal}
                backdrop="static"
                keyboard={false}
                centered
                size="md"
            >
                <Modal.Header closeButton><h4>Add Program</h4></Modal.Header>
                <Modal.Body>
                    <LoadingOverlay
                        active={this.state.loader}
                        spinner
                        text="Loading..."
                    >
                        <FloatingLabel
                            controlId="floatingInput"
                            label="Name"
                            className="mb-3"
                            >
                            <Form.Control
                                type="text"
                                name="name"
                                placeholder="name"
                                onChange={(event) => this.handleChange(event, "name")}
                                isInvalid={this.state.errors.name}
                            />
                            <Form.Control.Feedback type="invalid">{this.state.errors.name}</Form.Control.Feedback>
                        </FloatingLabel>
                        <FloatingLabel
                            controlId="floatingInput"
                            label="Price"
                            className="mb-3"
                            >
                            <Form.Control
                                type="number"
                                name="price"
                                placeholder="price"
                                onChange={(event) => this.handleChange(event, "price")}
                                isInvalid={this.state.errors.price}
                            />
                            <Form.Control.Feedback type="invalid">{this.state.errors.price}</Form.Control.Feedback>
                        </FloatingLabel>
                        <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Upload Image for the program</Form.Label>
                            <Form.Control type="file" onChange={(event) => this.uploadFile(event)} />
                        </Form.Group>
                        <FloatingLabel
                            controlId="floatingInput"
                            label="Description"
                            className="mb-3"
                            >
                            <Form.Control
                                type="textarea"
                                name="description"
                                placeholder="description"
                                onChange={(event) => this.handleChange(event, "description")}
                                isInvalid={this.state.errors.description}
                            />
                            <Form.Control.Feedback type="invalid">{this.state.errors.description}</Form.Control.Feedback>
                        </FloatingLabel>
                    </LoadingOverlay>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="success"
                        type="button"
                        disabled={this.state.submitted}
                        onClick={this.handleSubmit}
                    >
                        {this.state.submitted && (
                            <Spinner
                                size="sm"
                                variant="light"
                                className="mr-1"
                            />
                        )}
                        Submit
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        apiUrl: state.apiUrl,
        baseUrl: state.baseUrl,
        baseUrl: state.baseUrl,
        userId: state.userId,
        userName: state.userName,
    };
};
export default connect(mapStateToProps)(AddEditProgram);
