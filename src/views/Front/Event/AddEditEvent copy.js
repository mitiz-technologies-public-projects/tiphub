/* eslint-disable react/prop-types */
import {
  Row,
  Col,
  Input,
  Button,
  Label,
  FormFeedback,
  Spinner,
  Form,
  Modal,
  ModalHeader,
  ModalBody,FormGroup, CustomInput
} from "react-bootstrap";
import React, { Component } from "react";
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import events from "../../../services/events";
import user from "../../../services/user";
import common from "../../../services/common";
import ReactSummernote from "react-summernote";
import "react-summernote/dist/react-summernote.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { connect } from "react-redux";
import classnames from "classnames";
import ManageMembershipPlans from "./ManageMembershipPlans";
import { FaQuestionCircle } from "react-icons/fa";
import AsyncSelect from "react-select/async";
import ManageTickets from "./ManageTickets";
import NonProfitQuesion from "./NonProfitQuesion";
import ManageGroups from "./ManageGroups";
import ManagePrograms from "./ManagePrograms";

class AddEditEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        fundraising_for: "0",
        existing_gofundme_campaign: "1",
        work_for_non_profit:"0",
        require_email_collection:"1",
        recurring: "0",
        show_name: "1",
        published: 1,
        start_date:new Date(moment()),
        end_date: new Date(moment().add(7, 'day')),
        event_type:0,
        show_target:1,
        currency:0
      },
      errors: {},
      step: 1,
      submitted: false,
      defaultAmountArray: [
        { label: "$1", value: "1", checked: false },
        { label: "$5", value: "5", checked: true },
        { label: "$10", value: "10", checked: false },
        { label: "$20", value: "20", checked: false },
        { label: "Other", value: "other", checked: false },
      ],
      checkinArray: [
        { label: "In Person", value: "1", checked: false },
        { label: "Online", value: "2", checked: false },
        { label: "Both", value: "3", checked: false },
        { label: "No Check In", value: "4", checked: false },
      ],
      recurringArray: [
        { label: "Yes", value: "1" },
        { label: "No", value: "2" },
      ],
      description: null,
      overLayLoading: false,
      showOtherFeild: false,
      currentCheckedIndex: 1,
      loading: false,
      categories: [],
      currentStep: 0,
      subCategories:[],
      locations:[],
      timeStamp:Date.now(),
      showReligiousCategory: false
    };
  }

  setCurrentStep = (step) => {
    if (this.state.currentStep < this.state.step) {
      this.setState({ currentStep: step - 1 });
    }
  };

  eventFormValidation = () => {
    let formIsValid = true;
    const errors = {};
    if (this.state.step === 1) {
      if (!this.state.fields.category && this.state.fields.category !== 0) {
        formIsValid = false;
        errors["category"] = "Please choose category from the list.";
      }
      if (
        this.state.fields.existing_gofundme_campaign === "0" &&
        !this.state.fields.campaign_url
      ) {
        formIsValid = false;
        errors["campaign_url"] = "Please enter existing gofundme campaign url.";
      }
      if (this.state.fields.fundraising_for === "1") {
        if (!this.state.fields.non_profit_name) {
          formIsValid = false;
          errors["non_profit_name"] = "Please enter non profit name.";
        }
        if (!this.state.fields.city) {
          formIsValid = false;
          errors["city"] = "Please enter city.";
        }
        if (!this.state.fields.state) {
          formIsValid = false;
          errors["state"] = "Please enter state.";
        }
        if (!this.state.fields.ein_number) {
          formIsValid = false;
          errors["ein_number"] = "Please enter EIN number.";
        }
      }
      if (this.state.fields.category && (parseInt(this.state.fields.category) === 59 || parseInt(this.state.fields.category) === 71)) {
        if (!this.state.fields.location) {
          formIsValid = false;
          errors["location"] = "Please enter venue details.";
        }
        if (parseInt(this.state.fields.category) === 59 && !this.state.fields.sub_category) {
          formIsValid = false;
          errors["sub_category"] = "Please choose subcategory.";
        }
        if (!this.props.selectedEventId && !this.props.EventId && !this.state.fields.tickets) {
          formIsValid = false;
          errors["tickets"] = "Please create ticket.";
        }
      }
      if (formIsValid) {
        formIsValid = false;
        this.setState({ errors: errors, step: 2 });
        this.setCurrentStep(2);
      } else {
        formIsValid = false;
        this.setState({ errors, timeStamp:Date.now() });
      }
    } else if (this.state.step === 2) {
      if (!this.state.fields.title) {
        errors["title"] = "Event Title can not be empty";
        formIsValid = false;
      }
      if (!this.state.fields.target_amount) {
        errors["target_amount"] = "Event Target Amount can not be empty";
        formIsValid = false;
      } else if (this.state.fields.target_amount <= 0) {
        errors["target_amount"] = "Event Target Amount can not be zero or negative!";
        formIsValid = false;
      }
      if (this.state.description === null) {
        errors["description"] = "Event Description can not be empty!";
        formIsValid = false;
      }
      if (parseInt(this.state.fields.event_type) === 1 && !this.state.fields.passcode) {
        errors["passcode"] = "Passcode can not be empty!";
        formIsValid = false;
      }
      this.setState({ errors });
    }
    return formIsValid;
  };

  navigateStep = (e) => {
    let innerHTML = e.target.innerHTML;
    this.setState((prevState) => {
      if (innerHTML === "Next") {
        return { step: prevState.step + 1 };
      } else {
        return { step: prevState.step - 1 };
      }
    });
  };

  handleSubmit = (e = null) => {
    let fields = this.state.fields;
    if (e !== null) {
      e.preventDefault();
      fields["published"] = 1;
    }
    if (this.eventFormValidation()) {
      this.setState({ submitted: true });
      fields["description"] = this.state.description;
      if(fields["start_date"]){
        fields["start_date_custom_format"] = moment(fields["start_date"]).format(
          "YYYY-MM-DD h:mm:ss a"
        );
      }
      if(fields["end_date"]){
        fields["end_date_custom_format"] = moment(fields["end_date"]).format(
          "YYYY-MM-DD h:mm:ss a"
        );
      }
      
      let params = { fields: fields };
      events
        .add(params)
        .then((res) => {
          if (res.data.success) {
            this.props.closeModal();
            if (this.props.loadEvents) {
              this.props.loadEvents();
            }
            if (this.props.fetchEventDetails) {
              this.props.fetchEventDetails();
            }
            toast.success(res.data.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          } else if (res.data.error) {
            this.setState({ errors: res.data.message });
            toast.error(res.data.message, {
              position: toast.POSITION.TOP_RIGHT,
            });
          }
          this.setState({ submitted: false });
        })
        .catch((err) => {
          toast.error("Unexpected error !", {
            position: toast.POSITION.TOP_RIGHT,
          });
        });
    }
  };

  handleChange = (e, field) => {
    let step = this.state.step;
    let fields = this.state.fields;
    fields[field] = e.target.value;
    if(field === 'category'){
      if(parseInt(e.target.value) === 9){
        fields['fundraising_for'] = "1";
      }
      else{
        fields['fundraising_for'] = "0";
      }
      // if(parseInt(e.target.value) === 58){
      //   step = 2;
      // }
      if(parseInt(e.target.value) === 59){
        let category = this.state.categories.filter(c=>parseInt(c.id) === parseInt(e.target.value));
        this.setState({ subCategories: category[0].subcategory });
      }
    }
    else if(e.target.type === 'checkbox' && field === 'show_target'){
      if(e.target.checked){
        fields['show_target'] = 1;
      }
      else{
        fields['show_target'] = 0;
      }
    }
    this.setState({ fields, step, timeStamp: Date.now()});
  };

  uploadFile = (e) => {
    this.setState({ overLayLoading: true });
    const formData = new FormData();
    formData.append("image", e.target.files[0]);
    events
      .uploadEventImage(formData)
      .then((res) => {
        if (res.data.success) {
          let fields = this.state.fields;
          fields["image"] = res.data.file;
          this.setState({ fields });
        } else if (res.data.error) {
          toast.error(res.data.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
        this.setState({ overLayLoading: false });
      })
      .catch((err) => {
        this.setState({ overLayLoading: false });
        toast.error("Unexpected Error!", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  fetchEventDetails = (id) => {
    this.setState({ overLayLoading: true });
    events
      .getToEdit({ id })
      .then((res) => {
        if (res.data.success) {
          let subCategories = [];
          let eventDetails = Object.assign({}, res.data.event);
          if (eventDetails.start_date != null) {
            var t = eventDetails.start_date.split(/[- :]/);
            eventDetails.start_date = new Date(
              t[0],
              t[1] - 1,
              t[2],
              t[3],
              t[4],
              t[5]
            );
            eventDetails["start_date_custom_format"] = moment(
              new Date(eventDetails.start_date)
            ).format("YYYY-MM-DD h:mm:ss a");
          }
          if (eventDetails.end_date != null) {
            var t = eventDetails.end_date.split(/[- :]/);
            eventDetails.end_date = new Date(
              t[0],
              t[1] - 1,
              t[2],
              t[3],
              t[4],
              t[5]
            );
            eventDetails["end_date_custom_format"] = moment(
              new Date(eventDetails.end_date)
            ).format("YYYY-MM-DD h:mm:ss a");
            eventDetails["location"] = {label:eventDetails.location, value:eventDetails.location};
          }
          if(parseInt(eventDetails.category) === 59){
            let category = this.state.categories.filter(c=>parseInt(c.id) === parseInt(eventDetails.category));
            subCategories = category[0].subcategory;
          }
          this.setState({
            fields: eventDetails,
            description: eventDetails.description,
            subCategories
          });
        } else if (res.data.error) {
          toast.error(res.data.message, {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
        this.setState({ overLayLoading: false });
      })
      .catch(() => {
        this.setState({ overLayLoading: false });
        toast.error("Unexpected error !", {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };

  componentDidMount = () => {
    let categoryArr = [];
    let fields = this.state.fields;
    events.getEventCategory().then((res) => {
      if (res.data.success) {
        let categories = res.data.categories;
        user.getMembership().then((res) => {
          if (res.data.success) {
            if(res.data.membership !== null && parseInt(res.data.membership.plan_id) === 1){
              categoryArr = categories.filter(category => parseInt(category.id) === 72);
            }
            else{
              categoryArr = categories;
            }
          }
          this.setState({ categories: categoryArr, fields }, () => {
            if (this.props.selectedEventId) { // selectedEventId is used from list event page. 
              this.fetchEventDetails(this.props.selectedEventId);
            } else if (this.props.EventId) { // selectedEventId is used from list event details page. 
              this.fetchEventDetails(this.props.EventId);
            }
          });
        });
      }
    });
  };

  handleDescription = (description) => {
    this.setState({ description });
  };

  handleStartDate = (date) => {
    let fields = this.state.fields;
    fields["start_date"] = date;
    fields["start_date_custom_format"] = moment(date).format(
      "YYYY-MM-DD h:mm:ss a"
    );
    this.setState({ fields });
  };

  handleEndDate = (date) => {
    let fields = this.state.fields;
    fields["end_date"] = date;
    fields["end_date_custom_format"] = moment(date).format(
      "YYYY-MM-DD h:mm:ss a"
    );
    this.setState({ fields });
  };

  navigateTab = (e, t) => {
    if (e.target.parentElement.className.includes("hoverPointer")) {
      this.setState({ step: t });
    }
  };

  saveDraft = () => {
    let fields = this.state.fields;
    fields["published"] = 0;
    this.setState({ fields }, () => {
      this.handleSubmit();
    });
  };
  
  renderButtons = (step) => {
    return (
      <React.Fragment>
        {step > 1 && (
          <Button
            color="secondary"
            onClick={this.navigateStep}
            className="mr-1"
            style={{ width: "auto" }}
          >
            Back
          </Button>
        )}
        {step === 2 && (
          <Button
            color="warning"
            onClick={this.saveDraft}
            className="mr-1"
            style={{ width: "auto" }}
          >
            Save As Draft
          </Button>
        )}
        
        <Button
          type="submit"
          color="success"
          disabled={this.state.submitted}
          style={{ backgroundColor: "limegreen", width: "auto" }}
        >
          {this.state.submitted && (
            <Spinner className="mr-1" color="light" size="sm" />
          )}
          {step === 2 ? "Publish" : "Next"}
        </Button>
      </React.Fragment>
    );
  };
  
  updatePlans = (plans) =>{
    let fields = this.state.fields;
    fields['plans'] = plans;
    this.setState({fields})
    console.log(this.state.fields);
  }
  updateTickets = (tickets) =>{
    let fields = this.state.fields;
    fields['tickets'] = tickets;
    this.setState({fields})
  }
  updatePrograms = (programs) =>{
    let fields = this.state.fields;
    fields['programs'] = programs;
    this.setState({fields})
    console.log(this.state.fields);
  }
  updateFormData = (formData) =>{
    let fields = this.state.fields;
    fields['form_data'] = formData;
    this.setState({fields})
  }
  updateTeams = (teams) =>{
    let fields = this.state.fields;
    fields['groups'] = teams;
    this.setState({fields})
    console.log(this.state.fields);
  }
  promiseLocationOptions = (inputValue) => {
    return new Promise((resolve) => {
      if (inputValue !== "") {
        let that = this;
        setTimeout(() => {
          events.getLocations({location:inputValue}).then(function (response) {
            if (response.data.locations.resourceSets) {
              let locations = [];
              response.data.locations.resourceSets[0].resources[0].value.forEach((loc,index)=>{
                locations[index] = {
                  label:loc.address.formattedAddress, 
                  value:loc.address.formattedAddress
                };
              });
              that.setState({locations}, () => {
                  resolve(that.filterLocation(inputValue));
                }
              );
            } else if (response.data.error) {
              
            }
          }).catch(function (error) {
            console.log(error);
          });
        }, 1000);
      } else {
        resolve(this.filterLocation(inputValue));
      }
    });
  };
  handleLocationChange = (location) => {
    let fields = this.state.fields;
    fields['location'] = location;
    this.setState({ fields });
  };
  filterLocation = (inputValue) => {
    return this.state.locations.filter((i) =>
      i.label.toLowerCase().includes(inputValue.toLowerCase())
    );
  };
  render() {
    return (
      <>
      
      <Modal className="event_modal" size="lg" isOpen={this.props.showModal}>
        <ModalHeader
          toggle={() => {
            if (this.props.resetEventId) {
              this.props.resetEventId(null);
            }
            this.props.closeModal(false);
          }}
        >
          {this.props.selectedEventId || this.props.EventId ? "Edit " : "Add "} Event
        </ModalHeader>
        <ModalBody className="manage-event">
          <LoadingOverlay
            active={this.state.overLayLoading}
            spinner
            text="Loading. Please wait..."
          >
            <Row>
              <Col>
                <div className="step-form mb-5">
                  <ul className="step d-flex flex-nowrap">
                    <li
                      onClick={(e) => this.navigateTab(e, 1)}
                      className={`${
                        this.state.step === 1 ? "step-item active" : "step-item"
                      } ${this.state.currentStep > 0 && "hoverPointer"}`}
                    >
                      <span />
                    </li>
                    <li
                      onClick={(e) => this.navigateTab(e, 2)}
                      className={`${
                        this.state.step === 2 ? "step-item active" : "step-item"
                      } ${this.state.currentStep > 2 && "hoverPointer"}`}
                    >
                      <span />
                    </li>
                  </ul>
                </div>
              </Col>
            </Row>
            {this.state.step === 1 && (
              <Form onSubmit={this.handleSubmit} method="post">
                <Row>
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> What are you fundraising for?
                    </h5>
                    <Row>
                      <Col className="mb-3" md={(parseInt(this.state.fields.category) !== 59) ? 12 : 6}>
                        <Input
                          type="select"
                          name="category"
                          id="category"
                          onChange={(event) =>
                            this.handleChange(event, "category")
                          }
                          invalid={this.state.errors.category ? true : false}
                          value={
                            this.state.fields.category !== undefined
                              ? this.state.fields.category
                              : ""
                          }
                          disabled={this.props.selectedEventId || this.props.EventId}
                        >
                          <option value="">Choose a category</option>
                          {this.state.categories.length > 0 &&
                            this.state.categories.map((category, index) => 
                              <option
                                value={category.id}
                                key={`category-${index}`}
                              >
                                {category.name}
                              </option>
                            )}
                        </Input>
                        <FormFeedback invalid>
                          {this.state.errors.category}
                        </FormFeedback>
                      </Col>
                      {(parseInt(this.state.fields.category) === 59 || parseInt(this.state.fields.category) === 71 || parseInt(this.state.fields.category) === 72)  && <>
                      {parseInt(this.state.fields.category) !== 71 && parseInt(this.state.fields.category) !== 72 && <Col className="mb-3" md={6}>
                          <Input
                            type="select"
                            name="sub_category"
                            id="sub_category"
                            onChange={(event) =>
                              this.handleChange(event, "sub_category")
                            }
                            invalid={this.state.errors.sub_category ? true : false}
                            value={
                              this.state.fields.sub_category !== undefined
                                ? this.state.fields.sub_category
                                : ""
                            }
                          >
                            <option value="">Choose sub category</option>
                            {this.state.subCategories.length > 0 &&
                              this.state.subCategories.map((sc, index) => (
                                <option
                                  value={sc.id}
                                  key={`sub-category-${index}`}
                                >
                                  {sc.name}
                                </option>
                              ))}
                          </Input>
                          <FormFeedback invalid>
                            {this.state.errors.sub_category}
                          </FormFeedback>
                        </Col>}
                        
                        <Col className="mb-3" md={6}>
                          <h5>
                            <b><FaQuestionCircle/></b> Where is the venue name?
                          </h5>
                          <Input
                              type="text"
                              id="venue_name"
                              name="venue_name"
                              placeholder="Event Venue"
                              value={
                                this.state.fields.venue_name !== undefined
                                  ? this.state.fields.venue_name
                                  : ""
                              }
                              invalid={this.state.errors.venue_name ? true : false}
                              onChange={(event) => this.handleChange(event, "venue_name")}
                            />
                            <FormFeedback invalid>
                              {this.state.errors.venue_name}
                            </FormFeedback>
                          {this.state.errors.location && <small className="text-danger">{this.state.errors.location}</small>}
                        </Col>
                        <Col className="mb-3"  md={6}>
                          <h5>
                            <b><FaQuestionCircle/></b> Where is the venue address?
                          </h5>
                          <AsyncSelect
                            cacheOptions
                            name="location"
                            value={
                              this.state.fields["location"] &&
                              this.state.fields["location"]
                            }
                            defaultOptions={this.state.locations}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            loadOptions={this.promiseLocationOptions}
                            onChange={this.handleLocationChange}
                            isClearable={true}
                            placeholder="Enter address"
                          />
                          {this.state.errors.location && <small className="text-danger">{this.state.errors.location}</small>}
                        </Col>
                        <NonProfitQuesion 
                          fields={this.state.fields} 
                          errors={this.state.errors} 
                          handleChange={this.handleChange}
                          timeStamp={this.state.timeStamp}
                        />
                        <Col md={12} sm="12" className="mb-2">
                          <FormGroup>
                            <Label>Currency</Label>
                            <div>
                              {common.currencySymbols.map((currency, index)=><CustomInput type="radio" id={currency} label={`${currency}(${common.currencySymbolsArr[index]})`} inline name="currency" value={index} onChange={(event) => this.handleChange(event, "currency")} checked={parseInt(this.state.fields.currency) === index}/>)}
                            </div>
                          </FormGroup>
                        </Col>
                        {(parseInt(this.state.fields.category) === 59 || parseInt(this.state.fields.category) === 71) && <Col>
                          <ManageTickets 
                            eventId={this.props.selectedEventId?this.props.selectedEventId:this.props.EventId}
                            updateTickets={this.updateTickets}
                            updateFormData = {this.updateFormData}
                            formData={this.state.fields.form_data?this.state.fields.form_data:[]}
                            handleChange={this.handleChange}
                            service_fee={this.state.fields.service_fee}
                            currency={this.state.fields.currency}
                          />
                          {this.state.errors.tickets && <p className="text-danger">{this.state.errors.tickets}</p>}
                        </Col>}
                        
                        {parseInt(this.state.fields.category) === 72 && <Col>
                          <ManagePrograms 
                            eventId={this.props.selectedEventId?this.props.selectedEventId:this.props.EventId}
                            updatePrograms={this.updatePrograms}
                            updateFormData = {this.updateFormData}
                            formData={this.state.fields.form_data?this.state.fields.form_data:[]}
                            handleChange={this.handleChange}
                            currency={this.state.fields.currency}
                          />
                          {this.state.errors.programs && <p className="text-danger">{this.state.errors.programs}</p>}
                        </Col>}
                      </>}
                      {parseInt(this.state.fields.category) === 70 && <>
                        <NonProfitQuesion 
                          fields={this.state.fields} 
                          errors={this.state.errors} 
                          handleChange={this.handleChange}
                          timeStamp={this.state.timeStamp}
                        />
                        <Col>
                          <ManageGroups
                           eventId={this.props.selectedEventId?this.props.selectedEventId:this.props.EventId}
                           updateTeams={this.updateTeams}
                           updateFormData = {this.updateFormData}
                           formData={this.state.fields.form_data?this.state.fields.form_data:[]}
                           />
                          {this.state.errors.teams && <small className="text-danger">{this.state.errors.teams}</small>}
                        </Col>
                      </>}
                    </Row>
                  </Col>
                </Row>
                {(parseInt(this.state.fields.category) !== 59 && parseInt(this.state.fields.category) !== 70 && parseInt(this.state.fields.category) !== 71 && parseInt(this.state.fields.category) !== 72) && <>
                {parseInt(this.state.fields.category) === 57 && (<Row>
                  <Col>
                    <ManageMembershipPlans 
                      eventId={this.props.selectedEventId?this.props.selectedEventId:this.props.EventId}
                      updatePlans={this.updatePlans}
                      updateFormData = {this.updateFormData}
                      formData={this.state.fields.form_data?this.state.fields.form_data:[]}
                    />
                  </Col>
                </Row>)}
                {parseInt(this.state.fields.category) === 9 && <Row>
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> Do you work for this non-profit?
                    </h5>
                    <Row className="mb-4">
                      <Col md={3} sm={6}>
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.work_for_non_profit
                              ) === 1,
                          })}
                          value="1"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "work_for_non_profit"
                            )
                          }
                        >
                          Yes
                        </Button>
                      </Col>
                      <Col md={3} sm={6}>
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.work_for_non_profit
                              ) === 0,
                          })}
                          value="0"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "work_for_non_profit"
                            )
                          }
                        >
                          No
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>}
                {parseInt(this.state.fields.category) === 9 && parseInt(this.state.fields.work_for_non_profit) === 1 && <Row>
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> Would you like to require email collection for each donation?
                    </h5>
                    <Row className="mb-4">
                      <Col md={3} sm={6}>
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.require_email_collection
                              ) === 1,
                          })}
                          value="1"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "require_email_collection"
                            )
                          }
                        >
                          Yes
                        </Button>
                      </Col>
                      <Col md={3} sm={6}>
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.require_email_collection
                              ) === 0,
                          })}
                          value="0"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "require_email_collection"
                            )
                          }
                        >
                          No
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>}
                <Row>
                  <NonProfitQuesion 
                    fields={this.state.fields} 
                    errors={this.state.errors} 
                    handleChange={this.handleChange}
                    timeStamp={this.state.timeStamp}
                  />
                </Row>
                {parseInt(this.state.fields.category) !== 57 && parseInt(this.state.fields.category) !== 58 && parseInt(this.state.fields.category) !== 18 && parseInt(this.state.fields.category) !== 56 && <><Row className="mt-3">
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> Does your event have an existing gofundme
                      campaign?
                    </h5>
                    <Row className="mb-4">
                      <Col className="col-sm-2">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.existing_gofundme_campaign
                              ) === 0,
                          })}
                          value="0"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "existing_gofundme_campaign"
                            )
                          }
                        >
                          Yes
                        </Button>
                      </Col>
                      <Col className="col-sm-2">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(
                                this.state.fields.existing_gofundme_campaign
                              ) === 1,
                          })}
                          value="1"
                          onClick={(event) =>
                            this.handleChange(
                              event,
                              "existing_gofundme_campaign"
                            )
                          }
                        >
                          No
                        </Button>
                      </Col>
                    </Row>
                    {parseInt(this.state.fields.existing_gofundme_campaign) ===
                      0 && (
                      <Row>
                        <Col sm="12">
                          <div className="mb-3">
                            <Input
                              type="textarea"
                              id="campaign_url"
                              name="campaign_url"
                              rows="3"
                              placeholder="Add Campaign Url..."
                              invalid={
                                this.state.errors.campaign_url ? true : false
                              }
                              onChange={(event) =>
                                this.handleChange(event, "campaign_url")
                              }
                              defaultValue={
                                this.state.fields.campaign_url !== undefined
                                  ? this.state.fields.campaign_url
                                  : ""
                              }
                            />
                            <FormFeedback invalid>
                              {this.state.errors.campaign_url}
                            </FormFeedback>
                            <p className="text-primary mb-0">
                              *Note: Please add one url per line
                            </p>
                          </div>
                        </Col>
                      </Row>
                    )}
                  </Col>
                </Row>
                {parseInt(this.state.fields.category) !== 9 &&  <Row>
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> Event type
                    </h5>
                    <Row className="mb-4">
                      <Col className="col-sm-4">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.recurring) === 0,
                          })}
                          value="0"
                          onClick={(event) =>
                            this.handleChange(event, "recurring")
                          }
                        >
                          One time event
                        </Button>
                      </Col>
                      <Col className="col-sm-4">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.recurring) === 1,
                          })}
                          value="1"
                          onClick={(event) =>
                            this.handleChange(event, "recurring")
                          }
                        >
                          Recurring
                        </Button>
                      </Col>
                      <Col className="col-sm-4">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.recurring) === 2,
                          })}
                          value="2"
                          onClick={(event) =>
                            this.handleChange(event, "recurring")
                          }
                        >
                          Ongoing
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>}
                </>}
                {parseInt(this.state.fields.category) !== 18 &&  parseInt(this.state.fields.category) !== 57 && <Row>
                  <Col>
                    <h5>
                      <b><FaQuestionCircle/></b> How would you like public supporters names to show up?
                    </h5>
                    <Row>
                      <Col sm="12" md="3" className="mb-3">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.show_name) === 0,
                          })}
                          value="0"
                          onClick={(event) =>
                            this.handleChange(event, "show_name")
                          }
                        >
                          Anonymous
                        </Button>
                      </Col>
                      <Col sm="12" md="3" className="mb-3">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.show_name) === 1,
                          })}
                          value="1"
                          onClick={(event) =>
                            this.handleChange(event, "show_name")
                          }
                        >
                          First Name
                        </Button>
                      </Col>
                      <Col sm="12" md="3" className="mb-3">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.show_name) === 2,
                          })}
                          value="2"
                          onClick={(event) =>
                            this.handleChange(event, "show_name")
                          }
                        >
                          Full Name
                        </Button>
                      </Col>
                      <Col sm="12" md="3" className="mb-3">
                        <Button
                          outline
                          color="primary"
                          className={classnames({
                            selected:
                              parseInt(this.state.fields.show_name) === 3,
                          })}
                          value="3"
                          onClick={(event) =>
                            this.handleChange(event, "show_name")
                          }
                        >
                          Initials
                        </Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>}
                </>}
                <Row>
                  <Col className="text-right mt-3" md={12}>
                    {this.renderButtons(1)}
                  </Col>
                </Row>
              </Form>
            )}
            {this.state.step === 2 && (
              <Form onSubmit={this.handleSubmit} method="post">
                <FormGroup row>
                  <Col md={12} className="mb-2">
                    <Label id="title">Event Title</Label>
                      <Input
                        type="text"
                        id="title"
                        name="title"
                        placeholder="Event Title"
                        value={
                          this.state.fields.title !== undefined
                            ? this.state.fields.title
                            : ""
                        }
                        invalid={this.state.errors.title ? true : false}
                        onChange={(event) => this.handleChange(event, "title")}
                      />
                      <FormFeedback invalid>
                        {this.state.errors.title}
                      </FormFeedback>
                  </Col>
                  <Col sm={6} className="mb-2">
                      <div>
                      <Label id="target_amount">Targeted amount</Label>
                      <FormGroup check style={{float:'right'}}>
                        <Label check>
                          <Input type="checkbox" name="show_target" onChange={(event) => this.handleChange(event, "show_target")} checked={parseInt(this.state.fields.show_target) === 1}/> Display on event details page</Label>
                      </FormGroup>
                      </div>
                      <Input
                        type="number"
                        id="target_amount"
                        name="target_amount"
                        placeholder="Targeted amount($)"
                        value={
                          this.state.fields.target_amount !== undefined
                            ? this.state.fields.target_amount
                            : ""
                        }
                        invalid={this.state.errors.target_amount ? true : false}
                        onChange={(event) =>
                          this.handleChange(event, "target_amount")
                        }
                        onKeyDown={ e => ( e.keyCode === 69 || e.keyCode === 190 ) && e.preventDefault() }
                      />
                      <FormFeedback invalid>
                        {this.state.errors.target_amount}
                      </FormFeedback>
                    </Col>
                    <Col sm={6} className="mb-2">
                      <Label htmlFor="file">Event Image</Label>
                      <Input
                        type="file"
                        id="file"
                        name="file"
                        onChange={(event) => this.uploadFile(event)}
                        style={{ border: "1px solid #e4e7ea" }}
                      />
                    </Col>
                    <Col sm="6" className="mb-2">
                      <Label for="start_date">Start Date</Label>
                      <DatePicker
                        className="form-control"
                        selected={
                          this.state.fields["start_date"]
                            ? this.state.fields["start_date"]
                            : null
                        }
                        onChange={this.handleStartDate}
                        dateFormat="MM-dd-yyyy h:mm aa"
                        showTimeSelect
                      />
                    </Col>
                    <Col sm="6" className="mb-2">
                      <Label for="end_date">End Date</Label>
                      <DatePicker
                        className="form-control"
                        selected={
                          this.state.fields["end_date"]
                            ? this.state.fields["end_date"]
                            : null
                        }
                        onChange={this.handleEndDate}
                        dateFormat="MM-dd-yyyy h:mm aa"
                        showTimeSelect
                        disabled={parseInt(this.state.fields.recurring) === 2}
                      />
                    </Col>
                    <Col sm="12" className="mb-2">
                      <div className="mb-3">
                        <Label id="description">Event Description</Label>
                        <ReactSummernote
                          options={{
                            height: 138,
                            dialogsInBody: true,
                            toolbar: [
                              ["style", ["style"]],
                              ["font", ["bold", "underline", "clear"]],
                              ["fontname", ["fontname"]],
                              ["para", ["ul", "ol", "paragraph"]],
                              ["table", ["table"]],
                              ["view", ["fullscreen", "codeview"]],
                            ],
                          }}
                          onChange={this.handleDescription}
                        >
                          {this.state.description && (
                            <div
                              dangerouslySetInnerHTML={{
                                __html: this.state.description,
                              }}
                            />
                          )}
                        </ReactSummernote>{" "}
                        {this.state.errors.description && (
                          <span style={{ color: "red" }}>
                            {this.state.errors.description}
                          </span>
                        )}
                      </div>
                    </Col>
                    <Col md={3} sm="12" className="mb-2">
                      <FormGroup>
                        <Label for="exampleCheckbox">Event Type</Label>
                        <div>
                          <CustomInput type="radio" id="public" label="Public" inline name="event_type" value="0" onChange={(event) => this.handleChange(event, "event_type")} checked={parseInt(this.state.fields.event_type) === 0}/>
                          <CustomInput type="radio" id="private" label="Private" inline name="event_type" value="1" onChange={(event) => this.handleChange(event, "event_type")} checked={parseInt(this.state.fields.event_type) === 1}/>
                          {this.state.fields.event_type && parseInt(this.state.fields.event_type) === 1 ? <div ><CustomInput type="text" id="passcode" inline name="passcode" className="form-control mt-2" onChange={(event) => this.handleChange(event, "passcode")} style={{width:300}} placeholder="Enter passcode"/> <small>Please make sure you copied the passcode as it will not be visible again.</small>{this.state.errors.passcode && <p className="text-danger">{this.state.errors.passcode}</p>}</div>:<div></div>}
                        </div>
                      </FormGroup>
                    </Col>
                    <Col md={3} sm="12" className="mb-2">
                      <FormGroup>
                        <Label for="show_supporters">Show Supporters</Label>
                        <div>
                          <CustomInput type="radio" id="yes" label="Yes" inline name="show_supporters" value="1" onChange={(event) => this.handleChange(event, "show_supporters")} checked={parseInt(this.state.fields.show_supporters) === 1}/>
                          <CustomInput type="radio" id="no" label="No" inline name="show_supporters" value="0" onChange={(event) => this.handleChange(event, "show_supporters")} checked={parseInt(this.state.fields.show_supporters) === 0}/>
                        </div>
                      </FormGroup>
                    </Col>
                    {(parseInt(this.state.fields.category) !== 59 && parseInt(this.state.fields.category) !== 71) && <Col md={12} sm="12" className="mb-2">
                      <FormGroup>
                        <Label>Currency</Label>
                        <div>
                          {common.currencySymbols.map((currency, index)=><CustomInput type="radio" id={currency} label={`${currency}(${common.currencySymbolsArr[index]})`} inline name="currency" value={index} onChange={(event) => this.handleChange(event, "currency")} checked={parseInt(this.state.fields.currency) === index}/>)}
                        </div>
                      </FormGroup>
                    </Col>}
                    <Col className="text-right mt-3" md={12}>
                      {this.renderButtons(2)}
                    </Col>
                </FormGroup>
              </Form>
            )}
          </LoadingOverlay>
        </ModalBody>
      </Modal>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    apiUrl: state.apiUrl,
    baseUrl: state.baseUrl,
    userId: state.userId,
    userName: state.userName,
  };
};
export default connect(mapStateToProps)(AddEditEvent);
