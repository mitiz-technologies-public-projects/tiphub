import React from "react";
import {
  Col,
  Progress,
} from "react-bootstrap";

const MobilePreview = () => {
  return (
    <div>
      <Col md="3" sm="12">
        <h4 className="text-center">Preview</h4>
        {/* <CIcon name="cil-arrow-right" className="preview-arrow" /> */}
        <div className="preview">
          <img src="assets/mobile.png" className="img-fluid" />
          <div className="preview-layout">
            <div className="preview-details">
              <div className="descriptions">
                <h6>Lorem Ipsum Dolar sit amet settings dolar.</h6>
                <ul className="supporter">
                  <li>
                    <img src="assets/profile.png" size="sm" />
                    <div className="profile-details">
                      <a>Anne Deo</a>
                    </div>
                    {/* <CIcon content={freeSet.cilCalendar} /> */}
                    <div className="profile-details">
                      <a>02 June, 2021</a>
                    </div>
                  </li>
                </ul>
                <p>
                  In Publishing and graphic design. Lorem ipsum is a placeholder
                  text commonly used to demo-nstrate the visual form of a
                  document.
                </p>
              </div>

              <hr />
              <div className="descriptions">
                <h6>Targeted Amount</h6>
                <p>$1400 raised of $2500 goal</p>
                <div className="progress_bar">
                    <Progress color="success" value={55} />
                  <span>55%</span>
                </div>
              </div>

              <hr />

              <div className="descriptions">
                <h6>Tipping apps</h6>
                <ul className="tipping-apps">
                  <li>
                    <a href="">
                      <div className="tipping-app-payment">
                        <img src="assets/1.svg" />
                        <span>CashApp</span>
                      </div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="tipping-app-payment">
                        <img src="assets/2.svg" />
                        <span>CashApp</span>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>

              <hr />

              <div className="descriptions">
                <h6>
                  Supporters <span>(50)</span>
                </h6>
                <ul className="supporter">
                  <li>
                    <img src="assets/profile.png" />
                    <div className="profile-details">
                      <p>Raj Singh</p>
                      <p>
                        $100 <span>1 Week</span>
                      </p>
                    </div>
                  </li>
                  <li>
                    <img src="assets/profile.png" />
                    <div className="profile-details">
                      <p>Raj Singh</p>
                      <p>
                        $100 <span>1 Week</span>
                      </p>
                    </div>
                  </li>
                  <li>
                    <img src="assets/profile.png" />
                    <div className="profile-details">
                      <p>Raj Singh</p>
                      <p>
                        $100 <span>1 Week</span>
                      </p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </Col>
    </div>
  );
};

export default MobilePreview;
