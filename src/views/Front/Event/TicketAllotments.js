/* eslint-disable react/prop-types */
import React, { Component } from "react";
import {
    Button, Modal, ModalBody, ModalHeader, Table, Spinner
} from "react-bootstrap";
import { connect } from "react-redux";
import events from "../../../services/events";
import { toast } from "react-toastify";

class TicketAllotments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allotments: [],
            errors: {},
            submitted: false,
            loader: true,
        };
    }
    getAlloments(){
        events
        .getAllotments({
            event_id: this.props.eventDetails.id,
        })
        .then((res) => {
            if (res.data.success) {
                this.setState({ allotments: res.data.allotments, loader: false  })
            } else if (res.data.error) {
                console.log(res.data.message);
            }
        })
        .catch((err) => {
            console.log(err);
        });
    }
    componentDidMount(){
        this.getAlloments();
    }

    deleteRecord = (id) => {
        if(window.confirm('Are you sure to delete this record?')){
            let data={id:id};
            events.deleteAlloment(data).then((res) => {
                if (res.data.success) {
                    toast.success(res.data.message, {
                        position: toast.POSITION.TOP_RIGHT,
                    });
                    this.getAlloments();
                } else if (res.data.error) {
                    console.log(res.data.message);
                }
            }).catch((err) => {
                console.log(err);
            });
        }
    }
    countTotalSoldTickets = (ticket) => {
        let totalTickets = 0;
        if(ticket.bookings.length > 0){
            ticket.bookings.forEach(booking=>{
                totalTickets = totalTickets + booking.details.length;
            })
        }
        return totalTickets;
    }
    render() {
        return (
            <Modal show={true} onHide={() => this.props.closeModal(false)} size="lg">
                <Modal.Header closeButton><p className="mb-1" style={{fontSize:25}}>Allot Physical Tickets</p></Modal.Header>
                <Modal.Body className="pt-0">
                    {this.state.loader ? <div className="text-center pb-3"><Spinner /></div> : this.state.allotments.length === 0 ? <p className="text-center text-danger">You have not allotted your tickets to any verdors yet.</p> : <div><p className="mb-0 mt-3 font-weight-bold text-center" style={{fontSize:22}}>{this.props.eventDetails.title}</p><Table className="table table-responsive">
                    <thead>
                        <tr>
                            <th className="border-top-0">#</th>
                            <th className="border-top-0">Vendor Name</th>
                            <th className="border-top-0">Ticket Name</th>
                            <th className="border-top-0">Total Tickets Alloted</th>
                            <th className="border-top-0" colSpan={2}>Total Tickets Sold</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.allotments.map((ele, index) => (
                            <tr key={`bookings-${index}`}>
                                <td className="border-bottom-0">{index + 1}.</td>
                                <td className="border-bottom-0">{ele.team.username}</td>
                                <td className="border-bottom-0">{ele.ticket.name}</td>
                                <td className="border-bottom-0">{ele.total_tickets}</td>
                                <td className="border-bottom-0">{this.countTotalSoldTickets(ele)}</td>
                                <td className="border-bottom-0"><Button color="danger" size="sm" onClick={()=>this.deleteRecord(ele.id)}>Delete</Button></td>
                            </tr>
                        ))}
                        </tbody>
                    </Table><p className="text-center"><Button variant="success" size="lg" onClick={()=>{
                        this.props.closeModal(false);
                        this.props.showAllotTicketModal(true);
                    }}>Allot Physical Tickets</Button></p></div>}
                </Modal.Body>
            </Modal>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        apiUrl: state.apiUrl,
        baseUrl: state.baseUrl,
        userId: state.userId,
        userName: state.userName,
    };
};
export default connect(mapStateToProps)(TicketAllotments);