import React, { Component } from "react";
import { connect } from "react-redux";

import {
  Row,
  Col,
  Button,
  FormGroup,
  Label,
  Input,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Spinner,
  Progress,
  Container,
  Form
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import common from "../../../services/common";
import events from "../../../services/events";
import Masonry, { ResponsiveMasonry } from "react-responsive-masonry"

class DiscoverEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventLoader: true,
      loadMoreLoader: false,
      showLoadMore: false,
      totalCount: 0,
      error: {},
      fields: {},
      page: 1,
      events: [],
      eventCategories: [],
    };
  }
  searchEvent = (e = null, form = false) => {
    if (e !== null) {
      e.preventDefault();
    }
    events
      .searchEvent({
        fields: this.state.fields,
        page: this.state.page,
      })
      .then((res) => {
        this.setState({
          events: form === true ? res.data.events : [...this.state.events, ...res.data.events],
          totalCount: res.data.pages.totalCount,
          eventLoader: false,
          showLoadMore: this.state.page < Math.ceil(res.data.pages.totalCount / 6),
          loadMoreLoader:false
        });
      });
  }
  componentDidMount = () => {
    events.getEventCategory().then((res) => {
      if (res.data.success) {
        this.setState({ eventCategories: res.data.categories }, () => {
          this.searchEvent();
        });
      }
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.page !== this.state.page) {
      this.searchEvent();
    }
  };

  handleChange = (e) => {
    let type = e.target.type;
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({ fields }, () => {
      if (type == "select-one") {
        this.searchEvent(null, true);
      }
    });
  };

  loadMore = (e) => {
    e.preventDefault();
    let checkMoreData = Math.ceil(this.state.totalCount / 6);
    if (this.state.page < checkMoreData) {
      this.setState({ page: this.state.page + 1, loadMoreLoader: true });
    } else {
      this.setState({ showLoadMore: false });
    }
  };

  render() {
    return (
      <>
        <Helmet>
          <title>Home - Discover Event</title>
        </Helmet>
        <div className="discover-event">
          <Container>
            <div className="discover-event-filter">
              <Row>
                <Col sm="12">
                  {/* <h1>
                    Discover <span>Events</span>
                  </h1> */}
                  <h2 className="text-center mb-4">
                    <strong>
                      Discover <span>Events</span>
                    </strong>
                  </h2>
                  <h5>
                    {/* Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia,
                  perferendis facilis! Magni explicabo nam velit nulla officiis
                  dolorum? Ducimus illum */}
                  </h5>
                </Col>
              </Row>
              <Form onSubmit={(event)=>this.searchEvent(event, true)}>
                <Row>
                  <Col sm="12">
                    <FormGroup className="discover-search">
                      <Input
                        className="search"
                        placeholder="Search events by title or event creator"
                        name="title"
                        onChange={(e) => this.handleChange(e)}
                      />
                      <Button color="light" type="submit">
                        <img src="/assets/search.png" />
                      </Button>
                    </FormGroup>
                  </Col>
                </Row>
                <FormGroup row className="filtering-categories">
                  <div className="categories">
                    <Label for="exampleSelect">Showing fundraiser for</Label>
                    <Col>
                      <Input
                        name="category"
                        type="select"
                        onChange={(e) => this.handleChange(e)}
                      >
                        <option value="">All Categories</option>
                        {this.state.eventCategories.length > 0 &&
                          this.state.eventCategories.map((ele, index) => (
                            <option key={index} value={ele.id}>
                              {ele.name}
                            </option>
                          ))}
                      </Input>
                    </Col>
                  </div>
                </FormGroup>
              </Form>
            </div>
            {this.state.eventLoader ? <Row><Col md="12" sm="12" className="text-center mt-5">
              <Spinner size="lg" />
            </Col></Row> : <ResponsiveMasonry
              columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}
            >
              <Masonry>
                {this.state.events.map((ele, index) => <Card className="shadow m-3" key={index + 1}>
                  <Link to={`${ele.url}/${ele.id}`} target="_blank">
                    <div className="discover-card-image">
                      <div
                        className="discover-image-align"
                        style={{
                          backgroundImage: `url("${this.props.apiUrl}/${ele.image !== null
                            ? `web/events/${ele.image}`
                            : `web/images/no-image.jpeg`
                            }")`,
                        }}
                      ></div>
                    </div>
                    <CardBody>
                      <CardSubtitle className="mb-2" tag="h6">
                        {ele.categoryDetails &&
                          `#${ele.categoryDetails.name}`}
                      </CardSubtitle>
                      <CardTitle tag="h4">{ele.title}</CardTitle>
                      <CardSubtitle
                        className="mt-2 mb-2 text-muted"
                        tag="p"
                        dangerouslySetInnerHTML={{
                          __html: ele.description,
                        }}
                      >
                      </CardSubtitle>
                      {parseInt(ele.category) !== 59 && <>
                        <Row>
                          <Col sm="12" md="6">
                            <CardSubtitle tag="p" className="mt-1">
                              Raised: {common.currencySymbolsArr[ele.currency]}
                              <b>
                                {ele.transactions.length > 0
                                  ? common.numberFormat(
                                    ele.transactions[0].totalAmount
                                  )
                                  : 0}
                              </b>
                            </CardSubtitle>
                          </Col>
                          <Col sm="12" md="6">
                            <CardSubtitle tag="p" className="mt-1">
                              Goal: {common.currencySymbolsArr[ele.currency]}<b>
                                {common.numberFormat(
                                  ele.target_amount
                                )}
                              </b>
                            </CardSubtitle>
                          </Col>
                        </Row>
                        <Progress
                          value={parseInt(
                            common.getTragetPercentageNew(
                              ele.target_amount,
                              ele.transactions
                            )
                          )}
                          color="success"
                          className="mt-3"
                        >
                          {common.getTragetPercentageNew(
                            ele.target_amount,
                            ele.transactions
                          )}
                          %
                        </Progress></>}
                      {parseInt(ele.category) !== 59 && <CardText className="text-center mt-3 mb-0">
                        Donated by{" "}
                        <span>
                          {ele.transactions.length > 0
                            ? ele.transactions[0].total_people
                            : 0}{" "}
                          people
                        </span>
                      </CardText>}
                      <div className="text-center">
                        <Button color="dark" className="donate-now">
                          Donate Now
                        </Button>
                      </div>
                    </CardBody>
                  </Link>
                </Card>)}

              </Masonry>
            </ResponsiveMasonry>}
            {this.state.showLoadMore && (
              <Col md="12" className="mt-4 text-center">
                <Button
                  color="success"
                  size="lg"
                  type="button"
                  style={{ borderRadius: 30 }}
                  onClick={(e) => this.loadMore(e)}
                  disabled={this.state.loadMoreLoader}
                >
                  {this.state.loadMoreLoader && (
                    <Spinner color="light" size="sm" className="mb-1 mr-1" />
                  )}
                  Load More
                </Button>
              </Col>
            )}
          </Container>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    apiUrl: state.apiUrl,
  };
};

export default connect(mapStateToProps)(DiscoverEvent);
