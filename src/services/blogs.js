import http from './http'

const blogs = {
  list: (param) => http.get('/blog/index', { params: param }),
  getDetail: (param) => http.get('/blog/get-detail', { params: param }),
  getFreatured: (param) => http.get('/blog/get-featured', { params: param }),
}

export default blogs