import http from './http'

const plans = {
  add: (data) => http.post('/event/add', data),
  getPlans: (param) => http.get('/plans/get-plans', { params: param }),
}

export default plans