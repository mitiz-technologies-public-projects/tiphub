<?php

$head = '<title>Tiphub.co</title>';
$head .= '<meta name="keyword" content="Mitiz Technologies,Admin,Open,Source,CSS,SCSS,HTML,RWD,React,Yii2,MySql">';
$head .= '<meta name="description" content="Receive money from supporters as quick and easy as possible."/>';
$head .= '<meta name="og:type" content="tiphub.co">';
$head .= '<meta name="twitter:site" content="@tiphub.co">';

if (isset($_SERVER['REQUEST_URI']) && !empty($_SERVER['REQUEST_URI'])) {
    $urlArr = explode('/', $_SERVER['REQUEST_URI']);
    //print_r($urlArr);die;
    //echo count($urlArr);die;
    if (strpos($_SERVER['REQUEST_URI'], 'home') !== false) {
        $head = '<title>Tip Hub | Best Fundraising Platform for Crowdfunding</title>';
        $head .= '<meta name="keyword" content="Fundraising Platform For Crowdfunding, Tip Hub Fundraising, Fund Raising Platforms, Online Fundraising Platforms, Best Online Fundraising Platforms, Best Way to Raise Funds for Charity, Best Way to Raise Money On Tip Hub, Best Fundraising Platform for Nonprofits , Raise Money for Charity">';
        $head .= '<meta name="description" content="Looking for the best online fundraising Platforms or the way to raise your funds or money for charity or nonprofits? Visit on the tip Hub platform today!"/>';
    } else if (strpos($_SERVER['REQUEST_URI'], 'discover-events') !== false) {
        $head = '<title>Charity Fundraising Events | How to Donate to Tip Hub</title>';
        $head .= '<meta name="keyword" content="Fundraising Events, Charity Fundraising Events, How to Donate to Tip Hub, Online Donation Services">';
        $head .= '<meta name="description" content="Check out the categories of online donation services and charity fundraising events to start your funding right away! Get more insights by visiting us now!"/>';
    } else if (strpos($_SERVER['REQUEST_URI'], 'pricing') !== false) {
        $head = '<title>How Does Tiphub Work |Donate to Tigra</title>';
        $head .= '<meta name="keyword" content="Send and Receive Payments Online, How Does Tiphub Work, Donate to Tigray Crisis, Donate Money with Paypal, Donate to Tigra, Donate With Apple Pay, Donation Platforms for Nonprofits">';
        $head .= '<meta name="description" content="Choose the pricing on donation platforms for nonprofits where you can send and receive payments online. Donate money With Apple Pay or Paypal to Tigray Crisis!"/>';
    } else if (!empty($urlArr) && count($urlArr) == 3) {

        $conn = new mysqli("localhost", 'tiphub_dbuser@‘localhost', 'tiphub@Gloves123#', 'tiphub');
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        if (is_int((int) $urlArr[2])) {
            $result = $conn->query("select * from events where id = " . $urlArr[2]);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $head = '<title>' . $row['title'] . '</title>';
                if (isset($row['meta_title']) && !empty($row['meta_title'])) {
                    $head .= '<meta name="title" content="' . $row['meta_title'] . '">';
                    $head .= '<meta property="og:title" content="' . $row['meta_title'] . '">';
                    $head .= '<meta name="twitter:title" content="' . $row['meta_title'] . '">';
                    $head .= '<meta name="og:type" content="Event">';
                } else {
                    $head .= '<meta property="og:title" content="' . $row['title'] . '">';
                    $head .= '<meta name="og:type" content="Event">';
                    $head .= '<meta property="twitter:title" content="' . $row['title'] . '">';
                }
                if (isset($row['meta_keyword']) && !empty($row['meta_keyword'])) {
                    $head .= '<meta name="keyword" content="' . $row['meta_keyword'] . '">';
                }
                if (isset($row['meta_description']) && !empty($row['meta_description'])) {
                    $head .= '<meta name="description" content="' . $row['meta_description'] . '">';
                    $head .= '<meta property="og:description" content="' . $row['meta_description'] . '">';
                    $head .= '<meta name="twitter:description" content="' . $row['meta_description'] . '">';
                } else {
                    $head .= '<meta name="description" content="' . substr(strip_tags($row['description'], 0, 200)) . '"/>';
                }
                $head .= '<meta property="og:url" content="https://tiphub.co/' . $row['url'] . '/' . $row['id'] . '"/>';
                $head .= '<meta property="twitter:url" content="https://tiphub.co/' . $row['url'] . '/' . $row['id'] . '"/>';
                $head .= '<meta name="twitter:card" content="summary_large_image" />';
                $head .= '<meta name="twitter:creator" content="    .co" />';
                if (!is_null($row['image'])) {
                    $head .= '<meta property="og:image" content="https://tiphub.co/api/web/events/' . $row['image'] . '">';
                    $head .= '<meta name="twitter:image" content="https://tiphub.co/api/web/events/' . $row['image'] . '">';
                    $head .= '<link rel="preload" as="image" href="https://tiphub.co/api/web/events/' . $row['image'] . '">';
                }
            }
            $conn->close();
        }
    } else if (!empty($urlArr) && count($urlArr) == 2) {
        $conn = new mysqli("localhost", 'tiphub_dbuser@‘localhost', 'tiphub@Gloves123#', 'tiphub');
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        if (isset($urlArr[1])) {
            $result = $conn->query("select * from users where username = '" . $urlArr[1] . "'");
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $name = $row['name'] ? $row['name'] : $row['username'];
                $head = '<title>TipHub.Co profile: ' . $name . '</title>';
                $head .= '<meta name="twitter:title" content="' . $name . '">';
                $head .= '<meta property="og:title" content="TipHub.Co profile: ' . $name . '"/>';
                $head .= '<meta property="og:description" content="' . substr(strip_tags($row['description'], 0, 200)) . '"/>';
                $head .= '<meta property="og:url" content="https://tiphub.co/' . $row['username'] . '"/>';
                $head .= '<meta name="twitter:card" content="summary_large_image" />';
                if (!is_null($row['image'])) {
                    $head .= '<meta property="og:image" itemprop="image" content="https://tiphub.co/api/web/profile_images/' . $row['image'] . '">';
                    $head .= '<meta name="twitter:image" content="https://tiphub.co/api/web/profile_images/' . $row['image'] . '">';
                }
            }
            $conn->close();
        }
    } else if (!empty($urlArr) && count($urlArr) == 4) {
        $conn = new mysqli("localhost", 'tiphub_dbuser@‘localhost', 'tiphub@Gloves123#', 'tiphub');
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        if (isset($urlArr[3]) && !empty($urlArr[3])) {
            $result = $conn->query("select * from blog where id = " . $urlArr[3]);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $head = '<title>' . $row['title'] . '</title>';
                $head .= '<meta name="title" content="' . $row['meta_title'] . '"/>';
                $head .= '<meta name="og:title" content="' . $row['meta_title'] . '"/>';
                $head .= '<meta name="twitter:title" content="' . $row['meta_title'] . '">';
                $head .= '<meta name="twitter:title" content="' . $row['meta_title'] . '">';
                $head .= '<meta property="og:url" content="https://tiphub.co/blog/' . $row['url'] . '/' . $row['id'] . '"/>';
                $head .= '<meta name="twitter:url" content="https://tiphub.co/blog/' . $row['url'] . '/' . $row['id'] . '"/>';
                if (isset($row['meta_description']) && !empty($row['meta_description'])) {
                    $head .= '<meta name="description" content="' . $row['meta_description'] . '">';
                    $head .= '<meta property="og:description" content="' . $row['meta_description'] . '">';
                    $head .= '<meta name="twitter:description" content="' . $row['meta_description'] . '">';
                } else {
                    $head .= '<meta name="description" content="' . substr(strip_tags($row['description'], 0, 200)) . '"/>';
                    $head .= '<meta property="og:description" content="' . substr(strip_tags($row['description'], 0, 200)) . '">';
                    $head .= '<meta name="twitter:description" content="' . substr(strip_tags($row['description'], 0, 200)) . '">';
                }
                $head .= '<meta name="twitter:card" content="summary_large_image" />';
                if (!is_null($row['image'])) {
                    $head .= '<meta property="og:image" content="https://tiphub.co/api/web/blogs/' . $row['id'] . '_' . $row['image'] . '">';
                    $head .= '<meta name="twitter:image" content="https://tiphub.co/api/web/blogs/' . $row['id'] . '_' . $row['image'] . '">';
                }
            }
            $conn->close();
        }
    }
}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="theme-color" content="#000000" />
    <meta name="author" content="Mitiz Technologies Pvt Ltd.">
    <link rel="manifest" href="/manifest.json" />
    <?php echo $head; ?>
    <script defer="defer" src="/static/js/main.fa465a7a.js"></script>
    <link href="/static/css/main.ff10209a.css" rel="stylesheet">
</head>

<body><noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-192542878-1"></script>
    <script>function gtag() { dataLayer.push(arguments) } window.dataLayer = window.dataLayer || [], gtag("js", new Date), gtag("config", "UA-192542878-1")</script>
    <script
        type="text/javascript">!function () { if (!document.URL.includes("localhost")) { window.__insp = window.__insp || [], __insp.push(["wid", 541936189]); setTimeout((function () { if (void 0 === window.__inspld) { window.__inspld = 1; var t = document.createElement("script"); t.type = "text/javascript", t.async = !0, t.id = "inspsync", t.src = ("https:" == document.location.protocol ? "https" : "http") + "://cdn.inspectlet.com/inspectlet.js?wid=541936189&r=" + Math.floor((new Date).getTime() / 36e5); var e = document.getElementsByTagName("script")[0]; e.parentNode.insertBefore(t, e) } }), 0) } }()</script>
</body>

</html>