<?php
namespace app\widgets;
use yii\base\Widget;

class Popup extends Widget{
	public $popup_id = 'default';
	public $title = "";
	public $size = "";//For Large - modal-lg, Small - modal-sm
	public $width = "";
	public $message = "";	
	public $padding = '0px';
	public $close_button = true;			
	public $header_bottom_padding = "";
	public $header_bottom_border = "";
	
	public function run(){
		$data['popup_id'] = $this->popup_id;
		$data['title'] = $this->title;
		$data['message'] = $this->message;		
		$data['size'] = $this->size; 
		$data['width'] = $this->width; 		
		$data['padding'] = $this->padding;
		$data['close_button'] = $this->close_button; 								
		$data['header_bottom_padding'] = $this->header_bottom_padding;
		$data['header_bottom_border'] = $this->header_bottom_border;
		return $this->render('popup',$data);
	}
}
?>
