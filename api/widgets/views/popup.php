<?php
use yii\helpers\Html;
?>
<div class="modal fade" id="<?=$popup_id?>" role="dialog" style="z-index: 99999;">
    <div class="modal-dialog <?=$size?>" <?=$width?>>
        <img src="<?=Yii::$app->request->baseUrl?>/web/images/page-loader.gif" style="position:absolute;z-index:1;left:45%;top:45%;display:none;" id="process_loader">
        <!-- Modal content-->
        <div class="modal-content">
            <?php if(!empty($title)){?>
            <div class="modal-header" style="padding-bottom:<?=$header_bottom_padding?>;border-bottom:<?=$header_bottom_border?>">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"><?=$title?></h4>
            </div>
            <?php }?>
            <div class="modal-body" style="padding:0px <?=$padding?> <?=$padding?> <?=$padding?>;"><?=$message?></div>
        </div>
    </div>
</div>
