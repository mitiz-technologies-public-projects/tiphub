<?php
namespace app\models;

use Yii;
use GuzzleHttp\Psr7\UploadedFile;

class Blog extends \yii\db\ActiveRecord
{
    
    
    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    
    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'title',
                    'url',
                    'description'
                ],
                'required'
            ],
            [
                [
                    'image'
                ],
                'file',
                'extensions' => 'jpg, png',
                'mimeTypes' => 'image/jpeg, image/png'
            ],
            [
                [
                    'image',
                ],
                $this->isNewRecord?'required':'string',
            ],
            [
                [
                    'title',
                    'description',
                    'image',
                    'url',
                ],
                'string'
            ],
            [
                [
                    'title',
                    'description',
                ],
                'trim'
            ],
            [
                [
                    'added_on',
                    'id',
                    'tag',
                    'meta_title',
                    'meta_keyword',
                    'meta_discription',
                    'other_meta_tag'
                ],
                'safe'
            ],
        ];
    }
    public function ValidateImage()
    {
        if ($this->isNewRecord && empty($this->image)) {
            $this->addError($attr, 'Image cannot be blank.');
        }
    }
    public function FileSave($file) {
        $uploaded_files = \yii\web\UploadedFile::getInstances(new Blog(), 'image');
        foreach ($uploaded_files as $uploaded_file) {
            if (! empty($uploaded_file->name)) {
                $upload_path = Yii::getAlias('@webroot').'/web/blogs/';
                $fileName = $this->id . "_".$uploaded_file->name;
                $uploaded_file->name = $fileName ;
                $filename = $upload_path . $fileName;
                $uploaded_file->saveAs($filename);
            }
        }
    }

    /* public function afterFind(){
        parent::afterFind();
        $this->newtitle = $this->title.' ~ '.$this->url;
        return true;
    } */

    /* public function fields(){
        return [
            'id', 'title', 'url', 'description', 'image', 'added_on', 'tag',  'meta_title',  'meta_keyword', 'meta_discription', 'other_meta_tag',
            'newtitle' => function () {
                return $this->title . ' ~ ' . $this->url;
            },
        ];
    } */

}
