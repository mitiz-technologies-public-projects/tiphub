<?php

namespace app\models;

use Yii;


class UserAppVisitors extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_app_visitors';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','service_id','ip_address'], 'required'],
            [['ip_address','device'], 'string'],
            [['user_id','service_id'], 'integer'],
            [['added_on'], 'safe'],
        ];
    }

    public function getServicedetails(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }
}
