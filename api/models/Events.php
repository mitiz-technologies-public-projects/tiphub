<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


class Events extends \yii\db\ActiveRecord
{
    public $default_amount;
    public $passcode;
    public $other_default_amount;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
        if ($this->isNewRecord && !empty($this->category) && ($this->category == 59 || $this->category == 71)) {
            $this->service_fee =1.5;
        }
        if(!$this->isNewRecord && $this->sub_category != 9){           
            $this->fundraising_for = 0;
        }
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','title','target_amount'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png'],
            //[['title'], 'match', 'pattern' => '/^[a-z0-9 .\-]+$/i', 'message' => 'Only letters, numbers and spaces are allowed.'],
            [['title','description','image','campaign_url','url','color_code','non_profit_name','city','state', 'venue_name', 'stripe_product_id','event_passcode', 'passcode'], 'string'],
            [['title','description','non_profit_name', 'location','meta_title','meta_key','meta_description'], 'trim'],
            [['id','checkin','recurring','fundraising_for','category','existing_gofundme_campaign','show_name','published','featured','discovered','disable_donor_form', 'work_for_non_profit', 'require_email_collection', 'sub_category', 'sub_sub_category', 'approved', 'service_fee','event_type', 'show_target','show_supporters', 'currency', 'show_tipping_app', 'allow_custom_price'], 'integer'],
            [['default_amount','other_default_amount'], 'number'],
            [['added_on','start_date','end_date','ein_number','meta_title','meta_key','meta_description'], 'safe'],
            [
                [
                    'sub_sub_category',
                ],
                'ValidateChld',
                'skipOnEmpty' => false,
            ],
        ];
    }
    public function ValidateChld($attr)
    {
        if (empty($this->$attr)) {
            $category = $this->category;
            if ($category == 5 || $category == 59 || $category == 71) {
                   $this->addError($attr, 'Child Category cannot be blank.');
            }
        }
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getPrograms(){
        return $this->hasMany(EventPrograms::className(), ['event_id' => 'id']);
    }

    public function getBeneficial(){
        return $this->hasOne(Beneficials::className(), ['user_id' => 'user_id']);
    }

    public function getCategoryDetails(){
        return $this->hasOne(Categories::className(), ['id' => 'category']);
    }

    public function getTransactions(){
        return $this->hasMany(Transactions::className(), ['event_id' => 'id']);
    }

    public function getApps(){
        return $this->hasMany(EventApps::className(), ['event_id' => 'id']);
    }

    public function getVisitors(){
        return $this->hasMany(EventVisitors::className(), ['event_id' => 'id']);
    }

    public function getAppvisitors(){
        return $this->hasMany(EventAppVisitors::className(), ['event_id' => 'id']);
    }

    public function getQrcodes(){
        return $this->hasMany(EventQrCodes::className(), ['event_id' => 'id']);
    }

    public function getTickets(){
        return $this->hasMany(EventTickets::className(), ['event_id' => 'id']);
    }
    public function getEventTicketBookings(){
        return $this->hasOne(EventTicketBookings::className(), ['event_id' => 'id']);
    }
    public function getInvoiceValue($data=[]) {
        $totalAmount =  EventTicketBookings::find()->where(["event_id"=>$this->id,'status'=>1])->sum('total_amount');
        $data['total_amount'] = number_format($totalAmount,2);
        $totalTicketsSold = EventTicketBookings::find()->joinWith(['details'])->where(['event_id'=>$this->id,'status'=>1])->sum('event_ticket_booking_details.total_tickets');
        $data["total_tickets_sold"] = $totalTicketsSold;
        $service_fee =  0;
        if (!empty($this->service_fee) && !empty($totalAmount)) {
            $service_fee = number_format(($this->service_fee / 100) * $totalAmount,2);
        }
        $data['total_service_fee'] = $service_fee;
        return $data;
    }
    public function getSubCategory() 
    {
        if (!empty($this->category)) {
            return ArrayHelper::map(Categories::find()->where(['parent_id'=>$this->category])->all(), 'id', 'name');
        }
        return [];
    }

}
