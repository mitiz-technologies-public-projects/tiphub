<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventApps extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    public $username;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_apps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'service_id','user_id'], 'required'],
            [['url','email','phone'], 'trim'],
            [['url'], 'url'],
            //[['email'], 'email'],
            [['event_id','user_id','id','sequence','recurring', 'recurring_cycle', 'for_tipping', 'for_booking'], 'integer'],
            [['default_amount'], 'number'],
            [['payment_service_name','url','username','phone'], 'string'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getAppname(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }
    
}
