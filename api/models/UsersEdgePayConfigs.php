<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


class UsersEdgePayConfigs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_edgepay_configs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'sID'], 'integer'],
        ];
    }
}
