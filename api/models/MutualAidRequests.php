<?php

namespace app\models;

use Yii;


class MutualAidRequests extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mutual_aid_requests';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'first_name', 'last_name','amount', 'email', 'service_id', 'service_identifier'], 'required'],
            [['first_name','last_name','email','service_identifier'], 'trim'],
            [['email'], 'email'],
            //['service_id', 'unique', 'targetAttribute' => ['user_id','service_id','username'], 'message' => 'Service already been added with same username!'],
            [['event_id','service_id'], 'integer'],
            [['amount'], 'number'],
            [['first_name','last_name','email','reason'], 'string'],
            [['added_on'], 'safe'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getAppname(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }
    
}
