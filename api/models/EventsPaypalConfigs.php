<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_payment_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $plan_id
 */
class EventsPaypalConfigs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events_paypal_configs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'paypal_merchant_id'], 'required'],
            [['event_id'], 'integer'],
            
        ];
    }


     
  
}
