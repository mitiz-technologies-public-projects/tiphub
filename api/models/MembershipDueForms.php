<?php

namespace app\models;

use Yii;


class MembershipDueForms extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_due_forms';
    }

    public function beforeValidate(){
        $this->form_data = json_encode($this->form_data);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id','form_data'], 'required'],
            [['form_data'], 'string'],
            [['event_id'], 'integer'],
        ];
    }

    /* public function getTeam(){
        return $this->hasOne(User::className(), ['id' => 'team_id']);
    } */
}
