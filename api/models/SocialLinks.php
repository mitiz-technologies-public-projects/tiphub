<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class SocialLinks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social_links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'url', 'plateform'], 'required'],
            [['url','name'], 'trim'],
            [['url'], 'url'],
            ['plateform', 'unique', 'targetAttribute' => ['user_id','plateform'], 'message' => 'Link already been added.'],
            [['user_id'], 'integer'],
            [['plateform', 'url','name'], 'string'],
        ];
    }
    
}
