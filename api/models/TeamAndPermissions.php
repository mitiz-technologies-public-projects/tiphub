<?php

namespace app\models;

use Yii;


class TeamAndPermissions extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_and_permissions';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','team_id','event_id', 'role'], 'required'],
            [['permissions'], 'string'],
            [['user_id','team_id','event_id', 'role'], 'integer'],
        ];
    }

    public function getTeam(){
        return $this->hasOne(User::className(), ['id' => 'team_id']);
    }
}
