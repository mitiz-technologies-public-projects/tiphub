<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class DonorDetails extends \yii\db\ActiveRecord
{  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'donor_details';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['donor_email', 'event_id', 'service_id','donor_first_name', 'donor_last_name', 'donor_email'], 'required', 'on' => 'Donor'],
            [['donor_email', 'event_id', 'service_id','amount_donated','donor_first_name', 'donor_last_name','donor_country', 'donor_phone', 'donor_city', 'donor_state', 'file_name'], 'required', 'on' => 'FamilyGiving'],
            
            [['donor_first_name', 'donor_last_name', 'donor_email', 'donor_city', 'donor_state', 'ip_address', 'donor_country', 'donor_phone', 'file_name','note','bank_confirmation_number'], 'string'],
            [['thanks_email_sent', 'receipt_sent', 'verified','currency','payment_method'], 'integer'],
            [['donor_email'], 'email'],
            [['donor_email', 'donor_first_name', 'donor_last_name'], 'trim'],
            [['amount_donated'], 'number'],
            [['added_on'], 'safe'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getBeneficiaries(){
        return $this->hasMany(BeneficiaryDetails::className(), ['donor_id' => 'id']);
    }

    public function getService(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }
    
}
