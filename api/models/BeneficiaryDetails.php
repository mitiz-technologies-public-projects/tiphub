<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class BeneficiaryDetails extends \yii\db\ActiveRecord
{  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beneficiary_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['beneficiary_name', 'beneficiary_city', 'beneficiary_wereda', 'beneficiary_kelebe', 'beneficiary_phone','fathers_name','grand_fathers_name'], 'required'],
            
            [['beneficiary_name', 'beneficiary_city', 'beneficiary_wereda', 'beneficiary_kelebe', 'beneficiary_tabla', 'beneficiary_email', 'beneficiary_phone','fathers_name','grand_fathers_name'], 'string'],
            [['beneficiary_email'], 'email'],
            [['donor_id','user_id'], 'integer'],
            [['beneficiary_amount'],'number'],
            [['beneficiary_email'], 'trim'],
        ];
    }

    public function getDonor(){
        return $this->hasOne(DonorDetails::className(), ['id' => 'donor_id']);
    }
    
}
