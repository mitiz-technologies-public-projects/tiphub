<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Transactions extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'name', 'amount', 'added_by','service_id'], 'required'],
            [['name'], 'trim'],
            //['service_id', 'unique', 'targetAttribute' => ['user_id','service_id','username'], 'message' => 'Service already been added with same username!'],
            [['event_id','currency','service_id', 'added_by','check_in', 'group_id','member_id','membership_id'], 'integer'],
            [['name','payment_service_name','screenshot', 'stripe_price_id','notes', 'display_name'], 'string'],
            [['added_on','check_in_date','actual_payment_date'], 'safe'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getAppname(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }

    public function getProgram(){
        return $this->hasOne(EventPrograms::className(), ['id' => 'program_id']);
    }

    public function getgroup(){
        return $this->hasOne(EventGroups::className(), ['id' => 'group_id']);
    }
    public function getMember(){
        return $this->hasOne(Members::className(), ['id' => 'member_id']);
    }
//     public static function getTransectionData($find,$event_id) 
//     {
//         $months = ['January','February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
//         $arrayData = [];
//         $currentMonth = date('m');
//         foreach ($find as $value){
//             $fullName = $value['first_name']." ".$value['last_name'];
//             $arrayData[$value['id']]=[
//                 'name'=>$fullName,
//                 'id'=>$value['id']
//             ];
//             $membershipPlan = $value->plan;
//             $max = $min = $currentMonth;
//             $amount = 0;
//             $date = "";
//             $member_ship_amount="";
//             $monthCount = 0;
//             if (!empty($value->transaction)) {
//                 foreach ($value->transaction as $key=> $transaction){
//                     $amount += $transaction->amount;
//                     if ($key == 0) {
//                         $date = $transaction->added_on;
//                     }else {
//                         $date = $date > $transaction->added_on ? $transaction->added_on:$date;
//                     }
//                 }
//                 $count =(int) ($amount / $membershipPlan->price);
//                 $monthCount = (($count*($membershipPlan->duration/30))-1);
//                 $yrdata= strtotime($date);
//                 $min= date('m', $yrdata);
//                 $max = $min + $monthCount;
//                 $status = 1;
//                 $member_ship_amount=$amount;
//             } else {
//                 $min= date('m', strtotime($value['added_on']));
//                 $max = $max > $min ? $max : $min + abs($max - $min);
//                 $monthCount = $max - $min;
//                 $amount = "Due";
//                 $status = 0;
//             }
//             $lasIndex = 0;
//             foreach ($months as $monthKey=>$month){
//                 if($monthKey < ($min-1) || $monthKey > $max-1){
//                     continue;
//                 }
//                 if ($amount == "Due" || $amount == "Overdue") {
//                     if ($monthKey != $min-1) {
//                         $amount = "Overdue";
//                     }
//                 }else {
//                     if ($monthKey != $min-1) {
//                         $amount = "Paid";
//                     }
//                 }
//                 $arrayData[$value['id']]['month'][]=[
//                     'name'=>$month,
//                     'value'=>$amount,
//                     'amount'=>$member_ship_amount,
//                     'payment_status'=>$status
//                 ];
//                 $lasIndex = $monthKey;
//                 $monthCount--;
//             }
//             if ($monthCount > 0) {
//                 foreach ($months as $monthKey=>$month){
//                     if($monthCount <= 0 ||  $min-1 == $monthKey){
//                         break;
//                     }
//                     $arrayData[$value['id']]['month'][]=[
//                         'name'=>$month,
//                         'value'=>$amount,
//                         'amount'=>$member_ship_amount,
//                         'payment_status'=>$status
//                     ];
//                     $lasIndex = $monthKey;
//                     $monthCount--;
//                 }
//             }
//             if (count($arrayData[$value['id']]['month']) == 12) {
//                 continue;
//             }
//             $length =  $max > $currentMonth ? $currentMonth+12-$max:$currentMonth-$max;
//             for ($i = 0; $i < $length; $i++) {
//                 $lasIndex++;
//                 if ($lasIndex == 12) {
//                     $lasIndex = 0;
//                 }
//                 $arrayData[$value['id']]['month'][]=[
//                     'name'=>$months[$lasIndex],
//                     'value'=>$i == 0 ? "Due":"Overdue",
//                     'amount'=>$member_ship_amount,
//                     'payment_status'=>0
//                 ];
//             }
            
//         }
//         return $arrayData;
//     }
    public function getTransectionData($find,$event_id)
    {
        $currentYear =  date('Y');
        $nextYear = $currentYear+1;
        $months = ['Jan-'.$currentYear,'Feb-'.$currentYear,'Mar-'.$currentYear, 'Apr-'.$currentYear,'May-'.$currentYear,'Jun-'.$currentYear,'Jul-'.$currentYear,'Aug-'.$currentYear,'Sep-'.$currentYear,'Oct-'.$currentYear,'Nov-'.$currentYear,'Dec-'.$currentYear,'Jan-'.$nextYear,'Feb-'.$nextYear,'Mar-'.$nextYear,'Apr-'.$nextYear,'May-'.$nextYear,'Jun-'.$nextYear,'Jul-'.$nextYear,'Aug-'.$nextYear,'Sep-'.$nextYear,'Oct-'.$nextYear,'Nov-'.$nextYear,'Dec-'.$nextYear];
        $arrayData = [];
        $currentMonth = date('m');
        $monthName = $months[$currentMonth-1];
        foreach ($find as $value){
            $fullName = $value['first_name']." ".$value['last_name'];
            $arrayData[$value['id']]=[
                'name'=>$fullName,
                'id'=>$value['id']
            ];
            $monthSet=[];
            $amount = 0;
            $member_ship_amount="";
            $membershipPlan = $value->plan;
            $min= date('m', strtotime($value['added_on']));
            $max = $currentMonth;
            $OneMonthAmount =  ($membershipPlan->price/($membershipPlan->duration/30));
            $monthCount = $currentMonth >= $min ? $currentMonth-$min : ($currentMonth + 12) - $min;
            if (!empty($value->transaction)) {
                foreach ($value->transaction as $key=> $transaction){
                    $amount += $transaction->amount;
                    if ($key == 0) {
                        $date = $transaction->added_on;
                    }else {
                        $date = $date > $transaction->added_on ? $transaction->added_on:$date;
                    }
                }
                $member_ship_amount=$amount;
                $yrdata= strtotime($date);
                $min= date('m', $yrdata);
                $count =(int) ($amount / $membershipPlan->price);
                $monthCount = (($count*($membershipPlan->duration/30))-1);
            }
            $result_value =$amount;
            $count =0;
            foreach($months as $monthKey=>$month){
                $unpaid =  "Overdue";
                $status = 0;
                if($monthKey < ($min-1)){
                    continue;
                }
                if(!empty($amount) && $amount > $OneMonthAmount){
                    if($monthKey == ($min-1)){
                        $result_value = $amount;
                    }else{
                        $result_value = "Paid";
                    }
                    $status = 1;
                    $amount =  $amount-$OneMonthAmount;
                } else {
                    $result_value = "Due";
                }
                if (!isset($monthSet[$month])) {
                    $arrayData[$value['id']]['month'][]=[
                        'name'=>$month,
                        'value'=>$result_value,
                        'amount'=>$member_ship_amount,
                        'payment_status'=>$status
                    ];
                    if (!empty($arrayData[$value['id']]['month'][$count-1]['value']) && $arrayData[$value['id']]['month'][$count-1]['value'] == "Due") {
                        $arrayData[$value['id']]['month'][$count-1]['value'] = "Overdue";
                    }
                    $monthSet[$month]=$month;
                    $count++;
                }
                $monthCount--;
                if($monthName == $month && $monthCount <= 0){
                    break;
                }
                
            }
        }
        return $arrayData;
    }
}
