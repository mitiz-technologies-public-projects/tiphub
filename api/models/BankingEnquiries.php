<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class BankingEnquiries extends \yii\db\ActiveRecord
{  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banking_enquiries';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone'], 'string'],
            [['email'], 'email'],
            [['added_on'], 'safe'],
        ];
    }
    
}


