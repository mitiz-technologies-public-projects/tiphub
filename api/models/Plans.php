<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property string $subject
 * @property string $content
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%plans}}';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan', 'yearly_price', 'monthly_price','description'], 'required'],
            [['plan'], 'trim'],
            [['yearly_price', 'monthly_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'plan' => 'Plan Name',
            'yearly_price' => 'Yearly Price',
            'monthly_price' => 'Monthly Price'
        ];
    }
}
