<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventTicketBookings extends \yii\db\ActiveRecord
{
    public $name;
    public $tickets = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_ticket_bookings';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [            
            [['event_id', 'service_id'], 'required'],
            [['first_name', 'last_name', 'email'], 'required', 'when' => function ($model) { 
                return $model->service_id != 73; 
            }],
            [['first_name', 'last_name', 'email', 'phone'], 'trim'],
            [['email'], 'email'],
            [['first_name', 'last_name', 'email', 'phone', 'name', 'app_username'], 'string'], 
            [['event_id', 'service_id', 'status', 'service_fee', 'term_accepted', 'is_physical_ticket', 'team_id', 'transaction_id'], 'integer'], 
            [['total_amount','donation_amount'], 'number'], 
            [['total_amount', 'donation_amount'], 'default', 'value' => 0],
            [['added_on','tickets'], 'safe'],                         
        ];
    }

    public function getService(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }

    public function getDetails(){
        return $this->hasMany(EventTicketBookingDetails::className(), ['booking_id' => 'id']);
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getTeam(){
        return $this->hasOne(User::className(), ['id' => 'team_id']);
    }
}
