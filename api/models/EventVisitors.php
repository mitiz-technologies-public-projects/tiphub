<?php

namespace app\models;

use Yii;


class EventVisitors extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_visitors';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id','ip_address'], 'required'],
            [['ip_address','device'], 'string'],
            [['event_id'], 'integer'],
            [['added_on'], 'safe'],
        ];
    }
}
