<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Payments extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /* public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    } */
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_dump'], 'required'],
            [['payment_id','payment_identifier','name','email','payment_mode', 'phone'], 'string'],
            [['txn_amount','net_amount','gateway_charges'], 'number'],
            [['user_id','payment_service','status','transaction_id', 'currency','donor_id', 'confirmation_email', 'dwolla'], 'integer'],
            [['received_at','processed_at'], 'safe'],
        ];
    }

    public function getService(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'payment_service']);
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getDonor(){
        return $this->hasOne(DonorDetails::className(), ['id' => 'donor_id']);
    }

    public function getTransaction(){
        return $this->hasOne(Transactions::className(), ['id' => 'transaction_id']);
    }
}
