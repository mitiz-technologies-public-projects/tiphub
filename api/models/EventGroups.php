<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique', 'targetAttribute' => ['name', 'event_id'], 'message' => 'Name already exists.'],
            [['name'], 'trim'],
            [['name'], 'string'],
            [['event_id'], 'integer'],
        ];
    }
}
