<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventUpdates extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_updates';
    }

    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'description'], 'required'],
            [['event_id'], 'integer'],
            [['description','image'], 'string'],
            [['added_on','check_in_date'], 'safe'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
    
}
