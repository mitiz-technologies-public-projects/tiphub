<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "membership_payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $plan_id
 */
class UsersMembership extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_membership';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'plan_id','plan_price'], 'required'],
            [['user_id', 'plan_id'], 'integer'],
            [['plan_price'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'plan_id' => Yii::t('app', 'Membership Id'),
        ];
    }
    
    public function getPlan(){
        return $this->hasOne(Plans::className(), ['id' => 'plan_id']);
    }
  
}
