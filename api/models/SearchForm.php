<?php
namespace app\models;

use yii\base\Model;

/**
 * SearchForm is the model behind the login form.
 */
class SearchForm extends Model
{

    public $id;

    public $name;

    public $title;

    public $first_name;

    public $last_name;

    public $email;

    public $phone;

    public $username;

    public $City;

    public $state;

    public $status;

    public $check_in;

    public $middle_name;

    public $Zipcode;

    public $BirthDate;

    public $sex;

    public $user_id;

    public $day_index;

    public $subject;

    public $referred_from;

    public $location_id;

    public $legal_name;

    public $publish_name;

    public $ein;

    public $code;

    public $ada_code;

    public $cpt_code;

    public $contact_name;

    public $parent_id;

    public $role_id;

    public $date_range;

    public $category;

    public $event_id;

    public $role;

    public $month;

    public $visitor_date_range;

    public $expense_date_range;

    public $recurring;

    public $type;

    public $payment_identifier;

    public $amount;

    public $ticket_number;

    public $ticket_id;

    public $checked_in;

    public $broadcast_sms;

    public $sms_content;

    public $broadcast_email;

    public $email_content;

    public $email_subject;

    public $status_unconfirmed;
    
    public $tag;

    public $program_id;
    public $service_id;

    public function rules()
    {
        return [
            [
                [
                    'name',
                    'first_name',
                    'last_name',
                    'email',
                    'username',
                    'phone',
                    'City',
                    'state',
                    'middle_name',
                    'Zipcode',
                    'referred_from',
                    'legal_name',
                    'publish_name',
                    'ein',
                    'code',
                    'ada_code',
                    'cpt_code',
                    'contact_name',
                    'invoice_id',
                    'title',
                    'payment_identifier',
                    'ticket_number',
                    'tag'
                ],
                'trim'
            ],
            [
                [
                    'clinic_id',
                    'user_id',
                    'day_index',
                    'location_id',
                    'ein',
                    'parent_id',
                    'id',
                    'role_id',
                    'event_id',
                    'role',
                    'ticket_id',
                    'checked_in',
                    'program_id',
                    'service_id'
                ],
                'integer'
            ],
            [
                [
                    'name',
                    'email',
                    'phone',
                    'username',
                    'City',
                    'state',
                    'sex',
                    'SocialSecurityNumber',
                    'subject',
                    'referred_from',
                    'legal_name',
                    'publish_name',
                    'code',
                    'ada_code',
                    'cpt_code',
                    'contact_name',
                    'Zipcode',
                    'date_range',
                    'invoice_id',
                    'title',
                    'category',
                    'recurring',
                    'payment_identifier',
                    'ticket_number',
                    'sms_content',
                    'email_content',
                    'email_subject'
                ],
                'string'
            ],
            [
                [
                    'amount'
                ],
                'number'
            ],
            [
                [
                    'broadcast_sms',
                    'broadcast_email'
                ],
                'boolean'
            ],
            [
                [
                    'BirthDate',
                    'status',
                    'check_in',
                    'status_unconfirmed'
                ],
                'safe'
            ]
        ];
    }
}
