<?php

namespace app\models;

use Yii;


class InvoicePayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_payment';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
            $this->paid_by = Yii::$app->user->identity->id;
        }
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id','amount','paid_by','payment_service','event_id','payment_date'], 'required'],
            [['invoice_id'], 'trim'],
            [['added_on','payment_service','payment_date'], 'safe'],
        ];
    }
    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'paid_by']);
    }
    public function getEventinvoice(){
        return $this->hasOne(EventInvoice::className(), ['event_id' => 'event_id']);
    }
   
    
}
