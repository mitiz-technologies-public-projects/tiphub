<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class FailedPayments extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'failed_payments';
    }

    /* public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    } */
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_dump'], 'required'],
            
        ];
    }

    
}
