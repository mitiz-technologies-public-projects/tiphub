<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $content
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%static_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content', 'url'], 'required'],
            [['name', 'url'], 'trim'],
            [['name', 'url'], 'unique'],
            [['content', 'page_title', 'meta_keyword', 'meta_description', 'meta_info'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Page Name',
            'url' => 'Url',
            'content' => 'Page Content',
            'page_title' => 'Page Title',
            'meta_keyword' => 'Meta Keyword',
            'meta_description' => 'Meta Description',
            'meta_info' => 'Meta Information',
        ];
    }
}
