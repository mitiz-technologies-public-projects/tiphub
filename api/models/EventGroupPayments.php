<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventGroupPayments extends \yii\db\ActiveRecord
{
    public $name;
    public $tickets = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_group_payments';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [            
            [['event_id', 'first_name', 'last_name', 'email', 'phone','group_id', 'city'], 'required'],
            [['first_name', 'last_name', 'email', 'phone'], 'trim'],
            [['email'], 'email'],
            [['first_name', 'last_name', 'email', 'phone', 'city', 'middle_name'], 'string'], 
            [['event_id', 'group_id', 'status'], 'integer'], 
            [['amount'], 'number'], 
            [['added_on'], 'safe'],                         
        ];
    }

    public function getGroup(){
        return $this->hasOne(EventGroups::className(), ['id' => 'group_id']);
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getDonors(){
        return $this->hasMany(EventGroupPayments::className(), ['group_id' => 'group_id']);
    }
}
