<?php

namespace app\models;

use Yii;


class ProfileVisitors extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_visitors';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','ip_address'], 'required'],
            [['ip_address','device'], 'string'],
            [['user_id'], 'integer'],
            [['added_on'], 'safe'],
        ];
    }
}
