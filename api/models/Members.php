<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Members extends \yii\db\ActiveRecord
{  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'members';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    /* public function beforeValidate(){
        $this->answer = json_encode($this->answer);
        return parent::beforeValidate();
    } */


    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'membership_level'], 'required'],
            [['event_id', 'membership_level', 'service_id','term_accepted'], 'integer'],
            [['first_name','last_name','email','phone','answer','dwolla_customer_url', 'funding_source_url'], 'string'],
            [['added_on'], 'safe'],
        ];
    }

    public function getEvent(){
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getPlan(){
        return $this->hasOne(MembershipPlans::className(), ['id' => 'membership_level']);
    }

    public function getForm(){
        return $this->hasOne(MembershipDueForms::className(), ['event_id' => 'event_id']);
    }
    public function getTransaction(){
        return $this->hasMany(Transactions::className(), ['member_id' => 'id']);
    }
    
}
