<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [        
            [['name'], 'required'],    
            [['id','parent_id'], 'integer'],
            ['name', 'unique', 'targetAttribute' => ['parent_id', 'name'],'message'=>'Category already exists.'],
            [['name'], 'trim'],
            [['description'], 'trim'],
            [['name'], 'string'],                        
        ];
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParentCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    public function getSubcategory()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id']);
    }
}
