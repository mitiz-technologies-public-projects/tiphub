<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Beneficials extends \yii\db\ActiveRecord
{  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'beneficials';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'firstName', 'lastName','email', 'dateOfBirth', 'ssn', 'address1', 'city', 'stateProvinceRegion', 'postalCode', 'type'], 'required'],
            [['email'], 'email'],
            [['email'], 'unique'],
            /* [['passport_country','passport_number'], 'required', 'when' => function ($model) {
                return $model->ssn != '';
            }], */
            [['businessType','firstName', 'lastName', 'address1', 'city', 'postalCode', 'passport_country', 'passport_number', 'stateProvinceRegion', 'ssn', 'ipAddress', 'phone', 'dwolla_customer_url', 'funding_source_url','email', 'address1', 'address2', 'businessName', 'doingBusinessAs', 'businessClassification','ein', 'website', 'cFirstName', 'cLastName', 'title', 'cSsn', 'cAddress1', 'cAddress2', 'cAddress3', 'cCity', 'cPostalCode', 'cCountry', 'cStateProvinceRegion','type'], 'string'],
            [['firstName', 'lastName', 'address1', 'address2', 'businessName', 'doingBusinessAs', 'businessClassification', 'city', 'postalCode', 'email', 'phone'], 'trim'],
            [['added_on', 'cDateOfBirth'], 'safe'],
        ];
    }
    public function getState(){
        return $this->hasOne(States::className(), ['state_code' => 'stateProvinceRegion']);
    }
    public function getCcountry(){
        return $this->hasOne(Countries::className(), ['code' => 'cCountry']);
    }
    public function getCstate(){
        return $this->hasOne(States::className(), ['state_code' => 'cStateProvinceRegion']);
    }
    public function getPcountry(){
        return $this->hasOne(Countries::className(), ['code' => 'passport_country']);
    }
    
}


