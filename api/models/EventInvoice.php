<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_invoice';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->created_on = date("Y-m-d H:i:s");
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'invoice_id', 'total_sold_amount', 'service_charge', 'status','created_on'], 'safe'],
            [['event_id', 'invoice_id', 'total_sold_amount', 'service_charge','created_on'], 'required'],
            [['event_id'], 'number'],
            [['total_sold_amount', 'service_charge','status','amount'], 'default', 'value' => 0],
        ];
    }
    
}
