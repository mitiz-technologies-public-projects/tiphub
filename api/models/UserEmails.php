<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class UserEmails extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_emails';
    }
    
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email','user_id'], 'required', 'on' => 'Link'],
            [['verification_code'], 'required', 'on' => 'Confirm'],
            [['email'], 'string'],
            [['email'], 'email'],
            [['user_id','status'], 'integer'],
            [['added_on','verification_code'], 'safe'],
        ];
    }
    
}
