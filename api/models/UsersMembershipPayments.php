<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "membership_payments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $users_membership_id
 */
class UsersMembershipPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_membership_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'users_membership_id','amount'], 'required'],
            [['user_id', 'users_membership_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'users_membership_id' => Yii::t('app', 'Membership Id'),
        ];
    }
    
     
  
}
