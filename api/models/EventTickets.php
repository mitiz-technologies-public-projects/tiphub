<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventTickets extends \yii\db\ActiveRecord
{
    
    public function beforeSave($insert){
        if (empty($this->physical_ticket_limit)) {
            $this->physical_ticket_limit = 0;
        }
        return true;
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [            
            [['name', 'price', 'user_id'], 'required'],
            ['name', 'unique', 'targetAttribute' => ['name', 'event_id'], 'message'=>'Name already exists.'],
            [['name'], 'trim'],
            [['name', 'description'], 'string'], 
            [['event_id', 'user_id', 'max_limit', 'physical_ticket_limit', 'visibility'], 'integer'], 
            [['price'], 'number'],                         
        ];
    }

    public function getBookings(){
        return $this->hasMany(EventTicketBookingDetails::className(), ['ticket_id' => 'id']);
    }

    public function getAllotments(){
        return $this->hasMany(PhysicalTicketAllotments::className(), ['ticket_id' => 'id']);
    }
}
