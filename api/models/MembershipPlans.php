<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class MembershipPlans extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'membership_plans';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [            
            [['name', 'duration', 'price', 'user_id'], 'required'],
            ['name', 'unique', 'targetAttribute' => ['name', 'event_id'], 'message'=>'Plan name already exists.'],
            [['name'], 'trim'],
            [['name'], 'string'], 
            [['event_id', 'duration', 'user_id'], 'integer'], 
            [['price'], 'number'],                         
        ];
    }
}
