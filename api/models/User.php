<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 'Y';
    const SUPER_ADMIN_ROLE_ID = '1';
    const ROLE_ADMIN = 1;
    const ROLE_USER = 0;

    public $password;

    public static function tableName()
    {
        return '{{%users}}';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
            $this->updated_on = date("Y-m-d H:i:s");
        }
    }
    public function rules()
    {
        return [
            [['email', 'username'], 'required'],
            [['email', 'name', 'username','phone'], 'trim'],
            [['email'], 'email'],
            [['password'], 'string', 'min' => 12],

            /* ['email', 'unique', 'targetAttribute' => ['email', 'deleted'], 'message'=>'Email has already been taken.'],
            ['username', 'unique', 'targetAttribute' => ['username', 'deleted'],'message'=>'Username has already been taken.'], */

            [['email','username', 'phone'], 'unique'],
            
            [['password'], 'match', 'pattern' => '/^(?=.*[a-z])/', 'message' => 'Must contain at least one lowercase letter.'],
            [['password'], 'match', 'pattern' => '/^(?=.*[A-Z])/', 'message' => 'Must contain at least one uppercase letter.'],
            [['password'], 'match', 'pattern' => '/^(?=.*\d)/', 'message' => 'Must contain at least one digit.'],
            [['password'], 'match', 'pattern' => '/^(?=.*\W)/', 'message' => 'Must contain at least one special character.'],
            [['phone', 'name', 'status','city','first_name','middle_name','last_name','facebook_id','google_id','image','description','transaction_prefix','funding_source_url', 'dwolla_customer_url', 'stripe_connect_account_id', 'stripe_customer_id'], 'string'],
            [['role_id','country_id','state_id','profile_updated','parent_id','featured','next_transaction_id','term_accepted', 'stripe_connect_status'], 'integer'],
            [['added_on', 'updated_on','deleted','twitter_id'], 'safe'],
            [['username', 'email', 'password', 'auth_key', 'twitter_username', 'facebook_username', 'google_username'], 'string', 'max' => 100],
            [['access_token', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['verification_code'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'password' => Yii::t('app', 'Password'),
            'status' => Yii::t('app', 'Status'),
            'verification_code' => Yii::t('app', 'Verification Code'),
            'added_on' => Yii::t('app', 'Added On'),
            'updated_on' => Yii::t('app', 'Updated On'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    public function getRole()
    {
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }

    public function getServices()
    {
        return $this->hasMany(Services::className(), ['user_id' => 'id']);
    }

    public function getQrcodes()
    {
        return $this->hasMany(Qrcodes::className(), ['user_id' => 'id']);
    }

    public function getSociallinks()
    {
        return $this->hasMany(SocialLinks::className(), ['user_id' => 'id']);
    }

    public function getEvents()
    {
        return $this->hasMany(Events::className(), ['user_id' => 'id']);
    }

    public function getAllotments()
    {
        return $this->hasMany(PhysicalTicketAllotments::className(), ['team_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        # throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    /* public static function findByUsername($username){
    return static::findOne(['username'=>$username,'status' => self::STATUS_ACTIVE]);
    } */

    public static function findByUsernameOrEmail($username)
    {
        return static::find()->where(['status' => self::STATUS_ACTIVE])->andWhere("username = '$username' OR email = '$username'")->one();
    }

    public static function findUsername($username)
    {
        if (!empty($username)) {
            return static::findOne(['username' => $username]);
        } else {
            return false;
        }
    }

    public static function findMCode($code)
    {
        $session = new \yii\web\Session;
        $session->open();
        $user_id = $session->get('registered_mobile_number_user_id');
        return static::findOne(['m_verification_code' => $code, 'id' => $user_id]);
    }

    public static function findEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by email or phone
     *
     * @param string $username
     * @return static|null
     */
    /*public static function findByUsername($username){
    return static::findOne("status = ".self::STATUS_ACTIVE." AND (email = '".$username."' OR phone = '".$username."')");
    }*/

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getRoles()
    {
        return $this->hasMany(AssignedRoles::className(), ['user_id' => 'id']);
    }
    // Get Users Membership data
    public function getUsersMembership()
    {
        return $this->hasMany(UsersMembership::className(), ['user_id' => 'id']);
    }
    
    public function getBeneficial(){
        return $this->hasOne(Beneficials::className(), ['user_id' => 'id']);
    }

    public static function checkSuperAdmin($user_id)
    {
        $count = \app\models\User::find()->where(["users.id" => $user_id, "assigned_roles.user_id" => $user_id, "assigned_roles.role_id" => self::SUPER_ADMIN_ROLE_ID])->joinWith(['roles'])->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add()
    {
        if ($this->validate() && $this->save()) {
            return $this;
        } else {print_r($this->getErrors());exit;
            $this->addErrors($this->getErrors());
            throw new \Exception('Invalid data submitted');
        }
        return null;
    }

    public static function findUserid($user_id)
    {
        if (!empty($user_id)) {
            return static::findOne(['id' => $user_id]);
        } else {
            return false;
        }
    }
    
}
