<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_paypal_configs".
 *
 * @property integer $id
 * @property integer $user_id
 */
class UsersPaypalConfigs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_paypal_configs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'paypal_merchant_id'], 'required'],
            [['user_id'], 'integer'],
            
        ];
    }


     
  
}
