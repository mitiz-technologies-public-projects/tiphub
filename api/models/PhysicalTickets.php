<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "physical_ticket_allotments".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class PhysicalTickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'physical_tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [        
            [['allotment_id', 'qrcode'], 'required'],    
            [['allotment_id', 'sold'], 'integer'],
            [['qrcode','custom_ticket_id'], 'string'],                    
        ];
    }

    public function getTicket()
    {
        return $this->hasOne(EventTickets::className(), ['id' => 'ticket_id']);
    }

    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getTeam()
    {
        return $this->hasOne(User::className(), ['id' => 'team_id']);
    }
    public function getBookings()
    {
        return $this->hasMany(EventTicketBookings::className(), ['team_id' => 'team_id']);
    }
}
