<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class Services extends \yii\db\ActiveRecord
{
    public $payment_service_name;
    public $username;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'service_id'], 'required'],
            [['url','email','username','phone'], 'trim'],
            //[['email'], 'email'],
            ['service_id', 'unique', 'targetAttribute' => ['user_id','service_id','url'], 'message' => 'Service already been added.'],
            [['url'], 'url'],
            //['username', 'unique', 'targetAttribute' => ['user_id','username']],
            [['user_id','sequence'], 'integer'],
            [['default_amount'], 'number'],
            [['url','payment_service_name','username','phone'], 'string'],
            [['added_on'], 'safe'],
        ];
    }

    public function getAppname(){
        return $this->hasOne(PaymentServiceNames::className(), ['id' => 'service_id']);
    }
    
}
