<?php

namespace app\models;

use Yii;


class EventMembers extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_members';
    }

    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id','user_id'], 'required'],
            [['event_id','user_id'], 'integer'],
            ['user_id', 'unique', 'targetAttribute' => ['event_id', 'user_id'], 'message'=>'Member already added to this event.'],
            [['added_on'], 'safe'],
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
