<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "physical_ticket_allotments".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class PhysicalTicketAllotments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'physical_ticket_allotments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [        
            [['event_id', 'user_id', 'team_id', 'ticket_id', 'total_tickets'], 'required'],    
            [['event_id', 'user_id', 'team_id', 'ticket_id', 'total_tickets'], 'integer'],
            ['ticket_id', 'unique', 'targetAttribute' => ['ticket_id', 'event_id', 'user_id', 'team_id'],'message'=>'Tickets already alloted to selected vendor.'],               
        ];
    }

    public function getTicket()
    {
        return $this->hasOne(EventTickets::className(), ['id' => 'ticket_id']);
    }

    public function getTickets()
    {
        return $this->hasMany(PhysicalTickets::className(), ['allotment_id' => 'id']);
    }

    public function getEvent()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    public function getTeam()
    {
        return $this->hasOne(User::className(), ['id' => 'team_id']);
    }
    public function getBookings()
    {
        return $this->hasMany(EventTicketBookings::className(), ['team_id' => 'team_id']);
    }
}
