<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventPrograms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_programs';
    }
    public function init()
    {
        if ($this->isNewRecord) {
            $this->added_on = date("Y-m-d H:i:s");
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'price', 'event_id'], 'required'],
            [['name'], 'trim'],
            [['name', 'description'], 'string'],
            [['price'], 'number'],
            [['added_on','image'], 'safe'],
        ];
    }
    public function getUser()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }
    
    
}
