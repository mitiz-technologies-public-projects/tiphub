<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 */
class EventTicketBookingDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_ticket_booking_details';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [            
            [['ticket_id', 'booking_id', 'total_tickets', 'total_price'], 'required'],
            [['ticket_id', 'booking_id', 'total_tickets', 'checked_in'], 'integer'], 
            [['total_price', 'custom_price'], 'number'], 
            [['qrcode','custom_ticket_id'], 'string'],                      
        ];
    }

    public function getBooking(){
        return $this->hasOne(EventTicketBookings::className(), ['id' => 'booking_id']);
    }

    public function getTicket(){
        return $this->hasOne(EventTickets::className(), ['id' => 'ticket_id']);
    }
}
