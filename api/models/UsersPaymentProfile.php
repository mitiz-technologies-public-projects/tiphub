<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_payment_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $plan_id
 */
class UsersPaymentProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_payment_profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cc_no','ccv','expiry','first_name','last_name','address','city','state','zip','country','phone','ccv','ccv'], 'required'],
            [['user_id'], 'integer'],
            
        ];
    }


    
    public function getPlan(){
        return $this->hasOne(Plans::className(), ['id' => 'plan_id']);
    }
  
}
