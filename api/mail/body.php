<?php
$content = "";
if ((isset($case) && $case === 1) || (isset($templateDetails) && $templateDetails->id == 4)) {
    $content = str_replace('[name]', $name, $templateDetails->content);
    $content = str_replace('[IP]',$ip, $content);
    $content = str_replace('[url]',$url, $content);
} else if (isset($case) && ($case === 2 || $case === 3)) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[donor-name]', $name, $content);
    if(isset($event_title)){
        $content = str_replace('[event-name]', $event_title, $content);
    }
    if(isset($event_url)){
        $content = str_replace('[event-link]', $event_url, $content);
    }
} 
else if ((isset($case) && $case === 4) || (isset($templateDetails) && $templateDetails->id == 8)) {
    $content = str_replace('[name]', $eventDetails->user->name, $templateDetails->content);
    $content = str_replace('[currency]', $currency, $content);
    $content = str_replace('[amount]', $amount, $content);
    $content = str_replace('[event-title]', $eventDetails->title, $content);
    $content = str_replace('[date]', $date, $content);

    $donorDetails = '<table width="100%">
    <tr>
        <td valign="top" align="center">    
            <table width="60%" style="background-color:#f1e8e8">
                <tr>
                    <td colspan="2" align="left" style="font-size: 25px;"><strong>Diaspora Donation</strong></td>
                </tr>
                <tr><td><strong>Amount Donated: </strong></td><td>'.$currency[$donorDetails->currency].$donorDetails->amount_donated.'</td></tr>
                <tr><td><strong>First Name: </strong></td><td>'.$donorDetails->donor_first_name.'</td></tr>
                <tr><td><strong>Last Name: </strong></td><td>'.$donorDetails->donor_last_name.'</td></tr>
                <tr><td><strong>City: </strong></td><td>'.$donorDetails->donor_city.'</td></tr>
                <tr><td><strong>State: </strong></td><td>'.$donorDetails->donor_state.'</td></tr>
                <tr><td><strong>Country: </strong></td><td>'.$donorDetails->donor_country.'</td></tr>
                <tr><td><strong>Phone No: </strong></td><td>'.$donorDetails->donor_phone.'</td></tr>
                <tr><td><strong>Email: </strong></td><td>'.$donorDetails->donor_email.'</td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><strong>Note:</strong></td>
    </tr>
    <tr>
        <td style="margin-bottom:10px">'.$donorDetails->note.'</td>
    </tr></table>';
    $content = str_replace('[donor-details]', $donorDetails, $content);
    
    if(isset($donorDetails->beneficiaries) && !empty($donorDetails->beneficiaries)){
        $beneficiaryDetails = '<table width="100%">
        <tr>
            <th align="left">#</th>
            <th align="left">Name</th>
            <th align="left">City</th>
            <th align="left">Wereda</th>
            <th align="left">Kelebe</th>
            <th align="left">Tabla</th>
            <th align="left">Phone</th>
            <th align="left">Email</th>
            <th align="left">Fathers Name</th>
            <th align="left">Grand Fathers Name</th>
        </tr>';
        $i = 0;
        foreach($donorDetails->beneficiaries as $beneficiary){
            $beneficiaryDetails .= '<tr>';
                $beneficiaryDetails .= '<td>'.++$i.'.</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_name.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_city.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_wereda.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_kelebe.'</td>';
                //$content .= '<td>'.$beneficiary->beneficiary_tabla.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_phone.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->beneficiary_email.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->fathers_name.'</td>';
                $beneficiaryDetails .= '<td>'.$beneficiary->grand_fathers_name.'</td>';
            $beneficiaryDetails .= '<tr>';
        }
        $beneficiaryDetails .= '</table>';
        $content = str_replace('[beneficiary-details]', $beneficiaryDetails, $content);
    }
    else{
        $content = str_replace('[beneficiary-details]', '', $content);
    }
}
else if ((isset($case) && ($case === 5 || $case === 7)) || (isset($templateDetails) && $templateDetails->id == 7)) {
    $content = str_replace('[name]', $name, $templateDetails->content);
    if(isset($currency)){
        $content = str_replace('[currency]', $currency, $content);
    }
    $content = str_replace('[amount]', $amount, $content);
    $content = str_replace('[event-title]', $eventDetails->title, $content);
    $content = str_replace('[event]', '<a href="'.Yii::$app->params['siteUrl'].'/'.$eventDetails->url.'/'.$eventDetails->id.'">'.$eventDetails->title.'</a>', $content);
    if(isset($date)){
        $content = str_replace('[date]', $date, $content);
    }

    if(isset($eventDetails->ein_number)){
        $content = str_replace('[ein-number]', '<strong>EIN Number:</strong> '.$eventDetails->ein_number, $content);
    }
    else{
        $content = str_replace('[ein-number]', '', $content);
    }
    if(isset($paymentDetails) && isset($paymentDetails->payment_identifier)){
        $content = str_replace('[transaction-id]', '<strong>Transaction ID:</strong> '.$paymentDetails->payment_identifier, $content);
    }
    else{
        $content = str_replace('[transaction-id]', '', $content);
    }
}
else if (isset($case) && $case === 6) {
    $content = 'Hi '.$donorDetails->donor_first_name.'<br/><br/>We have received your donation form and we will contact you once your donation has been verified. <h2>'.$donorDetails->event->title.'</h2>'.Yii::$app->params['siteUrl'].'/'.$donorDetails->event->url.'/'.$donorDetails->event->id.'<br/><br/>Thank you,<br/>Tiphub Team';
}
else if (isset($case) && $case === 8) {
    $content = 'Hi '.$paymentDetails->donor->donor_first_name.'<br/><br/>This is a confirmation email that '.$paymentDetails->event->user->name.' had distributed your donation to your beneficiary.<h2>'.$paymentDetails->payment_identifier.'</h2>Email at donations@tdana.org for any further questions.<br/><br/>';
    $content .= 'Thank you,<br/>Tiphub Team';
}
else if (isset($case) && $case === 9) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event-title]', $event_title, $content);
    $content = str_replace('[event-organiser-email]', $event_organiser_email, $content);
    $content = str_replace('[event-url]', $event_url, $content);
}
else if (isset($case) && $case === 12) {
    $content = $body;
}
else if (isset($case) && $case === 14) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', '<a href="'.Yii::$app->params['siteUrl'].'/'.$eventDetails->url.'/'.$eventDetails->id.'">'.$eventDetails->title.'</a>', $content);
    if(isset($ticketDetails)){
        $bookingDetails = '<table border="1" cellpadding="5">';
            $bookingDetails .= '<thead>';
                $bookingDetails .= '<th>Sr No.</th>';
                $bookingDetails .= '<th>Ticket Type</th>';
                $bookingDetails .= '<th>Price</th>';
                $bookingDetails .= '<th>Total Tickets</th>';
                $bookingDetails .= '<th>Sub Totals</th>';
            $bookingDetails .= '</thead>';
                $bookingDetails .= '<tbody>';
                        foreach($ticketDetails as $key=>$val){
                            $bookingDetails .= '<tr>';
                                $bookingDetails .= '<td>'.($key+1).'</td>';
                                $bookingDetails .= '<td>'.$val->ticket->name.'</td>';
                                $bookingDetails .= '<td>$'.$val->ticket->price.'</td>';
                                $bookingDetails .= '<td>'.$val->total_tickets.'</td>';
                                $bookingDetails .= '<td>$'.$val->total_price.'</td>';
                            $bookingDetails .= '</tr>';
                        }
                $bookingDetails .= '</tbody>';
        $bookingDetails .= '</table>';
        $content = str_replace('[ticket_details]', $bookingDetails, $content);
    }
}
else if (isset($case) && $case === 15) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', '<a href="'.Yii::$app->params['siteUrl'].'/'.$eventDetails->url.'/'.$eventDetails->id.'">'.$eventDetails->title.'</a>', $content);
    $buyerDetails = '<table border="1" cellpadding="5">';
        $buyerDetails .= '<thead>';
            $buyerDetails .= '<th>First Name</th>';
            $buyerDetails .= '<th>Last Name</th>';
            $buyerDetails .= '<th>Email</th>';
            $buyerDetails .= '<th>Phone</th>';
        $buyerDetails .= '</thead>';
            $buyerDetails .= '<tbody>';
                    $buyerDetails .= '<tr>';
                        $buyerDetails .= '<td>'.$buyer->first_name.'</td>';
                        $buyerDetails .= '<td>'.$buyer->last_name.'</td>';
                        $buyerDetails .= '<td>'.$buyer->email.'</td>';
                        $buyerDetails .= '<td>'.$buyer->phone.'</td>';
                    $buyerDetails .= '</tr>';
            $buyerDetails .= '</tbody>';
    $buyerDetails .= '</table>';
    $content = str_replace('[buyer_details]', $buyerDetails, $content);
    if(isset($ticketDetails)){
        $bookingDetails = '<table border="1" cellpadding="5">';
            $bookingDetails .= '<thead>';
                $bookingDetails .= '<th>Sr No.</th>';
                $bookingDetails .= '<th>Ticket Type</th>';
                $bookingDetails .= '<th>Ticket</th>';
                $bookingDetails .= '<th>Price</th>';
                $bookingDetails .= '<th>Total Tickets</th>';
                $bookingDetails .= '<th>Sub Totals</th>';
            $bookingDetails .= '</thead>';
                $bookingDetails .= '<tbody>';
                        foreach($ticketDetails as $key=>$val){
                            $bookingDetails .= '<tr>';
                                $bookingDetails .= '<td>'.($key+1).'</td>';
                                $bookingDetails .= '<td>'.$val->ticket->name.'</td>';
                                $bookingDetails .= '<td>'.$val->custom_ticket_id.'</td>';
                                $bookingDetails .= '<td>$'.$val->ticket->price.'</td>';
                                $bookingDetails .= '<td>'.$val->total_tickets.'</td>';
                                $bookingDetails .= '<td>$'.$val->total_price.'</td>';
                            $bookingDetails .= '</tr>';
                        }
                $bookingDetails .= '</tbody>';
        $bookingDetails .= '</table>';
        $content = str_replace('[ticket_details]', $bookingDetails, $content);
    }
    
}
else if (isset($case) && $case === 17) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', '<a href="'.Yii::$app->params['siteUrl'].'/'.$eventDetails->url.'/'.$eventDetails->id.'">'.$eventDetails->title.'</a>', $content);
    $buyerDetails = '<table border="1" cellpadding="5">';
        $buyerDetails .= '<thead>';
            $buyerDetails .= '<th>First Name</th>';
            $buyerDetails .= '<th>Last Name</th>';
            $buyerDetails .= '<th>Email</th>';
            $buyerDetails .= '<th>Phone</th>';
        $buyerDetails .= '</thead>';
            $buyerDetails .= '<tbody>';
                    $buyerDetails .= '<tr>';
                        $buyerDetails .= '<td>'.$buyer->first_name.'</td>';
                        $buyerDetails .= '<td>'.$buyer->last_name.'</td>';
                        $buyerDetails .= '<td>'.$buyer->email.'</td>';
                        $buyerDetails .= '<td>'.$buyer->phone.'</td>';
                    $buyerDetails .= '</tr>';
            $buyerDetails .= '</tbody>';
    $buyerDetails .= '</table>';
    $content = str_replace('[buyer_details]', $buyerDetails, $content);
    if(isset($ticketDetails)){
        $bookingDetails = '<table border="1" cellpadding="5">';
            $bookingDetails .= '<thead>';
                $bookingDetails .= '<th>Sr No.</th>';
                $bookingDetails .= '<th>Ticket Type</th>';
                $bookingDetails .= '<th>Ticket</th>';
                $bookingDetails .= '<th>Price</th>';
                $bookingDetails .= '<th>Total Tickets</th>';
                $bookingDetails .= '<th>Sub Totals</th>';
            $bookingDetails .= '</thead>';
                $bookingDetails .= '<tbody>';
                        foreach($ticketDetails as $key=>$val){
                            $bookingDetails .= '<tr>';
                                $bookingDetails .= '<td>'.($key+1).'</td>';
                                $bookingDetails .= '<td>'.$val->ticket->name.'</td>';
                                $bookingDetails .= '<td>'.$val->custom_ticket_id.'</td>';
                                $bookingDetails .= '<td>$'.$val->ticket->price.'</td>';
                                $bookingDetails .= '<td>'.$val->total_tickets.'</td>';
                                $bookingDetails .= '<td>$'.$val->total_price.'</td>';
                            $bookingDetails .= '</tr>';
                        }
                $bookingDetails .= '</tbody>';
        $bookingDetails .= '</table>';
        $content = str_replace('[ticket_details]', $bookingDetails, $content);
    }
    
}
else if (isset($case) && $case === 13) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[full_name]', $full_name, $content);
    $content = str_replace('[total_amount]', "$".$total_amount, $content);
    $details = '<table border="1" cellpadding="5">';
        $details .= '<thead>';
            $details .= '<th>Sr No.</th>';
            $details .= '<th>Ticket Type</th>';
            $details .= '<th>Ticket</th>';
            $details .= '<th>Price</th>';
            $details .= '<th>Total Ticket</th>';
            $details .= '<th>Sub Total</th>';
        $details .= '</thead>';
            $details .= '<tbody>';
                
                if(isset($ticketDetails) && !empty($ticketDetails)){
                    foreach($ticketDetails as $key=>$val){
                        $details .= '<tr>';
                            $details .= '<td>'.($key+1).'</td>';
                            $details .= '<td>'.$val->ticket->name.'</td>';
                            $details .= '<td>'.$val->custom_ticket_id.'</td>';
                            $details .= '<td>$'.($val->total_price/$val->total_tickets).'</td>';
                            $details .= '<td>'.$val->total_tickets.'</td>';
                            $details .= '<td>$'.$val->total_price.'</td>';
                        $details .= '</tr>';
                    }   
                }
                else{
                    $details .= '<tr><td colspan="4" align="center">Ticket not available.</td></tr>';
                }
                
            $details .= '</tbody>';
        $details .= '</table>';
    $content = str_replace('[ticket]', $details, $content);
}
else if (isset($case) && $case === 16) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[full_name]', $full_name, $content);
    $content = str_replace('[total_amount]', "$".$total_amount, $content);
    $details = '<table border="1" cellpadding="5">';
        $details .= '<thead>';
            $details .= '<th>Sr No.</th>';
            $details .= '<th>Ticket Type</th>';
            $details .= '<th>Ticket</th>';
            $details .= '<th>Price</th>';
            $details .= '<th>Total Ticket</th>';
            $details .= '<th>Sub Total</th>';
        $details .= '</thead>';
            $details .= '<tbody>';
                
                if(isset($ticketDetails) && !empty($ticketDetails)){
                    foreach($ticketDetails as $key=>$val){
                        $details .= '<tr>';
                            $details .= '<td>'.($key+1).'</td>';
                            $details .= '<td>'.$val->ticket->name.'</td>';
                            $details .= '<td>'.$val->custom_ticket_id.'</td>';
                            $details .= '<td>$'.($val->total_price/$val->total_tickets).'</td>';
                            $details .= '<td>'.$val->total_tickets.'</td>';
                            $details .= '<td>$'.$val->total_price.'</td>';
                        $details .= '</tr>';
                    }   
                }
                else{
                    $details .= '<tr><td colspan="4" align="center">Ticket not available.</td></tr>';
                }
                
            $details .= '</tbody>';
        $details .= '</table>';
    $content = str_replace('[ticket]', $details, $content);
}
else if (isset($case) && $case === 18) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', $event, $content);
}
else if (isset($case) && $case === 20) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[username]', $username, $content);
    $content = str_replace('[password]', $password, $content);
    if(isset($event)){
        $content = str_replace('[event]', $event, $content);
    }
}
else if (isset($case) && $case === 23) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', $event, $content);
}
else if (isset($case) && $case === 21) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event]', $event, $content);
    $content = str_replace('[username]', $username, $content);
    $content = str_replace('[email]', $email, $content);
}
else if (isset($case) && $case === 26) {
    $content = str_replace('[name]', $name, $body);
    $content = str_replace('[event_name]', $event_name, $content);
    $content = str_replace('[total_tickets_sold]', $total_tickets_sold, $content);
    $content = str_replace('[total_amount]', $total_amount, $content);
    $content = str_replace('service_charge_percentage', $service_charge_percentage, $content);
    $content = str_replace('[service_charge]', $service_charge, $content);
}
else{
    $content = $body;
}
echo $content;