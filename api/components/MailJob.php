<?php

namespace app\components;
use yii\base\BaseObject;
use yii\swiftmailer\Mailer;

class MailJob extends BaseObject implements \yii\queue\JobInterface
{
    public $case;
    public $name;
    public $body;
    public $eventDetails;
    public $ticketDetails;
    public $templateDetails;
    public $to;
    public $bookingId;
                            
    public function execute($queue){
        $model = new \app\models\EventTicketBookings;
        $find = $model->find()->where(['id'=>$this->bookingId,'status'=>0])->one();
        if($find){
            $mailer = \Yii::createObject([
                'class' => \Yii::$app->params['mailerSettings']['class'],
                //'viewPath' => '@common/mail',
                'transport' => [
                    'class' => \Yii::$app->params['mailerSettings']['transport']['class'],
                    'host' => \Yii::$app->params['mailerSettings']['transport']['host'],
                    'username' => \Yii::$app->params['mailerSettings']['transport']['username'],
                    'password' => \Yii::$app->params['mailerSettings']['transport']['password'],
                    'port' => \Yii::$app->params['mailerSettings']['transport']['port'],
                    'encryption' => \Yii::$app->params['mailerSettings']['transport']['encryption'],
                ],
            ]);
    
            $compose = $mailer->compose(['html' => 'body'],[
                'case'=>$this->case,
                'name'=>$this->name,
                'body'=>$this->body,
                'eventDetails'=>$this->eventDetails,
                'ticketDetails'=>$this->ticketDetails,
            ]);
            $compose->setFrom([$this->templateDetails->from_email => $this->templateDetails->from_label]);
            $compose->setTo($this->to);
            $compose->setSubject($this->templateDetails->subject); 
            $compose->send();
        }
    }
}
