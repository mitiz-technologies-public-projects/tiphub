<?php

namespace app\components;
use yii\base\BaseObject;
use Yii;
use yii\swiftmailer\Mailer;

class SendSms extends BaseObject implements \yii\queue\JobInterface
{
    public $query;
    public $number;
    public $sms_body;
    public $twilioSid;
    public $twilioToken;
    public $twilioFromNumber;
                            
    public function execute($queue){
        if (!empty($this->number)) {
            $phoneNumbers = $this->number;
        }else{
            $events = $this->query;
            $find = $events->asArray()->all();
            if($find){
                foreach($find as $key => $event['bookings']){
                    $phoneNumbers[$key]['name'] = $event['bookings']['first_name']." ".$event['bookings']['last_name'];
                    $phoneNumbers[$key]['phone'] = $event['bookings']['phone'];
                }
            }
        }
        if(!empty($phoneNumbers)){
            foreach($phoneNumbers as $pn){
                //echo Yii::$app->params['twilioFromNumber'].'~'.$pn['phone'];die;
                $client = new \Twilio\Rest\Client($this->twilioSid, $this->twilioToken);
                try{
                    // Use the client to do fun stuff like send text messages!
                    $client->messages->create(
                        // the number you'd like to send the message to
                        $pn['phone'],
                        [
                            // A Twilio phone number you purchased at twilio.com/console
                            'from' => $this->twilioFromNumber,
                            // the body of the text message you'd like to send
                            'body' => $this->sms_body
                        ]
                    );
                    Yii::debug('Success - ('.$pn['phone'].'): Sms sent successfully.');
                }
                catch (\Exception $e) {
                    Yii::debug('Exception occured: '.Yii::$app->common->returnException($e));
                }
            }
        }
    }
}
