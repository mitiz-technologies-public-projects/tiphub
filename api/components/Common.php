<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\swiftmailer\Mailer;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use app\models\User;


class Common extends Component
{

    public function sendEmail($data)
    {
        $mail = new Mailer();
        $compose = $mail->compose('body', $data);
        $compose->setFrom([$data['body']->from_email => $data['body']->from_label]);
        $compose->setTo($data['to']);
        if (isset($data['body']->reply_to_email) && !empty($data['body']->reply_to_email)) {
            $compose->setReplyTo($data['body']->reply_to_email);
        }
        $compose->setSubject($data['subject']);
        return $compose->send();
    }

    public function getUserIdByUsername($username)
    {
        $find = \app\models\User::find()->select('id')->where(['username' => $username])->one();
        if ($find) {
            return $find->id;
        } else {
            return null;
        }
    }

    public function checkImgUrl($url)
    {
        if (isset($url) && !empty($url)) {
            $urlArr = explode("?", $url);
            return $urlArr[0];
        }
    }

    public function getMetaData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $html = curl_exec($ch);
        curl_close($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML($html);
        $nodes = $doc->getElementsByTagName('title');
        $title = $nodes->item(0)->nodeValue;

        $metas = $doc->getElementsByTagName('meta');
        for ($i = 0; $i < $metas->length; $i++) {
            $meta = $metas->item($i);
            if ($meta->getAttribute('name') == 'description') {
                $description = $meta->getAttribute('content');
            }

            if ($meta->getAttribute('property') == 'og:image') {
                $image = $meta->getAttribute('content');
            }

        }
        return [
            'title' => isset($title) ? $title : '',
            'description' => isset($description) ? $description : '',
            'image' => isset($image) ? $image : '',
        ];
    }

    public function returnException($e)
    {
        //\Yii::info($e->getMessage(), 'mycategory');
        //\Yii::info("Exception occured! =>>> ".$e->getMessage());
        return $e->getMessage();
        return 'There is some technical issue at server end! Please contact administrator or try again later.';
    }

    public function checkModuleActionName($permissions, $module_name, $action_name)
    {
        $permissions = unserialize($permissions);
        if (isset($permissions) && !empty($permissions)) {
            if (isset($permissions[$module_name]) && !empty($permissions[$module_name])) {
                foreach ($permissions[$module_name] as $key => $val) {
                    if ($val == $action_name) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* public function checkPermission($module_name, $action_name)
    {
    if (Yii::$app->user->isGuest) {
    return false;
    }

    $find = \app\models\AssignedRoles::find()->select('role_id')->where(['user_id' => Yii::$app->user->identity->id])->all();
    //echo $query->createCommand()->getRawSql();die;
    if (isset($find) && !empty($find)) {
    if (array_key_exists($module_name, Yii::$app->params['admin_exclude_module']) && in_array($action_name, Yii::$app->params['admin_exclude_module'][$module_name])) {
    return true;
    }
    foreach ($find as $role) {
    if ($role->role_id == 1) {
    return true;
    }
    $rFind = \app\models\Roles::find()->select('permission')->where(['id' => $role->role_id])->one();
    $permissions = unserialize($rFind->permission);
    if (isset($permissions[$module_name]) && !empty($permissions[$module_name])) {
    foreach ($permissions[$module_name] as $key => $val) {
    if (trim($val) == trim(strtolower($action_name))) {
    return true;
    }
    }
    }
    }
    }
    return false;
    } */

    public function checkPermission($module_name, $action_name)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->identity->role_id == 1) {
            return true;
        }

        $find = \app\models\Roles::find()->where(['id' => Yii::$app->user->identity->role_id])->one();
        //echo $query->createCommand()->getRawSql();die;
        if (isset($find) && !empty($find)) {
            $permission = unserialize($find->permission);
            //print_r($permission);die;
            if (array_key_exists($module_name, $permission) && in_array($action_name, $permission[$module_name])) {
                return true;
            }
        }
        return false;
    }

    public function checkModulePermission($module_name)
    {
        $session_action_allowed = Yii::$app->session->get('session_action_allowed');
        if ($session_action_allowed == "all" || (is_array($session_action_allowed) && array_key_exists($module_name, $session_action_allowed))) {
            return true;
        } else {
            return false;
        }
    }

    public function checkModuleActionPermission($module_name, $action_name)
    {
        $session_action_allowed = Yii::$app->session->get('session_action_allowed');
        if ($session_action_allowed == "all" || (is_array($session_action_allowed) && array_key_exists($module_name, $session_action_allowed) && in_array($action_name, $session_action_allowed[$module_name]))) {
            return true;
        } else {
            return false;
        }
    }

    public function addLog($case_id, $title, $message = null){
        $model = new \app\models\CaseLog;
        $model->case_id = $case_id;
        $model->added_by = Yii::$app->user->identity->id;
        $model->title = $title;
        $model->message = $message;
        if($model->validate()){
            $model->save();
        }
    }

    public function updateBalance($case_id){
        $total_amount_to_be_paid_by_doctor = [];
        $query = \app\models\CaseServices::find()->where(['case_id'=>$case_id,'who_will_pay'=>0]);
        $total_amount_to_be_paid_by_doctor['sub_total'] = $query->sum('sub_total');
        $total_amount_to_be_paid_by_doctor['discount'] = $query->sum('discount');

        //print_r($total_amount_to_be_paid_by_doctor);die;
        $total_amount_to_be_paid_by_patient = [];
        $query = \app\models\CaseServices::find()->where(['case_id'=>$case_id,'who_will_pay'=>1]);
        $total_amount_to_be_paid_by_patient['sub_total'] = $query->sum('sub_total');
        $total_amount_to_be_paid_by_patient['discount'] = $query->sum('discount');

        //$total_amount_to_be_paid_by_patient = \app\models\CaseServices::find()->where(['case_id'=>$case_id,'who_will_pay'=>1])->sum(['sub_total','discount']);
        //print_r($total_amount_to_be_paid_by_doctor);die;
        //$total_amount_paid_by_doctor = \app\models\Payments::find()->where(['case_id'=>$case_id])->andWhere(['!=','user_id',NULL])->sum('amount');
        //$total_amount_paid_by_patient = \app\models\Payments::find()->where(['case_id'=>$case_id])->andWhere(['!=','patient_id',NULL])->sum('amount');
        $find = \app\models\Cases::findOne(['id'=>$case_id]);
        if($find){
            /* $find->doctor_balance = $total_amount_to_be_paid_by_doctor - $total_amount_paid_by_doctor;
            $find->patient_balance = $total_amount_to_be_paid_by_patient - $total_amount_paid_by_patient; */

            $find->doctor_balance = $total_amount_to_be_paid_by_doctor['sub_total'];
            $find->doctor_discount = $total_amount_to_be_paid_by_doctor['discount'];
            $find->patient_balance = $total_amount_to_be_paid_by_patient['sub_total'];
            $find->patient_discount = $total_amount_to_be_paid_by_patient['discount'];
            $find->total_price = $total_amount_to_be_paid_by_doctor['sub_total'] + $total_amount_to_be_paid_by_patient['sub_total'];
            if($find->save()){
                $model = new \app\models\Payments;
                $pdfind = $model->find()->where(['case_id'=>$case_id,'status'=>0])->andWhere(['!=','user_id','NULL'])->one();
                if($pdfind){
                    $pdfind->amount = $total_amount_to_be_paid_by_doctor['sub_total'] + $total_amount_to_be_paid_by_doctor['discount'];
                    $pdfind->sub_total = $total_amount_to_be_paid_by_doctor['sub_total'];
                    $pdfind->discount = $total_amount_to_be_paid_by_doctor['discount'];
                    if($pdfind->save()){
                        $model = new \app\models\Payments;
                        $ppfind = $model->find()->where(['case_id'=>$case_id,'status'=>0])->andWhere(['!=','patient_id','NULL'])->one();
                        if($ppfind){
                            $ppfind->amount = $total_amount_to_be_paid_by_patient['sub_total'] + $total_amount_to_be_paid_by_patient['discount'];
                            $ppfind->sub_total = $total_amount_to_be_paid_by_patient['sub_total'];
                            $ppfind->discount = $total_amount_to_be_paid_by_patient['discount'];
                            $ppfind->save();
                        }
                    }
                }
            }
            else{
                print_r($find->getErrors());die;
            }
        }
    }

    public function getFullName($data) {
        $name = "";
        if (isset($data['prefix'])) {
          $name .= $data['prefix'] . ' ';
        }
        if (isset($data['first_name'])) {
          $name .= $data['first_name'] . ' ';
        }
        if (isset($data['middle_name'])) {
          $name .= $data['middle_name'] . ' ';
        }
        if (isset($data['last_name'])) {
            $name .= $data['last_name'] . ' ';
        }
        if (isset($data['suffix'])) {
            $name .= $data['suffix'] . ' ';
        }
        return $name;
    }

    public function findColorCode(){
        $model = new \app\models\Events;
        $find = $model->find()->select(['color_code'])->where(['user_id'=>Yii::$app->user->identity->id])->all();
        $colorCodes = [];
        if($find){
            foreach($find as $row){
                if(isset($row->color_code) && !empty($row->color_code)){
                    $colorCodes[] = $row->color_code;
                }
            }
        }
        $colorsToAssign = array_diff(Yii::$app->params['color_codes'], $colorCodes);
        if(empty($colorsToAssign)){
            return sprintf('#%06x', rand(0, 16777215));
        }
        else{
            return $colorsToAssign[array_rand($colorsToAssign)];
        }
        
    }

    public function sendTestEmail(){
        $compose = Yii::$app->mailer->compose('body', ['body'=>'<p>Hello</p>']);
        $compose->setFrom(['no-reply@tiphub.co' => 'Tiphub.co']);
        $compose->setTo('opnsrc.devlpr@gmail.com');
        $compose->setSubject('Testing');
        $compose->send();
    }

    public function getAssingedEvents($action_name, $user_id = 0){
        if(isset(Yii::$app->user)){
            $user_id = @Yii::$app->user->identity->id;
        }
        $eventIds = [];
        $find = \app\models\TeamAndPermissions::find()->where(new \yii\db\Expression('FIND_IN_SET("' . $action_name . '", permissions)'))->andWhere(['team_id'=>$user_id])->all();
        
        foreach($find as $f){
            $eventIds[] = $f->event_id;
        }
        return $eventIds;   
    }

    public function getDwollaTransactions($dwolla_customer_url){

        \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
        \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];

        $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
        $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
        $appToken = $tokensApi->token();
        \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
        //\DwollaSwagger\Configuration::$debug = 1;

        $tapi = new \DwollaSwagger\TransfersApi($apiClient);
        $transfers = $tapi->getCustomerTransfers($dwolla_customer_url);
        //print_r($transfers);die;
        $transactions = json_decode(json_encode($transfers->_embedded->transfers), true);

        foreach($transactions as $key=>$val){
            $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
            $customer = $customersApi->getCustomer($val['_links']['source']['href']);
            $transactions[$key]['customer'] = $customer;
        }
        return $transactions;   
    }

    public function createStripeCustomer($paymentMethod, $email, $name, $subscription){
        $model = new \app\models\User;
        $emailArr = explode('@', $email);
        $findUser = $model->find()->where(['email'=>$email])->orWhere(['username'=>$emailArr[0]])->one();
        if(!$findUser){
            $model = new \app\models\User;
            $model->username = $emailArr[0];
            $model->email = $email;
            $model->status = 'Y';
            $model->name = $name;
            $model->password_hash = Yii::$app->security->generatePasswordHash('tiphub@123');
            //Creating customer in Stripe if subscriptin is needed
            if(isset($subscription) && !empty($subscription) && $subscription == true){
                $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
                $customer = $stripe->customers->create([
                    'name'  => $model->name,
                    'email' => $model->email,
                    //'invoice_prefix' => 'TIBHUB-'.rand(1111, 9999),
                    'metadata' => [
                        'user_id' => $model->id
                    ],
                    'payment_method' => $paymentMethod,
                    'invoice_settings' => ['default_payment_method' => $paymentMethod]
                ]);
                if($customer){
                    $model->stripe_customer_id = $customer->id;
                }
            }
            
            if($model->save()){
                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 20])->one();
                $data = [
                    'case' => 20,
                    'name' => $name,
                    'username' => $emailArr[0],
                    'password' => 'tiphub@123',
                    'body' => $templateDetails->content
                ];
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                $compose->setTo($email);
                //$compose->setTo('opnsrc.devlpr@gmail.com');
                $compose->setSubject($templateDetails->subject);
                if ($compose->send()) {
                    return $model->stripe_customer_id;
                }
                
            }
            else{
                return $model->getErrors();
            }
        }
        else if(isset($subscription) && !empty($subscription) && $subscription == true){
            //Creating customer in Stripe if subscriptin is needed
            if(empty($findUser->stripe_customer_id)){
                $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
                $customer = $stripe->customers->create([
                    'name'  => $name,
                    'email' => $email,
                    //'invoice_prefix' => 'TIBHUB-'.rand(1111, 9999),
                    'metadata' => [
                        'user_id' => $model->id
                    ],
                    'payment_method' => $paymentMethod,
                    'invoice_settings' => ['default_payment_method' => $paymentMethod]
                ]);
                if($customer){
                    $findUser->stripe_customer_id = $customer->id;
                    if($findUser->save()){
                        return $findUser->stripe_customer_id;
                    }
                }
            }
            else{
                return $findUser->stripe_customer_id;
            }
        }
        else{
            return null;
        }
    }

    // Function to execute Curl request for Paypal or other APIs
    public function setCurlParams($options = []){
        $ch = curl_init();
        $response['error'] = false;
        $response['response'] = "Something went wrong";
        if($options !='' && is_array($options)){
            
            $default_params = [
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
            ];
            curl_setopt_array($ch, $default_params);

            curl_setopt_array($ch, $options);
            $result = curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);
           
            if ($err) {
                $response['error'] = true;
                $response['response'] = json_decode($err);
            }else if(isset($result->error)){
                $response['error'] = true;
                $response['response'] = $result->error."=>".$result->error_description;
            }
            else
            {
                $response['error'] = false;
                $response['response'] = json_decode($result);
            }
        }
        return $response;
    }
    function getPaypalAccessToken(){
        $params = [
            CURLOPT_URL => Yii::$app->params['paypalUrl']."v1/oauth2/token",
            CURLOPT_USERPWD => Yii::$app->params['clientId'].":".Yii::$app->params['clientSecret'],
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        ];
        $response = Yii::$app->common->setCurlParams($params);
        
        if ($response['error'] == true) {
            return [
                'error' => true,
                'message' => $response['error']
            ];
        }
        else
        {
           //print_r($response['response']);die;
           $access_token = $response['response']->access_token;
          
            if($access_token != ''){
                return $access_token;
            }
        }
        return false;
    }
    function sendSmtpEmail($to,$body,$subject=""){
        $mailer = \Yii::createObject([
            'class' => \Yii::$app->params['mailerSettings']['class'],
            //'viewPath' => '@common/mail',
            'transport' => [
                'class' => \Yii::$app->params['mailerSettings']['transport']['class'],
                'host' => \Yii::$app->params['mailerSettings']['transport']['host'],
                'username' => \Yii::$app->params['mailerSettings']['transport']['username'],
                'password' => \Yii::$app->params['mailerSettings']['transport']['password'],
                'port' => \Yii::$app->params['mailerSettings']['transport']['port'],
                'encryption' => \Yii::$app->params['mailerSettings']['transport']['encryption'],
            ],
        ]);

        $compose = $mailer->compose(['html' => 'body'],[
            'body'=>$body
        ]);
        $compose->setFrom(['no-reply@tiphub.co' => 'Tiphub.Co']);
        $compose->setTo($to);
        $compose->setSubject($subject); 
        $compose->send();
    }

    // Function to send Ticket confirmation emails
    function sendTicketConfirmationEmail($find, $send_to_owner = false){
        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 13])->one();
        $data = [
            'case' => 13,
            'name' => $find->first_name,
            'full_name' => $find->first_name . " " . $find->last_name,
            'total_amount' => $find->total_amount,
            'body' => $templateDetails->content,
            'ticketDetails' => $find->details
        ];

        $compose = Yii::$app->mailer->compose('body', $data);
        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
        $compose->setTo($find->email);
        $compose->setSubject($templateDetails->subject);
        foreach ($find->details as $ticket) {
            $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/ticket/' . $ticket->id))->setSize(300)->setMargin(5);
            $compose->attachContent($qrCode->writeString(), ['fileName' => $ticket->custom_ticket_id . '.png', 'contentType' => $qrCode->getContentType()]);
        }
        if ($compose->send()) {
            return true;
        }
        return false;
    }

    function registerUser($data){
        $umodel = new \app\models\User;
        $ufind = $umodel->find()->where(['email'=>$data['email']])->count();
        if($ufind == 0){
            $uemailmodel = \app\models\UserEmails::find()->where(['email'=>$data['email']])->count();
            if($uemailmodel == 0){
                $password = 'tiphub@'.date('Y');
                $umodel->first_name = $data['first_name'];
                $umodel->last_name = $data['last_name'];
                $umodel->name = $data['first_name']." ".$data['last_name'];
                $emailArr = explode("@", $data['email']);
                $umodel->username = $emailArr[0];
                $umodel->email = $data['email'];
                $umodel->phone = $data['phone'];
                $umodel->password_hash = Yii::$app->security->generatePasswordHash($password);
                if($umodel->save()){
                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 20])->one();
                    $emailData = [
                        'case' => 20,
                        'username' => $umodel->username,
                        'name' => $umodel->name,
                        'password' => $password,
                        'body' => $templateDetails->content,
                    ];
                    $compose = Yii::$app->mailer->compose('body', $emailData);
                    $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                    $compose->setTo($data['email']);
                    //$compose->setTo('opnsrc.devlpr@gmail.com');
                    $compose->setSubject($templateDetails->subject);
                    $compose->send();
                }
                /* else{
                    print_r($umodel->getErrors());
                } */
            }
        }
    }

    function removeSpecialChar($string){
        $string = strtolower(trim($string));
        // Replaces all spaces with hyphens.
        $string = str_replace(' ', '-', $string);
        
        // Removes special chars.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        // Replaces multiple hyphens with single one.
        $string = preg_replace('/-+/', '-', $string);
        
        return $string;
    }
}
