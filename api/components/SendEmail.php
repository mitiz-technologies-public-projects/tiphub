<?php

namespace app\components;
use yii\base\BaseObject;
use Yii;
use yii\swiftmailer\Mailer;

class SendEmail extends BaseObject implements \yii\queue\JobInterface
{
    public $query;
    public $email_content;
    public $email_subject;
   
                            
    public function execute($queue){
        $events = $this->query;
       // $find = $events->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        $find = $events->asArray()->all();
       // Yii::debug('Send SMS debug log'. print_r($find,true));
        if($find){
            foreach($find as $key => $event['bookings']){
                 $toEmail = $event['bookings']['email'];

                $mailer = \Yii::createObject([
                    'class' => \Yii::$app->params['mailerSettings']['class'],
                    //'viewPath' => '@common/mail',
                    'transport' => [
                        'class' => \Yii::$app->params['mailerSettings']['transport']['class'],
                        'host' => \Yii::$app->params['mailerSettings']['transport']['host'],
                        'username' => \Yii::$app->params['mailerSettings']['transport']['username'],
                        'password' => \Yii::$app->params['mailerSettings']['transport']['password'],
                        'port' => \Yii::$app->params['mailerSettings']['transport']['port'],
                        'encryption' => \Yii::$app->params['mailerSettings']['transport']['encryption'],
                    ],
                ]);
        
                $compose = $mailer->compose(['html' => 'body'],[
                    'body'=>$this->email_content
                ]);
                $compose->setFrom(['no-reply@tiphub.co' => 'Tiphub.Co']);
                $compose->setTo($toEmail);
                //$compose->setTo('opnsrc.devlpr@gmail.com');
                $compose->setSubject($this->email_subject); 
                $compose->send();
            }
        }

       
    }
}
