<?php

namespace app\components;
use yii\base\BaseObject;
use Yii;

class DataBaseBackup extends BaseObject implements \yii\queue\JobInterface
{
    public function execute($queue)
    {
        $sql = 'SHOW TABLES';
        $cmd = \Yii::$app->db->createCommand($sql);
        $tables = $cmd->queryColumn();
        $filePath =  '../backup/'.date("Y-m-d").'-DataBase-backup.sql';
        if (is_file($filePath)) {
            unlink($filePath);
        }
        $sqlFile = fopen($filePath, 'w+');
        fwrite($sqlFile, 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";' . PHP_EOL);
        fwrite($sqlFile, 'SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;' . PHP_EOL);
        fwrite($sqlFile, 'SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;' . PHP_EOL);
        fwrite($sqlFile, '-- -------------------------------------------' . PHP_EOL);
        foreach ($tables as $key => $table) {
            fwrite($sqlFile, PHP_EOL);
            fwrite($sqlFile, '-- ----------------------------' . $table . '---------------------------');
            fwrite($sqlFile, PHP_EOL);
            fwrite($sqlFile, PHP_EOL);
            $sql = 'SHOW CREATE TABLE ' . $table;
            $cmd = Yii::$app->db->createCommand($sql);
            $tableStructure = $cmd->queryOne();
            $tableStructure = $tableStructure['Create Table'] . ';';
            $tableStructure = preg_replace('/^CREATE TABLE/', 'CREATE TABLE IF NOT EXISTS', $tableStructure);
            $tableStructure = preg_replace('/AUTO_INCREMENT\s*=\s*([0-9])+/', '', $tableStructure);
            if ($table == "users") {
                $tableStructure = preg_replace('/ROW_FORMAT=COMPACT/', 'ROW_FORMAT=DYNAMIC', $tableStructure);
            }
            fwrite($sqlFile, $tableStructure);
            fwrite($sqlFile, PHP_EOL);
            $countQuery = "SELECT count(*) FROM " . $table;
            $cmdCount = Yii::$app->db->createCommand($countQuery);
            $excuteCount = $cmdCount->queryOne();
            $count = $excuteCount['count(*)'];
            $rowFetched = 100;
            if ($count > 0) {
                $i = 1;
                $limit = 0;
                while ($count > $rowFetched) {
                    $query = "SELECT * FROM " . $table . " LIMIT  100  OFFSET " . $limit . ";";
                    $cmd = Yii::$app->db->createCommand($query);
                    $tableData = $cmd->queryAll();
                    foreach ($tableData as $row) {
                        $insertQuery = '';
                        $columnNames = array_keys($row);
                        $columnNames = array_map("addslashes", $columnNames);
                        $columnNames = join('`,`', $columnNames);
                        $columnData = array_values($row);
                        $columnData = array_map("addslashes", $columnData);
                        $columnData = join("','", $columnData);
                        $insertQuery = "INSERT INTO `" . $table . "` (`" . $columnNames . "`) VALUES ('" . $columnData . "');" . PHP_EOL;
                        fwrite($sqlFile, $insertQuery);
                    }
                    $limit = $i * $rowFetched;
                    $i ++;
                    $count = $count - $rowFetched;
                }
                if ($count > 0) {
                    $query = "SELECT * FROM " . $table . " LIMIT  100 OFFSET " . $limit . ";";
                    $cmd = Yii::$app->db->createCommand($query);
                    $tableData = $cmd->queryAll();
                    foreach ($tableData as $row) {
                        $insertQuery = '';
                        $columnNames = array_keys($row);
                        $columnNames = array_map("addslashes", $columnNames);
                        $columnNames = join('`,`', $columnNames);
                        $columnData = array_values($row);
                        $columnData = array_map("addslashes", $columnData);
                        $columnData = join("','", $columnData);
                        $insertQuery = "INSERT INTO `" . $table . "` (`" . $columnNames . "`) VALUES ('" . $columnData . "');" . PHP_EOL;
                        fwrite($sqlFile, $insertQuery);
                    }
                }
            }
        }
        fwrite($sqlFile, PHP_EOL);
        fwrite($sqlFile, '-- -------------------------------------------' . PHP_EOL);
        fwrite($sqlFile, 'SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;' . PHP_EOL);
        fwrite($sqlFile, 'SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;' . PHP_EOL);
        fclose($sqlFile);
        $sqlFile = $filePath;
        if (class_exists(\ZipArchive::class)) {
            $zip = new \ZipArchive();
            $zipName = '../backup/' . date("Y-m-d").'-DataBase-backup.zip';
            if ($zip->open($zipName, \ZipArchive::CREATE) === TRUE) {
                $zip->addFile($sqlFile, date("Y-m-d").'-DataBase-backup.sql');
                $zip->close();
                $previousDate = date('Y-m-d', strtotime('-10 days'));
                @unlink($sqlFile);
                $zipFileName = '../backup/' . $previousDate.'-DataBase-backup.zip';
                if (file_exists($zipFileName)) {
                    unlink($zipFileName);
                }
            }
        } else {
            return false;
        }
        return true;
    }
}