<?php

/* return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=mitizt5_tiphub',
    'username' => 'mitizt5_mazeeca',
    'password' => 'kCSEZ+-+BjwD',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    /* 'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache', 
]; */


return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1:3306;dbname=mitiz_tiphub',
    'username' => 'root',
    'password' => 'secret',
    //'charset' => 'utf8',
    'charset'=>'utf8mb4',

    // Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];


