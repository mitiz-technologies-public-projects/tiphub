<?php
    use yii\helpers\Html;
?>
<div class="outer-border">
    <div class="inner-dotted-border">
        <span class="certification">Certificate Of Appreciation</span>
        <br><br>
        <span class="certify"><i>This is to certify that</i></span>
        <br><br>
        <span class="name"><b>
            <?php 
                echo $record->first_name;
                if(isset($record->middle_name)){
                    echo " ".$record->middle_name;
                }
                if(isset($record->last_name)){
                    echo " ".$record->last_name;
                }
            ?></b></span><br/><br/>
        <span class="certify"><i>has successfully donated for <?php echo $record->event->title?></i></span> <br/><br/>
        <span class="certify"><i>dated</i></span><br>
        <span class="fs-30"><?php echo date("M d, Y", strtotime($record->added_on))?></span><br/><br/>
        <span><img src="/assets/tiphub-app.png" width="150"/></span>
    </div>
</div>
