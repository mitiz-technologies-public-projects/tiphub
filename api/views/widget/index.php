<?php
    use yii\helpers\Html;
?>
<link href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" rel="stylesheet">

    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div class="panel shadow">
                <?php /*
                <div class="profile">
                  <img src="assets/ProfileMain.jpg" />
                  <div class="profile-details">
                    <h3><?php echo $record->name?></h3>
                    <a
                        href=""
                        target="_blank"
                        class="details_link"
                      >
                      <?php 
                        $fa_class = "";
                        if(isset($record->facebook_id) && !empty($record->facebook_id)){
                            $fa_class = "fa-facebook";
                        }
                        else if(isset($record->twitter_id) && !empty($record->twitter_id)){
                            $fa_class = "fa-twitter";
                        }
                        else if(isset($record->google_id) && !empty($record->google_id)){
                            $fa_class = "fa-google";
                        }
                    ?>
                      <i class="fab <?php echo $fa_class?>"></i>
                      <?php echo $record->username?>
                      </a>
                  </div>
                </div>

                <?php 
                if(isset($record->sociallinks) && !empty($record->sociallinks)){
                    echo '<div class="social-media"><div class="icons">';
                    foreach($record->sociallinks as $sociallink){
                        echo '<img src="'.Yii::$app->params['siteUrl'].'/assets/'.$sociallink->plateform.'.png"/>';
                    }
                    echo '</div></div>';
                }
                ?>
                <div class="descriptions"><p><?php echo $record->description?></p></div>*/?>

                <?php if(isset($record->services) && !empty($record->services)){?>
                    <div class="panel-header descripation_title">Tipping apps</div>
                    <div class="panel-body">
                        <?php foreach($record->services as $service){?>
                            <div class="trending-apps text-left mt-2 mb-2">
                                <a href="<?php echo $service->url?>">
                                    <div class="tipping-app-payment"> 
                                        <img src="<?php echo Yii::$app->params['siteUrl']?>/assets/<?php echo $service->appname->icon?>" />
                                        <span><?php echo $service->appname->name?></span>
                                    </div>
                                </a>
                            </div>
                        <?php }?>
                    </div>
                <?php }?>  
                              
                <?php if(isset($record->qrcodes) && !empty($record->qrcodes)){?>
                <h4 class="panel-header descripation_title qr_code">QR-CODES</h4>
                    <?php foreach($record->qrcodes as $code){?>
                    <div class="qr_code-preview mt-2">
                        <h4 style="text-transform: capitalize;"><?php echo $code->title?></h4>
                        <div class="qr_code_image media">
                            <img src="<?php echo Yii::$app->params['apiUrl']?>/web/qrcodes/<?php echo $code->image?>" class="media-object"/>
                            <button type="button" onClick="copyQrCode('<?php echo $code->address?>')"><i class="fa fa-copy"></i> Copy QR Code</button>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
    <script>
    function copyQrCode(code){
        navigator.clipboard.writeText(code).then(() => {
            alert('Code copied to clipboard.');
        },
        function () {
            alert('Sorry! Your browser does not support this feature.');
        },)
    }
    </script>