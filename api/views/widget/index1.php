<?php
    use yii\helpers\Html;
?>
<div class="row">
    <div class="col-sm-12 col-md-3">
        <div class="panel shadow">
            <?php if(isset($record->services) && !empty($record->services)){?>
            <div class="panel-header descripation_title">Tipping apps</div>
            <div class="panel-body">
                <?php foreach($record->services as $service){?>
                <div class="trending-apps text-left mt-2 mb-2">
                    <img src="<?php echo Yii::$app->params['siteUrl']?>/assets/tiphub/<?php echo $service->service_id?>.svg" />
                    <a href="<?php echo $servicesLinkArr[$service->service_id]."/".$record->username?>"><?php echo $servicesArr[$service->service_id]?></a>
                    <div class="trending-apps-actions ml-auto mr-0">
                        <i class="fas fa-copy"></i>
                    </div>
                </div>
                <?php }?>
            </div>
            <?php }?>

            <?php if(isset($record->qrcodes) && !empty($record->qrcodes)){?>
            <h4>QR-CODES</h4>
                <?php foreach($record->qrcodes as $code){?>
                <div class="qr_code-preview mt-2">
                    <h6 style="text-transform: capitalize;"><?php echo $code->title?></h6>
                    <div class="qr_code_image media">
                        <img src="<?php echo Yii::$app->params['apiUrl']?>/web/qrcodes/<?php echo $code->image?>" class="media-object"/>
                    </div>
                </div>
                <?php }?>
            </div>
            <?php }?>
        </div>
    </div>
</div>