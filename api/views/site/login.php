<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\authclient\widgets\AuthChoice;
    use app\widgets\Alert;
    use yii\helpers\Url;
    $this->title = Yii::$app->name . ' | Login';
?>
<div class="login-box-body" style="min-height: 250px;">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php
        $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => ''],
            'validateOnBlur'=>true,
            'enableClientValidation'=>true,
            'fieldConfig' => [
                'template' => "<div class=\"form-group\">{label}{input}{error}</div>",    
                'labelOptions' => [],
            ],
        ]);
    ?>
    <?php 
        echo $form->field($login, 'username', ['template' => "{label}\n<div class='has-feedback'>{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span></div>\n{hint}\n{error}"])->textinput([
            'placeholder'=>'Email or Username','class'=>'form-control input_user'
        ])->label(false);
        echo $form->field($login, 'password', ['template' => "{label}\n<div class='has-feedback'>{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span></div>\n{hint}\n{error}"])->passwordInput(['placeholder'=>'Password','class'=>'form-control input_user'])->label(false);;
    ?>
    <div class="row" style="margin-top: 40px;">
        <div class="col-xs-8">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                    <label class="custom-control-label" for="customControlInline">Remember me</label>
                </div>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
    </div>
    <?php ActiveForm::end();?>

        <!-- <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a> -->

</div>
<!-- /.login-box-body -->
<?php
if(Yii::$app->session->hasFlash('success')){
	$this->registerJs('	
	    toastr.success("'.Yii::$app->session->getFlash('success').'");
	', \yii\web\VIEW::POS_END);
	
}
else if(Yii::$app->session->hasFlash('error')){
	$this->registerJs('	
	    toastr.error("'.Yii::$app->session->getFlash('error').'");
	', \yii\web\VIEW::POS_END);
}

$this->registerJs('	
    $(function () {
        $(".custom-control-input").iCheck({
            checkboxClass: "icheckbox_square-blue",
            radioClass: "iradio_square-blue",
            increaseArea: "20%" /* optional */
        });
    });
', \yii\web\VIEW::POS_END);