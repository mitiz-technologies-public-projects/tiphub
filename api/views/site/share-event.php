<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $transaction->event->title;
?>
<div class="panel panel-default" style="width:300px">
  <div class="panel-body text-center">
      <img src="<?php echo Yii::$app->params['apiUrl']?>/web/events/sharable-<?php echo $transaction->event->id?>-<?php echo $transaction->id?>.png" />
      <a href="<?php echo Yii::$app->params['siteUrl']?>/<?php echo $transaction->event->url?>/<?php echo $transaction->event->id?>" class="btn btn-success">Go To Event</a>
  </div>
</div>
