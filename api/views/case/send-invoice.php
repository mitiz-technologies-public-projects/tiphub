<?php
    use yii\helpers\Html;
?>
<table class="table table-dark">
    <thead>
        <tr>
            <th>Invoice No</th>
            <th>Case Id</th>
            <th>Patient Name</th>
            <th>Case Fee</th>
            <th>Payable Amount</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $record->invoice_id?></td>
            <td><?php echo $record->case->c_id?></td>
            <td>
                <?php echo $record->case->patient->prefix?> <?php echo $record->case->patient->first_name?> <?php echo $record->case->patient->middle_name?> <?php echo $record->case->patient->last_name?> <?php echo $record->case->patient->suffix?>
            </td>
            <td>$<?php echo $record->case->total_price?></td>
            <td>
                <?php 
                    if(isset($record->user_id)){
                        echo "$".$record->case->doctor_balance;
                    }
                    else if(isset($record->patient_id)){
                        echo "$".$record->case->patient_balance;
                    }
                ?>
            </td>
        </tr>
    </tbody>
</table>