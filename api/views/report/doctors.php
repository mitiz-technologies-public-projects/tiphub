<?php
    use yii\helpers\Html;
?>
<?php if(isset($location_name)){?>
<div class="alert alert-dark" style="padding:7px;margin-bottom:10px;font-size:20px;background-color:#cccccc">
    <?php echo $location_name;?> - Doctors
</div>
<?php }?>
<table class="table table-dark">
    <thead>
        <tr>
            <th style="border-top:0px">Name</th>
            <th style="border-top:0px">Username</th>
            <th style="border-top:0px">Email</th>
            <th style="border-top:0px">Phone</th>
            <th style="border-top:0px">Added On</th>
        </tr>
    </thead>
    <tbody>
        <?php if($records){foreach($records as $row){?>
        <tr>
            <td><?php echo Yii::$app->common->getFullName($row); ?></td>
            <td><?php echo $row['username']?></td>
            <td><?php echo $row['email']?></td>
            <td><?php echo $row['phone']?></td>
            <td><?php echo date("M d, Y",strtotime($row['added_on']));?></td>
        </tr>
        <?php }}?>
    </tbody>
</table>