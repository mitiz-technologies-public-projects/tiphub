<?php
    use yii\helpers\Html;
    $modeArr = ["Cash", "Check", "Visa", "MasterCard", "Amex", "Discover"];
?>
<?php if(isset($location_name)){?>
<div class="alert alert-dark" style="padding:7px;margin-bottom:10px;font-size:20px;background-color:#cccccc">
    <?php echo $location_name;?> - Doctor A/R
</div>
<?php }?>
<table class="table table-dark">
    <thead>
        <tr>
            <th style="font-size:13px;border-top:0px">Patient</th>
            <th style="font-size:13px;border-top:0px">Doctor</th>
            <th style="font-size:12px;border-top:0px">Total Of Fees</th>
            <th style="font-size:13px;border-top:0px">Payment</th>
            <th style="font-size:13px;border-top:0px">Case ID</th>
            <th style="font-size:13px;border-top:0px">Date</th>
            <th style="font-size:13px;border-top:0px">Adjustments</th>
        </tr>
    </thead>
    <tbody>
        <?php if($records){foreach($records as $row){?>
        <tr>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold">
                <?php 
                    echo $row['case']['patient']['first_name']." ". $row['case']['patient']['last_name'];
                ?>
            </td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold">
                <?php 
                    echo Yii::$app->common->getFullName($row['user']);
                ?>
            </td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold">$<?php echo $row['amount']?></td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold">$<?php echo $row['sub_total']?></td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold"><?php echo $row['case']['c_id']?></td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold">
                <?php 
                    if(isset($row['added_on'])){
                        echo date("M d, Y",strtotime($row['added_on']));
                    }
                    else{
                        echo "--";
                    }
                ?>
            </td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold;color:red">$<?php echo $row['discount']?></td>
            <td style="border-top:3px solid;font-size:11px;font-weight:bold"><?php echo $modeArr[$row['mode']]?></td>
        </tr>
        <tr>
            <td colspan="3" style="font-size:12px;"><strong>Services Rendered</strong></td>
            <td style="font-size:12px;"><strong>Fee</strong></td>
            <td style="font-size:12px;"><strong>Discount</strong></td>
            <td style="font-size:12px;"><strong>Sub Total</strong></td> 
        </tr>
        <?php
            if(count($row['case']['services']) > 0){
                if(isset($row['user_id'])){
                    foreach($row['case']['services'] as $cs){
                        if($cs['who_will_pay'] == 0){
        ?>
                            <tr>
                                <td colspan="3" style="font-size:10px;"><?php echo $cs['service']['name']?></td>
                                <td style="font-size:10px;">$<?php echo $cs['price']?></td>
                                <td style="font-size:10px;color:red">$<?php echo $cs['discount']?></td>
                                <td style="font-size:10px;">$<?php echo $cs['sub_total']?></td>
                            </tr>
        <?php           }
                    }
                }
                else if(isset($row['patient_id'])){
                    foreach($row['case']['services'] as $cs){
                        if($cs['who_will_pay'] == 1){
        ?>
                            <tr>
                                <td colspan="3" style="font-size:10px;"><?php echo $cs['service']['name']?></td>
                                <td style="font-size:10px;">$<?php echo $cs['price']?></td>
                                <td style="font-size:10px;color:red">$<?php echo $cs['discount']?></td>
                                <td style="font-size:10px;">$<?php echo $cs['sub_total']?></td>
                                <td  style="font-size:10px;" colspan="2">Patient</td>   
                            </tr>
        <?php           }
                    }
                }
            }
        ?>
        <?php }}?>
    </tbody>
</table>
<div class="alert alert-dark" style="margin-top:20px;font-size:25px;background-color:#cccccc">
    Total Doctor A/R: $<?php echo $totalAr?>
</div>