<?php
    use yii\helpers\Html;
?>
<?php if(isset($location_name)){?>
<div class="alert alert-dark" style="padding:7px;margin-bottom:10px;font-size:20px;background-color:#cccccc">
    <?php echo $location_name;?> - Existing Patients
</div>
<?php }?>
<table class="table table-dark">
    <thead>
        <tr>
            <th style="border-top:0px">Name</th>
            <th style="border-top:0px">Birth Date</th>
            <th style="border-top:0px">Age</th>
            <th style="border-top:0px">Phone</th>
            <th style="border-top:0px">City</th>
            <th style="border-top:0px">State</th>
            <th style="border-top:0px">Sex</th>
            <th style="border-top:0px">Added On</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if($records){foreach($records as $r){
                $row = $r['patient'];
        ?>
        <tr>
            <td><?php echo Yii::$app->common->getFullName($row); ?></td>
            <td><?php echo date("M d, Y",strtotime($row['BirthDate']));?></td>
            <td><?php echo $row['Age']?></td>
            <td><?php echo $row['WorkPhone']?></td>
            <td><?php echo $row['City']?></td>
            <td><?php echo $row['State']?></td>
            <td><?php echo $row['Sex']?></td>
            <td><?php echo date("M d, Y",strtotime($row['added_on']));?></td>
        </tr>
        <?php }}?>
    </tbody>
</table>