<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;
use yii\helpers\Url;

AdminAsset::register($this);
$session = Yii::$app->session;
$current_page = $session->get('current_page');
$current_page_payments = $session->get('current_page_payments');
$current_subpage = $session->get('current_subpage');
$class_teacher = $session->get('class_teacher');
$session->set('current_subpage', '');
$session->set('current_page', '');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?=$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::$app->getHomeUrl().'favicon.ico']);?>
    <title><?= Html::encode($this->title) ?> :: <?=Yii::$app->name?> Admin</title>
    <script>
		var FULL_PATH = "<?=Url::base(true) ?>";
	</script>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">

<?php $this->beginBody() ?>
<!-- Site wrapper -->
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?=Yii::$app->params['siteUrl']?>"  class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>T</b>hub</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><?=Yii::$app->name?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" style="padding-top:8px;padding-bottom:10px">
                        <?php if(isset(Yii::$app->user->identity->image) && !empty(Yii::$app->user->identity->image) && file_exists(\Yii::getAlias('@webroot')."/web/site/images/user_image/".Yii::$app->user->identity->image)){ ?>
                        <img src="<?=Yii::$app->request->baseURL?>/web/site/images/user_image/<?=Yii::$app->user->identity->image?>" class="img-xs user-image" alt="User Image">
                        <?php } else { ?>
                        <i class="fa fa-user-circle" style="color:#FFFFFF;font-size:30px"></i> 
                        <?php } ?>
                        <span class="hidden-xs" style="float: right;margin-top: 6px;"> <?php if(isset(Yii::$app->user->identity->name)) echo ucwords(Yii::$app->user->identity->name)?></span>
                    </a>
                    <ul class="dropdown-menu animated zoomIn">
                        <!-- User image -->
                        <li class="user-header">
                        <?php if(isset(Yii::$app->user->identity->image) && !empty(Yii::$app->user->identity->image) && file_exists(\Yii::getAlias('@webroot')."/web/site/images/user_image/".Yii::$app->user->identity->image)){ ?>
                        <img src="<?=Yii::$app->request->baseURL?>/web/site/images/user_image/<?=Yii::$app->user->identity->image?>" class="img-circle" alt="User Image">
                        <?php } else { ?>
                            <i class="fa fa-user-circle" style="color:#FFFFFF;font-size:100px"></i>
                        <?php } ?>
                            <p>
                                <?php if(isset(Yii::$app->user->identity->name)) echo ucwords(Yii::$app->user->identity->name)?>                                    
                                <small><?php if(isset(Yii::$app->user->identity->last_login) && (Yii::$app->user->identity->last_login != null || Yii::$app->user->identity->last_login != "0000-00-00 00:00:00")) echo "Last Login: " . date('F j, Y h:i A', strtotime(Yii::$app->user->identity->last_login));?></small>
                            </p>
                        </li>
                        <!-- Menu Footer--> 
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?=Yii::$app->request->baseURL?>/admin/settings/profile" class="btn btn-default btn-flat">Update Profile</a>
                            </div>
                            <div class="pull-right">
                                <?php echo Html::beginForm(['/site/logout'], 'post'); ?>
                                <?php echo Html::submitButton( 'Sign out ',['class' => 'btn btn-default btn-flat']
                                );?>
                                <!-- <a href="<?=Yii::$app->request->baseURL?>/user/logout" class="btn btn-default btn-flat">Sign out</a> -->
                                <?php echo Html::endForm(); ?>
                            </div>
                            <!-- <div class="pull-right">
                                <a href="<?=Yii::$app->request->baseURL?>/site/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div> -->
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <a href="javascript:void(0);">
                <div class="user-panel">
                    <div class="pull-left image">
                        <i class="fa fa-user-circle" style="color:#FFFFFF;font-size:20px"></i>
                    </div>
                    <div class="pull-left info">
                        <p><?php if(isset(Yii::$app->user->identity->username)) echo ucwords(Yii::$app->user->identity->username)?></p>                    
                    </div>
                </div>
            </a>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
				<li class="header">MAIN NAVIGATION</li>
                <?php if(!empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id != 2){?>
                <li <?php if(Yii::$app->controller->id == "default") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li <?php if(Yii::$app->controller->id == "user" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/user"><i class="fa fa-user-circle"></i> Users</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "plans" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/plans"><i class="fa fa-id-card"></i> Memership Plans</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "event" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/event"><i class="fa fa-list"></i> Events</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "category" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/category"><i class="fa fa-list-alt"></i> Categories</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "email" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/email/templates"><i class="fa fa-envelope"></i> Email Templates</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "email" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/banking"><i class="fa fa-envelope"></i> Banking Inquiries</a>
                </li>
                <li <?php if(Yii::$app->controller->id == "payment" && Yii::$app->controller->action->id == "index") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/payment"><i class="fa fa-area-chart"></i> Payments</a>
                </li>
                <?php }?>
                <li <?php if(Yii::$app->controller->id == "blog") echo 'class="active"' ?> >
                    <a href="<?=Yii::$app->request->baseURL?>/admin/blog"><i class="fa fa-book"></i> Blogs</a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?= $content ?>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer hidden-print">
        <div class="pull-right">
            <p class="pull-left">
                <a target="_blank" href="<?=Yii::$app->params['siteUrl']?>/terms-and-conditions">Terms &amp; Conditions</a> | <a target="_blank" href="<?=Yii::$app->params['siteUrl']?>/privacy-policy">Privacy Policy</a>
            </p>
        </div>
        <strong>
            Copyright &copy; <?= date('Y') ?> <a href="<?=Yii::$app->params['siteUrl']?>">tiphub.co</a> All rights reserved.
        </strong>
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>