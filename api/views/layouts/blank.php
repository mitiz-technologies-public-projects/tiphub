<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php if(isset($this->params['transaction'])){?>
        <meta property="og:title" content="<?php echo $this->params['transaction']->event->title?>"/>
        <meta property="og:url" content="<?php echo Yii::$app->params['siteUrl']?>/<?php echo $this->params['transaction']->event->url?>/<?php echo $this->params['transaction']->event->id?>"/>
        <meta property="og:description" content="<?php echo strip_tags($this->params['transaction']->event->description)?>"/>
        <?php if(!is_null($this->params['transaction']->event->image)){?>
        <meta property="og:image" itemprop="image" content="<?php echo Yii::$app->params['apiUrl']?>/web/events/sharable-<?php echo $this->params['transaction']->event->id?>-<?php echo $this->params['transaction']->id?>.png" />
    <?php 
        }
    }?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
