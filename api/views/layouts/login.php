<?php
use yii\helpers\Html;
use app\assets\LoginAsset;
use yii\helpers\Url;
use app\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?=$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Yii::$app->getHomeUrl().'web/images/favicon.ico']);?>
    <title><?= Html::encode($this->title) ?></title>
    <script>var FULL_PATH = "<?=Url::base(true) ?>";</script>
    <?php $this->head() ?>
</head>
<body class="hold-transition login-page">
    <?php $this->beginBody() ?>
    <div class="login-box">
        <div class="login-logo">
            <a href="<?=Yii::$app->params['siteUrl']?>">TIPHUB</a>
            <!-- <img src="https://tiphub.co/assets/tiphub-app.png" style="width: 100px;" class=""> -->
        </div>
        <!-- /.login-logo -->

        <?php echo $content?>

    </div>
    <!-- /.login-box -->
    <?php $this->endBody()?>
</body>
</html>
<?php $this->endPage() ?>
