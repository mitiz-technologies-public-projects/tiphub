<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\Response;
use xstreamka\mobiledetect\Device;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\EventTickets;
use Carbon\Carbon;
use app\models\MembershipDueForms;
use app\models\Transactions;
use app\models\Members;
use yii\helpers\ArrayHelper;

class BlogController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
        //$_GET['fields'] = json_decode($_GET['fields'], true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => [
                    'index',
                    'get-detail',
                    'get-featured'
                ],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        /* $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'except' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        return true;
                    },
                ],
            ],
        ]; */
        return $behaviors;
    }

    public function actionIndex($pageSize = 6)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Blog;
        $query = $model->find();

        $search = new \app\models\SearchForm;
        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->title)) {
                    $query->where(['LIKE', 'title', $search->title])->orWhere(new \yii\db\Expression('FIND_IN_SET("' . $search->title . '", tag)'));
                }
            }
        }
        

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        // $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'records' => $find,
            'pages' => $pages,
        ];
    }
    
    public function actionGetDetail($id)
    {
         
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Blog;
                $find = $model->find()->where(['id' => $id])->one();
                if ($find) {
                    return [
                        'success' => true,
                        'record' => $find,
                    ];
                }
                else{
                    return [
                        'error' => true,
                        'message' => 'Blog not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Blog not found!',
            ];
        }
    }

    public function actionGetFeatured($pageSize = 5)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'target' => [
                    'asc' => ['target' => SORT_ASC],
                    'desc' => ['target' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Blog;
        $query = $model->find();

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        if ($find) {
            $newFind = [];
            foreach ($find as $key => $val) {
                $newFind[$key] = $val;
                $newFind[$key]['description'] = strip_tags($val['description']);
            }
            $response = [
                'success' => true,
                'blogs' => $newFind,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'blogs' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }
}
