<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\Response;
use xstreamka\mobiledetect\Device;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class UserController extends Controller
{

    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
        //$_GET['fields'] = json_decode($_GET['fields'], true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => ['login', 'forgot-password', 'check-password-reset-token', 'reset-password', 'social-login', 'get', 'signup', 'track-app', 'update-username', 'get-featured', 'get-user-by-access-token'],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    //QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'except' => ['login', 'forgot-password', 'check-password-reset-token', 'reset-password', 'social-login', 'get', 'signup'],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        //return Yii::$app->common->checkPermission('Users', $action->id);
                        return true;
                    },
                ],
            ],
        ];
        return $behaviors;
    }

    //Label~Module~Action~Url~Icon:Social Login~Users~social-login~users~users
    public function actionSocialLogin()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                if ($model->load($POST)) {
                    if (isset($model->facebook_id) || isset($model->google_id) || isset($model->twitter_id)) {
                        if (isset($model->image) && !empty($model->image)) {
                            ini_set('allow_url_fopen', 1);
                            if (isset($model->twitter_id) && !empty($model->twitter_id)) {
                                $content = file_get_contents(str_replace("_normal", "", $model->image));
                            } else {
                                $content = file_get_contents($model->image);
                            }
                            $file_name = time() . ".jpg";
                            file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $file_name, $content);
                            $POST['User']['image'] = $file_name;
                        }
                        if (isset($model->google_id) && !empty($model->google_id)) {
                            $find = $model->find()->where(['google_id' => $model->google_id])->orWhere(['email' => $model->email])->one();
                            if (!$find) {
                                $find = $model->find()->where(['id' => \app\models\UserEmails::find()->select('user_id')->where(['email' => $model->email])])->one();
                                if ($find) {
                                    if (empty($find->access_token)) {
                                        $find->access_token = Yii::$app->security->generateRandomString();
                                    }
                                    if ($find->save()) {
                                        return [
                                            'success' => true,
                                            'user_id' => $find->id,
                                            'username' => $find->username,
                                            'email' => $find->email,
                                            'name' => $find->name,
                                            'user_type' => $find->role_id,
                                            'token' => $find->access_token,
                                            'image' => $find->image,
                                            'description' => $find->description,
                                            'message' => 'User has been updated successfully.'
                                        ];
                                    }
                                }
                            }
                        } else if (isset($model->facebook_id) && !empty($model->facebook_id)) {
                            $find = $model->find()->where(['facebook_id' => $model->facebook_id])->orWhere(['email' => $model->email])->one();
                            if (!$find) {
                                $find = $model->find()->where(['id' => \app\models\UserEmails::find()->select('user_id')->where(['email' => $model->email])])->one();
                                if ($find) {
                                    if (empty($find->access_token)) {
                                        $find->access_token = Yii::$app->security->generateRandomString();
                                    }
                                    if ($find->save()) {
                                        return [
                                            'success' => true,
                                            'user_id' => $find->id,
                                            'username' => $find->username,
                                            'email' => $find->email,
                                            'name' => $find->name,
                                            'user_type' => $find->role_id,
                                            'token' => $find->access_token,
                                            'image' => $find->image,
                                            'description' => $find->description,
                                            'message' => 'User has been updated successfully.'
                                        ];
                                    }
                                }
                            }
                        } else if (isset($model->twitter_id) && !empty($model->twitter_id)) {
                            $find = $model->find()->where(['twitter_id' => $model->twitter_id])->orWhere(['email' => $model->email])->one();
                            if (!$find) {
                                $find = $model->find()->where(['id' => \app\models\UserEmails::find()->select('user_id')->where(['email' => $model->email])])->one();
                                if ($find) {
                                    if (empty($find->access_token)) {
                                        $find->access_token = Yii::$app->security->generateRandomString();
                                    }
                                    if ($find->save()) {
                                        return [
                                            'success' => true,
                                            'user_id' => $find->id,
                                            'username' => $find->username,
                                            'email' => $find->email,
                                            'name' => $find->name,
                                            'user_type' => $find->role_id,
                                            'token' => $find->access_token,
                                            'image' => $find->image,
                                            'description' => $find->description,
                                            'message' => 'User has been updated successfully.'
                                        ];
                                    }
                                }
                            }
                        }
                        //echo $find->createCommand()->getRawSql();die;
                        //print_r($find);die;
                        if ($find) {
                            $find->status = 'Y';
                            if (empty($find->access_token)) {
                                $find->access_token = Yii::$app->security->generateRandomString();
                            }
                            if ($find->profile_updated == 1) {
                                if (isset($model->description) && ($find->description == null || $find->description == '')) {
                                    $find->description = $model->description;
                                }
                            }
                            if ($find->save()) {
                                return [
                                    'success' => true,
                                    'user_id' => $find->id,
                                    'username' => $find->username,
                                    'email' => $find->email,
                                    'name' => $find->name,
                                    'user_type' => $find->role_id,
                                    'token' => $find->access_token,
                                    'image' => $find->image,
                                    'description' => $find->description,
                                    'message' => 'User has been updated successfully.'
                                ];
                            } else {
                                Yii::debug('Signup update-' . $_POST);
                                return [
                                    'error' => true,
                                    'message' => $find->getErrors(),
                                    'userDetails' => $find
                                ];
                            }
                        } else {
                            $model->load($POST);
                            $model->access_token = Yii::$app->security->generateRandomString();
                            $model->status = 'Y';
                            if ($model->save()) {
                                if (isset($model->twitter_id)) {
                                    $smodel = new \app\models\SocialLinks;
                                    $smodel->plateform = "twitter";
                                    $smodel->user_id = $model->id;
                                    $smodel->url = "https://twitter.com/" . $model->username;
                                    if ($smodel->validate()) {
                                        $smodel->save();
                                    }
                                }
                                if (isset($model->google_id)) {
                                    $email = explode("@", $model->email);
                                    $model->google_username = $email[0];
                                    $model->username = $email[0];
                                    $model->save();
                                }
                                if (isset($model->facebook_id)) {
                                    $email = explode("@", $model->email);
                                    $model->facebook_username = $email[0];
                                    $model->username = $email[0];
                                    $model->save();
                                }

                                return [
                                    'success' => true,
                                    'user_id' => $model->id,
                                    'username' => $model->username,
                                    'email' => $model->email,
                                    'name' => $model->name,
                                    'user_type' => $model->role_id,
                                    'token' => $model->access_token,
                                    'image' => $model->image,
                                    'description' => $model->description,
                                    'message' => 'User has been updated successfully.'
                                ];
                            } else {
                                Yii::debug('Signup insert-' . $_POST);
                                return [
                                    'error' => true,
                                    'message' => $model->getErrors()
                                ];
                            }
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => "There is some problem with the authentication!"
                        ];
                    }

                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    //Label~Module~Action~Url~Icon:Update Description~Users~update-description~users~users
    public function actionUpdateDescription()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                if ($model->load($POST)) {
                    $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->one();
                    if ($find && $find->load($POST)) {
                        if ($find->save()) {
                            return [
                                'success' => true,
                                'description' => $find->description,
                                'message' => 'Description updated successfully.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }

                    } else {
                        return [
                            'error' => true,
                            'message' => "User not found.",
                        ];
                    }
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    //Label~Module~Action~Url~Icon:Add/Edit User~Users~add~users~users
    public function actionAdd()
    {
        if (isset($_POST) && !empty($_POST)) {
            $scenario = [];
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                $POST['User']['country_id'] = isset($_POST['fields']['country_id']['value']) ? $_POST['fields']['country_id']['value'] : NULL;
                $POST['User']['state_id'] = isset($_POST['fields']['state_id']['value']) ? $_POST['fields']['state_id']['value'] : NULL;

                if ($model->load($POST)) {
                    if (isset($model->id)) {
                        $find = $model->find()->where(['id' => $model->id])->one();
                        if ($find && $find->load($POST)) {
                            if ($find->save()) {
                                if (isset($POST['User']['password']) && !empty($POST['User']['password'])) {
                                    $find->password_hash = Yii::$app->security->generatePasswordHash($POST['User']['password']);
                                    $find->save();
                                }
                                return [
                                    'success' => true,
                                    'message' => 'User has been updated successfully.',
                                ];
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $find->getErrors(),
                                ];
                            }

                        } else {
                            return [
                                'error' => true,
                                'message' => "User not found.",
                            ];
                        }
                    } else if ($model->validate()) {
                        if (isset($model->password) && !empty($model->password)) {
                            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                        }

                        if ($model->save()) {
                            return [
                                'success' => true,
                                'message' => 'User has been added successfully.',
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }

                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    //Label~Module~Action~Url~Icon:List User~Users~list~users~users
    public function actionList($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'username' => [
                    'asc' => ['username' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\User;
        $query = $model->find()->where(['deleted' => 'N'])->joinWith(["role", "location"]);

        $search = new \app\models\SearchForm;
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        if ($search->load($GET)) {
            if (!empty($search->name)) {
                $query->andWhere(['LIKE', 'users.name', $search->name]);
            }
            if (!empty($search->email)) {
                $query->andWhere(['LIKE', 'users.email', $search->email]);
            }
            if (!empty($search->username)) {
                //echo $search->username;die;
                $query->andWhere(['LIKE', 'users.username', $search->username]);
            }
            if (!empty($search->phone)) {
                $query->andWhere(['LIKE', 'users.phone', preg_replace('/[^\p{L}\p{N}]/', '', $search->phone)]);
            }
            if (!empty($search->location_id)) {
                $query->andWhere(new \yii\db\Expression('FIND_IN_SET(' . $search->location_id . ',users.locations)'));
            }
            if (!empty($search->role_id)) {
                $query->andWhere(['=', 'role_id', $search->role_id]);
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'users' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'users' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Get User Details~Users~get~users~users
    public function actionGet($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\User;
                $query = $model->find()->select(Yii::$app->params['user_table_select_columns'])->joinWith([
                    'services' => function ($q) {
                        $q->orderby('sequence');
                    },
                    'services.appname',
                    'qrcodes',
                    'sociallinks'
                ])->where(['users.username' => $id]);
                if (is_int($id)) {
                    $query->orWhere(['users.id' => $id]);
                }
                $find = $query->asArray()->one();
                if ($find) {
                    $dmodel = new \app\models\ProfileVisitors;
                    $fdmodel = $dmodel->find()->where(['user_id' => $find['id'], 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => date('Y-m-d')])->one();
                    //echo $fdmodel->createCommand()->getRawSql();die;
                    if (!$fdmodel) {
                        $dmodel->user_id = $find['id'];
                        $dmodel->ip_address = Yii::$app->request->userIP;
                        $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                        $dmodel->save();
                    }
                    $file_name = "user-" . $find['id'];
                    $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/' . $find['username']))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                    $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png');
                    return [
                        'success' => true,
                        'user' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'User not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'User not found!',
            ];
        }
    }

    public function actionGetWithLoggedIn($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\User;
                $query = $model->find()->select(Yii::$app->params['user_table_select_columns'])->joinWith([
                    'services' => function ($q) {
                        $q->orderby('sequence');
                    },
                    'services.appname',
                    'qrcodes',
                    'sociallinks'
                ])->where(['users.username' => $id]);
                if (is_int($id)) {
                    $query->orWhere(['users.id' => $id]);
                }
                $find = $query->asArray()->one();
                if ($find) {
                    $dmodel = new \app\models\ProfileVisitors;
                    $fdmodel = $dmodel->find()->where(['user_id' => $find['id'], 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => date('Y-m-d')])->one();
                    //echo $fdmodel->createCommand()->getRawSql();die;
                    if (!$fdmodel) {
                        $dmodel->user_id = $find['id'];
                        $dmodel->ip_address = Yii::$app->request->userIP;
                        $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                        $dmodel->save();
                    }
                    $file_name = "user-" . $find['id'];
                    $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/' . $find['username']))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                    $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png');
                    return [
                        'success' => true,
                        'user' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'User not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'User not found!',
            ];
        }
    }

    public function actionGetUserByAccessToken($token = "")
    {
        $model = new \app\models\User;
        $find = $model->find()->where(['access_token' => $token])->one();
        if ($find) {
            return [
                'success' => true,
                'token' => $find->access_token,
                'userType' => $find->role_id,
                'userName' => $find->username,
                'uName' => $find->name,
                'userId' => $find->id,
                'userImage' => $find->image,
            ];
        } else {
            return [
                'error' => true,
                'message' => 'User does not exist.'
            ];
        }
    }
    public function actionConfirmEmail($code)
    {
        if (isset($code) && !empty($code)) {
            try {
                $model = new \app\models\User;
                $find = $model->find()->where(['verification_code' => $code])->one();
                if ($find) {
                    if ($find->status == 'N') {
                        $find->status = 'Y';
                        $find->save();
                        return [
                            'success' => true,
                            'message' => 'Email verified successfully. Please login to continue.',
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Error: Email address already verified!',
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Error: Invalid verification code!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid verification code!',
            ];
        }
    }

    public function actionDelete()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\User;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $find->deleted = 'Y';
                    if ($find->update()) {
                        return [
                            'success' => true,
                            'message' => 'User has been deleted successfully.',
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors(),
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'User not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'User not found!',
            ];
        }
    }

    public function actionChangePassword()
    {
        if (isset($_POST) && !empty($_POST)) {
            $model = new \app\models\ChangePasswordForm;
            if (isset($_POST['current_password']) && !empty($_POST['current_password'])) {
                $model->current_password = $_POST['current_password'];
            }
            if (isset($_POST['new_password']) && !empty($_POST['new_password'])) {
                $model->new_password = $_POST['new_password'];
            }
            if (isset($_POST['confirm_password']) && !empty($_POST['confirm_password'])) {
                $model->confirm_password = $_POST['confirm_password'];
            }

            if ($model->new_password == $model->current_password) {
                return [
                    'error' => true,
                    'message' => ['password' => ['New password must be different form your current password.']],
                ];
            }
            if ($model->checkPassword(Yii::$app->user->identity->username)) {
                $user = new \app\models\User;
                $find = $user->findOne(['username' => Yii::$app->user->identity->username]);
                try {
                    if ($find) {
                        $find->password = $model->new_password;
                        $find->password_hash = Yii::$app->security->generatePasswordHash($model->new_password);
                        if ($find->save()) {
                            return [
                                'success' => true,
                                'message' => 'Your password has been updated successfully.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }

                    } else {
                        return [
                            'error' => true,
                            'message' => 'User does not exist.',
                        ];
                    }
                } catch (\Exception $e) {
                    return [
                        'error' => true,
                        'message' => Yii::$app->common->returnException($e),
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => $model->getErrors(),
                ];
            }
        }

    }

    public function actionResetPassword()
    {
        if (isset($_POST) && !empty($_POST)) {
            $model = new \app\models\User;
            if (isset($_POST['token']) && !empty($_POST['token'])) {
                $token = $_POST['token'];
            }
            if (empty($_POST['new_password'])) {
                return [
                    'error' => true,
                    'message' => ['new_password' => 'Password can not be blank.'],
                ];
            } else if ($_POST['new_password'] != $_POST['confirm_password']) {
                return [
                    'error' => true,
                    'message' => ['confirm_password' => 'Confirm password did not match with your new password.'],
                ];
            }
            $find = $model->find()->where(['password_reset_token' => $token])->one();
            try {
                if ($find) {
                    $find->password = $_POST['new_password'];
                    $find->password_hash = Yii::$app->security->generatePasswordHash($_POST['new_password']);
                    if ($find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Your password has been updated successfully. Please login to access your account.',
                        ];
                    } else {
                        $message = $find->getErrors();
                        return [
                            'error' => true,
                            'message' => ['new_password' => $message['password']]
                        ];
                    }

                } else {
                    return [
                        'error' => true,
                        'message' => 'Password reset URL does not seem to be valid.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }

    }

    public function actionLogin()
    {
        //echo Yii::$app->security->generatePasswordHash("admin");die;
        $data = $reponnse = [];
        $model = new \app\models\LoginForm;
        if (isset($_POST['username']) && !empty($_POST['username'])) {
            $model->username = $_POST['username'];
        }
        if (isset($_POST['password']) && !empty($_POST['password'])) {
            $model->password = $_POST['password'];
        }
        //echo Yii::$app->security->generatePasswordHash("admin");die;
        if ($model->login()) {
            $user = new \app\models\User;
            $find = $user->findOne(['id' => Yii::$app->user->getId(), 'status' => \app\models\User::STATUS_ACTIVE]);
            try {
                /* $msg = print_r($_POST, true);
                mail('opnsrc.devlpr@gmail.com', 'Login Success', $msg); */
                if ($find) {
                    if (empty($find->access_token)) {
                        $find->access_token = Yii::$app->security->generateRandomString();
                    }
                    if ($find->save()) {
                        $file_name = "user-" . $find->id;
                        if (!file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png')) {
                            $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/' . $find->username))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                            $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png');
                        }

                        $response = [
                            'success' => true,
                            'user_id' => $find->id,
                            'username' => $find->username,
                            'name' => $find->name,
                            'email' => $find->email,
                            'user_type' => $find->role_id,
                            'token' => $find->access_token,
                            'image' => $find->image,
                            'description' => $find->description,
                        ];
                    } else {
                        $response['response'] = [
                            'status' => 'error',
                            'message' => $find->getErrors(),
                        ];
                    }

                } else {
                    $response = [
                        'error' => 'Sorry! You are not authorized to login.',
                    ];
                }
            } catch (\Exception $e) {
                $response['response'] = [
                    'status' => 'error',
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            $response = [
                'error' => "Incorrect username or password.",
            ];
        }
        return $response;
    }

    public function actionCheckUsername($username)
    {
        if (isset($username) && !empty($username)) {
            $model = new \app\models\User;
            $find = $model->find()->where(['username' => $username])->count();
            if ($find) {
                return [
                    'success' => true,
                ];
            } else {
                return [
                    'error' => true,
                ];
            }
        }
    }

    public function actionForgotPassword()
    {
        //mail("opnsrc.devlpr@gmail.com","My subject","Hello");die;
        //mail("amrender.kumar1986@gmail.com","My subject","Hello");die;
        $data = $response = [];
        $username = $_POST['username'];
        if (isset($username) && !empty($username)) {
            $model = new \app\models\User;
            $find = $model->find()->where(['email' => $username])->orWhere(['username' => $username])->one();
            if ($find) {
                $token = sha1(uniqid($find->email, true));
                $find->password_reset_token = $token;
                if ($find->save()) {
                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 4])->one();
                    $data = [
                        'case' => 1,
                        'template_id' => 4,
                        'name' => isset($find->name) ? $find->name : $find->username,
                        'url' => Yii::$app->params['siteUrl'] . "/reset-password/" . $token,
                        'ip' => Yii::$app->request->userIP,
                        'templateDetails' => $templateDetails
                    ];
                    $compose = Yii::$app->mailer->compose('body', $data);
                    $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                    $compose->setTo($find->email);
                    $compose->setSubject($templateDetails->subject);
                    if ($compose->send()) {
                        $response = [
                            'success' => true,
                            'message' => 'We have sent an email on your registered email address, please click on the given link to reset your password.',
                        ];
                    }
                } else {
                    $response = [
                        'error' => true,
                        'message' => $find->getErrors(),
                    ];
                }
            } else {
                $response = [
                    'error' => true,
                    'message' => "There is no user registered with this email or username.",
                ];
            }
        } else {
            $response = [
                'error' => true,
                'message' => "There is no user registered with this email.",
            ];
        }
        return $response;
    }

    public function actionCheckPasswordResetToken()
    {
        $data = $response = [];
        $token = $_POST['token'];

        if (isset($token) && !empty($token)) {
            $find = \app\models\User::find()->where(['password_reset_token' => $token])->one();
            if ($find) {
                $response = [
                    'success' => true,
                ];
            } else {
                $response = [
                    'error' => true,
                    'message' => "Password reset url does not seem to be valid.",
                ];
            }
        } else {
            $response = [
                'error' => true,
                'message' => "Password reset url does not seem to be valid.",
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Delete Document~Users~delete-document~users~users
    public function actionDeleteDocument()
    {
        if (isset($_POST['file_name']) && !empty($_POST['file_name'])) {
            try {
                if (strpos($_POST['file_name'], "temp") != false) {
                    unlink($_SERVER['DOCUMENT_ROOT'] . "/documents/temp/" . $_POST['file_name']);
                    return [
                        'success' => true,
                        'message' => 'Document has been deleted successfully.',
                    ];
                } else {
                    $model = new \app\models\Documents;
                    $find = $model->find()->where(['document_name' => $_POST['file_name']])->one();
                    if ($find && $find->delete()) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . "/documents/" . $_POST['file_name']);
                        return [
                            'success' => true,
                            'message' => 'Document has been deleted successfully.',
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionDeleteQrCode()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Qrcodes;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();

                if ($find) {
                    $file_name = $find->image;
                    if ($find->delete()) {
                        if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name)) {
                            @unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name);
                        }
                        return [
                            'success' => true,
                            'message' => 'Qrcode deleted successfully.',
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Qrcode not found!',
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }

        } else {
            return [
                'error' => true,
                'message' => 'Service not found!',
            ];

        }
    }

    //Label~Module~Action~Url~Icon:Add/Edit User~Users~add~users~users
    public function actionAddSocialLink()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\SocialLinks;
                $POST['SocialLinks'] = $_POST['fields'];
                $POST['SocialLinks']['user_id'] = Yii::$app->user->identity->id;
                $POST['SocialLinks']['url'] = $POST['SocialLinks']['url'] . $POST['SocialLinks']['username'];
                if ($model->load($POST) && $model->save()) {
                    return [
                        'success' => true,
                        'message' => 'Link added successfully.',
                        'links' => \app\models\SocialLinks::find()->where(['user_id' => Yii::$app->user->identity->id])->asArray()->all()
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionDeleteLink()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\SocialLinks;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();

                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Link deleted successfully.',
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Link not found!',
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }

        } else {
            return [
                'error' => true,
                'message' => 'Service not found!',
            ];

        }
    }

    public function actionLogout()
    {
        $model = new \app\models\User;
        $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->one();
        if ($find) {
            $find->access_token = '';
            if ($find->save()) {
                return [
                    'success' => true,
                ];
            }
        }
    }

    public function actionDeleteAccount()
    {
        if (isset($_POST['delete'])) {
            $model = new \app\models\User;
            $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->one();
            if ($find) {
                \app\models\SocialLinks::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\EventApps::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\PaymentServiceNames::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\Transactions::deleteAll(['added_by' => Yii::$app->user->identity->id]);
                \app\models\Services::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\Events::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\UserEmails::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\EventGallery::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                \app\models\EventQrCodes::deleteAll(['user_id' => Yii::$app->user->identity->id]);
                if ($find->delete()) {
                    return [
                        'success' => true,
                    ];
                }
            }
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function actionGetLoggedInUser()
    {
        try {
            $model = new \app\models\User;
            $find = $model->find()->select(Yii::$app->params['user_table_select_columns'])->where(['id' => Yii::$app->user->identity->id])->asArray()->one();
            if ($find) {
                return [
                    'success' => true,
                    'user' => $find,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'User not found!',
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionUploadProfileImage()
    {
        $response = [];
        if (isset($_FILES) && !empty($_FILES['image'])) {
            try {
                include "vendor/Upload.php";
                $handle = new \Upload($_FILES['image']);
                if ($handle->uploaded) {
                    $file_name = mt_rand(111111, 999999) . "-temp-";
                    $handle->file_new_name_body = $file_name;
                    $handle->allowed = ['image/*'];
                    $handle->file_overwrite = true;
                    if ($handle->image_src_x > 2000) {
                        $handle->image_resize = true;
                        $handle->image_x = 1024;
                        $handle->image_ratio_y = true;
                    }
                    $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/temp");
                    if ($handle->processed) {
                        $response = [
                            'success' => true,
                            'file' => [
                                'file_name_original' => $handle->file_src_name,
                                'file_name' => $handle->file_dst_name,
                                'extension' => $handle->file_src_name_ext,
                            ],
                        ];
                        $handle->clean();
                    } else {
                        Yii::$app->response->statusCode = 415;
                        $response = [
                            'type' => 'error',
                            'message' => $handle->error,
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionUpdateProfile()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->one();
                if ($find->load($POST)) {
                    $find->profile_updated = 1;
                    if (isset($POST['User']['file'])) {
                        $new_file_name = mt_rand(111111, 999999) . "." . $POST['User']['file']['extension'];
                        copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/temp/" . $POST['User']['file']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $new_file_name);
                        $find->image = $new_file_name;
                        unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/temp/" . $POST['User']['file']['file_name']);
                    }
                    if ($find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Profile has been updated successfully.',
                            'username' => $find->username,
                            'name' => $find->name,
                            'image' => $find->image
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors(),
                        ];
                    }
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionSignup()
    {
        if (isset($_POST) && !empty($_POST)) {
            $scenario = [];
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                if ($model->load($POST) && $model->validate()) {
                    $model->status = 'Y';
                    if (isset($model->password) && !empty($model->password)) {
                        $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                        $model->access_token = Yii::$app->security->generateRandomString();
                    }
                    if ($model->save()) {

                        $file_name = "user-" . $model->id;
                        $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/' . $model->username))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                        $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png');
                        $redirect = 0;
                        if (isset($_POST['beneficiaries']) && !empty($_POST['beneficiaries'])) {
                            \app\models\BeneficiaryDetails::updateAll(['user_id' => $model->id], ['id' => $_POST['beneficiaries']]);
                            $redirect = 1;
                        }

                        // Attach the user membership details to the related table
                        if (isset($POST['User']['plan']) && !empty($POST['User']['plan'])) {
                            $plan_id = $POST['User']['plan'];

                            // Gettign plan data
                            $plans = new \app\models\Plans;
                            $plan = $plans->find()->where(['id' => $plan_id])->one();
                            //print_r($plan);
                            if ($plan) {
                                $modelMembership = new \app\models\UsersMembership;
                                $modelMembership->user_id = $model->id;
                                $modelMembership->plan_id = $plan_id;

                                if ($plan->trial_period > 0 && $plan->is_free == 0) { // If it's a paid plan, assign trial period
                                    $trial_period = $plan->trial_period;
                                    $trial_starts_date = date('Y-m-d H:i:s');

                                    $trial_ends_on = (new \DateTime($trial_starts_date))->add(new \DateInterval("P" . $trial_period . "D"))->format('Y-m-d H:i:s');

                                    $modelMembership->trial_period_starts = $trial_starts_date;
                                    $modelMembership->trial_period_ends = $trial_ends_on;
                                    $modelMembership->plan_price = $plan->monthly_price;
                                    $modelMembership->plan_cycle = $plan->plan_cycle;

                                } else if ($plan->is_free == 1) { // If it's a free plan
                                    $plan_starts_on = date('Y-m-d H:i:s');
                                    $modelMembership->plan_price = 0;
                                }

                                $modelMembership->save();
                            }


                        }

                        return [
                            'success' => true,
                            'user_id' => $model->id,
                            'username' => $model->username,
                            'email' => $model->email,
                            'name' => $model->name,
                            'user_type' => $model->role_id,
                            'token' => $model->access_token,
                            'image' => $model->image,
                            'description' => $model->description,
                            'redirect' => $redirect
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionCheckSocialUser()
    {
        try {
            $model = new \app\models\User;
            $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->andWhere(['is', 'password_hash', NULL])->one();
            if ($find) {
                return [
                    'error' => true,
                ];
            } else {
                return [
                    'success' => true
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionPayments($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['payments.id' => SORT_ASC],
                    'desc' => ['payments.id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'txn_amount' => [
                    'asc' => ['txn_amount' => SORT_ASC],
                    'desc' => ['txn_amount' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'received_at' => [
                    'asc' => ['received_at' => SORT_ASC],
                    'desc' => ['received_at' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        /* $model = new \app\models\Beneficials;
        $find = $model->findOne(['user_id'=>Yii::$app->user->identity->id]);
        if($find && isset($find->dwolla_customer_url) && !empty($find->dwolla_customer_url)){
            $transactions = Yii::$app->common->getDwollaTransactions($find->dwolla_customer_url);
            if(!empty($transactions)){
                foreach($transactions as $transaction){
                    $model = new \app\models\Payments;
                    $find = $model->findOne(['payment_id'=>$transaction['id']]);
                    if(!$find){
                        $model->payment_identifier = $transaction['individualAchId'];
                        $model->payment_service = 64;
                        $model->email = $transaction['customer']['email'];
                        $model->name = $transaction['customer']['first_name'] ." ". $transaction['customer']['last_name'];
                        $model->received_at = date("Y-m-d H:i:s", strtotime($transaction['created']));
                        $model->txn_amount = $transaction['amount']['value'];
                        $model->payment_id = $transaction['id'];
                        $model->dwolla = 1;
                        $model->user_id = Yii::$app->user->identity->id;
                        $model->data_dump = json_encode($transaction);
                        $model->type = 1;
                        $model->save();
                    }
                }
            }
        } */

        $model = new \app\models\Payments;
        $query = $model->find()->joinWith(['service', 'event', 'transaction.member'])->where(['type' => 1]);
        $query = $query->andWhere([
            'OR',
            ['payments.user_id' => Yii::$app->user->identity->id],
            ['payments.event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])],
        ]);

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->date_range)) {
                    $date_range = explode(' to ', $search->date_range);
                    $fromArr = explode('-', $date_range[0]);
                    $toArr = explode('-', $date_range[1]);
                    $from = $fromArr[2] . '-' . $fromArr[0] . '-' . $fromArr[1];
                    $to = $toArr[2] . '-' . $toArr[0] . '-' . $toArr[1];

                    $query->andWhere([
                        'AND',
                        ['>=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $from],
                        ['<=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $to],
                    ]);
                }
                if (!empty($search->payment_identifier)) {
                    $query->andWhere(['LIKE', 'payment_identifier', $search->payment_identifier]);
                }
                if (!empty($search->name)) {
                    $query->andWhere(['LIKE', 'payments.name', $search->name]);
                }
                if (!empty($search->amount)) {
                    $query->andWhere(['payments.txn_amount' => $search->amount]);
                }
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $response = [
            'success' => true,
            'payments' => $find,
            'pages' => $pages,
        ];
        return $response;
    }

    public function actionGetEmails($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\UserEmails;
        $query = $model->find()->where(['user_id' => Yii::$app->user->identity->id, 'status' => 1]);

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $response = [
            'success' => true,
            'emails' => $find,
            'pages' => $pages,
        ];
        return $response;
    }

    public function actionLinkEmail()
    {
        if (isset($_POST) && !empty($_POST)) {
            $scenario = [];
            try {
                $model = new \app\models\UserEmails(['scenario' => 'Link']);
                $POST['UserEmails'] = $_POST['fields'];
                if ($model->load($POST)) {
                    $find = \app\models\User::find()->where(['email' => $model->email])->one();
                    if ($find) {
                        return [
                            'error' => true,
                            'message' => ["email" => "Email address has already a separate TipHub account."],
                        ];
                    }
                    //$find = $model->find()->where(['user_id'=>Yii::$app->user->identity->id,'email'=>$model->email])->one();
                    $find = $model->find()->where(['email' => $model->email])->one();
                    if ($find) {
                        if ($find->status === 1) {
                            return [
                                'error' => true,
                                'message' => ["email" => "Email address has already been linked."],
                            ];
                        } else {
                            $find->verification_code = (string) rand(111111, 999999);
                            $find->scenario = 'Link';
                            if ($find->save()) {
                                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 11])->one();
                                $data = [
                                    'case' => 2,
                                    'name' => isset(Yii::$app->user->identity->name) ? Yii::$app->user->identity->name : Yii::$app->user->identity->username,
                                    'verification_code' => $find->verification_code,
                                    'templateDetails' => $templateDetails
                                ];
                                $compose = Yii::$app->mailer->compose('body', $data);
                                $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                $compose->setTo($find->email);
                                //$compose->setTo('opnsrc.devlpr@gmail.com');
                                $compose->setSubject($templateDetails->subject);
                                if ($compose->send()) {
                                    return [
                                        'success' => true,
                                        'message' => 'We have sent a verification code. Please confirm to link your email.',
                                    ];
                                }
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $find->getErrors(),
                                ];
                            }
                        }
                    } else {
                        $model->user_id = Yii::$app->user->identity->id;
                        $model->verification_code = (string) rand(111111, 999999);
                        if ($model->save()) {
                            $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 11])->one();
                            $data = [
                                'case' => 2,
                                'name' => isset(Yii::$app->user->identity->name) ? Yii::$app->user->identity->name : Yii::$app->user->identity->username,
                                'verification_code' => $find->verification_code,
                                'templateDetails' => $templateDetails
                            ];
                            $compose = Yii::$app->mailer->compose('body', $data);
                            $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                            $compose->setTo($find->email);
                            //$compose->setTo('opnsrc.devlpr@gmail.com');
                            $compose->setSubject($templateDetails->subject);
                            if ($compose->send()) {
                                return [
                                    'success' => true,
                                    'message' => 'We have sent a verification code. Please confirm to link your email.',
                                ];
                            }
                        } else {
                            return [
                                'error' => true,
                                'message' => $model->getErrors(),
                            ];
                        }
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionConfirmCode()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\UserEmails;
                $POST['UserEmails'] = $_POST['fields'];
                if ($model->load($POST)) {
                    $find = $model->find()->where(['user_id' => Yii::$app->user->identity->id, 'verification_code' => $model->verification_code])->one();
                    if ($find) {
                        $find->status = 1;
                        $find->scenario = 'Confirm';
                        if ($find->save()) {
                            return [
                                'success' => true,
                                'message' => 'Your email has been successfully linked.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => ["verification_code" => "Invalid verification code"],
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionTrackApp($user_id, $service_id)
    {
        if (isset($user_id) && !empty($user_id) && isset($service_id) && !empty($service_id)) {
            try {
                $dmodel = new \app\models\UserAppVisitors;
                $fdmodel = $dmodel->find()->where(['user_id' => $user_id, 'service_id' => $service_id, 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => '%Y-%m-%d'])->one();
                if (!$fdmodel) {
                    $dmodel->user_id = $user_id;
                    $dmodel->service_id = $service_id;
                    $dmodel->ip_address = Yii::$app->request->userIP;
                    $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                    $dmodel->save();
                }
                return [
                    'success' => true,
                ];
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }

    }

    public function actionReports()
    {
        $user_id = Yii::$app->user->identity->id;
        if (isset($_POST) && !empty($_POST)) {
            $totalVisitoryQuery = \app\models\ProfileVisitors::find()->where(['user_id' => $user_id]);
            $fromDeviceQuery = \app\models\ProfileVisitors::find()->select(['count(*) as totalVisitors', 'device'])->where(['user_id' => $user_id])->groupBy('device');
            $tippingAppQuery = \app\models\UserAppVisitors::find()->select(['count(*) as totalVisitors', 'service_id'])->joinWith(['servicedetails'])->where(['user_app_visitors.user_id' => $user_id])->groupBy('service_id');
            $trafficQuery = \app\models\ProfileVisitors::find()->where(['user_id' => $user_id]);
            $non_event_payments = \app\models\Payments::find()->where(['user_id' => $user_id, 'type' => 1])->andWhere(['is', 'event_id', NULL]);
            $payments_through_app = \app\models\Payments::find()->select(['payment_service', 'sum(txn_amount) as total_amount'])->joinWith(['service'])->where(['payments.user_id' => $user_id, 'type' => 1])->andWhere(['is', 'event_id', NULL])->andWhere(['!=', 'payment_service', '0']);

            if (isset($_POST['fields']['date_range']) && !empty($_POST['fields']['date_range'])) {
                $date_range = explode(' to ', $_POST['fields']['date_range']);
                $fromArr = explode('-', $date_range[0]);
                $toArr = explode('-', $date_range[1]);
                $from = $fromArr[2] . '-' . $fromArr[0] . '-' . $fromArr[1];
                $to = $toArr[2] . '-' . $toArr[0] . '-' . $toArr[1];
            }

            if (isset($from) && isset($to)) {
                $totalVisitoryQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $trafficQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $fromDeviceQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $tippingAppQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $non_event_payments->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $to],
                ]);

                $payments_through_app->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $to],
                ]);

            }
            $countQuery = clone $trafficQuery;
            $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
            $pages->pageSize = 30;
            $traffic = $trafficQuery->offset($pages->offset)->limit($pages->limit)->orderBy('added_on desc')->asArray()->all();

            return [
                'success' => true,
                'totalVisitors' => $totalVisitoryQuery->count(),
                //'traffic'=>$trafficQuery->asArray()->all(),
                'traffic' => [
                    'records' => $traffic,
                    'pages' => $pages
                ],
                'fromDevice' => $fromDeviceQuery->asArray()->all(),
                'tippingApps' => $tippingAppQuery->asArray()->all(),
                'non_event_payments' => $non_event_payments->asArray()->sum('txn_amount'),
                'payments_through_app' => $payments_through_app->groupBy('payment_service')->asArray()->all(),
            ];
        }

    }

    public function actionUpdateUsername()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                if ($model->load($POST)) {
                    $find = $model->find()->where(['email' => $model->email])->one();
                    if ($find && $find->load($POST)) {
                        if (empty($find->access_token)) {
                            $find->access_token = Yii::$app->security->generateRandomString();
                        }
                        if ($find->save()) {
                            return [
                                'success' => true,
                                'user_id' => $find->id,
                                'username' => $find->username,
                                'email' => $find->email,
                                'name' => $find->name,
                                'user_type' => $find->role_id,
                                'token' => $find->access_token,
                                'image' => $find->image,
                                'description' => $find->description
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionDeleteEmail()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\UserEmails;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Email deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Email not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Email not found!'
            ];
        }
    }

    public function actionCheckUserExistence()
    {
        if (isset($_POST) && !empty($_POST)) {
            $find = \app\models\User::find()->where(['email' => trim($_POST['email'])])->one();
            if ($find) {
                if ($find->parent_id == Yii::$app->user->identity->id) {
                    return [
                        'found' => true,
                        'message' => ['email' => 'User already added.'],
                        'd' => $find->parent_id . '~' . Yii::$app->user->identity->id
                    ];
                } else {
                    $find->parent_id = Yii::$app->user->identity->id;
                    if ($find->save()) {
                        return [
                            'found' => true,
                            'message' => "User added successfully."
                        ];
                    }
                }
            } else {
                return [
                    'found' => false,
                ];
            }
        }
    }

    public function actionAddTeam()
    {
        if (isset($_POST) && !empty($_POST)) {
            $scenario = [];
            try {
                $model = new \app\models\User;
                $POST['User'] = $_POST['fields'];
                if ($model->load($POST) && $model->validate()) {
                    $model->status = 'Y';
                    $model->parent_id = Yii::$app->user->identity->id;
                    if (isset($model->password) && !empty($model->password)) {
                        $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                        $model->access_token = Yii::$app->security->generateRandomString();
                    }
                    if ($model->save()) {
                        $file_name = "user-" . $model->id;
                        $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/' . $model->username))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                        $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name . '.png');
                        if (isset($POST['User']['event_id'])) {
                            $eventDetails = \app\models\Events::find()->where(['id' => $POST['User']['event_id']])->one();
                            $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 27])->one();
                            $data = [
                                'case' => 20,
                                'template_id' => 27,
                                'name' => isset($model->name) ? $model->name : $model->username,
                                'username' => $model->username,
                                'password' => $model->password,
                                'event' => "<a href='" . Yii::$app->params['siteUrl'] . "/" . $eventDetails->url . "/" . $eventDetails->id . "'>" . $eventDetails->title . "</a>",
                                'body' => $templateDetails->content
                            ];
                            $compose = Yii::$app->mailer->compose('body', $data);
                            $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                            $compose->setTo($model->email);
                            $compose->setSubject($templateDetails->subject);
                            $compose->send();
                        }
                        return [
                            'success' => true,
                            'message' => 'Member added successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionGetTeam($pageSize = 50)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'username' => [
                    'asc' => ['username' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\User;
        $query = $model->find()->where(['parent_id' => Yii::$app->user->identity->id]);

        /* $search = new \app\models\SearchForm;
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        if ($search->load($GET)) {
            if (!empty($search->name)) {
                $query->andWhere(['LIKE', 'users.name', $search->name]);
            }
            if (!empty($search->email)) {
                $query->andWhere(['LIKE', 'users.email', $search->email]);
            }
            if (!empty($search->username)) {
                //echo $search->username;die;
                $query->andWhere(['LIKE', 'users.username', $search->username]);
            }
            if (!empty($search->phone)) {
                $query->andWhere(['LIKE', 'users.phone', preg_replace('/[^\p{L}\p{N}]/', '', $search->phone)]);
            }
            if (!empty($search->location_id)) {
                $query->andWhere(new \yii\db\Expression('FIND_IN_SET('.$search->location_id.',users.locations)'));
            }
            if (!empty($search->role_id)) {
                $query->andWhere(['=','role_id',$search->role_id]);
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'team' => $find,
            'pages' => $pages,
        ];
    }

    public function actionGetTraffic($pageSize = 30)
    {

        $sort = new \yii\data\Sort([
            'attributes' => [
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\ProfileVisitors;
        $query = $model->find()->where(['user_id' => Yii::$app->user->identity->id]);

        if (isset($_GET['date_range']) && !empty($_GET['date_range'])) {
            $date_range = explode(' to ', $_GET['date_range']);
            $fromArr = explode('-', $date_range[0]);
            $toArr = explode('-', $date_range[1]);
            $from = $fromArr[2] . '-' . $fromArr[0] . '-' . $fromArr[1];
            $to = $toArr[2] . '-' . $toArr[0] . '-' . $toArr[1];

            if (isset($from) && isset($to)) {
                $query->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'traffic' => [
                'records' => $find,
                'pages' => $pages
            ]
        ];
    }

    public function actionGetFeatured($pageSize = 2)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'username' => [
                    'asc' => ['username' => SORT_ASC],
                    'desc' => ['username' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['users.added_on' => SORT_ASC],
                    'desc' => ['users.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\User;
        //$query = $model->find()->select(['sum(target_amount) as goal','sum(amount) as raised','users.id','users.username','users.image','users.name'])->where(['users.featured' => 1]);

        $query = $model->find()->select(['users.id', 'users.username', 'users.image', 'users.name', 'users.description'])->where(['users.featured' => 1]);

        $countQuery = clone $query;
        //$query->joinWith(['services.appname','events.transactions']);
        $query->joinWith(['services.appname']);

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->groupBy('users.id')->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $newFind = [];
        foreach ($find as $key => $val) {
            $newFind[$key] = $val;
            $newFind[$key]['description'] = strip_tags($val['description']);
        }
        return [
            'success' => true,
            'users' => $newFind,
            'pages' => $pages,
        ];
    }

    /* public function actionExportPayments(){
        if(isset($_POST['export']) && !empty($_POST['export'])){
            $model = new \app\models\Payments;
            $payments = $model->find()->joinWith(['service','donor.beneficiaries'])->where(['payments.user_id' => Yii::$app->user->identity->id,'type'=>1])->all();
            if(!empty($payments)){
                $file_name = 'payments-'.date('m-d-Y');
                $cols = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P'];
                //$currencySymbols = ["$", "€", "£"];
                $currencySymbols = ["USD", "CAD", "EUR", "GBP", "SEK", "CHF"];
                $paymentMethods = ['Zelle','PayPal','ACH','Wire Transfer','Cash Deposit','Check Deposit'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach($cols as $col){
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
                $sheet->setCellValue($cols[0].$rows, '#')->getStyle($cols[0].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[1].$rows, 'Name')->getStyle($cols[1].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[2].$rows, 'Payment ID')->getStyle($cols[2].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[3].$rows, 'Currency')->getStyle($cols[3].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[4].$rows, 'Amount')->getStyle($cols[4].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[5].$rows, 'Payment Method')->getStyle($cols[5].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[6].$rows, 'Service Name')->getStyle($cols[6].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[7].$rows, 'Vefified On')->getStyle($cols[7].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[8].$rows, 'Phone')->getStyle($cols[8].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[9].$rows, 'Email')->getStyle($cols[9].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->setCellValue($cols[10].$rows, 'Note')->getStyle($cols[10].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>15]]);
                $sheet->mergeCells("K$rows:L$rows");
                $rows = 2;
                $srno = 1;
                foreach($payments as $payment){
                    $sheet->mergeCells("K$rows:L$rows");
                    $sheet->setCellValue($cols[0].$rows, $srno)->getStyle($cols[0].$rows)->applyFromArray(['font'=>['bold'=>true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[1].$rows, !empty($payment->name)?$payment->name:"")->getStyle($cols[1].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $sheet->setCellValue($cols[2].$rows, !empty($payment->payment_identifier)?$payment->payment_identifier:"")->getStyle($cols[2].$rows)->applyFromArray(['font'=>['bold'=>true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[3].$rows, $currencySymbols[$payment->currency])->getStyle($cols[3].$rows)->applyFromArray(['font'=>['bold'=>true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[4].$rows, !empty($payment->txn_amount)?(string)$payment->txn_amount:"")->getStyle($cols[4].$rows)->applyFromArray(['font'=>['bold'=>true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[5].$rows, ($payment->donor && !empty($payment->donor->payment_method))?$paymentMethods[$payment->donor->payment_method]:"")->getStyle($cols[5].$rows)->applyFromArray(['font'=>['bold'=>true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[6].$rows, !empty($payment->service)?$payment->service->name:"")->getStyle($cols[6].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $sheet->setCellValue($cols[7].$rows, !empty($payment->received_at)?date("M-d-Y",strtotime($payment->received_at)):"")->getStyle($cols[7].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $sheet->setCellValue($cols[8].$rows, (isset($payment->donor) && !empty($payment->donor->pone))?$payment->donor->phone:"")->getStyle($cols[8].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $sheet->setCellValue($cols[9].$rows, (isset($payment->email) && !empty($payment->email))?$payment->email:"")->getStyle($cols[9].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $sheet->setCellValue($cols[10].$rows, !empty($payment->donor)?$payment->donor->note:"")->getStyle($cols[10].$rows)->applyFromArray(['font'=>['bold'=>true]]);
                    $rows++;
                    if($payment->donor && $payment->donor->beneficiaries && !empty($payment->donor->beneficiaries)){
                        $mergeRowNumber = $rows;
                        $sheet->mergeCells("A$mergeRowNumber:L$mergeRowNumber");
                        $sheet->setCellValue($cols[0].$mergeRowNumber,'Beneficiaries')->getStyle($cols[0].$mergeRowNumber)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        //$sheet->getStyle($cols[0].$mergeRowNumber)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle($cols[0].$mergeRowNumber)->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('FFA0A0A0');
                        $rows++;
                        $nextRow = $rows;
                        $sheet->setCellValue($cols[0].$nextRow, 'Name')->getStyle($cols[0].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[1].$nextRow, "Father's Name")->getStyle($cols[1].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[2].$nextRow, "Grandfather's Name")->getStyle($cols[2].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[3].$nextRow, 'City')->getStyle($cols[3].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[4].$nextRow, 'Wereda')->getStyle($cols[4].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[5].$nextRow, 'Kebele')->getStyle($cols[5].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[6].$nextRow, 'Email')->getStyle($cols[6].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[7].$nextRow, 'Phone')->getStyle($cols[7].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[8].$nextRow, 'Amount')->getStyle($cols[8].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[9].$nextRow, 'Birr')->getStyle($cols[9].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[10].$nextRow, 'Delivered Date')->getStyle($cols[10].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $sheet->setCellValue($cols[11].$nextRow, 'Remarks')->getStyle($cols[11].$nextRow)->applyFromArray(['font'=>['bold'=>true,'size'=>13]]);
                        $rows++;
                        foreach($payment->donor->beneficiaries as $beneficiary){
                            $sheet->setCellValue($cols[0].$rows, $beneficiary->beneficiary_name);
                            $sheet->setCellValue($cols[1].$rows, $beneficiary->fathers_name);
                            $sheet->setCellValue($cols[2].$rows, $beneficiary->grand_fathers_name);
                            $sheet->setCellValue($cols[3].$rows, $beneficiary->beneficiary_city);
                            $sheet->setCellValue($cols[4].$rows, $beneficiary->beneficiary_wereda);
                            $sheet->setCellValue($cols[5].$rows, $beneficiary->beneficiary_kelebe);
                            $sheet->setCellValue($cols[6].$rows, $beneficiary->beneficiary_email);
                            $sheet->setCellValue($cols[7].$rows, $beneficiary->beneficiary_phone)->getStyle($cols[7].$rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[8].$rows, $beneficiary->beneficiary_amount)->getStyle($cols[8].$rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[9].$rows, "");
                            $sheet->setCellValue($cols[10].$rows, "");
                            $sheet->setCellValue($cols[11].$rows, "");
                            $rows++;
                        }
                        $rows++;
                    }
                    $srno++;
                }

                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name .'.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                   unset($spreadsheet);
                exit;
            }
        }
    } */

    public function actionExportPayments()
    {
        if (isset($_POST['export']) && !empty($_POST['export'])) {
            $model = new \app\models\Payments;
            $payments = $model->find()->joinWith(['service', 'donor.beneficiaries'])->where(['payments.user_id' => Yii::$app->user->identity->id, 'type' => 1])->all();
            if (!empty($payments)) {
                $file_name = 'payments-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X'];
                $currencySymbols = ["USD", "CAD", "EUR", "GBP", "SEK", "CHF"];
                $paymentMethods = ['Zelle', 'PayPal', 'ACH', 'Wire Transfer', 'Cash Deposit', 'Check Deposit'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
                $sheet->mergeCells("A$rows:K$rows");
                $sheet->setCellValue($cols[0] . $rows, 'Donors')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);
                $sheet->mergeCells("L$rows:X$rows");
                $sheet->setCellValue($cols[11] . $rows, 'Beneficiaries')->getStyle($cols[11] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);

                $rows = 2;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Payment ID')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Currency')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Amount')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Payment Method')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[6] . $rows, 'Service Name')->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[7] . $rows, 'Vefified On')->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[8] . $rows, 'Phone')->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[9] . $rows, 'Email')->getStyle($cols[9] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[10] . $rows, 'Note')->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[11] . $rows, '#')->getStyle($cols[11] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[12] . $rows, "Name")->getStyle($cols[12] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[13] . $rows, "Father's Name")->getStyle($cols[13] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[14] . $rows, "Grandfather's Name")->getStyle($cols[14] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[15] . $rows, 'City')->getStyle($cols[15] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[16] . $rows, 'Wereda')->getStyle($cols[16] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[17] . $rows, 'Kebele')->getStyle($cols[17] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[18] . $rows, 'Email')->getStyle($cols[18] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[19] . $rows, 'Phone')->getStyle($cols[19] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[20] . $rows, 'Amount')->getStyle($cols[20] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[21] . $rows, 'Birr')->getStyle($cols[21] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[22] . $rows, 'Delivered Date')->getStyle($cols[22] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[23] . $rows, 'Remarks')->getStyle($cols[23] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);

                $rows = 3;
                $srno = 1;
                foreach ($payments as $payment) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[1] . $rows, !empty($payment->name) ? $payment->name : "")->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue($cols[2] . $rows, !empty($payment->payment_identifier) ? $payment->payment_identifier : "")->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[3] . $rows, $currencySymbols[$payment->currency])->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[4] . $rows, !empty($payment->txn_amount) ? (string) $payment->txn_amount : "")->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[5] . $rows, ($payment->donor && !empty($payment->donor->payment_method)) ? $paymentMethods[$payment->donor->payment_method] : "")->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[6] . $rows, !empty($payment->service) ? $payment->service->name : "")->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue($cols[7] . $rows, !empty($payment->received_at) ? date("M-d-Y", strtotime($payment->received_at)) : "")->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue($cols[8] . $rows, (isset($payment->donor) && !empty($payment->donor->pone)) ? $payment->donor->phone : "")->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue($cols[9] . $rows, (isset($payment->email) && !empty($payment->email)) ? $payment->email : "")->getStyle($cols[9] . $rows)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue($cols[10] . $rows, !empty($payment->donor) ? $payment->donor->note : "")->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['bold' => true]]);

                    if ($payment->donor && $payment->donor->beneficiaries && !empty($payment->donor->beneficiaries)) {
                        $bsrn = 1;
                        foreach ($payment->donor->beneficiaries as $beneficiary) {
                            $sheet->setCellValue($cols[11] . $rows, $bsrn);
                            $sheet->setCellValue($cols[12] . $rows, $beneficiary->beneficiary_name);
                            $sheet->setCellValue($cols[13] . $rows, $beneficiary->fathers_name);
                            $sheet->setCellValue($cols[14] . $rows, $beneficiary->grand_fathers_name);
                            $sheet->setCellValue($cols[15] . $rows, $beneficiary->beneficiary_city);
                            $sheet->setCellValue($cols[16] . $rows, $beneficiary->beneficiary_wereda);
                            $sheet->setCellValue($cols[17] . $rows, $beneficiary->beneficiary_kelebe);
                            $sheet->setCellValue($cols[18] . $rows, $beneficiary->beneficiary_email);
                            $sheet->setCellValue($cols[19] . $rows, $beneficiary->beneficiary_phone)->getStyle($cols[19] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[20] . $rows, $beneficiary->beneficiary_amount)->getStyle($cols[20] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[21] . $rows, "");
                            $sheet->setCellValue($cols[22] . $rows, "");
                            $sheet->setCellValue($cols[23] . $rows, "");
                            $bsrn++;
                            $rows++;
                        }
                    }
                    $rows++;
                    $srno++;
                }

                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        }
    }

    public function actionExportDonors()
    {
        if (isset($_POST['export']) && !empty($_POST['export'])) {
            $model = new \app\models\DonorDetails;
            $donors = $model->find()->joinWith(['service', 'beneficiaries'])->where(['verified' => 0, 'event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])])->all();
            if (!empty($donors)) {
                $file_name = 'donors-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                $currencySymbols = ["USD", "CAD", "EUR", "GBP", "SEK", "CHF"];
                $paymentMethods = ['Zelle', 'PayPal', 'ACH', 'Wire Transfer', 'Cash Deposit', 'Check Deposit'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
                $sheet->mergeCells("A$rows:M$rows");
                $sheet->setCellValue($cols[0] . $rows, 'Donors')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);
                $sheet->mergeCells("N$rows:Z$rows");
                $sheet->setCellValue($cols[13] . $rows, 'Beneficiaries')->getStyle($cols[13] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 18]]);

                $rows = 2;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'First Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Last Name')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Email')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Currency')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Amount Donated')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[6] . $rows, 'Bank Confirmation Number')->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[7] . $rows, 'Payment Method')->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[8] . $rows, 'City')->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[9] . $rows, 'State')->getStyle($cols[9] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[10] . $rows, 'Country')->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[11] . $rows, 'Phone')->getStyle($cols[11] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[12] . $rows, 'Note')->getStyle($cols[12] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[13] . $rows, '#')->getStyle($cols[13] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[14] . $rows, "Name")->getStyle($cols[14] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[15] . $rows, "Father's Name")->getStyle($cols[15] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[16] . $rows, "Grandfather's Name")->getStyle($cols[16] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[17] . $rows, 'City')->getStyle($cols[17] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[18] . $rows, 'Wereda')->getStyle($cols[18] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[19] . $rows, 'Kebele')->getStyle($cols[19] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[20] . $rows, 'Email')->getStyle($cols[20] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[21] . $rows, 'Phone')->getStyle($cols[21] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[22] . $rows, 'Amount')->getStyle($cols[22] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[23] . $rows, 'Birr')->getStyle($cols[23] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[24] . $rows, 'Delivered Date')->getStyle($cols[24] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[25] . $rows, 'Remarks')->getStyle($cols[25] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);

                $rows = 3;
                $srno = 1;
                foreach ($donors as $donor) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[1] . $rows, $donor->donor_first_name);

                    $sheet->setCellValue($cols[2] . $rows, $donor->donor_last_name)->getStyle($cols[2] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[3] . $rows, $donor->donor_email)->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[4] . $rows, $currencySymbols[$donor->currency])->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[5] . $rows, !empty($donor->amount_donated) ? (string) $donor->amount_donated : "")->getStyle($cols[5] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[6] . $rows, !empty($donor->payment_method) ? $paymentMethods[$donor->payment_method] : "")->getStyle($cols[6] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[7] . $rows, $donor->donor_city);

                    $sheet->setCellValue($cols[8] . $rows, $donor->donor_state);

                    $sheet->setCellValue($cols[9] . $rows, $donor->donor_country);

                    $sheet->setCellValue($cols[10] . $rows, $donor->donor_phone);

                    $sheet->setCellValue($cols[11] . $rows, !empty($donor->added_on) ? date("M-d-Y", strtotime($donor->added_on)) : "");

                    $sheet->setCellValue($cols[12] . $rows, $donor->note);

                    if ($donor->beneficiaries && !empty($donor->beneficiaries)) {
                        $bsrn = 1;
                        foreach ($donor->beneficiaries as $beneficiary) {
                            $sheet->setCellValue($cols[13] . $rows, $bsrn);
                            $sheet->setCellValue($cols[14] . $rows, $beneficiary->beneficiary_name);
                            $sheet->setCellValue($cols[15] . $rows, $beneficiary->fathers_name);
                            $sheet->setCellValue($cols[16] . $rows, $beneficiary->grand_fathers_name);
                            $sheet->setCellValue($cols[17] . $rows, $beneficiary->beneficiary_city);
                            $sheet->setCellValue($cols[18] . $rows, $beneficiary->beneficiary_wereda);
                            $sheet->setCellValue($cols[19] . $rows, $beneficiary->beneficiary_kelebe);
                            $sheet->setCellValue($cols[20] . $rows, $beneficiary->beneficiary_email);
                            $sheet->setCellValue($cols[21] . $rows, $beneficiary->beneficiary_phone)->getStyle($cols[19] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[22] . $rows, $beneficiary->beneficiary_amount)->getStyle($cols[20] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[23] . $rows, "");
                            $sheet->setCellValue($cols[24] . $rows, "");
                            $sheet->setCellValue($cols[25] . $rows, "");
                            $bsrn++;
                            $rows++;
                        }
                    }
                    $rows++;
                    $srno++;
                }

                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        }
    }

    public function actionDeletePayment()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Payments;
                $find = $model->find()->where(['id' => $_POST['id']])->andWhere(['!=', 'transaction_id', 'NULL'])->one();
                if ($find) {
                    $transaction_id = $find->transaction_id;
                    if ($find->delete()) {
                        \app\models\Transactions::deleteAll(['id' => $transaction_id]);
                        return [
                            'success' => true,
                            'message' => 'Payment deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Payment not found '
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Payment not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Payment not found!'
            ];
        }
    }

    public function actionBeneficiaries($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'name' => [
                    'asc' => ['beneficiary_name' => SORT_ASC],
                    'desc' => ['beneficiary_name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'received_at' => [
                    'asc' => ['received_at' => SORT_ASC],
                    'desc' => ['received_at' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\BeneficiaryDetails;
        $query = $model->find()->where(['user_id' => Yii::$app->user->identity->id]);

        /* if(isset($_GET['fields']) && !empty($_GET['fields'])){
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->date_range)) {
                    $date_range = explode(' to ', $search->date_range);
                    $fromArr = explode('-',$date_range[0]);
                    $toArr = explode('-',$date_range[1]);
                    $from = $fromArr[2].'-'.$fromArr[0].'-'.$fromArr[1];
                    $to = $toArr[2].'-'.$toArr[0].'-'.$toArr[1];

                    $query->andWhere(['AND',
                        ['>=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $from],
                        ['<=', 'DATE_FORMAT(received_at, "%Y-%m-%d")', $to],
                    ]);
                }
                if (!empty($search->payment_identifier)) {
                    $query->andWhere(['LIKE', 'payment_identifier', $search->payment_identifier]);
                }
                if (!empty($search->name)) {
                    $query->andWhere(['LIKE', 'payments.name', $search->name]);
                }
                if (!empty($search->amount)) {
                    $query->andWhere(['payments.txn_amount'=>$search->amount]);
                }
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $response = [
            'success' => true,
            'beneficiaries' => $find,
            'pages' => $pages,
        ];
        return $response;
    }

    public function actionSaveFundingSourceUrl()
    {
        if (isset($_POST) && !empty($_POST)) {
            $model = new \app\models\Beneficials;
            $find = $model->findOne(['user_id' => Yii::$app->user->identity->id]);
            if ($find) {
                $find->funding_source_url = $_POST['fundingSourceUrl']['_links']['funding-source']['href'];
                if ($find->save()) {
                    return [
                        'success' => true,
                        'message' => 'Bank has been verified successfully.',
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $find->getErrors(),
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Member not found.',
                ];
            }
        }
    }

    public function actionGetBeneficial()
    {
        try {
            $model = new \app\models\Beneficials;
            $find = $model->find()->joinWith(['state', 'ccountry', 'pcountry', 'cstate'])->where(['user_id' => Yii::$app->user->identity->id])->asArray()->one();
            return [
                'success' => true,
                'beneficial' => $find
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionGetDwollaAccount()
    {
        try {
            \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
            \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];

            $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
            $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
            $appToken = $tokensApi->token();
            \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
            //\DwollaSwagger\Configuration::$debug = 1;

            $model = new \app\models\Beneficials;
            $find = $model->findOne(['user_id' => Yii::$app->user->identity->id]);
            if ($find) {
                $fsApi = new \DwollaSwagger\FundingsourcesApi($apiClient);
                $balance = $fsApi->getBalance($find->funding_source_url);

                $tapi = new \DwollaSwagger\TransfersApi($apiClient);
                $transfers = $tapi->getCustomerTransfers($find->dwolla_customer_url);
                //print_r($transfers);die;
                $transactions = json_decode(json_encode($transfers->_embedded->transfers), true);

                foreach ($transactions as $key => $val) {
                    $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
                    $customer = $customersApi->getCustomer($val['_links']['source']['href']);
                    $transactions[$key]['customer'] = $customer;
                }
                return [
                    'success' => true,
                    'transactions' => $transactions,
                    'balance' => $balance->balance
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'Please verfify your account'
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $e->getResponseBody(),
            ];
        }
    }

    public function actionConnectStripe()
    {
        $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
        $find = \app\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        if ($find) {
            if (isset($find->stripe_connect_account_id) && !empty($find->stripe_connect_account_id)) {
                $account_id = $find->stripe_connect_account_id;
            } else {
                $account = $stripe->accounts->create(['type' => 'express']);
                $find->stripe_connect_account_id = $account_id = $account->id;
                $find->save();
            }
            $response = $stripe->accountLinks->create([
                'account' => $account_id,
                //'account' => 'ca_LsGB6FvU3f7hpv8bXL5hh2wT8nChAh8E',
                'refresh_url' => Yii::$app->params['apiUrl'] . '/site/stripe-reauth?id=' . $account_id,
                'return_url' => Yii::$app->params['apiUrl'] . '/site/stripe-connect-return',
                'type' => 'account_onboarding',
            ]);
            return [
                'success' => true,
                'account_id' => $account_id,
                'url' => $response->url
            ];
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function actionCheckStripeConnectRequest()
    {
        $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
        $find = \app\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        if ($find) {
            $response = $stripe->accounts->retrieve($find->stripe_connect_account_id, []);
            if ($response->payouts_enabled === true) {
                $find->stripe_connect_status = 1;
                if ($find->save()) {
                    return [
                        'success' => true,
                        'account' => $response,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $find->getErrors()
                    ];
                }
            } else {
                return [
                    'error' => true,
                ];
            }
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function actionCheckStripeConnectStatus()
    {
        $find = \app\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        if ($find && $find->stripe_connect_status === 1) {
            return [
                'success' => true,
            ];
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function actionRemoveStripeAccount()
    {
        $find = \app\models\User::findOne(['id' => Yii::$app->user->identity->id]);
        if ($find) {
            $find->stripe_connect_status = 0;
            $find->stripe_connect_account_id = null;
            if ($find->save()) {
                return [
                    'success' => true,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => $find->getErrors()
                ];
            }
        } else {
            return [
                'error' => true,
            ];
        }
    }

    // Function to get the user's Membership data
    public function actionGetMembership()
    {
        try {
            $model = new \app\models\UsersMembership;
            $find = $model->find()->joinWith(['plan'])->where(['user_id' => Yii::$app->user->identity->id, 'users_membership.status' => 1])->asArray()->one();

            return [
                'success' => true,
                'membership' => $find
            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }
}
