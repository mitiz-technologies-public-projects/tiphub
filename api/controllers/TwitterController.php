<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;



class TwitterController extends Controller
{
    public $enableCsrfValidation = false;
    public $consumer_key = 'NSv9kS2VtRJ3Q4LDBJ0MQL0jH';
    public $secret_key = 'Y65kVGKnXgglpkmQbi6beloERHltrirUU4c2dT4uZ1a3GpHZK7';
    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'except' => ['login','request-token','return'],
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
                //QueryParamAuth::className(),
            ],
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        return $behaviors;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        phpinfo();
        //return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin(){                  
        require 'vendor/twitter/autoload.php';
        if(isset($_GET['oauth_token'], $_GET['oauth_verifier'])){
            $connection = new \Abraham\TwitterOAuth\TwitterOAuth($this->consumer_key, $this->secret_key, $_GET['oauth_token'], $_GET['oauth_verifier']);
            $token = $connection->oauth('oauth/access_token',['oauth_verifier' => $_GET['oauth_verifier']]);   
            if(isset($token)){                
                $connection = new \Abraham\TwitterOAuth\TwitterOAuth($this->consumer_key, $this->secret_key, $token['oauth_token'], $token['oauth_token_secret']);
                return $connection->get('account/verify_credentials',['include_entities' => true, 'skip_status' => true, 'include_email' => true]);                   
            }  
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    /* public function actionLogin(){          
        /* require 'vendor/twitter/autoload.php';
        $connection = new \Abraham\TwitterOAuth\TwitterOAuth($this->consumer_key, $this->secret_key);    
        $request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => 'https://civ-works.mitiztechnologies.in/api/twitter/return'));
        $url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));               
        $this->redirect($url); 
        if(isset($_GET['oauth_token'])){
            print_r($_SESSION);die;
            //https://civ-works.mitiztechnologies.in/api/twitter/return?oauth_token=3kbgUgAAAAAA_XW3AAABbF5pPyk&oauth_verifier=HiiCPMwPFok7KMljh25csv7HXKq8VUej
        }
    } */

    public function actionRequestToken(){       
        require 'vendor/twitter/autoload.php';
        if(strpos($_SERVER['HTTP_ORIGIN'],"localhost")){
            $call_back_url = "http://localhost:3000";
        }
        else{
            //$call_back_url = Yii::$app->params['apiUrl']."/twitter/return";
            $call_back_url = Yii::$app->params['apiUrl'].'/site/twitter-return';
            //$call_back_url = Yii::$app->params['siteUrl'];
        }
        $connection = new \Abraham\TwitterOAuth\TwitterOAuth($this->consumer_key, $this->secret_key);    
        $response = $connection->oauth('oauth/request_token', array('oauth_callback' => $call_back_url));        
        $_SESSION['oauth_token'] = $response['oauth_token'];
        $_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];   
        return $response;     
    }
    
    public function actionReturn(){
        //Yii::$app->response->format = Response::FORMAT_RAW;
        /* return [
            'response'=>'ok'
        ]; */
        /* return [
            'oauth_token'=>$_GET['oauth_token'],
            'oauth_verifier'=>$_GET['oauth_verifier']
        ]; */ //https://civ-works.mitiztechnologies.in/api/twitter/return?oauth_token=7WwVwAAAAAAA_XW3AAABbF5zcJc&oauth_verifier=mG2YKCTyQZqwNSANT8pqjWs49SKOpV2F
        //require 'vendor/twitter/autoload.php';
        //print_r($_SESSION);die;
        /*if(isset($_GET['oauth_verifier'])){
            print_r($_SESSION);die;
            //https://civ-works.mitiztechnologies.in/api/twitter/return?oauth_token=3kbgUgAAAAAA_XW3AAABbF5pPyk&oauth_verifier=HiiCPMwPFok7KMljh25csv7HXKq8VUej
        } */
        //$connection = new \Abraham\TwitterOAuth\TwitterOAuth($this->consumer_key, $this->secret_key);
        //$test = $connection->post('account/verify_credentials',['include_entities' => true, 'skip_status' => true, 'include_email' => true]);        
        //$test = $connection->post('oauth/access_token',['oauth_verifier'=>$_GET['oauth_verifier']]);      
        //print_r($test);die;
    }
    
}
