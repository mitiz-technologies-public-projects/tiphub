<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use linslin\yii2\curl;

use app\models\Transactions;
use app\models\Events;
use app\models\PaymentServiceNames;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action){
        if (parent::beforeAction($action)) {
			
            if ($action->id=='error'){
                if(!Yii::$app->user->isGuest){
                    $this->layout ='admin';
                    return true;
                }
            }
           
            $this->layout = 'login';
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //phpinfo();
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        $data = [];
        $data['login'] = $login = new LoginForm();
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->request->baseUrl.'/admin');
        }
        

        if ($login->load(Yii::$app->request->post()) && $login->login()) {
           // $data['notification'] = $notification = \app\models\Notification::find()->where(['reporting_manager_id' => Yii::$app->user->identity->id])->all();
            
            if (Yii::$app->user->identity->deleted == 'Y') {
                Yii::$app->user->logout();
                //Yii::$app->getSession()->setFlash('error', 'Your account has been suspended. Please contact administrator.');
                $data['login']->addError('username', 'Your account has been suspended.');
                return $this->render('login', $data);
            } 
            else {
                $return_url = \Yii::$app->request->get('return');
                if(!empty($return_url)){
                    return $this->redirect($return_url);
                }else{
                    if (Yii::$app->user->identity->role_id == 2) {
                        return $this->redirect(Yii::$app->request->baseUrl.'/admin/blog');
                    }
                    return $this->redirect(Yii::$app->request->baseUrl.'/admin');
                }
            }
        }
        return $this->render('login', $data);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        $redirect_url = Yii::$app->request->baseUrl . '/site/login';
        Yii::$app->user->logout();
        return $this->redirect($redirect_url);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAddModuleAction()
    {
        $msg = "";
        $controllers = \yii\helpers\FileHelper::findFiles(Yii::getAlias('@app/controllers'), ['recursive' => true]);
        foreach ($controllers as $controller) {
            if ($controller == $_SERVER['DOCUMENT_ROOT']."/api/controllers/EventController.php") {
                $contents = file_get_contents($controller);
                //print_r($contents);die;
                #get all action function file in individual controllers
                preg_match_all('/Label~Module~Action(.*)/', $contents, $label_module_action);
                if (isset($label_module_action[0]) && !empty($label_module_action[0])) {
                    foreach ($label_module_action[0] as $lma) {
                        $function_details = explode(":", $lma);
                        $function_details = explode("~", $function_details[1]);
                        $module_name = trim($function_details[1]);
                        $action_label_name = trim($function_details[0]);
                        $action_name = trim($function_details[2]);
                        $url = trim($function_details[3]);
                        $icon = trim($function_details[4]);
                        $roleArr = explode(',', trim($function_details[5]));
                        if($roleArr){
                            foreach($roleArr as $role){
                                $model = new \app\models\ModuleActions;
                                $msg .= "<br/>" . $model->find()->where(['module_name' => $module_name, 'action_label_name' => $action_label_name, 'action_name' => $action_name, 'role'=>$role])->createCommand()->getRawSql() . "<br/>";
        
                                $find = $model->find()->where(['module_name' => $module_name, 'action_label_name' => $action_label_name, 'action_name' => $action_name, 'role'=>$role])->one();
                                if ($find) {
                                    $msg .= "Found - Updating<br/>";
                                    $find->module_name = $module_name;
                                    $find->action_label_name = $action_label_name;
                                    $find->action_name = $action_name;
                                    $find->url = $url;
                                    $find->icon = $icon;
                                    $find->role = $role;
                                    $find->save();
                                } else {
                                    $msg .= "Not found - Inserting<br/>";
                                    $model->module_name = $module_name;
                                    $model->action_label_name = $action_label_name;
                                    $model->action_name = $action_name;
                                    $model->url = $url;
                                    $model->icon = $icon;
                                    $model->role = $role;
                                    $model->save();
                                }
                            }
                        }
                    }
                }
            }
        }
        die($msg);
    }

    public function actionReceiveSms(){
        $post = print_r($_POST,true);
		mail('opnsrc.devlpr@gmail.com', 'Incomming sms details', $post);
    }

    public function actionTwitterReturn(){
        $this->layout = "blank";
        return $this->render('twitter-return', []);
    }

    //site/get-data
    public function actionGetData($limit = null, $offset = null,  $event_id = null, $added_by = null){
        mail("opnsrc.devlpr@gmail.com","Tiphub","Hello");
        $events = Events::find()->where(['<>', 'campaign_url', ""])->orWhere(['<>', 'campaign_url', null])->all();
        //$events = Events::find()->andWhere(['user_id'=>'23'])->andWhere(['<>', 'campaign_url', ""])->orWhere(['<>', 'campaign_url', null])->all();
        //dd($events);
        //echo Events::find()->where(['<>', 'campaign_url', ""])->orWhere(['<>', 'campaign_url', null])->createCommand()->getRawSql();die;
        
        if(!empty($events)){
            
            foreach($events as $event){

                if(!empty($event->campaign_url)){
                    //echo $event->campaign_url."<br>";
                    $skuLists = explode(PHP_EOL, $event->campaign_url);

                    if(!empty($skuLists) && is_array($skuLists)){
                        foreach($skuLists as $list){
                            if(!empty(trim($list))){
                                
                                if (str_contains(trim($list), 'gofundme.com')) { 


                                    echo "<pre>"; print_r(trim($list)); echo "</pre>";

                                    $tags = explode('?', trim($list));

                                    if(isset($tags[0]) && !empty($tags[0])){

                                        $extract_tags = explode('/', trim($tags[0]));

                                        if(!empty($extract_tags) && is_array($extract_tags)){
                                            $tag = end($extract_tags);

                                        
                                            //$tag = substr($tags[0], 0, strpos($tags[0], "?"));
                                            //echo $event->campaign_url."<br>";
                                            //echo $tags[0]."<br>";
                                            //echo $tag."<br>";
                                            //echo $tag."<br>";
                                            if(!empty($tag)){
                                                $this->UpdateGofundmeTransaction($tag, $event->id, $event->user_id);
                                            }
                                        }
                                    }
                                    
                                }

                            }
                        }
                    }
                }

                
            }
        }
        die();
        
    }

    public function UpdateGofundmeTransaction($tag, $event_id, $user_id, $limit = null, $offset = null){
        
        if($limit == null){
            $limit = 20;
        }

        if($offset == null){
            $offset = 0;
        }

        //echo "<br><br>" . $tag . "====" . $event_id . "====" . $user_id . "====" . $limit . "====" . $offset ."<br><br>";

        $curl = new curl\Curl();
        $response = $curl->get('https://gateway.gofundme.com/web-gateway/v1/feed/'.$tag.'/donations?limit='.$limit.'&offset='.$offset.'&sort=recent');
        $records = json_decode($response,true);

        $has_next = true;

        if(!empty($records) && isset($records['references']) && isset($records['references']['donations']) && !empty($records['references']['donations'])){
            foreach($records['references']['donations'] as $record){
                
                if(isset($record['name']) && !empty($record['name'])){
                    $created_at = null;
                    if(isset($record['created_at']) && !empty($record['created_at'])){
                        $created_at = date("Y-m-d H:i:s", strtotime($record['created_at']));
                    }
                    //check record exist or not
                    if(Transactions::find()->where(["event_id" => $event_id, "added_by" => $user_id, "name" => $record['name'], "amount" => $record['amount'], "added_on" => $created_at])->count() <= 0){
                        $model = new Transactions();
                        $model->event_id = $event_id;
                        $model->service_id = '9';
                        $model->added_by = $user_id;
                        $model->added_on = $created_at;
                        $model->name = $record['name'];
                        $model->amount = $record['amount'];
                        if($model->validate()){
                            $transaction = $model->save();
                            echo $record['name']. " Added  success <br> ";
                        }
                        else{
                            $errors = [];
                            foreach ($model->getErrors() as $key => $error) {
                                $errors[$key] = $error[0];
                            }
                            echo  implode(", ", $errors) . " <br> ";
                            echo " Errors Not Added ======================================== <br> ";
                        }
                        
                    }
                    else{
                        $has_next = false;
                        echo $record['name'] . " Already Added <br> ";
                        break;
                    }
                }

            }
        }

        if(!empty($records) && isset($records['meta']) && isset($records['meta']['has_next']) && $records['meta']['has_next'] === true && $has_next === true){
            $this->UpdateGofundmeTransaction($tag, $event_id, $user_id, $limit, ($offset + $limit));
        }

    }

    public function actionQrcode(){
        $qrCode = (new \Da\QrCode\QrCode('https://google.com'))->setSize(250)->setMargin(5);//->useForegroundColor(51, 153, 255);
        $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . '/code.png');
        die("Done");
    }

    public function actionCreateEventUrl(){
        $find = \app\models\Events::find()->all();
        if($find){
            foreach($find as $event){
                $emodel = \app\models\Events::findOne(['id'=>$event->id]);
                if($emodel){
                    $emodel->url = preg_replace('/\s+/', '-', strtolower(trim($event->title)));//."<br/>";
                    $emodel->save();
                }
            }
        }
    }

    public function actionWebhook(){

        /* return [
            [['data_dump'], 'required'],
            [['payment_id','payment_identifier','name','email','payment_mode'], 'string'],
            [['txn_amount'], 'number'],
            [['received_at','processed_at'], 'safe'],
        ]; */
        $casAppSource = ['cash@square.com'];
        $venmoSource = ['venmo@venmo.com'];
        $paypalSource = ['service@paypal.com'];

        $data = $_POST;
        print_r($data); 
        
        //die;

        $sender = "";
        $payment_service_id = 0;
        if (isset($data['sender'])){
            $sender = $data['sender'];
        }

        // Checking Subject strings to skip the payments that the user sent. We need to capture the only payments that user has received
        $mail_subject = "";
        $mail_body = "";
        $type = 3; // 1=>Payment Received, 2 =>Payment sent 3 => Unknown
        if (isset($data['mail_subject'])){
            $mail_subject = $data['mail_subject'];

            if(preg_match("/(you paid|you sent|you spent|Reminder:)/i", $mail_subject)){ 
                $type = 2; 
            }else if(preg_match("/(You've got money|sent you|paid you|you received|Money is waiting for you|Notification of payment received|You received a payment)/i", $mail_subject)){ 
                $type = 1; 
            }
            
        }

        


        // Checking email/payment source
        if (in_array($sender,$casAppSource)){ 
            $payment_service_id = 1; // Cashapp
        }elseif (in_array($sender,$venmoSource)){ 
            $payment_service_id = 2; // Venmo
        }elseif (in_array($sender,$paypalSource)){ 
            $payment_service_id = 4; //Paypal
        }

        if(preg_match("/(Zelle|Zelle(R)️)/i", $mail_subject)){ 
            $payment_service_id = 7; // Zelle = 7
        }
        if (isset($data['mail_body'])){ // Checking mail body for Zelle
            $mail_body = $data['mail_body'];

            if(preg_match("/(Zelle|Zelle(R)️|you spent|Reminder:)/i", $mail_body)){ 
                $payment_service_id = 7; // Zelle = 7
            } 
            
        }
        $all_data = json_encode($data);
       // echo "Amount final=>>>>>".$this->checkAmount($data);
        $model = new \app\models\Payments;
        $model->payment_id = isset($data['id']) ? $data['id'] : "";
        $model->payment_identifier = $this->checkPaymentReference($data); 
        $model->txn_amount = $this->checkAmount($data); 
        $model->payment_mode = isset($data['payment_mode']) ? $data['payment_mode'] : "";
        $model->received_at = isset($data['received_at']) ? $data['received_at'] : ""; 
        $model->email = isset($data['from_email'][0]) ? $data['from_email'][0]['address'] : ""; 
        $model->name = $this->checkPayerName($data); 
        $model->payment_service = $payment_service_id; 
        $model->type = $type; 
        $model->mail_subject = $mail_subject; 
        $model->data_dump = $all_data;

        if(isset($model->email) && !empty($model->email)){
            $ufind = \app\models\User::find()->where(["email"=>$model->email])->one();
            if($ufind){
                $model->user_id = $ufind->id;
            }
            else{
                $ufind = \app\models\UserEmails::find()->where(["email"=>$model->email])->one();
                if($ufind){
                    $model->user_id = $ufind->user_id;
                }
            }

            

            /*Start:  Link payment to event */
            if ($type == 1){ // Only when you received money
                $eventQuery = new \app\models\Events;
                $findEvents = $eventQuery->find()->where(["user_id"=>$model->user_id])->orderBy("added_on desc")->limit(1)->all();
                if($findEvents){
                    echo "Event end date =>> ".$findEvents[0]->end_date."  ID =>>".$findEvents[0]->id;

                    // && $findEvents[0]->end_date >= date("Y-m-d H:i:s")
                    if(($findEvents[0]->end_date !== NULL || $findEvents[0]->recurring == 2)  && $model->txn_amount > 0){
                        $model->event_id = $findEvents[0]->id;
                        $transactionQuery = new \app\models\Transactions;
                        $transactionQuery->event_id = $model->event_id;
                        $transactionQuery->service_id = $model->payment_service;
                        $transactionQuery->name = $model->name;
                        $transactionQuery->amount = $model->txn_amount;
                        $transactionQuery->added_by = $model->user_id;
                        if($transactionQuery->save())
                        {
                            $model->transaction_id = $transactionQuery->id;
                            $model->status = 1;
                        }
                    }
                }
            } 
           
            /*End:  Link payment to event */
        }
        if (count($data) > 0) { // Make sure there is no blank data inserted in to the database
           // $model->save();
            if(!$model->save()){
                print_r($model->getErrors());
            }
        }
        print_r($model);
       // die();
        
       
    }
    
    // Function to check the valid payer name
    function checkPayerName($data=""){
        $payer_name = "";
        if($data['payer_name'] == "|" || strlen($data['payer_name']) <= 3){
            $payer_name = isset($data['payer_name_2']) ? $data['payer_name_2'] : "";
        }else if(isset($data['payer_name_2'])) {
           if($data['payer_name_2'] == "|" || strlen($data['payer_name_2']) <= 3){
                $payer_name = isset($data['payer_name_3']) ? $data['payer_name_3'] : "";
            } 
        }else {
            $payer_name = $data['payer_name'];
        }
        return $payer_name;
    }

    // Function to check the amounts from different formats posted
    private function checkAmount($data){
        // isset($data['txn_amount']) ? str_replace('$','',$data['txn_amount']) : ""
        $default_amount = 0;
        $amount = "";
        $data['txn_amount'] = isset($data['txn_amount']) ? trim(str_replace('USD','',$data['txn_amount'])) : "";
        if (isset($data['txn_amount']) && $data['txn_amount'] != ''){
            $amount = str_replace('$','',$data['txn_amount']);
            $amount = str_replace(',','',$amount);
        }
        if (!is_numeric($amount)){
           
            if (isset($data['txn_amt2']) && $data['txn_amt2'] != ''){ // Check next amount variable if it has correct amount parsed
                $amount = str_replace('$','',$data['txn_amt2']);
                $amount = str_replace(',','',$amount);
            }
        }
        if (!is_numeric($amount)){
            if (isset($data['txn_amt3']) && $data['txn_amt3'] != ''){ // Check next amount variable if it has correct amount parsed
                $amount = str_replace('$','',$data['txn_amt3']);
                $amount = str_replace(',','',$amount);
            }
        }
        if (!is_numeric($amount)){
             if (isset($data['txn_amt4']) && $data['txn_amt4'] != ''){ // Check next amount variable if it has correct amount parsed
                $amount = str_replace('$','',$data['txn_amt4']);
                $amount = str_replace(',','',$amount);
            }
        }
        if (!is_numeric($amount)){
            if (isset($data['txn_amt5']) && $data['txn_amt5'] != ''){ // Check next amount variable if it has correct amount parsed
               $amount = str_replace('$','',$data['txn_amt5']);
               $amount = str_replace(',','',$amount);
           }
       }
        return $amount;
            
    }
    
     // Function to check the reference number from different formats posted
    private function checkPaymentReference($data){
        $payment_identifier = "";
        if (isset($data['payment_identifier']) && $data['payment_identifier'] != '' && strlen($data['payment_identifier']) <= 25){
            $payment_identifier = $data['payment_identifier'];
            
        }else if (isset($data['payment_identifier2']) && $data['payment_identifier2'] != '' && strlen($data['payment_identifier2']) <= 25 ){ // Check next variable
            $payment_identifier = $data['payment_identifier2'];
        } 
        
        if (strlen($payment_identifier) >=25) {
            $payment_identifier = "";
        }
        return $payment_identifier;    
    }

    public function actionTest(){
        echo "<pre>";
        $payments = \app\models\Payments::find()->select("id")->where(["event_id"=>49])->column();
        print_r($payments);die;
    }

    public function actionShareEvent($transid){
        $this->layout = "blank";
        if(isset($transid)){
            $model = new \app\models\Transactions;
            $find = $model->find()->joinWith(["event"])->where(["transactions.id"=>$transid])->one();
            $this->view->params["transaction"] = $find;
            return $this->render("share-event", [
                "transaction"=>$find
            ]);
        }
    }

    public function actionCertificate(){
        $this->layout = "blank";
        return $this->render("/event/certificate");
    }

    public function actionStripeConnectReturn(){
        $this->layout = "blank";
        return $this->render("stripe-connect-return", ['data'=>$_POST]);
    }

    public function actionStripeReauth($id){
        $this->layout = "blank";
        $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
        $response = $stripe->accountLinks->create([
            'account' => $id,
            'refresh_url' => Yii::$app->params['apiUrl'].'/site/stripe-reauth?id='.$id,
            'return_url' => Yii::$app->params['apiUrl'].'/site/stripe-connect-return',
            'type' => 'account_onboarding',
        ]);
        return $this->redirect($response->url);
    }

    public function actionStripeWebhook(){
        // This is a public sample test API key.
        // Don’t submit any personally identifiable information in requests made with this key.
        // Sign in to see your own test API key embedded in code samples.
        \Stripe\Stripe::setApiKey(Yii::$app->params['stripeApiKey']);
        // Replace this endpoint secret with your endpoint's unique secret
        // If you are testing with the CLI, find the secret by running 'stripe listen'
        // If you are using an endpoint defined with the API or dashboard, look in your webhook settings
        // at https://dashboard.stripe.com/webhooks
        $endpoint_secret = Yii::$app->params['stripeSecretKey'];
        $payload = @file_get_contents('php://input');
        $event = null;

        try {
            $event = \Stripe\Event::constructFrom(json_decode($payload, true));
        } 
        catch(\UnexpectedValueException $e) {
            // Invalid payload
            echo 'Webhook error while parsing basic request.';
            http_response_code(400);
            exit();
        }
        if ($endpoint_secret) {
            // Only verify the event if there is an endpoint secret defined
            // Otherwise use the basic decoded event
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            try {
                $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
                );
            } catch(\Stripe\Exception\SignatureVerificationException $e) {
                // Invalid signature
                echo 'Webhook error while validating signature.';
                http_response_code(400);
                exit();
            }
        }

        // Handle the event
        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                // Then define and call a method to handle the successful payment intent.
                // handlePaymentIntentSucceeded($paymentIntent);
                break;
            case 'payment_method.attached':
                $paymentMethod = $event->data->object; // contains a \Stripe\PaymentMethod
                // Then define and call a method to handle the successful attachment of a PaymentMethod.
                // handlePaymentMethodAttached($paymentMethod);
                break;
            case 'account.updated':
                $account = $event->data->object;
                $data = ['body'=>print_r($account,true)];
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom(['no-reply@tiphub.co' => 'tipHub']);
                //$compose->setTo($eventDetails->user->email);
                $compose->setTo('opnsrc.devlpr@gmail.com');
                $compose->setSubject('Webook tiphub');
                $compose->send();
                break;
            case 'account.application.authorized':
                $application = $event->data->object;
                break;
            case 'account.application.deauthorized':
                $application = $event->data->object;
                break;
            case 'account.external_account.created':
                $externalAccount = $event->data->object;
                break;
            case 'account.external_account.deleted':
                $externalAccount = $event->data->object;
                break;
            case 'account.external_account.updated':
                $externalAccount = $event->data->object;
                break;
            default:
                // Unexpected event type
                error_log('Received unknown event type');
        }
        http_response_code(200);
    }
    public function actionEdgePayPaymentSettings(){
        if(count($_REQUEST) > 0){
            $data = ['body'=>print_r($_REQUEST,true)];
            $compose = Yii::$app->mailer->compose('body', $data);
            $compose->setFrom(['no-reply@tiphub.co' => 'tipHub']);
            $compose->setTo(['opnsrc.devlpr@gmail.com','amrender.kumar1986@gmail.com']);
            $compose->setSubject('Tiphub - Edge Pay Webook');
            $compose->send();
        }
    }
    
}


