<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use yii\web\Response;

class CommonController extends Controller
{

    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => ['upload-documents', 'get-banners', 'get-iav-token', 'transfer-fund', 'save-funding-source-url', 'banking-enquiry', 'add-dwolla-customer', 'create-payment', 'check-subscription-setting', 'validate-passcode', 'get-paypal-config', 'get-edgepay-config', 'paypal-ipn', 'get-customer-phone', 'get-reference-id', 'process-edgepay-payment', 'load-edgepay-script'],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    //QueryParamAuth::className(),
                ],
            ];

        }
        /* $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'except' => ['get-banners'],
        ]; */
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        return $behaviors;
    }
    public function actionUpload()
    {
        $response = [];
        //print_r($_FILES);   die;
        if (isset($_FILES) && !empty($_FILES['filepond'])) {
            try {
                include "vendor/Upload.php";
                $handle = new \Upload($_FILES['filepond']);
                if ($handle->uploaded) {
                    $file_name = mt_rand(111111, 999999) . "-temp-" . Yii::$app->user->identity->id;
                    $handle->file_new_name_body = $file_name;
                    $handle->allowed = ['image/*'];
                    $handle->file_overwrite = true;
                    $handle->image_resize = true;
                    $handle->image_x = 144;
                    $handle->image_ratio_y = true;
                    $handle->process($_SERVER['DOCUMENT_ROOT'] . "/images/temp");
                    if ($handle->processed) {
                        $response = [
                            'success' => true,
                            'file_name' => $handle->file_dst_name,
                            /* 'file_name'=>[
                        'source'=>$handle->file_dst_name,
                        'options'=>[
                        'type'=>'local'
                        ]
                        ] */
                        ];
                        $handle->clean();
                    } else {
                        $response = [
                            'error' => true,
                            'message' => $e->getMessage(),
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionUploadDocuments()
    {
        $response = [];
        if (isset($_FILES) && !empty($_FILES['filepond'])) {
            try {
                include "vendor/Upload.php";
                $handle = new \Upload($_FILES['filepond']);
                if ($handle->uploaded) {
                    $file_name = mt_rand(111111, 999999) . "-temp";
                    $handle->file_new_name_body = $file_name;
                    $handle->allowed = ['image/*', 'application/*'];
                    $handle->file_overwrite = true;
                    if ($handle->image_src_x > 600) {
                        $handle->image_resize = true;
                        $handle->image_x = 600;
                        $handle->image_ratio_y = true;
                    }
                    $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp");
                    if ($handle->processed) {
                        $response = [
                            'success' => true,
                            'file' => [
                                'document_name_original' => $handle->file_src_name,
                                'document_name' => $handle->file_dst_name,
                                'extension' => $handle->file_src_name_ext,
                            ],
                        ];
                        $handle->clean();
                    } else {
                        Yii::$app->response->statusCode = 415;
                        $response = [
                            'type' => 'error',
                            'message' => $handle->error,
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionUploadBanner()
    {
        $response = [];
        if (isset($_FILES) && !empty($_FILES['filepond'])) {
            try {
                include "vendor/Upload.php";
                $handle = new \Upload($_FILES['filepond']);
                if ($handle->uploaded) {
                    $file_name = mt_rand(111111, 999999) . "-temp-" . Yii::$app->user->identity->id;
                    $handle->file_new_name_body = $file_name;
                    $handle->allowed = ['image/*'];
                    $handle->file_overwrite = true;
                    if ($handle->image_src_x > 2000) {
                        $handle->image_resize = true;
                        $handle->image_x = 1024;
                        $handle->image_ratio_y = true;
                    }
                    $handle->process($_SERVER['DOCUMENT_ROOT'] . "/images/temp");
                    if ($handle->processed) {
                        $response = [
                            'success' => true,
                            'file' => [
                                'file_name_original' => $handle->file_src_name,
                                'file_name' => $handle->file_dst_name,
                                'extension' => $handle->file_src_name_ext,
                            ],
                        ];
                        $handle->clean();
                    } else {
                        Yii::$app->response->statusCode = 415;
                        $response = [
                            'type' => 'error',
                            'message' => $handle->error,
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionUploadEventImage()
    {
        $response = [];
        if (isset($_FILES) && !empty($_FILES['image'])) {
            try {
                include "vendor/Upload.php";
                $handle = new \Upload($_FILES['image']);
                //$handle->file_max_size = '30M';
                if ($handle->uploaded) {
                    $file_name = mt_rand(111111, 999999) . "-temp-";
                    $handle->file_new_name_body = $file_name;
                    $handle->allowed = ['image/*'];
                    $handle->file_overwrite = true;
                    if ($handle->image_src_x > 2000) {
                        $handle->image_resize = true;
                        $handle->image_x = 1024;
                        $handle->image_ratio_y = true;
                    }
                    $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp");
                    if ($handle->processed) {
                        $response = [
                            'success' => true,
                            'file' => [
                                'file_name_original' => $handle->file_src_name,
                                'file_name' => $handle->file_dst_name,
                                'extension' => $handle->file_src_name_ext,
                            ],
                        ];
                        $handle->clean();
                    } else {
                        Yii::$app->response->statusCode = 415;
                        $response = [
                            'error' => true,
                            'message' => $handle->error,
                        ];
                    }
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionUploadQrCode()
    {
        //ini_set('memory_limit', '-1');
        $response = [];
        if (isset($_FILES) && !empty($_FILES['filepond'])) {
            //try {
            include "vendor/Upload.php";
            $handle = new \Upload($_FILES['filepond']);
            if ($handle->uploaded) {
                $file_name = mt_rand(111111, 999999) . "-qrcode-" . Yii::$app->user->identity->id;
                $handle->file_new_name_body = $file_name;
                $handle->allowed = ['image/*'];
                $handle->file_overwrite = true;
                //print_r($handle);die;
                if ($handle->image_src_x > 800) {
                    //die($handle->image_src_x." I am here.");
                    $handle->image_resize = true;
                    $handle->image_x = 800;
                    $handle->image_ratio_y = true;
                }
                $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes");
                if ($handle->processed) {
                    $model = new \app\models\Qrcodes;
                    $model->title = $_REQUEST['title'];
                    $model->address = $_REQUEST['address'];
                    $model->user_id = Yii::$app->user->identity->id;
                    $model->image = $handle->file_dst_name;
                    if ($model->save()) {
                        $response = [
                            'success' => true,
                            'file' => [
                                'file_name_original' => $handle->file_src_name,
                                'file_name' => $handle->file_dst_name,
                                'extension' => $handle->file_src_name_ext,
                            ],
                            'qrcodes' => \app\models\Qrcodes::find()->where(['user_id' => Yii::$app->user->identity->id])->asArray()->all()
                        ];
                        $handle->clean();
                    } else {
                        $response = [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                } else {
                    $response = [
                        'type' => 'error',
                        'message' => $handle->error,
                    ];
                }
            }
            /* } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            } */
        }
        return $response;
    }


    public function actionSaveDocument()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $file = $_POST['file'];
                $document_details = $_POST['fields'];
                $file_details = array_merge($file, $document_details);
                return [
                    'success' => true,
                    'file_details' => $file_details,
                ];

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionGetCountries()
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['name' => SORT_ASC],
        ]);

        $model = new \app\models\Countries;
        $query = $model->find()->joinWith(['states']);
        $find = $query->orderBy($sort->orders)->asArray()->all();

        if ($find) {
            $response = [
                'success' => true,
                'countries' => $find,
            ];
        }
        return $response;
    }
    public function actionGetStates()
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'state' => [
                    'asc' => ['state' => SORT_ASC],
                    'desc' => ['state' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['state' => SORT_ASC],
        ]);

        $model = new \app\models\States;
        $find = $model->find()->where(['country_id' => 254])->orderBy($sort->orders)->asArray()->all();

        return [
            'success' => true,
            'states' => $find,
        ];
    }

    public function actionGetIavToken()
    {
        if (isset($_POST) && !empty($_POST)) {
            \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
            \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];

            $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
            $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
            $appToken = $tokensApi->token();
            \DwollaSwagger\Configuration::$access_token = $appToken->access_token;

            $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
            $fsToken = $customersApi->getCustomerIavToken($_POST['dwollaCustomerUrl']);
            return [
                'success' => true,
                'iavToken' => $fsToken->token
            ];
        }
    }

    public function actionSendBankResponse()
    {
        $compose = Yii::$app->mailer->compose('body', [
            'body' => $_POST['bankResponse'],
            'case' => 12
        ]);
        $compose->setFrom(['no-reply@tiphub.co' => 'Tiphub']);
        $compose->setTo('opnsrc.devlpr@gmail.com');
        $compose->setSubject('Bank response');
        if ($compose->send()) {
            $response = [
                'success' => true,
                'message' => 'Bank response sent',
            ];
        }
    }

    public function actionSaveFundingSourceUrl()
    {
        if (isset($_POST) && !empty($_POST)) {
            //print_r($_POST['fundingSourceUrl']);die;
            $model = new \app\models\Members;
            $find = $model->findOne(['id' => $_POST['memberId']]);
            if ($find) {
                $find->funding_source_url = $_POST['fundingSourceUrl']['_links']['funding-source']['href'];
                if ($find->save()) {
                    return [
                        'success' => true,
                        'message' => 'Bank has been verified successfully.',
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $find->getErrors(),
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Member not found.',
                ];
            }
        }
    }

    public function actionAddDwollaCustomer()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
                $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);

                $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                $appToken = $tokensApi->token();
                \DwollaSwagger\Configuration::$access_token = $appToken->access_token;

                $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
                $customerDetails = $customersApi->create([
                    'firstName' => $_POST['fields']['first_name'],
                    'lastName' => $_POST['fields']['last_name'],
                    'email' => $_POST['fields']['email'],
                    'ipAddress' => Yii::$app->request->userIP
                ]);
                return [
                    'success' => true,
                    'message' => 'You have been registered successfully.',
                    'dwollaCustomerUrl' => $customerDetails,
                ];
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    //'message' => $e->getResponseBody(),
                ];
            }
        }
    }
    public function actionTransferFund()
    {
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['fields']['eventId']) && !empty($_POST['fields']['eventId'])) {
                $model = new \app\models\Events;
                $find = $model->find()->joinWith(['user.beneficial'])->where(['events.id' => $_POST['fields']['eventId']])->one();
            } else if (isset($_POST['fields']['userId']) && !empty($_POST['fields']['userId'])) {
                $model = new \app\models\User;
                $find = $model->find()->joinWith(['beneficial'])->where(['user.id' => $_POST['fields']['userId']])->one();
            }

            if ($find) {
                if (isset($_POST['fields']['eventId']) && !empty($_POST['fields']['eventId'])) {
                    $destination_funding_source_url = $find->user->beneficial->funding_source_url;
                } else if (isset($_POST['fields']['userId']) && !empty($_POST['fields']['userId'])) {
                    $destination_funding_source_url = $find->beneficial->funding_source_url;
                }

                try {
                    \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                    \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];

                    $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
                    $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                    $appToken = $tokensApi->token();
                    \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
                    \DwollaSwagger\Configuration::$debug = 1;

                    $transfer_request = [
                        '_links' => [
                            'source' => ['href' => $_POST['fields']['funding_source_url']['_links']['funding-source']['href']],
                            'destination' => ['href' => $destination_funding_source_url],
                        ],
                        'amount' => [
                            'currency' => 'USD',
                            'value' => $_POST['fields']['amount'],
                        ]
                    ];
                    //print_r($transfer_request);die;
                    $transferApi = new \DwollaSwagger\TransfersApi($apiClient);
                    $transfer = $transferApi->create($transfer_request);
                    return [
                        'success' => true,
                        //'find'=>$find,
                        'transfer' => $transfer
                    ];
                } catch (\Exception $e) {
                    return [
                        'error' => true,
                        'message' => $e->getResponseBody(),
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Dwolla settings not found.',
                ];
            }
        }
    }

    public function actionBankingEnquiry()
    {
        if (isset($_POST) && !empty($_POST)) {
            $model = new \app\models\BankingEnquiries;
            $POST['BankingEnquiries'] = $_POST['fields'];
            if ($model->load($POST) && $model->validate() && $model->save()) {
                return [
                    'success' => true,
                    'message' => 'Bank enquiry form submitted successfully.',
                ];
            } else {
                return [
                    'error' => true,
                    'message' => $model->getErrors(),
                ];
            }
        }
    }

    public function actionCreatePayment()
    {
        if (isset($_POST) && !empty($_POST)) {
            $email = trim($_POST['email']);
            $name = trim($_POST['name']);
            $customer_id = $product_id = $price_id = $subscriptionData = null;
            try {
                $userSignup = false;
                if (isset($_POST['event_id']) && !empty($_POST['event_id'])) {
                    $model = new \app\models\Events;
                    $find = $model->find()->joinWith(['user'])->where(['events.id' => $_POST['event_id']])->one();
                    $stripe_destination_account = $find->user->stripe_connect_account_id;
                    $product_id = $find->stripe_product_id;
                } else if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
                    $model = new \app\models\User;
                    $find = $model->find()->where(['id' => $_POST['user_id']])->one();
                    $stripe_destination_account = $find->stripe_connect_account_id;
                }

                if ($find) {
                    $customer_id = Yii::$app->common->createStripeCustomer($_POST['payment_method_id'], $email, $name, $_POST['subscription']);
                    $amount = $_POST['amount'] * 100; //Cents conversion
                    $application_fee = ($amount * .029) + 30;
                    $amount_to_be_sent = $amount - $application_fee;
                    \Stripe\Stripe::setApiKey(Yii::$app->params['stripeSecretKey']);
                    $intent = \Stripe\PaymentIntent::create([
                        'amount' => $amount,
                        //Cents conversion
                        'currency' => 'USD',
                        'automatic_payment_methods' => [
                            'enabled' => 'true',
                        ],
                        'description' => $find->title,
                        'payment_method' => $_POST['payment_method_id'],
                        'application_fee_amount' => ceil($application_fee),
                        'transfer_data' => [
                            'destination' => $stripe_destination_account,
                        ],
                        'confirm' => true,
                        'customer' => $customer_id,
                        'return_url' => Yii::$app->params['apiUrl'] . '/site/stripe-connect-return',
                    ]);
                    if ($intent->status == 'succeeded') {
                        if (isset($_POST['event_id']) && !empty($_POST['event_id'])) {
                            $tmodel = new \app\models\Transactions;
                            $tmodel->event_id = $_POST['event_id'];
                            $tmodel->added_by = $find->user->id;
                            $tmodel->service_id = 65;
                            $tmodel->name = $name;
                            $tmodel->amount = $amount_to_be_sent / 100;
                            // If user go with subscription then create price in stripe;
                            if (isset($_POST['subscription']) && !empty($_POST['subscription']) && $_POST['subscription'] == true) {
                                if ($_POST['recurringCycle'] == 1) {
                                    $recurringCycle = 'day';
                                } else if ($_POST['recurringCycle'] == 2) {
                                    $recurringCycle = 'week';
                                } else if ($_POST['recurringCycle'] == 3) {
                                    $recurringCycle = 'month';
                                } else if ($_POST['recurringCycle'] == 4) {
                                    $recurringCycle = 'year';
                                }
                                $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
                                $price = $stripe->prices->create([
                                    'unit_amount' => $amount,
                                    'currency' => 'usd',
                                    'recurring' => ['interval' => $recurringCycle],
                                    'product' => $product_id,
                                    'metadata' => [
                                        'event_id' => $tmodel->event_id,
                                    ],
                                ]);
                                if ($price) {
                                    $tmodel->stripe_price_id = $price_id = $price->id;
                                }
                            }

                            if ($tmodel->save()) {
                                $pmodel = new \app\models\Payments;
                                $pmodel->payment_id = $intent->id;
                                //$pmodel->payment_identifier = ; 
                                $pmodel->txn_amount = $tmodel->amount;
                                $pmodel->payment_mode = "Credit Card";
                                $pmodel->received_at = date("Y-m-d H:i:s");
                                $pmodel->email = $email;
                                $pmodel->name = $tmodel->name;
                                $pmodel->payment_service = 65;
                                $pmodel->type = 1; // 1=>Payment Received, 2 =>Payment sent 3 => Unknown; 
                                $pmodel->transaction_id = $tmodel->id;
                                $pmodel->status = 1;
                                $pmodel->data_dump = json_encode($intent);
                                if ($pmodel->save()) {
                                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 7])->one();
                                    $eventDetails = \app\models\Events::find()->joinWith(['user'])->where(['events.id' => $tmodel->event_id])->one();
                                    $data = [
                                        'case' => 7,
                                        'name' => $tmodel->name,
                                        'currency' => '$',
                                        'amount' => $tmodel->amount,
                                        'date' => date("M d, Y"),
                                        'eventDetails' => $eventDetails,
                                        'paymentDetails' => $pmodel,
                                        'templateDetails' => $templateDetails
                                    ];
                                    $compose = Yii::$app->mailer->compose('body', $data);
                                    $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                    $compose->setTo($pmodel->email);
                                    //$compose->setTo('opnsrc.devlpr@gmail.com');
                                    $compose->setSubject(str_replace('[payment-identifier]', '', $templateDetails->subject));
                                    if ($compose->send()) {
                                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 19])->one();
                                        $data = [
                                            'case' => 7,
                                            'name' => $eventDetails->user->name,
                                            'amount' => '$' . $tmodel->amount,
                                            'eventDetails' => $eventDetails,
                                            'templateDetails' => $templateDetails
                                        ];
                                        $compose = Yii::$app->mailer->compose('body', $data);
                                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                        $compose->setTo($eventDetails->user->email);
                                        //$compose->setTo('opnsrc.devlpr@gmail.com');
                                        $compose->setSubject($templateDetails->subject);
                                        if ($compose->send()) {

                                            if (isset($_POST['subscription']) && !empty($_POST['subscription']) && $_POST['subscription'] == true) {
                                                //echo $product_id.'~'.$price_id.'~'.$customer_id;die;
                                                if ($product_id != null && $price_id != null && $customer_id != null) {
                                                    $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
                                                    $subscriptionData = $stripe->subscriptions->create([
                                                        'customer' => $customer_id,
                                                        'items' => [
                                                            ['price' => $price_id],
                                                        ],
                                                        'transfer_data' => [
                                                            'destination' => $stripe_destination_account
                                                        ]
                                                    ]);

                                                }
                                            }
                                            return [
                                                'success' => true,
                                                'payment' => $intent,
                                                'message' => 'Payment succeeded successfully.',
                                                'userSignup' => $userSignup,
                                                'subscriptionData' => $subscriptionData
                                            ];
                                        }
                                    }
                                } else {
                                    return [
                                        'error' => true,
                                        'message' => $pmodel->getErrors()
                                    ];
                                }
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $tmodel->getErrors()
                                ];
                            }
                        } else if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
                            $pmodel = new \app\models\Payments;
                            $pmodel->payment_id = $intent->id;
                            $pmodel->user_id = $_POST['user_id'];
                            $pmodel->txn_amount = $amount_to_be_sent;
                            $pmodel->payment_mode = "Credit Card";
                            $pmodel->received_at = date("Y-m-d H:i:s");
                            $pmodel->email = trim($_POST['email']);
                            $pmodel->name = trim($_POST['name']);
                            $pmodel->payment_service = 65;
                            $pmodel->type = 1; // 1=>Payment Received, 2 =>Payment sent 3 => Unknown; 
                            $pmodel->status = 1;
                            $pmodel->data_dump = json_encode($intent);
                            if ($pmodel->save()) {
                                $model = new \app\models\User;
                                $emailArr = explode('@', $pmodel->email);
                                if ($model->find()->where(['email' => $pmodel->email])->orWhere(['username' => $emailArr[0]])->count() == 0) {
                                    $model = new \app\models\User;
                                    $emailArr = explode('@', $pmodel->email);
                                    $model->username = $emailArr[0];
                                    $model->email = $pmodel->email;
                                    $model->status = 'Y';
                                    $model->name = $pmodel->name;
                                    $model->password_hash = Yii::$app->security->generatePasswordHash('tiphub@123');
                                    if ($model->save()) {
                                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 20])->one();
                                        $data = [
                                            'case' => 20,
                                            'name' => $pmodel->name,
                                            'username' => $emailArr[0],
                                            'password' => 'tiphub@123',
                                            'body' => $templateDetails->content
                                        ];
                                        $compose = Yii::$app->mailer->compose('body', $data);
                                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                        $compose->setTo($model->email);
                                        //$compose->setTo('opnsrc.devlpr@gmail.com');
                                        $compose->setSubject($templateDetails->subject);
                                        if ($compose->send()) {
                                            $userSignup = true;
                                        }
                                    }
                                }
                                return [
                                    'success' => true,
                                    'payment' => $intent,
                                    'message' => 'Payment succeeded successfully.',
                                    'userSignup' => $userSignup
                                ];
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $pmodel->getErrors()
                                ];
                            }
                        } else {
                            return [
                                'error' => true,
                                'message' => 'Something missing with active record model.',
                            ];
                        }
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event or user not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }

        }
    }
    public function actionGetStripeApiKey()
    {
        return [
            'success' => true,
            'stripeApiKey' => Yii::$app->params['stripeApiKey'],
        ];
    }

    public function actionCheckSubscriptionSetting($event_id = null)
    {
        if (isset($event_id) && !empty($event_id)) {
            $find = \app\models\EventApps::find()->where(['event_id' => $event_id, 'service_id' => 65, 'recurring' => 1])->one();
            if ($find) {
                return [
                    'success' => true,
                    'recurringCycle' => $find->recurring_cycle
                ];
            } else {
                return [
                    'error' => true,
                ];
            }
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function actionValidatePasscode()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $find = \app\models\Events::findOne($_POST['fields']['event_id']);
                if ($find) {
                    if (Yii::$app->security->validatePassword($_POST['fields']['passcode'], $find->event_passcode)) {
                        return [
                            'success' => true,
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Invalid passcode!',
                        ];
                    }
                }


            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionGetEdgepayConfig($user_id = null)
    {
        try {
            $model = new \app\models\UsersEdgePayConfigs;
            $find = $model->find()->where(['user_id' => $user_id])->asArray()->one();

            if ($find) {
                return [
                    'success' => true,
                    'record' => $find,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'Configuration not found!',
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }
    public function actionGetPaypalConfig($event_id = "", $user_id = "")
    {
        if (!empty($event_id)) {
            $model = new \yii\db\Query();
            $model = $model->from('events')
                ->select(['PC.*'])
                ->innerJoin('users_paypal_configs as PC', 'PC.user_id = events.user_id')
                ->where(['events.id' => $event_id])->createCommand()->queryOne();
        } else if (!empty($user_id)) {
            $model = new \yii\db\Query();
            $model = $model->from('users_paypal_configs')
                ->where(['user_id' => $user_id])->createCommand()->queryOne();
        }

        //echo $model->createCommand()->getRawSql();die;

        if ($model) {
            return [
                'success' => true,
                'client_id' => Yii::$app->params['clientId'],
                'paypal_merchant_id' => $model['paypal_merchant_id'],
                'BN_Code' => Yii::$app->params['BN_Code'],
                'guid' => sprintf(
                    '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                    // 32 bits for "time_low"
                    mt_rand(0, 0xffff),
                    mt_rand(0, 0xffff),

                    // 16 bits for "time_mid"
                    mt_rand(0, 0xffff),

                    // 16 bits for "time_hi_and_version",
                    // four most significant bits holds version number 4
                    mt_rand(0, 0x0fff) | 0x4000,

                    // 16 bits, 8 bits for "clk_seq_hi_res",
                    // 8 bits for "clk_seq_low",
                    // two most significant bits holds zero and one for variant DCE1.1
                    mt_rand(0, 0x3fff) | 0x8000,

                    // 48 bits for "node"
                    mt_rand(0, 0xffff),
                    mt_rand(0, 0xffff),
                    mt_rand(0, 0xffff)
                ),
                'page_id' => mt_rand(11111, 99999)
            ];
        }

        return [
            'error' => true,
            'message' => "It seems Event Organizer has not linked Paypal account. Kindly contact administrator."
        ];

    }

    public function actionPaypalIpn()
    {
        //print_r($_POST);
        $json = json_decode(json_encode($_POST));
        $transaction = Yii::$app->db->beginTransaction();

        try {
            //echo  $json->event_type."=>>".$json->resource->status;

            if (isset($json->event_type) && $json->event_type == "CHECKOUT.ORDER.COMPLETED" && ($json->resource->status == "COMPLETED" || $json->resource->status == "completed")) {

                // Capturing whole data in case the operations fail further
                $failedPayments = new \app\models\FailedPayments;
                $payment_identifier = isset($json->resource->id) ? $json->resource->id : "";
                $failedPayments->payment_identifier = $payment_identifier;
                $failedPayments->data_dump = json_encode($json);
                $failedPayments->added_on = date('Y-m-d H:i:s');
                $failedPayments->save();

                $event_id = "";
                $currency = "0";
                $currencyArr = Yii::$app->params['currency'];
                $purchase_details = $json->resource->purchase_units[0];
                $seller_receivables = isset($purchase_details->payments->captures[0]->seller_receivable_breakdown) ? $purchase_details->payments->captures[0]->seller_receivable_breakdown : "";
                //$request_status = isset($json->status) ? $json->status : "";
                $platform_fees = isset($purchase_details->payment_instruction->platform_fees[0]->amount->value) ? $purchase_details->payment_instruction->platform_fees[0]->amount->value : 0;
                $amount = $purchase_details->amount->value;
                $currency = $purchase_details->amount->currency_code;
                $custom_id = $purchase_details->custom_id;
                $payer_name = $json->resource->payer->name->given_name . " " . $json->resource->payer->name->surname;
                $email = $json->resource->payer->email_address;
                //$payment_identifier = $json->resource->id;

                $booking_id = $invoice_id = $event_id = $user_id = $phone = $display_name = '';
                $program_id = 0;
                if ($custom_id != '') {
                    $custom_id = json_decode($custom_id);
                    if (isset($custom_id->event_id)) {
                        $event_id = $custom_id->event_id;
                    }
                    if (!empty($custom_id->program_id)) {
                        $program_id = $custom_id->program_id;
                    }
                    if (isset($custom_id->booking_id)) {
                        $booking_id = $custom_id->booking_id;
                    }
                    if (isset($custom_id->phone)) {
                        $phone = $custom_id->phone;
                    }
                    if (isset($custom_id->display_name)) {
                        $display_name = $custom_id->display_name;
                    }

                    if (isset($custom_id->invoice_id)) {
                        $invoice_id = $custom_id->invoice_id;
                        $invoiceModel = new \app\models\EventInvoice;
                        $findInvoice = $invoiceModel->find()->where(['id' => $invoice_id])->one();
                        if ($findInvoice) {
                            $findInvoice->status = 1;
                            $findInvoice->save();
                        }
                    }
                    if (isset($custom_id->user_id)) {
                        $user_id = $custom_id->user_id;
                    }
                    if ($booking_id != '') {
                        $etbModel = new \app\models\EventTicketBookings;
                        $findEtbModel = $etbModel->find()->joinWith(['details.ticket', 'event.user'])->where(['event_ticket_bookings.id' => $booking_id])->one();
                        if ($findEtbModel) {
                            $findEtbModel->first_name = $json->resource->payer->name->given_name;

                            $findEtbModel->last_name = $json->resource->payer->name->surname;
                            $findEtbModel->email = $email;
                            $findEtbModel->status = 1;
                            $findEtbModel->save();

                            // send ticket confirmation email
                            Yii::$app->common->sendTicketConfirmationEmail($findEtbModel);
                        }
                    }

                }

                // Checking if the payment is already inserted
                $payments = new \app\models\Payments;
                $payments = $payments->find()->where(['payment_identifier' => $payment_identifier])->one();
                if (!$payments) {
                    $currency = array_search($currency, $currencyArr);

                    $tmodel = new \app\models\Transactions;
                    $tmodel->event_id = $event_id;
                    $tmodel->program_id = $program_id;
                    $tmodel->amount = $amount - $platform_fees;
                    $tmodel->name = $payer_name;
                    $tmodel->added_on = date("Y-m-d H:i:s");
                    $tmodel->added_by = 0;
                    $tmodel->service_id = 73; // Credit Card / Debit card
                    $tmodel->currency = $currency; // USD
                    $tmodel->display_name = $display_name;

                    if ($tmodel->save()) {
                        // Hitting Order API to get the Phone number from Paypal
                        if ($phone == '') {
                            $phone = $this->getCustomerPhone($payment_identifier);
                        }
                        $pmodel = new \app\models\Payments;
                        $pmodel->txn_amount = $amount - $platform_fees;
                        $pmodel->received_at = date("Y-m-d H:i:s");
                        $pmodel->name = $payer_name;
                        $pmodel->email = $email;
                        $pmodel->payment_service = 73; // Credit Card / Debit card
                        $pmodel->payment_identifier = $payment_identifier;
                        $pmodel->payment_id = $payment_identifier;
                        $pmodel->type = 1;
                        $pmodel->transaction_id = $tmodel->id;
                        $pmodel->event_id = $event_id;
                        $pmodel->invoice_id = $invoice_id;
                        $pmodel->user_id = $user_id;
                        $pmodel->currency = $currency; // USD
                        $pmodel->platform_charges = $platform_fees;
                        $pmodel->phone = $phone;


                        if (!empty($seller_receivables)) {
                            $pmodel->gateway_charges = $seller_receivables->paypal_fee->value;
                            $pmodel->net_amount = $seller_receivables->net_amount->value;
                            $pmodel->platform_charges = isset($seller_receivables->platform_fees) ? $seller_receivables->platform_fees[0]->amount->value : 0;
                        }

                        $pmodel->status = 1;
                        $pmodel->data_dump = json_encode($json);
                        $pmodel->save();

                        // Since the payment is succeeded here, delete from failed payments table
                        $failedPayments->delete();
                    }
                    //print_r($tmodel);
                    $transaction->commit(); // Saving all records
                    $this->sendTestEmail(['amrender.kumar1986@gmail.com'], "<pre>" . json_encode($json) . "</pre>", "Paypal TipHub response");

                } else {
                    $this->sendTestEmail(['amrender.kumar1986@gmail.com'], "<pre>" . json_encode($json) . "</pre>", "Duplicate Paypal Payment response");
                }
            }
        } catch (Exception $e) {
            // # if error occurs then rollback all transactions
            //echo $e->getMessage();
            $this->sendTestEmail(['amrender.kumar1986@gmail.com'], json_encode($json), "Catch Error Paypal" . $e->getMessage());
            $transaction->rollBack();
        }

    }

    // Function to get Phone number of the customer via Order API of Paypal
    private function getCustomerPhone($orderId = "")
    {
        //$orderId = "4AU07388PC394014P";
        try {
            $access_token = Yii::$app->common->getPaypalAccessToken();
            if ($access_token != false) {
                // Hitting API for getting account linking URL
                $params = [
                    CURLOPT_URL => Yii::$app->params['paypalUrl'] . "v2/checkout/orders/" . $orderId,
                    CURLOPT_HTTPGET => true,
                    CURLOPT_HTTPHEADER => array("Content-Type: application/json", "PayPal-Partner-Attribution-Id: " . Yii::$app->params['BN_Code'], "Authorization: Bearer " . $access_token)
                ];
                //print_r($params);die;
                $response = Yii::$app->common->setCurlParams($params);

                if ($response['error'] == true) {
                    return false;
                } else {
                    $data = $response['response'];
                    $countries = Yii::$app->params['countries'];
                    $country_code = isset($data->payer->address->country_code) ? $countries[$data->payer->address->country_code]['code'] : "+1";
                    $phone = isset($data->payer->phone->phone_number->national_number) ? "+" . $country_code . $data->payer->phone->phone_number->national_number : "";
                    return $phone;
                }
            }
        } catch (Exception $e) {
            // # if error occurs then rollback all transactions
            //echo $e->getMessage();
            //$this->sendTestEmail(['amrender.kumar1986@gmail.com'],json_encode($json),"Catch Error Paypal".$e->getMessage());
            //$transaction->rollBack();
        }
    }
    function sendTestEmail($to, $body, $subject = "")
    {
        $mailer = \Yii::createObject([
            'class' => \Yii::$app->params['mailerSettings']['class'],
            //'viewPath' => '@common/mail',
            'transport' => [
                'class' => \Yii::$app->params['mailerSettings']['transport']['class'],
                'host' => \Yii::$app->params['mailerSettings']['transport']['host'],
                'username' => \Yii::$app->params['mailerSettings']['transport']['username'],
                'password' => \Yii::$app->params['mailerSettings']['transport']['password'],
                'port' => \Yii::$app->params['mailerSettings']['transport']['port'],
                'encryption' => \Yii::$app->params['mailerSettings']['transport']['encryption'],
            ],
        ]);

        $compose = $mailer->compose(['html' => 'body'], [
            'body' => $body
        ]);
        $compose->setFrom(['no-reply@tiphub.co' => 'Tiphub.Co']);
        $compose->setTo($to);
        $compose->setSubject($subject);
        $compose->send();
    }

    public function actionGetReferenceId()
    {
        if (isset($_POST['user_id']) && $_POST['user_id'] != null) {
            $model = new \app\models\UsersEdgePayConfigs;
            $find = $model->find()->where(['user_id' => $_POST['user_id']])->one();
            if ($find) {
                $url = 'https://api.edgepay-uat.com/authKey';
                //$url = 'http://testgettrx.free.beeceptor.com';
                $curl = curl_init();
                $fields = json_encode([
                    'merchantID' => Yii::$app->params['merchantIdSandBox'],
                    'terminalID' => Yii::$app->params['terminalIdSandBox'],
                    'sID' => $find->sID
                    /* 'merchantID' => '88803072023',
                    'terminalID' => '88800000282601' */
                ]);
                curl_setopt($curl, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    //'merchantKey: 8AC802A8F1107FAE15B529761A92C7F14676B3AB4A5483AB',
                    'merchantKey: ' . Yii::$app->params['merchantKeySandBox'],
                    'externalReferenceID: ' . strtoupper(Yii::$app->getSecurity()->generateRandomString())
                ]);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($curl);
                curl_close($curl);
                if ($result) {
                    return [
                        'success' => true,
                        'result' => json_decode($result),
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Payment failed.'
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'EdgePay configuration not found for this event organiser.'
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'User does not exist.'
            ];
        }
    }

    public function actionProcessEdgepayPayment()
    {
        if (isset($_POST) && !empty($_POST)) {
            $model = new \app\models\UsersEdgePayConfigs;
            $find = $model->find()->where(['user_id' => $_POST['user_id']])->one();
            if ($find) {
                $url = 'https://api.edgepay-uat.com/payment';
                $curl = curl_init();
                $fields = json_encode([
                    'merchantID' => Yii::$app->params['merchantIdSandBox'],
                    'terminalID' => Yii::$app->params['terminalIdSandBox'],
                    'sID' => $find->sID,
                    'amount' => $_POST['amount'],
                    //'tip' => $_POST['tip'],
                    'tokenID' => $_POST['tokenId'],
                    //'billingAddress' => $_POST['billingAddress'],
                    'billingZip' => $_POST['billingZip'],
                    'paymentDataInput' => 'INTERNET',
                    //'currencyCode' => $_POST['currencyCode'],
                    'captureToo' => 'no',
                    'payFac' => 'Yes',
                    'purchaseOrder' => strtoupper(Yii::$app->getSecurity()->generateRandomString()),
                    "acctSplits" => [
                        //["accountID" => $find->merchant_id, "amount" => $_POST['tip']],
                        ["accountID" => Yii::$app->params['merchantIdSandBox'], "amount" => ($_POST['amount'] - $_POST['tip'])]
                    ]
                ]);
                curl_setopt($curl, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                    'merchantKey: ' . Yii::$app->params['merchantKeySandBox'],
                    'transactionDate: ' . $_POST['transactionDate'],
                    'externalReferenceID: ' . rand(1111111111111111, 9999999999999999),
                ]);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($curl);
                curl_close($curl);
                if ($result) {
                    $result = json_decode($result, true);
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if ($result['result'] == 'A') {
                            // Capturing whole data in case the operations fail further
                            $failedPayments = new \app\models\FailedPayments;
                            $failedPayments->payment_identifier = $result['transactionID'];
                            $failedPayments->data_dump = json_encode($result);
                            $failedPayments->added_on = date('Y-m-d H:i:s');
                            $failedPayments->save();

                            $currency = "USD";
                            $currencyArr = Yii::$app->params['currency'];

                            $booking_id = $invoice_id = $event_id = $phone = $display_name = '';
                            $program_id = 0;
                            if (isset($_POST['event_id'])) {
                                $event_id = $_POST['event_id'];
                            }
                            if (!empty($_POST['program_id'])) {
                                $program_id = $_POST['program_id'];
                            }
                            if (isset($_POST['booking_id'])) {
                                $booking_id = $_POST['booking_id'];
                            }
                            if (isset($_POST['phone'])) {
                                $phone = $_POST['phone'];
                            }
                            if (isset($_POST['email'])) {
                                $email = $_POST['email'];
                            }
                            if (isset($_POST['display_name'])) {
                                $display_name = $_POST['display_name'];
                            }
                            if (isset($_POST['invoice_id'])) {
                                $invoice_id = $_POST['invoice_id'];
                                $invoiceModel = new \app\models\EventInvoice;
                                $findInvoice = $invoiceModel->find()->where(['id' => $invoice_id])->one();
                                if ($findInvoice) {
                                    $findInvoice->status = 1;
                                    $findInvoice->save();
                                }
                            }
                            if ($booking_id != '') {
                                $etbModel = new \app\models\EventTicketBookings;
                                $findEtbModel = $etbModel->find()->joinWith(['details.ticket', 'event.user'])->where(['event_ticket_bookings.id' => $booking_id])->one();
                                if ($findEtbModel) {
                                    if (empty($findEtbModel->first_name)) {
                                        $display_name = explode(' ', $_POST['display_name']);
                                        $findEtbModel->first_name = $display_name[0];
                                        $findEtbModel->last_name = $display_name[1];
                                    }
                                    $findEtbModel->email = $email;
                                    $findEtbModel->status = 1;
                                    if ($findEtbModel->save()) {
                                        // send ticket confirmation email
                                        Yii::$app->common->sendTicketConfirmationEmail($findEtbModel);
                                    }
                                }
                            }
                            $currency = array_search($currency, $currencyArr);

                            $tmodel = new \app\models\Transactions;
                            $tmodel->event_id = $event_id;
                            $tmodel->program_id = $program_id;
                            $tmodel->amount = $_POST['amount'] - $_POST['tip'];
                            $tmodel->name = $display_name;
                            $tmodel->added_on = date("Y-m-d H:i:s");
                            $tmodel->added_by = 0;
                            $tmodel->service_id = 84; // Credit Card / Debit card
                            $tmodel->currency = $currency; // USD
                            $tmodel->display_name = $display_name;

                            if ($tmodel->save()) {
                                $pmodel = new \app\models\Payments;
                                $pmodel->txn_amount = $_POST['amount'] - $_POST['tip'];
                                $pmodel->received_at = date("Y-m-d H:i:s");
                                $pmodel->name = $display_name;
                                $pmodel->email = $email;
                                $pmodel->payment_service = 84; // Credit Card / Debit card
                                $pmodel->payment_identifier = $result['transactionID'];
                                $pmodel->payment_id = $result['transactionID'];
                                $pmodel->type = 1;
                                $pmodel->transaction_id = $tmodel->id;
                                $pmodel->event_id = $event_id;
                                $pmodel->invoice_id = $invoice_id;
                                $pmodel->user_id = $_POST['user_id'];
                                $pmodel->currency = $currency; // USD
                                $pmodel->platform_charges = $_POST['tip'];
                                $pmodel->phone = $phone;

                                $pmodel->status = 1;
                                $pmodel->data_dump = json_encode($result);
                                if ($pmodel->save()) {
                                    // Since the payment is succeeded here, delete from failed payments table
                                    $failedPayments->delete();
                                    $transaction->commit(); // Saving all records
                                    $this->sendTestEmail(['opnsrc.devlpr@gmail.com'], "<pre>" . print_r($result, true) . "</pre>", "EdgePay TipHub response");
                                    return [
                                        'success' => true,
                                        'message' => "Thank you for the payment.",
                                        'result' => $result
                                    ];
                                } else {
                                    return [
                                        'error' => true,
                                        'message' => $pmodel->getErrors()
                                    ];
                                }
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $tmodel->getErrors()
                                ];
                            }
                        }
                    } catch (Exception $e) {
                        // # if error occurs then rollback all transactions
                        //echo $e->getMessage();
                        $this->sendTestEmail(['opnsrc.devlpr@gmail.com'], json_encode($result), "Catch Error EdgePay" . $e->getMessage());
                        $transaction->rollBack();
                    }
                } else {
                    return [
                        'error' => true
                    ];
                }
            }
        }
    }

    public function actionLoadEdgepayScript()
    {
        return [
            'success' => true,
            'url' => Yii::$app->params['edgePayScript']['applePay']['sandbox']
        ];
    }

}