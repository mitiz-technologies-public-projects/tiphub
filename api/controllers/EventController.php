<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\Response;
use xstreamka\mobiledetect\Device;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use app\models\EventTickets;
use Carbon\Carbon;
use app\models\MembershipDueForms;
use app\models\Transactions;
use app\models\Members;
use yii\helpers\ArrayHelper;

class EventController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
        //$_GET['fields'] = json_decode($_GET['fields'], true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => [
                    'get',
                    'track-event-app',
                    'get-supporters',
                    'get-updates',
                    'save-base-incoded-image',
                    'get-gallery', 'search',
                    'save-donor-details',
                    'check-donor',
                    'get-apps',
                    'get-featured', 
                    'save-donor-details',
                    'get-categories', 
                    'get-basic-details', 
                    'save-mutual-aid-request', 
                    'get-plans',
                    'get-event-members',
                    'get-members',
                    'test-payment',
                    'get-membership-due-form',
                    'save-member',
                    'save-sold-ticket', 
                    'view-ticket-details',
                    'get-groups',
                    'save-group-payment',
                    'get-leadership-board',
                    'link-member',
                    'get-all-event-members',
                    'send-mail-payment-reminder',
                    'get-tickets-with-details',
                    'get-programs'
                ],//'paypal-return-response'
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'except' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        //return Yii::$app->common->checkPermission('Event', $action->id);
                        return true;
                    },
                ],
            ],
        ];
        return $behaviors;
    }


    //Label~Module~Action~Url~Icon:Add/Edit Event~Event~add~users~users~1
    public function actionAdd()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Events;
                $POST['Events'] = $_POST['fields'];
                //echo date("d-m-y H:i a",strtotime($POST['Events']['state_date_custom_format']));die;
                $POST['Events']['user_id'] = Yii::$app->user->identity->id;
                //if() 
                if (isset($POST['Events']['start_date_custom_format'])) {
                    $POST['Events']['start_date'] = date("Y-m-d H:i:s", strtotime($POST['Events']['start_date_custom_format']));
                }
                if (isset($POST['Events']['recurring']) && $POST['Events']['recurring'] == "2") {
                    $POST['Events']['end_date'] = NULL;
                } else if (isset($POST['Events']['end_date_custom_format'])) {
                    $POST['Events']['end_date'] = date("Y-m-d H:i:s", strtotime($POST['Events']['end_date_custom_format']));
                }
                if (isset($POST['Events']['location']) && !empty($POST['Events']['location'])) {
                    $POST['Events']['location'] = $POST['Events']['location']['value'];
                }
                if (isset($POST['Events']['category']) && $POST['Events']['category'] == 70) {
                    $POST['Events']['show_name'] = 2;
                }
                $POST['Events']['url'] = preg_replace('![^a-z0-9]+!i', '-', strtolower(trim($POST['Events']['title'])));
                if ($model->load($POST)) {
                    if (is_array($model->image)) {
                        //print_r($find->image['file_name']);die;
                        $temp_file_name = $model->image['file_name'];
                        if (strpos($model->image['file_name'], "temp") >= 0) {
                            //$new_file_name = mt_rand(111111, 999999) . "." . $model->image['extension'];
                            $new_file_name = mt_rand(111111, 999999) . "." . $model->image['extension'];
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $model->image['file_name'])) {
                                copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $model->image['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                $POST['Events']['image'] = $new_file_name;
                                unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                            }
                        }
                    }
                    if (isset($model->id)) {
                        $find = $model->find()->where(['id' => $model->id, 'user_id' => Yii::$app->user->identity->id])->one();
                        if ($find && $find->load($POST)) {
                            if(isset($find->event_type) && $find->event_type == 1 && !empty($find->passcode)){
                                $find->event_passcode = Yii::$app->security->generatePasswordHash($find->passcode);
                            }
                            if ($find->save()) {
                                if (isset($POST['Events']['plans']) && !empty($POST['Events']['plans'])) {
                                    foreach ($POST['Events']['plans'] as $plan) {
                                        $pmodel = new \app\models\MembershipPlans;
                                        $pmodel->event_id = $find->id;
                                        $pmodel->name = $plan['name'];
                                        $pmodel->price = $plan['price'];
                                        $pmodel->duration = $plan['duration'];
                                        $pmodel->user_id = Yii::$app->user->identity->id;
                                        $pmodel->save();
                                    }
                                }
                                if (isset($POST['Events']['form_data']) && !empty($POST['Events']['form_data'])) {
                                    $pmodel = new \app\models\MembershipDueForms;
                                    $pmodel->event_id = $find->id;
                                    $pmodel->form_data = $POST['Events']['form_data'];
                                    $pmodel->save();
                                }
                                if (isset($POST['Events']['tickets']) && !empty($POST['Events']['tickets'])) {
                                    foreach ($POST['Events']['tickets'] as $ticket) {
                                        $tmodel = new \app\models\EventTickets;
                                        $tmodel->event_id = $find->id;
                                        $tmodel->name = $ticket['name'];
                                        $tmodel->price = $ticket['price'];
                                        if(isset($ticket['description'])){
                                            $tmodel->description = $ticket['description'];
                                        }
                                        $tmodel->user_id = Yii::$app->user->identity->id;
                                        $tmodel->save();
                                    }
                                }
                                if (isset($POST['Events']['programs']) && !empty($POST['Events']['programs'])) {
                                    foreach ($POST['Events']['programs'] as $programs) {
                                        $pModel = new \app\models\EventPrograms();
                                        $pModel->event_id = $find->id;
                                        $pModel->name = $programs['name'];
                                        $pModel->price = $programs['price'];
                                        if(isset($programs['description'])){
                                            $pModel->description = $programs['description'];
                                        }
                                        if (is_array($programs['image'])) {
                                            $temp_file_name = $programs['image']['file_name'];
                                            if (strpos($programs['image']['file_name'], "temp") >= 0) {
                                                $new_file_name = mt_rand(111111, 999999) . "." . $programs['image']['extension'];
                                                if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $programs['image']['file_name'])) {
                                                    copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" .$programs['image']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                                    $pModel->image = $new_file_name;
                                                    unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                                                }
                                            }
                                        }
                                        $pModel->save();
                                    }
                                }
                                if (isset($POST['Events']['groups']) && !empty($POST['Events']['groups'])) {
                                    foreach ($POST['Events']['groups'] as $group) {
                                        $gmodel = new \app\models\EventGroups;
                                        $gmodel->event_id = $model->id;
                                        $gmodel->name = $group['name'];
                                        $gmodel->save();
                                    }
                                }
                                return [
                                    'success' => true,
                                    'message' => 'Event updated successfully.',
                                ];
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $find->getErrors(),
                                ];
                            }
                        } else {
                            return [
                                'error' => true,
                                'message' => "Event not found.",
                            ];
                        }
                    } else if ($model->load($POST) && $model->save()) {
                        if(isset($model->event_type) && $model->event_type == 1 && !empty($model->passcode)){
                            $model->event_passcode = Yii::$app->security->generatePasswordHash($model->passcode);
                        }
                        $model->color_code = Yii::$app->common->findColorCode();
                        $model->save();
                        $file_name = "event-" . Yii::$app->user->identity->id . "-" . $model->id;
                        $qrCode = (new \Da\QrCode\QrCode('https://tiphub.co/' . $model->url . '/' . $model->id))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                        $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . '/' . $file_name . '.png');

                        $smodel = new \app\models\Services;
                        $sfind = $smodel->findAll(['user_id' => Yii::$app->user->identity->id]);
                        if ($sfind) {
                            foreach ($sfind as $service) {
                                $epmodel = new \app\models\EventApps;
                                $epmodel->user_id = Yii::$app->user->identity->id;
                                $epmodel->event_id = $model->id;
                                $epmodel->url = $service->url;
                                $epmodel->service_id = $service->service_id;
                                $epmodel->email = $service->email;
                                if (isset($model->default_amount) && $model->default_amount == 'other') {
                                    $epmodel->default_amount = $model->other_default_amount;
                                } else {
                                    $epmodel->default_amount = $model->default_amount;
                                }
                                //$epmodel->default_amount = $service->default_amount;
                                $epmodel->save();
                            }
                        }
                        if (isset($POST['Events']['plans']) && !empty($POST['Events']['plans'])) {
                            foreach ($POST['Events']['plans'] as $plan) {
                                $pmodel = new \app\models\MembershipPlans;
                                $pmodel->event_id = $model->id;
                                $pmodel->name = $plan['name'];
                                $pmodel->price = $plan['price'];
                                $pmodel->duration = $plan['duration'];
                                $pmodel->user_id = Yii::$app->user->identity->id;
                                $pmodel->save();
                            }
                        }
                        if (isset($POST['Events']['form_data']) && !empty($POST['Events']['form_data'])) {
                            $pmodel = new \app\models\MembershipDueForms;
                            $pmodel->event_id = $model->id;
                            $pmodel->form_data = $POST['Events']['form_data'];
                            $pmodel->save();
                        }
                        if (isset($POST['Events']['tickets']) && !empty($POST['Events']['tickets'])) {
                            foreach ($POST['Events']['tickets'] as $ticket) {
                                $tmodel = new \app\models\EventTickets;
                                $tmodel->event_id = $model->id;
                                $tmodel->name = $ticket['name'];
                                $tmodel->price = $ticket['price'];
                                if(isset($ticket['description'])){
                                    $tmodel->description = $ticket['description'];
                                }
                                $tmodel->user_id = Yii::$app->user->identity->id;
                                $tmodel->save();
                            }
                        }
                        if (isset($POST['Events']['programs']) && !empty($POST['Events']['programs'])) {
                            foreach ($POST['Events']['programs'] as $program) {
                                $pModel = new \app\models\EventPrograms();
                                $pModel->event_id = $model->id;
                                $pModel->name = $program['name'];
                                $pModel->price = $program['price'];
                                if(isset($program['description'])){
                                    $pModel->description = $program['description'];
                                }
                                if (is_array($program['image'])) {
                                    $temp_file_name = $program['image']['file_name'];
                                    if (strpos($program['image']['file_name'], "temp") >= 0) {
                                        $new_file_name = mt_rand(111111, 999999) . "." . $program['image']['extension'];
                                        if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $program['image']['file_name'])) {
                                            copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" .$program['image']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                            $pModel->image = $new_file_name;
                                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                                        }
                                    }
                                }
                                $pModel->save();
                            }
                        }
                        if (isset($POST['Events']['groups']) && !empty($POST['Events']['groups'])) {
                            foreach ($POST['Events']['groups'] as $team) {
                                $tmmodel = new \app\models\EventGroups;
                                $tmmodel->event_id = $model->id;
                                $tmmodel->name = $team['name'];
                                $tmmodel->save();
                            }
                        }
                        if ($model->category == 58) {
                            $emModel = new \app\models\EventMembers;
                            $emModel->event_id = $model->id;
                            $emModel->user_id = Yii::$app->user->identity->id;
                            $emModel->save();
                        }
                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 21])->one();
                        $data = [
                            'case' => 21,
                            'body' => $templateDetails->content,
                            'event' => $model->title,
                            'name' => $model->user['name'],
                            'username' => $model->user['username'],
                            'email' => $model->user['email'],
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                        $compose->setTo(Yii::$app->params['adminEmail']);
                        $compose->setSubject($templateDetails->subject);
                        if ($compose->send()) {
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Event has been added successfully.');
                        }
                        return [
                            'success' => true,
                            'message' => 'Event added successfully.',
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionList($published = 1, $pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'target' => [
                    'asc' => ['target' => SORT_ASC],
                    'desc' => ['target' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['events.added_on' => SORT_ASC],
                    'desc' => ['events.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Events;
        $query = $model->find()->joinWith(['user' => function ($q) {
            $q->select(['users.id', 'users.name', 'users.username']);
        }, 'transactions' => function ($q) {
            $q->select(['sum(amount) as totalAmount', 'event_id'])->groupBy('transactions.event_id');
        }, 'apps.appname'])->where(['published' => $published]);

        $query->andWhere([
            'or',
            ['events.user_id' => Yii::$app->user->identity->id],
            ['events.id' => \app\models\TeamAndPermissions::find()->select('event_id')->where(['team_id' => Yii::$app->user->identity->id])],
            ['events.id' => \app\models\PhysicalTicketAllotments::find()->select('event_id')->where(['team_id' => Yii::$app->user->identity->id])],
        ]);

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->category)) {
                    $query->andWhere(['category'=>$search->category]);
                }
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->groupBy('events.id')->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'events' => $find,
                'pages' => $pages,
                'action' => Yii::$app->controller->action->id
            ];
        } else {
            $response = [
                'success' => true,
                'events' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Event Report~Event~reports~users~users~4
    public function actionReports()
    {
        if (isset($_POST) && !empty($_POST)) {
            $event_id = $_POST['fields']['event_id'];
            $service_id = isset($_POST['fields']['service_id']) ? $_POST['fields']['service_id'] : null;
            $checkEvent = \app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count();
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if ($checkEvent == 0 && empty($eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            } else if ($checkEvent == 0 && !empty($eventIds) && !in_array($event_id, $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }

            if (isset($_POST['fields']['date_range']) && !empty($_POST['fields']['date_range'])) {
                $date_range = explode(' to ', $_POST['fields']['date_range']);
                $fromArr = explode('-', $date_range[0]);
                $toArr = explode('-', $date_range[1]);
                $from = $fromArr[2] . '-' . $fromArr[0] . '-' . $fromArr[1];
                $to = $toArr[2] . '-' . $toArr[0] . '-' . $toArr[1];
            }

            $model = new \app\models\Transactions;
            $query = $model->find()->joinWith(['appname', 'event'])->where(['transactions.event_id' => $event_id]);
            if (isset($from) && isset($to)) {
                $query->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(transactions.added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(transactions.added_on, "%Y-%m-%d")', $to],
                ]);
            }
            $countQuery = clone $query;
            $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
            $pages->pageSize = 30;
            $suppoters = $query->offset($pages->offset)->limit($pages->limit)->orderBy('transactions.added_on desc')->asArray()->all();
            //echo $suppoters->createCommand()->getRawSql();die;

            $totalVisitoryQuery = \app\models\EventVisitors::find()->where(['event_id' => $event_id]);
            $topTippersQuery = \app\models\Transactions::find()->where(['event_id' => $event_id]);
            $trafficQuery = \app\models\EventVisitors::find()->where(['event_id' => $event_id]);
            $repeatTippersQuery = \app\models\Transactions::find()->select(['count(*) as totalDonations', 'name'])->where(['event_id' => $event_id])->andWhere(['!=', 'name', 'Anonymous'])->groupBy('name')->having(['>', 'totalDonations', 1]);
            $fromDeviceQuery = \app\models\EventVisitors::find()->select(['count(*) as totalVisitors', 'device'])->where(['event_id' => $event_id])->groupBy('device');
            $tippingAppQuery = \app\models\EventAppVisitors::find()->select(['count(*) as totalVisitors', 'service_id'])->joinWith(['servicedetails'])->where(['event_id' => $event_id])->groupBy('service_id');
            $totalTransactionSum = $topTippersQuery;
            $avgTippingAmountQuery = $topTippersQuery;
            //$paymentsThroughApps = \app\models\Transactions::find()->joinWith(['appname'])->where(['event_id'=>$event_id])->groupBy('service_id');
            if (isset($from) && isset($to)) {
                $totalVisitoryQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $topTippersQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $trafficQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $avgTippingAmountQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $fromDeviceQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $totalTransactionSum->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);

                $tippingAppQuery->andWhere([
                    'AND',
                    ['>=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $from],
                    ['<=', 'DATE_FORMAT(added_on, "%Y-%m-%d")', $to],
                ]);
            }

            if(isset($service_id) && !empty($service_id)){
                $totalTransactionSum->andWhere(['service_id'=>$service_id]);
                $topTippersQuery->andWhere(['service_id'=>$service_id]);
                $repeatTippersQuery->andWhere(['service_id'=>$service_id]);
                $avgTippingAmountQuery->andWhere(['service_id'=>$service_id]);
            }

            $model = new \app\models\EventVisitors;
            $query = $model->find()->where(['event_id' => $event_id]);

            $countQuery = clone $query;
            $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
            $pages->pageSize = 30;
            $traffic = $query->offset($pages->offset)->limit($pages->limit)->orderBy('added_on desc')->asArray()->all();

            return [
                'success' => true,
                'totalVisitors' => $totalVisitoryQuery->count(),
                'topTippers' => $topTippersQuery->limit(10)->orderBy('amount desc')->asArray()->all(),
                //'traffic'=>$trafficQuery->asArray()->all(),
                'repeatTippers' => $repeatTippersQuery->asArray()->all(),
                'fromDevice' => $fromDeviceQuery->asArray()->all(),
                'avgTippingAmount' => $avgTippingAmountQuery->asArray()->average('amount'),
                'tippingApps' => $tippingAppQuery->asArray()->all(),
                'supporters' => [
                    'records' => $suppoters,
                    'pages' => $pages
                ],
                'traffic' => [
                    'records' => $traffic,
                    'pages' => $pages
                ],
                'totalTransactionSum' => $totalTransactionSum->asArray()->sum('amount'),
                //'paymentsThroughApps'=>$paymentsThroughApps->asArray()->all()           
            ];
        }
    }

    public function actionGet($id)
    {
         
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->joinWith(['user'])->where(['events.id' => $id])->one();
                if ($find) {
                    if (isset($find->image)) {
                        $imageDetails = explode('.', $find->image);
                        if ($imageDetails[1] == 'png') {
                            $new_file_name = $imageDetails[0] . '.jpg';
                            copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $find->image, $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $find->image);
                            $find->image = $new_file_name;
                            $find->save();
                        }
                    }
                    if (isset($find->user->image)) {
                        $imageDetails = explode('.', $find->user->image);
                        if ($imageDetails[1] == 'png') {
                            $umodel = new \app\models\User;
                            $ufind = $umodel->find()->where(['id' => $find->user->id])->one();
                            if ($ufind) {
                                $new_file_name = $imageDetails[0] . '.jpg';
                                copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $ufind->image, $_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $new_file_name);
                                unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $ufind->image);
                                $ufind->image = $new_file_name;
                                $ufind->save();
                            }
                        }
                    }
                }
                //$find = $model->find()->joinWith(['user'=>function($q){$q->select(['users.id','users.name','users.username','users.image']);},'transactions.appname','apps.appname','qrcodes'=>function($q){$q->orderBy('id desc');}])->where(['events.id' => $id])->asArray()->one();
                $query = $model->find()->joinWith(['user' => function ($q) {
                    $q->select(['users.id', 'users.name', 'users.username', 'users.image']);
                }, 'apps.appname'/* =>function($q){$q->orderBy('event_apps.sequence asc');} */, 'qrcodes' => function ($q) {
                    $q->orderBy('id desc');
                }, 'transactions' => function ($q) {
                    $q->select(['sum(amount) as totalAmount', 'transactions.event_id'])->groupBy('transactions.event_id');
                }, 'categoryDetails']);
                //$query = $model->find()->joinWith(['user'=>function($q){$q->select(['users.id','users.name','users.username','users.image']);},'qrcodes'=>function($q){$q->orderBy('id desc');},'transactions'=>function($q){$q->select(['sum(amount) as totalAmount','transactions.event_id'])->groupBy('transactions.event_id');}]);
                $query->where(['events.id' => $id, 'events.published' => 1, 'approved' => 1]);
                $find = $query->asArray()->one();
                if ($find) {
                    //'published'=>1
                    $dmodel = new \app\models\EventVisitors;
                    $fdmodel = $dmodel->find()->where(['event_id' => $find['id'], 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => date('Y-m-d')])->one();
                    if (!$fdmodel) {
                        $dmodel->event_id = $find['id'];
                        $dmodel->ip_address = Yii::$app->request->userIP;
                        $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                        $dmodel->save();
                    }
                    return [
                        'success' => true,
                        'event' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }
    }

    public function actionGetWithLoggedIn($id, $preview = 0)
    {
         
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->joinWith(['user'])->where(['events.id' => $id])->one();
                if ($find) {
                    if (isset($find->image)) {
                        $imageDetails = explode('.', $find->image);
                        if ($imageDetails[1] == 'png') {
                            $new_file_name = $imageDetails[0] . '.jpg';
                            copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $find->image, $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $find->image);
                            $find->image = $new_file_name;
                            $find->save();
                        }
                    }
                    if (isset($find->user->image)) {
                        $imageDetails = explode('.', $find->user->image);
                        if ($imageDetails[1] == 'png') {
                            $umodel = new \app\models\User;
                            $ufind = $umodel->find()->where(['id' => $find->user->id])->one();
                            if ($ufind) {
                                $new_file_name = $imageDetails[0] . '.jpg';
                                copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $ufind->image, $_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $new_file_name);
                                unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $ufind->image);
                                $ufind->image = $new_file_name;
                                $ufind->save();
                            }
                        }
                    }
                }
                //$find = $model->find()->joinWith(['user'=>function($q){$q->select(['users.id','users.name','users.username','users.image']);},'transactions.appname','apps.appname','qrcodes'=>function($q){$q->orderBy('id desc');}])->where(['events.id' => $id])->asArray()->one();
                $query = $model->find()->joinWith(['user' => function ($q) {
                    $q->select(['users.id', 'users.name', 'users.username', 'users.image']);
                }, 'apps.appname'/* =>function($q){$q->orderBy('event_apps.sequence asc');} */, 'qrcodes' => function ($q) {
                    $q->orderBy('id desc');
                }, 'transactions' => function ($q) {
                    $q->select(['sum(amount) as totalAmount', 'transactions.event_id'])->groupBy('transactions.event_id');
                }, 'categoryDetails']);
                //$query = $model->find()->joinWith(['user'=>function($q){$q->select(['users.id','users.name','users.username','users.image']);},'qrcodes'=>function($q){$q->orderBy('id desc');},'transactions'=>function($q){$q->select(['sum(amount) as totalAmount','transactions.event_id'])->groupBy('transactions.event_id');}]);
                if ($preview == 1) {
                    $query->where(['events.id' => $id]);
                } else {
                    $query->where(['events.id' => $id, 'events.published' => 1, 'approved' => 1]);
                }
                $find = $query->asArray()->one();
                if ($find) {
                    //'published'=>1
                    $dmodel = new \app\models\EventVisitors;
                    $fdmodel = $dmodel->find()->where(['event_id' => $find['id'], 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => date('Y-m-d')])->one();
                    if (!$fdmodel) {
                        $dmodel->event_id = $find['id'];
                        $dmodel->ip_address = Yii::$app->request->userIP;
                        $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                        $dmodel->save();
                    }
                    return [
                        'success' => true,
                        'event' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }
    }

    public function actionGetToEdit()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    return [
                        'success' => true,
                        'event' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Delete Event~Event~delete~users~users~1
    public function actionDelete()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    if(\app\models\DonorDetails::find()->where(['event_id' => $_POST['id']])->count() > 0 || \app\models\Transactions::find()->where(['event_id' => $_POST['id']])->count() > 0 || \app\models\MutualAidRequests::find()->where(['event_id' => $_POST['id']])->count() > 0 || \app\models\EventTicketBookings::find()->where(['event_id' => $_POST['id']])->count() > 0 || \app\models\Payments::find()->where(['event_id' => $_POST['id']])->count() > 0){
                        return [
                            'error' => true,
                            'message' => "Can't be deleted as this event has dependent records."
                        ];
                    }
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if ($find->user_id == Yii::$app->user->identity->id || in_array($find->id, $eventIds)) {
                        $event_id = $find->id;
                        if ($find->delete()) {
                            \app\models\Transactions::deleteAll(['event_id' => $event_id]);
                            \app\models\DonorDetails::deleteAll(['event_id' => $event_id]);
                            \app\models\EventApps::deleteAll(['event_id' => $event_id]);
                            \app\models\EventGallery::deleteAll(['event_id' => $event_id]);
                            \app\models\EventQrCodes::deleteAll(['event_id' => $event_id]);
                            \app\models\EventVisitors::deleteAll(['event_id' => $event_id]);
                            \app\models\MutualAidRequests::deleteAll(['event_id' => $event_id]);
                            \app\models\EventTickets::deleteAll(['event_id' => $event_id]);
                            \app\models\EventTicketBookings::deleteAll(['event_id' => $event_id]);
                            $paymentIds = \app\models\Payments::find()->select('id')->where(['event_id' => $event_id])->column();
                            \app\models\Payments::updateAll(['event_id' => NULL, 'transaction_id' => NUll, 'status' => 0], ['id' => $paymentIds]);
                            return [
                                'success' => true,
                                'message' => 'Event deleted successfully.'
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors()
                            ];
                        }
                    } else if (!in_array($find->id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Add Transaction~Event~add-transaction~users~users~3
    public function actionAddTransaction()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                if (\app\models\Events::find()->where(['id' => $_POST['fields']['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_POST['fields']['event_id'], $eventIds)) {
                    return [
                        'error' => true,
                        'message' => 'Unauthorized request found.'
                    ];
                }
                $model = new \app\models\Transactions;
                $POST['Transactions'] = $_POST['fields'];
                if(isset($POST['Transactions']['added_on']) && !empty($POST['Transactions']['added_on'])){
                    $POST['Transactions']['added_on'] = date("Y-m-d H:i:s", strtotime($POST['Transactions']['added_on']));
                }
                if (isset($POST['Transactions']['id']) && !empty($POST['Transactions']['id'])) {
                    $find = \app\models\Transactions::find()->where(['id' => $POST['Transactions']['id']])->one();
                    if($find && isset($POST['Transactions']['added_on']) && !empty($POST['Transactions']['added_on']) && $find->actual_payment_date == null){
                        $POST['Transactions']['actual_payment_date'] = $find->added_on;
                    }
                    if ($find && $find->load($POST)) {
                        if (is_array($find->screenshot)) {
                            $temp_file_name = $find->screenshot['file_name'];
                            if (strpos($find->screenshot['file_name'], "temp") >= 0) {
                                $new_file_name = mt_rand(111111, 999999) . "." . $find->screenshot['extension'];
                                copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $find->screenshot['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                $find->screenshot = $new_file_name;
                                unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                            }
                        }
                        if ($find->service_id == "other") {
                            $psnmodel = new \app\models\PaymentServiceNames;
                            $psnfind = $psnmodel->findOne(['name' => $find->payment_service_name]);
                            if ($psnfind) {
                                $find->service_id = $psnfind->id;
                            } else {
                                $psnmodel->name = $find->payment_service_name;
                                $psnmodel->icon = "other.svg";
                                if ($psnmodel->save()) {
                                    $find->service_id = $psnmodel->id;
                                }
                            }
                        }
                        
                        if ($find->save()) {
                            $pmodel = new \app\models\Payments;
                            $pfind = $pmodel->find()->where(['transaction_id'=>$find->id])->one();
                            if($pfind){
                                $pfind->txn_amount = $find->amount;
                                $pfind->save();
                            }
                            return [
                                'success' => true,
                                'message' => 'Transaction updated successfully.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }
                    }
                } else {
                    $POST['Transactions']['added_by'] = Yii::$app->user->identity->id;
                    if ($model->load($POST)) {
                        if (is_array($model->screenshot)) {
                            $temp_file_name = $model->screenshot['file_name'];
                            if (strpos($model->screenshot['file_name'], "temp") >= 0) {
                                $new_file_name = mt_rand(111111, 999999) . "." . $model->screenshot['extension'];
                                copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $model->screenshot['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                $model->screenshot = $new_file_name;
                                unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                            }
                        }
                        if ($model->service_id == "other") {
                            $psnmodel = new \app\models\PaymentServiceNames;
                            $psnfind = $psnmodel->findOne(['name' => $model->payment_service_name]);
                            if ($psnfind) {
                                $model->service_id = $psnfind->id;
                            } else {
                                $psnmodel->name = $model->payment_service_name;
                                $psnmodel->icon = "other.svg";
                                if ($psnmodel->save()) {
                                    $model->service_id = $psnmodel->id;
                                }
                            }
                        }
                        if ($model->save()) {
                            $pmodel = new \app\models\Payments;
                            $pmodel->txn_amount = $model->amount;
                            $pmodel->received_at = date("Y-m-d H:i:s");
                            $pmodel->name = $model->name;
                            $pmodel->payment_service =  $model->service_id;
                            $pmodel->payment_identifier =  'TPH' . mt_rand();
                            $pmodel->type = 1;
                            $pmodel->transaction_id = $model->id;
                            $pmodel->event_id = $model->event_id;
                            $pmodel->user_id = Yii::$app->user->identity->id;
                            $pmodel->status = 1;
                            $pmodel->data_dump = json_encode($model);
                            if ($pmodel->save()) {
                                return [
                                    'success' => true,
                                    'message' => 'Transaction added successfully.',
                                ];
                            }
                            return [
                                'error' => true,
                                'message' => $pmodel->getErrors(),
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $model->getErrors(),
                            ];
                        }
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    //Label~Module~Action~Url~Icon:List Transactions~Event~list-transactions~users~users~3,2
    public function actionListTransactions($event_id)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'amount' => [
                    'asc' => ['amount' => SORT_ASC],
                    'desc' => ['amount' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['transactions.added_on' => SORT_ASC],
                    'desc' => ['transactions.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);
        $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
        if (\app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($event_id, $eventIds)) {
            return [
                'error' => true,
                'message' => 'Unauthorized request found.'
            ];
        }
        $model = new \app\models\Transactions;
        $query = $model->find()->joinWith(['event', 'appname'])->where(['event_id' => $event_id]);

        /* $search = new \app\models\SearchForm;
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        if ($search->load($GET)) {
            if (!empty($search->name)) {
                $query->andWhere(['LIKE', 'event_apps.name', $search->title]);
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 50;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'transactions' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'transactions' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Delete Transaction~Event~delete-transaction~users~users~2
    public function actionDeleteTransaction()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Transactions;
                $find = $model->find()->where(['id' => $_POST['id'], 'added_by' => Yii::$app->user->identity->id])->one();
                if ($find) {
                    $transaction_id = $find->id;
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    if ($find->delete()) {
                        \app\models\Payments::deleteAll(['transaction_id' => $transaction_id]);
                        return [
                            'success' => true,
                            'message' => 'Transaction deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Transaction not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Transaction not found!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Add Payment Method~Event~add-event-app~users~users~3
    public function actionAddEventApp()
    {
        if (isset($_POST) && !empty($_POST)) {
            $scenario = [];
            try {
                $model = new \app\models\EventApps;
                $POST['EventApps'] = $_POST['fields'];
                $POST['EventApps']['user_id'] = Yii::$app->user->identity->id;
                $servicesLinkArr = ["", "https://cash.app/$", "https://venmo.com/", "https://www.patreon.com/", "https://www.paypal.com/paypalme/"];
                if ($model->load($POST)) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $model->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($model->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    if (isset($model->id)) {
                        $find = $model->find()->where(['id' => $model->id])->one();
                        if ($find && $find->load($POST)) {
                            if ($find->service_id == "other") {
                                $psnmodel = new \app\models\PaymentServiceNames;
                                $psnfind = $psnmodel->findOne(['name' => $find->payment_service_name, 'user_id' => Yii::$app->user->identity->id]);
                                if ($psnfind) {
                                    $find->service_id = $psnfind->id;
                                } else {
                                    $psnmodel->name = $find->payment_service_name;
                                    $psnmodel->icon = "other.svg";
                                    $psnmodel->user_id = Yii::$app->user->identity->id;
                                    if ($psnmodel->save()) {
                                        $find->service_id = $psnmodel->id;
                                    }
                                }
                            }
                            if ($find->save()) {
                                return [
                                    'success' => true,
                                    'message' => 'Event app updated successfully.',
                                ];
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $find->getErrors(),
                                ];
                            }
                        } else {
                            return [
                                'error' => true,
                                'message' => "Event app not found.",
                            ];
                        }
                    } else if ($model->validate()) {
                        if ($model->service_id == "other") {
                            $psnmodel = new \app\models\PaymentServiceNames;
                            $psnfind = $psnmodel->findOne(['name' => $model->payment_service_name, 'user_id' => Yii::$app->user->identity->id]);
                            if ($psnfind) {
                                $model->service_id = $psnfind->id;
                            } else {
                                $psnmodel->name = $model->payment_service_name;
                                $psnmodel->icon = "other.svg";
                                $psnmodel->user_id = Yii::$app->user->identity->id;
                                if ($psnmodel->save()) {
                                    $model->service_id = $psnmodel->id;
                                }
                            }
                        } else if ($model->service_id <= 4) {
                            $model->url = $servicesLinkArr[$model->service_id] . $model->username;
                            if (isset($model->default_amount) && !empty($model->default_amount)) {
                                $model->url = $servicesLinkArr[$model->service_id] . $model->username . "/" . $model->default_amount;
                            }
                            if ($model->service_id == 2) {
                                $model->url = $servicesLinkArr[$model->service_id] . $model->username . "?txn=pay";
                                if (isset($model->default_amount) && !empty($model->default_amount)) {
                                    $model->url = $model->url . "&amount=" . $model->default_amount;
                                }
                            }
                        } else if ($model->service_id == 12) {
                            $model->url = "https://onlyfans.com/" . $model->username;
                        }
                        if ($model->save()) {
                            if ($model->service_id == 65) {
                                $emodel = new \app\models\Events;
                                $efind = $emodel->findOne(['id'=>$model->event_id]);
                                if($efind && empty($efind->stripe_product_id)){
                                    $stripe = new \Stripe\StripeClient(Yii::$app->params['stripeSecretKey']);
                                    $product = $stripe->products->create([
                                        'name' => $efind->title,
                                        'metadata' => [
                                            'event_id' => $efind->id,
                                            'user_id' => $efind->user_id
                                        ],
                                    ]);
                                    if($product){
                                        $efind->stripe_product_id = $product->id;
                                        $efind->save();
                                    }
                                }
                            }
                            return [
                                'success' => true,
                                'message' => 'Event app added successfully.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $model->getErrors(),
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionGetEventApp($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\EventApps;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'app' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'App not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'App not found!',
            ];
        }
    }

    public function actionGetEdgepayConfigs()
    {
        try {
            $model = new \app\models\UsersEdgePayConfigs;
            $find = $model->find()->select(['user_id', 'sID','id'])->where(['user_id' => Yii::$app->user->identity->id])->asArray()->one();
            
            if ($find) {
                return [
                    'success' => true,
                    'record' => $find,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'Configuration not found!',
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionListEventApps($event_id)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\EventApps;
        $query = $model->find()->joinWith(['event', 'appname'])->where(['event_id' => $event_id]);

        $search = new \app\models\SearchForm;
        /* $GET['SearchForm'] = json_decode($_GET['fields'], true);
        if ($search->load($GET)) {
            if (!empty($search->name)) {
                $query->andWhere(['LIKE', 'event_apps.name', $search->title]);
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 50;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'apps' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'apps' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Delete Payment Method~Event~delete-event-app~users~users~2
    public function actionDeleteEventApp()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventApps;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find && $find->user_id == Yii::$app->user->identity->id) {
                    if ($find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'App deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (!in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    } else if ($find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'App deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'App not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'App not found!'
            ];
        }
    }

    public function actionGenerateQrCode()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $eventDetail = \app\models\Events::find()->joinWith(['apps.appname'])->where(['events.id' => $_POST['event_id']])->asArray()->one();
                $file_name = "event-" . Yii::$app->user->identity->id . "-" . $_POST['event_id'];
                $qrCode = (new \Da\QrCode\QrCode('https://tiphub.co/'.$eventDetail['url'].'/' . $_POST['event_id']))->setSize(250)->setMargin(5); //->useForegroundColor(51, 153, 255);
                $qrCode->writeFile($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . '/' . $file_name . '.png');
                return [
                    'success' => true,
                    'file' => $file_name . '.png?time='.time(),
                    'eventDetails' => $eventDetail
                ];
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionTrackEventApp($event_id, $service_id)
    {
        if (isset($event_id) && !empty($event_id) && isset($service_id) && !empty($service_id)) {
            try {
                $dmodel = new \app\models\EventAppVisitors;
                $fdmodel = $dmodel->find()->where(['event_id' => $event_id, 'service_id' => $service_id, 'ip_address' => Yii::$app->request->userIP, 'DATE_FORMAT(added_on, "%Y-%m-%d")' => '%Y-%m-%d'])->one();
                if (!$fdmodel) {
                    $dmodel->event_id = $event_id;
                    $dmodel->service_id = $service_id;
                    $dmodel->ip_address = Yii::$app->request->userIP;
                    $dmodel->device = Device::$isPhone ? 'Mobile' : 'Pc';
                    $dmodel->save();
                }
                return [
                    'success' => true,
                ];
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }
    }

    public function actionTrackReport($event_id)
    {
        try {
            $modal = new \app\models\Events;
            //$find = $modal->find()->joinWith(['visitors'=>function($q){$q->select(['count(*) as totalVisitors','event_visitors.event_id'])->groupBy('event_visitors.event_id');},'appvisitors'=>function($q){$q->select(['count(*) as totalappvisitors','event_app_visitors.device'])->groupBy('event_app_visitors.device');}])->where(['events.id'=>$event_id])->asArray()->one();
            //$query = $model->find()->joinWith(['user'=>function($q){$q->select(['users.id','users.name','users.username']);},'transactions'=>function($q){$q->select(['sum(amount) as totalAmount','event_id'])->groupBy('event_id');}])->where(['events.user_id'=>Yii::$app->user->identity->id]);
            return [
                'success' => true,
                'totalVisitors' => \app\models\EventVisitors::find()->where(['event_id' => $event_id])->count(),
                'topTippers' => \app\models\Transactions::find()->where(['event_id' => $event_id])->limit(10)->orderBy('amount desc')->asArray()->all(),
                'traffic' => \app\models\EventVisitors::find()->where(['event_id' => $event_id])->asArray()->all(),
                'repeatTippers' => \app\models\Transactions::find()->select(['count(*) as totalDonations', 'name'])->where(['event_id' => $event_id])->andWhere(['!=', 'name', 'Anonymous'])->groupBy('name')->having(['>', 'totalDonations', 1])->asArray()->all(),
                'fromDevice' => \app\models\EventVisitors::find()->select(['count(*) as totalVisitors', 'device'])->where(['event_id' => $event_id])->groupBy('device')->asArray()->all(),
                'avgTippingAmount' => \app\models\Transactions::find()->where(['event_id' => $event_id])->asArray()->average('amount'),
                'tippingApps' => \app\models\EventAppVisitors::find()->select(['count(*) as totalVisitors', 'service_id'])->joinWith(['servicedetails'])->where(['event_id' => $event_id])->groupBy('service_id')->asArray()->all(),
                'supporters' => \app\models\Transactions::find()->joinWith(['appname', 'event'])->where(['added_by' => Yii::$app->user->identity->id])->limit(50)->asArray()->all()

            ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionUploadQrcode()
    {
        //ini_set('memory_limit', '-1');
        $response = [];
        if (isset($_FILES) && !empty($_FILES['file_names'])) {
            try {
                $files = [];
                foreach ($_FILES['file_names'] as $k => $l) {
                    foreach ($l as $i => $v) {
                        if (!array_key_exists($i, $files))
                            $files[$i] = [];
                        $files[$i][$k] = $v;
                    }
                }
                include "vendor/Upload.php";
                $uploaded = 0;
                foreach ($files as $file) {
                    //print_r($file);
                    $handle = new \Upload($file);
                    if ($handle->uploaded) {
                        $file_name = "-event-qrcode-" . mt_rand(111111, 999999);
                        $handle->file_new_name_body = $file_name;
                        $handle->allowed = ['image/*'];
                        $handle->file_overwrite = true;
                        if ($handle->image_src_x > 800) {
                            $handle->image_resize = true;
                            $handle->image_x = 800;
                            $handle->image_ratio_y = true;
                        }
                        $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes");
                        if ($handle->processed) {
                            $model = new \app\models\EventQrCodes;
                            $model->file_name = $handle->file_dst_name;
                            $model->user_id = Yii::$app->user->identity->id;
                            $model->event_id = $_REQUEST['event_id'];
                            if ($model->save()) {
                                $handle->clean();
                                $uploaded++;
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $model->getErrors()
                                ];
                            }
                        } else {
                            $response = [
                                'type' => 'error',
                                'message' => $handle->error,
                            ];
                        }
                    }
                }
                if ($uploaded > 0) {
                    $response = [
                        'success' => true,
                        'images' => \app\models\EventQrCodes::find()->where(['event_id' => $_REQUEST['event_id']])->orderBy('id desc')->asArray()->all(),
                        'message' => "Image uploaded successfully.",
                    ];
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionGetQrcodes($event_id)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\EventQrCodes;
        $query = $model->find()->where(['event_id' => $event_id]);

        $search = new \app\models\SearchForm;

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 50;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'qrcodes' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'qrcodes' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Delete QR Code~Event~delete-qrcode~users~users~2
    public function actionDeleteQrcode()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventQrCodes;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    $file_name = $find->file_name;
                    if ($find->delete()) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/qrcodes/" . $file_name);
                        return [
                            'success' => true,
                            'message' => 'QR code deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'QR code not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'QR code not found!'
            ];
        }
    }

    public function actionGetSupporters($event_id, $pageSize = 30)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['events.name' => SORT_ASC],
                    'desc' => ['events.name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'amount' => [
                    'asc' => ['amount' => SORT_ASC],
                    'desc' => ['amount' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['transactions.added_on' => SORT_ASC],
                    'desc' => ['transactions.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Transactions;
        $query = $model->find()->joinWith(['appname', 'event', 'group'])->where(['transactions.event_id' => $event_id]);
        $eventDetails = \app\models\Events::findOne($event_id);
        //recurring == 2 mean ongoing event that does not have end date.
        if($eventDetails->recurring != 2){
            $query->andWhere('DATE(transactions.added_on) <= DATE(events.end_date)');
        }
        //
       /*  ->andWhere([
            'or',
            ['<=', 'transactions.added_on', 'events.end_date'],
            ['<=', 'transactions.added_on', 'Now()'],
        ]); */

        /* $search = new \app\models\SearchForm;
        $GET['SearchForm'] = isset($_GET['fields'])?json_decode($_GET['fields'], true):[];
        
        if ($search->load($GET)) {
            if (!empty($search->event_id)) {
                $query->andWhere(['transactions.event_id'=>$search->event_id]);
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'supporters' => [
                'records' => $find,
                'pages' => $pages
            ]
        ];
    }

    //Label~Module~Action~Url~Icon:Check In Trasaction~Event~check-in~users~users~3
    public function actionCheckIn()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Transactions;
                $find = $model->find()->where(['id' => $_POST['id'], 'added_by' => Yii::$app->user->identity->id])->one();
                if ($find) {
                    if (isset($_POST['check_in']) && !empty($_POST['check_in'])) {
                        $find->check_in_date = date("Y-m-d H:i:s");
                        $find->check_in = 1;
                    } else {
                        $find->check_in_date = NULL;
                        $find->check_in = 0;
                    }
                    if ($find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Supporter checked in successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Supporter does not exist.'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Supporter does not exist.'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Export Supporters~Event~export-supporters~users~users~4
    public function actionExportSupporters()
    {
        if (isset($_POST) && !empty($_POST)) {
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if (\app\models\Events::find()->where(['id' => $_POST['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_POST['event_id'], $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }
            $eventDetails = \app\models\Events::find()->select('url')->where(['id' => $_POST['event_id']])->one();
            $supporters = \app\models\Transactions::find()->joinWith(['appname'])->where(['event_id' => $_POST['event_id']])->orderBy('added_on desc')->all();
            if (!empty($supporters)) {
                $file_name = 'supporters-' . date('m-d-Y');
                \moonland\phpexcel\Excel::export([
                    'models' => $supporters,
                    'fileName' => $file_name,
                    //'savePath' => $_SERVER['DOCUMENT_ROOT'] . "/api/web/exports/",
                    'columns' => [
                        [
                            'header' => 'Name',
                            'format' => 'text',
                            'value' => function ($supporters) {
                                return (!empty($supporters->name) ? $supporters->name : "");
                            },
                        ],
                        [
                            'header' => 'Amount',
                            'format' => 'text',
                            'value' => function ($supporters) {
                                return (!empty($supporters->amount) ? '$' . $supporters->amount : "");
                            },
                        ],
                        [
                            'header' => 'Payment Method',
                            'format' => 'text',
                            'value' => function ($supporters) {
                                return (!empty($supporters->service_id) ? (string)$supporters->appname->name . "" : "");
                            },
                        ],
                        [
                            'header' => 'Paid On',
                            'format' => 'text',
                            'value' => function ($supporters) {
                                return (!empty($supporters->added_on) ? date("M-d-Y", strtotime($supporters->added_on)) : "");
                            },
                        ],
                        [
                            'header' => 'Check In Time',
                            'format' => 'text',
                            'value' => function ($supporters) {
                                return (!empty($supporters->check_in_date) ? date("M-d-Y", strtotime($supporters->check_in_date)) : "");
                            },
                        ],
                    ],
                ]);
            }
        }
    }
    public function actionListByUser($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Events;
        $query = $model->find()->where(['user_id' => Yii::$app->user->identity->id,'published'=>1]);
        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->groupBy('events.id')->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $response = [
                'success' => true,
                'events' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'events' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    //Label~Module~Action~Url~Icon:Link Payment~Event~link-payment~users~users~2
    public function actionLinkPayment()
    {
        if (isset($_POST['payment_id']) && !empty($_POST['payment_id'])) {
            try {
                $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                if (\app\models\Events::find()->where(['id' => $_POST['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_POST['event_id'], $eventIds)) {
                    return [
                        'error' => true,
                        'message' => 'Unauthorized request found.'
                    ];
                }
                if (is_array($_POST['payment_id'])) {
                    foreach ($_POST['payment_id'] as $payment) {
                        $update = 0;
                        $pmodel = new \app\models\Payments;
                        $fpmodel = $pmodel->find()->where(['id' => $payment])->one();
                        if ($fpmodel && $fpmodel->status == 0) {
                            $fpmodel->status = 1;
                            $fpmodel->event_id = $_POST['event_id'];
                            if ($fpmodel->save()) {
                                $tmodel = new \app\models\Transactions;
                                $tmodel->event_id = $_POST['event_id'];
                                $tmodel->amount = $fpmodel->txn_amount;
                                $tmodel->name =  $fpmodel->name ? $fpmodel->name : $fpmodel->email;
                                $tmodel->added_on = $fpmodel->received_at;
                                $tmodel->service_id = $fpmodel->payment_service;
                                $tmodel->added_by = Yii::$app->user->identity->id;
                                if ($tmodel->save()) {
                                    $fpmodel->transaction_id = $tmodel->id;
                                    if ($fpmodel->save()) {
                                        $update++;
                                    }
                                }
                            }
                        }
                    }
                    if ($update > 0) {
                        return [
                            'success' => true,
                            'message' => 'Payments linked successfully.'
                        ];
                    } else {
                        return [
                            'success' => true,
                            'message' => 'Payments not found.'
                        ];
                    }
                } else {
                    $pmodel = new \app\models\Payments;
                    $fpmodel = $pmodel->find()->where(['id' => $_POST['payment_id']])->one();
                    if ($fpmodel) {
                        if ($fpmodel->status == 1) {
                            return [
                                'error' => true,
                                'message' => "Payment has already been linked."
                            ];
                        }
                        $fpmodel->status = 1;
                        $fpmodel->event_id = $_POST['event_id'];
                        if ($fpmodel->save()) {
                            $tmodel = new \app\models\Transactions;
                            $tmodel->event_id = $_POST['event_id'];
                            $tmodel->amount = $fpmodel->txn_amount;
                            $tmodel->name =  $fpmodel->name ? $fpmodel->name : $fpmodel->email;
                            $tmodel->added_on = $fpmodel->received_at;
                            $tmodel->service_id = $fpmodel->payment_service;
                            $tmodel->added_by = Yii::$app->user->identity->id;
                            if ($tmodel->save()) {
                                $fpmodel->transaction_id = $tmodel->id;
                                if ($fpmodel->save()) {
                                    $update++;
                                    return [
                                        'success' => true,
                                        'message' => 'Payment linked successfully.'
                                    ];
                                } else {
                                    return [
                                        'error' => true,
                                        'message' => $fpmodel->getErrors()
                                    ];
                                }
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $tmodel->getErrors(),
                                    'payment_servive' => $fpmodel->payment_service
                                ];
                            }
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found.'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Payment does not exist.'
            ];
        }
    }

    public function actionUnlinkPayment()
    {
        if (isset($_POST['payment_id']) && !empty($_POST['payment_id'])) {
            try {
                if (is_array($_POST['payment_id'])) {
                    $update = 0;
                    foreach ($_POST['payment_id'] as $payment) {
                        $pmodel = new \app\models\Payments;
                        $fpmodel = $pmodel->find()->where(['id' => $payment])->one();
                        if ($fpmodel && $fpmodel->status != 0) {
                            $transaction_id = $fpmodel->transaction_id;
                            $fpmodel->status = 0;
                            $fpmodel->event_id = NULL;
                            $fpmodel->transaction_id = NULL;
                            if ($fpmodel->save()) {
                                \app\models\Transactions::deleteAll(['id' => $transaction_id]);
                                $update++;
                            }
                        }
                    }
                    if ($update > 0) {
                        return [
                            'success' => true,
                            'message' => 'Payments un-linked successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found.'
                        ];
                    }
                } else {
                    $pmodel = new \app\models\Payments;
                    $fpmodel = $pmodel->find()->where(['id' => $_POST['payment_id']])->one();
                    if ($fpmodel) {
                        if ($fpmodel->status == 0) {
                            return [
                                'error' => true,
                                'message' => "Payment has already been unlinked."
                            ];
                        }
                        $transaction_id = $fpmodel->transaction_id;
                        $fpmodel->status = 0;
                        $fpmodel->event_id = NULL;
                        $fpmodel->transaction_id = NULL;
                        if ($fpmodel->save()) {
                            \app\models\Transactions::deleteAll(['id' => $transaction_id]);
                            return [
                                'success' => true,
                                'message' => 'Payment un-linked successfully.'
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $tmodel->getErrors(),
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found.'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Payment does not exist.'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Post Event Update~Event~post-update~users~users~3
    public function actionPostUpdate()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                if (\app\models\Events::find()->where(['id' => $_POST['fields']['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_POST['fields']['event_id'], $eventIds)) {
                    return [
                        'error' => true,
                        'message' => 'Unauthorized request found.'
                    ];
                }
                $model = new \app\models\EventUpdates;
                $POST['EventUpdates'] = $_POST['fields'];
                $POST['EventUpdates']['added_on'] = date("Y-m-d H:i:s", strtotime($POST['EventUpdates']['added_on_format']));
                if ($model->load($POST)) {
                    if (is_array($model->image)) {
                        $temp_file_name = $model->image['file_name'];
                        if (strpos($model->image['file_name'], "temp") >= 0) {
                            $new_file_name = str_replace('-temp', '', $temp_file_name);
                            copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name, $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                            $model->image = $new_file_name;
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                        }
                    }
                    if ($model->save()) {
                        return [
                            'success' => true,
                            'message' => 'Update added successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Invalid request!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetUpdates($event_id = null, $pageSize = 50)
    {
        if (!isset($event_id)) {
            return [
                'error' => true,
                'message' => 'Event dies not exist.'
            ];
        }

        $sort = new \yii\data\Sort([
            'attributes' => [
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\EventUpdates;
        $query = $model->find()->where(['event_id' => $event_id]);
        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'updates' => $find,
            'pages' => $pages,
        ];
    }

    public function actionGetTransactionDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Transactions;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'transaction' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Transaction not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Transaction not found!',
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Delete Event Update~Event~delete-update~users~users~2
    public function actionDeleteUpdate()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventUpdates;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    $file_name = $find->image;
                    if ($find->delete()) {
                        if ($file_name != NULL) {
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $file_name);
                        }
                        return [
                            'success' => true,
                            'message' => 'Update deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Update not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Update not found!'
            ];
        }
    }

    public function actionSaveBaseIncodedImage()
    {
        if (isset($_POST) && !empty($_POST)) {
            $fileImg_parts = explode(";base64,", $_POST['image']);
            $image_type_aux = explode("image/", $fileImg_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($fileImg_parts[1]);
            //copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $ufind->image, $_SERVER['DOCUMENT_ROOT'] . "/api/web/profile_images/" . $new_file_name);
            $results = $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/sharable-" . $_POST['event_id'] . '-' . $_POST['transaction_id'] . '.' . $image_type;
            if (file_put_contents($results, $image_base64)) {
                return [
                    'success' => true,
                    'image' => 'sharable-' . $_POST['event_id'] . '-' . $_POST['transaction_id'] . '.' . $image_type
                ];
            } else {
                return [
                    'error' => true,
                ];
            }
        }
    }

    //Label~Module~Action~Url~Icon:Upload Event Fallery~Event~upload-gallery~users~users~2
    public function actionUploadGallery()
    {
        //ini_set('memory_limit', '-1');
        $response = [];
        if (isset($_FILES) && !empty($_FILES['file_names'])) {
            try {
                $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                if (\app\models\Events::find()->where(['id' => $_REQUEST['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_REQUEST['event_id'], $eventIds)) {
                    return [
                        'error' => true,
                        'message' => 'Unauthorized request found.'
                    ];
                }
                $files = [];
                foreach ($_FILES['file_names'] as $k => $l) {
                    foreach ($l as $i => $v) {
                        if (!array_key_exists($i, $files))
                            $files[$i] = [];
                        $files[$i][$k] = $v;
                    }
                }
                include "vendor/Upload.php";
                $uploaded = 0;
                foreach ($files as $file) {
                    //print_r($file);
                    $handle = new \Upload($file);
                    if ($handle->uploaded) {
                        $file_name = "event-gallery-" . mt_rand(111111, 999999);
                        $handle->file_new_name_body = $file_name;
                        $handle->allowed = ['image/*'];
                        $handle->file_overwrite = true;
                        if ($handle->image_src_x > 800) {
                            $handle->image_resize = true;
                            $handle->image_x = 800;
                            $handle->image_ratio_y = true;
                        }
                        $handle->process($_SERVER['DOCUMENT_ROOT'] . "/api/web/events");
                        if ($handle->processed) {
                            $model = new \app\models\EventGallery;
                            $model->file_name = $handle->file_dst_name;
                            $model->user_id = Yii::$app->user->identity->id;
                            $model->event_id = $_REQUEST['event_id'];
                            if ($model->save()) {
                                $handle->clean();
                                $uploaded++;
                            } else {
                                return [
                                    'error' => true,
                                    'message' => $model->getErrors()
                                ];
                            }
                        } else {
                            $response = [
                                'type' => 'error',
                                'message' => $handle->error,
                            ];
                        }
                    }
                }
                if ($uploaded > 0) {
                    $response = [
                        'success' => true,
                        'gallery' => \app\models\EventGallery::find()->where(['event_id' => $_REQUEST['event_id']])->orderBy('id desc')->asArray()->all(),
                        'message' => "Image uploaded successfully.",
                    ];
                }
            } catch (\Exception $e) {
                $response = [
                    'error' => true,
                    'message' => $e->getMessage(),
                ];
            }
        }
        return $response;
    }

    public function actionGetGallery($event_id = null, $pageSize = 50)
    {
        if (!isset($event_id)) {
            return [
                'error' => true,
                'message' => 'Event does not exist.'
            ];
        }

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\EventGallery;
        $query = $model->find()->where(['event_id' => $event_id]);
        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'gallery' => $find,
            'pages' => $pages,
        ];
    }

    //Label~Module~Action~Url~Icon:Delete Gallery~Event~delete-gallery~users~users~2
    public function actionDeleteGallery()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventGallery;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    $file_name = $find->file_name;
                    if ($find->delete()) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $file_name);
                        return [
                            'success' => true,
                            'message' => 'Image deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Image not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Image not found!'
            ];
        }
    }

    public function actionGetTraffic($event_id, $pageSize = 30)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\EventVisitors;
        $query = $model->find()->where(['event_id' => $event_id]);

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'traffic' => [
                'records' => $find,
                'pages' => $pages
            ]
        ];
    }

    public function actionSearch($pageSize = 6)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'target' => [
                    'asc' => ['target' => SORT_ASC],
                    'desc' => ['target' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['events.added_on' => SORT_ASC],
                    'desc' => ['events.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Events;
        $query = $model->find()->where(['published' => 1])->andWhere(['discovered' => 1]);

        $search = new \app\models\SearchForm;
        if(isset($_GET['fields']) && !empty($_GET['fields'])){
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->title)) {
                    $query->andWhere(['LIKE', 'title', $search->title]);
                }
                if (!empty($search->category)) {
                    $query->andWhere(['LIKE', 'category', $search->category]);
                }
                if (!empty($search->recurring)) {
                    $query->andWhere(['LIKE', 'recurring', $search->recurring]);
                }
            }
        }
        

        $countQuery = clone $query;

        $query->joinWith(['transactions' => function ($q) {
            $q->select(['sum(amount) as totalAmount', 'count(id) as total_people', 'event_id'])->groupBy('event_id');
        }, 'categoryDetails']);

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->groupBy('events.id')->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        //SELECT `events`.* FROM `events` LEFT JOIN `transactions` ON `events`.`id` = `transactions`.`event_id` WHERE `published`=1 GROUP BY `event_id` ORDER BY `events`.`added_on` DESC LIMIT 6 OFFSET 60
        if ($find) {
            $newFind = [];
            foreach ($find as $key => $val) {
                $newFind[$key] = $val;
                $newFind[$key]['description'] = strip_tags($val['description']);
            }
            $response = [
                'success' => true,
                'events' => $newFind,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'events' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    public function actionBlogs($pageSize = 6)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['events.added_on' => SORT_ASC],
                    'desc' => ['events.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Blog;
        $query = $model->find();

        $search = new \app\models\SearchForm;
        /*$GET['SearchForm'] = json_decode($_GET['fields'], true);
         if ($search->load($GET)) {
            if (!empty($search->title)) {
                $query->andWhere(['LIKE', 'title', $search->title]);
            }
            if (!empty($search->category)) {
                $query->andWhere(['LIKE', 'category', $search->category]);
            }
            if (!empty($search->recurring)) {
                $query->andWhere(['LIKE', 'recurring', $search->recurring]);
            }
        } */

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'blogs' => $find,
            'pages' => $pages,
        ];
    }

    public function actionSaveDonorDetails()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\DonorDetails(['scenario' => isset($_POST['fields']) ? $_POST['fields']['scenario'] : $_POST['donor']['scenario']]);
                $POST['DonorDetails'] = isset($_POST['fields']) ? $_POST['fields'] : $_POST['donor'];
                $POST['DonorDetails']['ip_address'] = Yii::$app->request->userIP;

                if ($model->load($POST)) {
                    if (isset($_POST['donor']) && empty($_POST['beneficiary'])) {
                        return [
                            'error' => true,
                            'message' => 'Please add at least one beneficiary.'
                        ];
                    }
                    if (is_array($model->file_name) && !empty($model->file_name)) {
                        $temp_file_name = $model->file_name['document_name'];
                        if (strpos($temp_file_name, "temp") >= 0) {
                            $check_file = str_replace("-temp", "", $temp_file_name);
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $check_file)) {
                                $model->file_name = $check_file;
                            } else {
                                if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name)) {
                                    copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name, $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $check_file);
                                    $model->file_name = $check_file;
                                    unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                                } else {
                                    $new_file_name = mt_rand(111111, 999999) . "." . $model->file_name['extension'];
                                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $new_file_name)) {
                                        copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name, $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                                        $model->file_name = $new_file_name;
                                        unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                                    }
                                }
                            }
                        }
                    }
                    if ($model->save()) {
                        $beneficiaries = [];
                        if (isset($_POST['beneficiary']) && !empty($_POST['beneficiary'])) {
                            $benef = 0;
                            foreach ($_POST['beneficiary'] as $beneficiary) {
                                $POST = [];
                                $POST['BeneficiaryDetails'] = $beneficiary;
                                $POST['BeneficiaryDetails']['donor_id'] = $model->id;
                                $bmodel = new \app\models\BeneficiaryDetails;
                                if ($bmodel->load($POST) && $bmodel->save()) {
                                    $benef++;
                                    $beneficiaries[] = $bmodel->id;
                                } else {
                                    return [
                                        'error' => true,
                                        'message' => $bmodel->getErrors()
                                    ];
                                }
                            }
                            if ($benef > 0) {
                                $data = [
                                    'case' => 6,
                                    'donorDetails' => \app\models\DonorDetails::find()->joinWith(['event.user'])->where(['donor_details.id' => $model->id])->one()
                                ];
                                $compose = Yii::$app->mailer->compose('body', $data);
                                $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                                $compose->setTo($data['donorDetails']->donor_email);
                                //$compose->setTo('opnsrc.devlpr@gmail.com');
                                $compose->setSubject('Tiphub.co - Donation form submitted');
                                $compose->send();

                                $data = [
                                    'case' => 7,
                                    'donorDetails' => $data['donorDetails']
                                ];
                                $compose = Yii::$app->mailer->compose('body', $data);
                                $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                                $compose->setTo($data['donorDetails']->event->user->email);
                                $compose->setSubject('Tiphub.co - Donation form submitted');
                                $compose->send();
                                return [
                                    'success' => true,
                                    'beneficiaries' => $beneficiaries,
                                    'message' => 'Record has been saved successfully.'
                                ];
                            }
                        }
                        return [
                            'success' => true,
                            'message' => 'Record has been saved successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors()
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }
    public function actionCheckDonor($event_id, $service_id)
    {
        try {
            if (isset($service_id) && isset($event_id)) {
                $find = \app\models\DonorDetails::find()->where(['service_id' => $service_id, 'event_id' => $event_id, 'ip_address' => Yii::$app->request->userIP])->one();
                if ($find) {
                    return [
                        'success' => true,
                    ];
                } else {
                    return [
                        'error' => true,
                    ];
                }
            } else {
                return [
                    'error' => true,
                    'message' => 'Invalid request!'
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    //Label~Module~Action~Url~Icon:List Donors~Event~list-donors~users~users~3
    public function actionListDonors($event_id = null, $verified = 0)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\DonorDetails;
        $query = $model->find()->where(['verified' => $verified, 'event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])]);
        if (isset($event_id)) {
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if (\app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($event_id, $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }
            $query->andWhere(['event_id' => $event_id]);
        }

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->event_id)) {
                    $query->andWhere(['event_id' => $search->event_id]);
                }
                if (!empty($search->name)) {
                    $query->andWhere([
                        'or',
                        ['LIKE', 'donor_first_name', $search->name],
                        ['LIKE', 'donor_last_name', $search->name],
                    ]);
                }
                if (!empty($search->email)) {
                    $query->andWhere(['LIKE', 'donor_email', $search->email]);
                }
                if (!empty($search->phone)) {
                    $query->andWhere(['LIKE', 'donor_phone', $search->phone]);
                }
                if (!empty($search->amount)) {
                    $query->andWhere(['amount_donated' => $search->amount]);
                }
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 50;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $response = [
            'success' => true,
            'donors' => $find,
            'pages' => $pages,
        ];
        return $response;
    }
    public function actionUpdateSequence()
    {
        //print_r($_POST);die();
        if (isset($_POST['apps']) && !empty($_POST['apps'])) {
            try {
                $update = 0;
                foreach ($_POST['apps'] as $key => $val) {
                    $model = new \app\models\Events;
                    $find = $model->find()->where(['id' => $val['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                    if ($find) {
                        $find->sequence = $key;
                        if ($find->save()) {
                            $update++;
                        }
                    }
                }
                if ($update > 0) {
                    return [
                        'success' => true,
                        'message' => 'Applist updated successfully.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Service not found!',
            ];
        }
    }
    /* public function actionSendThanksEmail()
    {         
        if(isset($_POST) && !empty($_POST)){            
            try{
                $model = new \app\models\DonorDetails;
                if(isset($_POST['donor_id'])){
                    $find = $model->find()->where(['id'=>$_POST['donor_id']])->one();
                    if($find){
                        $find->thanks_email_sent = 1;
                        $paymentDetails = \app\models\Payments::find()->joinWith(['event'])->where(['payments.id'=>$_POST['payment_id']])->one();
                        $data = [
                            'case'=>3,
                            'name'=>$find->donor_first_name." ".$find->donor_last_name,
                            'body'=>"<h1>Thank you for your donation.</h1><br/><br/>Hi [name],<br/><br/>Thank you for your generous contribution. Keep this email for your records.<br/><br/><hr/><br/><br/><h2>Donation Summary</h2><br/><strong>Donation Amount: $".$paymentDetails->txn_amount."</strong><br/>Made on ".date("M d, Y",strtotime($paymentDetails->received_at))." for ".$paymentDetails->event->title."<br/><br/>",
                            'subject'=>"Your donation receipt"
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                        $compose->setTo($find->donor_email);
                        $compose->setSubject($data['subject']);
                        if($compose->send() && $find->save()){
                            return [
                                'success' => true,
                                'message' => 'Thanks email has been sent successfully.',
                            ];
                        } 
                    }
                    else{
                        return [
                            'error'=>true,
                            'message'=>'Invalid request!'
                        ];
                    }
                }
                
            }
			catch(\Exception $e){
                return [
                    'error'=>true,
                    'message' => Yii::$app->common->returnException($e),
                ];													
			}
        }
        else{
            return [
                'error'=>true,
                'message'=>'Invalid request!'
            ];
        }
    } */

    /* public function actionSendReceiptEmail()
    {         
        if(isset($_POST) && !empty($_POST)){            
            try{
                $model = new \app\models\DonorDetails;
                if(isset($_POST['donor_id'])){
                    $find = $model->find()->where(['id'=>$_POST['donor_id']])->one();
                    if($find){
                        $find->receipt_sent = 1;
                        $paymentDetails = \app\models\Payments::find()->joinWith(['event.user'])->where(['payments.id'=>$_POST['payment_id']])->one();
                        $data = [
                            'case'=>4,
                            'body'=>"<h1>Donation Received</h1><br/><br/>".$paymentDetails->event->title." has just received a contribution of $".$paymentDetails->txn_amount."! The donor's contact details are below. You may check on all contributions to your site in the Payments page.<br/><br/><hr/><br/><br/><strong>Donor Name</strong><br/>".$find->donor_first_name." ".$find->donor_last_name."<br/><br/><strong>Donor Email</strong><br/>".$find->email."<br/><br/><strong>Paid On</strong><br/>".date("M d, Y",strtotime($paymentDetails->received_at)),
                            'subject'=>$paymentDetails->event->title." has received a contribution"
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                        $compose->setTo($paymentDetails->event->user->email);
                        $compose->setSubject($data['subject']);
                        if($compose->send() && $find->save()){
                            return [
                                'success' => true,
                                'message' => 'Receipt has been sent successfully.',
                            ];
                        } 
                    }
                    else{
                        return [
                            'error'=>true,
                            'message'=>'Invalid request!'
                        ];
                    }
                }
                
            }
			catch(\Exception $e){
                return [
                    'error'=>true,
                    'message' => Yii::$app->common->returnException($e),
                ];													
			}
        }
        else{
            return [
                'error'=>true,
                'message'=>'Invalid request!'
            ];
        }
    } */

    //Label~Module~Action~Url~Icon:Send Payment Confirmation Email~Event~send-payment-confirmation-email~users~users~3
    public function actionSendPaymentConfirmationEmail()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Payments;
                if (isset($_POST['id'])) {
                    $find = $model->find()->joinWith(['event.user', 'donor'])->where(['payments.id' => $_POST['id']])->one();
                    if ($find) {
                        $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                        if (\app\models\Events::find()->where(['id' => $find->event->id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event->id, $eventIds)) {
                            return [
                                'error' => true,
                                'message' => 'Unauthorized request found.'
                            ];
                        }
                        $find->confirmation_email = 1;
                        $data = [
                            'case' => 8,
                            'paymentDetails' => $find,
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                        $compose->setTo($find->donor->donor_email);
                        $compose->setSubject($find->event->title);
                        if ($compose->send() && $find->save()) {
                            return [
                                'success' => true,
                                'message' => 'Payment has been confirmed successfully.',
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Invalid request!'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Update payment method sequence~Event~update-app-sequence~users~users~2
    public function actionUpdateAppSequence()
    {
        //print_r($_POST);die();
        if (isset($_POST['apps']) && !empty($_POST['apps'])) {
            try {
                $update = 0;
                foreach ($_POST['apps'] as $key => $val) {
                    $model = new \app\models\EventApps;
                    $find = $model->find()->where(['id' => $val['id']])->one();
                    if ($find) {
                        $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                        if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                            return [
                                'error' => true,
                                'message' => 'Unauthorized request found.'
                            ];
                        }
                        $find->sequence = $key;
                        if ($find->save()) {
                            $update++;
                        }
                    }
                }
                if ($update > 0) {
                    return [
                        'success' => true,
                        'message' => 'Order updated successfully.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Order not found!',
            ];
        }
    }

    public function actionGetApps($event_id)
    {
        if (isset($event_id) && !empty($event_id)) {
            try {
                $model = new \app\models\EventApps;
                $find = $model->find()->joinWith(['appname'])->where(['event_id' => $event_id])->orderBy('sequence')->asArray()->all();
                if ($find) {
                    return [
                        'success' => true,
                        'apps' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'App not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'App not found!',
            ];
        }
    }

    public function actionGetOverallReports()
    {
        $total_amount_received = \app\models\Transactions::find()->where(['event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])])->sum('amount');

        $payments_through_app = \app\models\Transactions::find()->joinWith(['appname'])->where(['event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])])->groupBy('service_id')->asArray()->all();

        $chart_records = \app\models\Transactions::find()->select(['sum(amount) as total_amount', "DATE_FORMAT(added_on, '%M') as added_on"])->where(['event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])])->groupBy('MONTH(added_on)')->orderBy('MONTH(added_on)')->asArray()->all();

        return [
            'success' => true,
            'total_amount_received' => $total_amount_received,
            'payments_through_app' => $payments_through_app,
            'chart_records' => $chart_records
        ];
    }

    public function actionAssignTeam()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\TeamAndPermissions;
                $POST['TeamAndPermissions'] = $_POST['fields'];
                $POST['TeamAndPermissions']['user_id'] = Yii::$app->user->identity->id;

                if ($model->load($POST)) {
                    $fpermissions = \app\models\ModuleActions::find()->where(['role' => $model->role])->all();
                    if ($fpermissions) {
                        $permissions = [];
                        foreach ($fpermissions as $p) {
                            $permissions[] = $p->action_name;
                        }
                        $POST['TeamAndPermissions']['permissions'] = implode(',', $permissions);
                        $model->load($POST);
                    }
                    if (isset($POST['TeamAndPermissions']['id']) && !empty($POST['TeamAndPermissions']['id'])) {
                        $find = $model->find()->where(['user_id' => $model->user_id, 'id' => $POST['TeamAndPermissions']['id']])->one();
                        if ($find->load($POST) && $find->save()) {
                            return [
                                'success' => true,
                                'message' => 'Team updated successfully.'
                            ];
                        }
                    } else {
                        $find = $model->find()->where(['user_id' => $model->user_id, 'team_id' => $model->team_id, 'event_id' => $model->event_id])->one();
                        if ($find) {
                            return [
                                'error' => true,
                                'message' => 'User already assigned.'
                            ];
                        } else if ($model->save()) {
                            return [
                                'success' => true,
                                'message' => 'User assigned successfully.'
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $model->getErrors()
                            ];
                        }
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionGetTeamDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\TeamAndPermissions;
                $find = $model->find()->joinWith('team')->where(['team_and_permissions.id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'team' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Team not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Team not found!',
            ];
        }
    }

    public function actionGetTeamList($event_id)
    {
        if (isset($event_id) && !empty($event_id)) {
            try {
                $model = new \app\models\TeamAndPermissions;
                $find = $model->find()->joinWith(['team'])->where(['event_id' => $event_id])->asArray()->all();
                if ($find) {
                    $findNew = [];
                    foreach ($find as $k => $v) {
                        $findNew[$k] = $v;
                        if (@unserialize($v['permissions']) !== false) {
                            $findNew[$k]['permissions'] = unserialize($v['permissions']);
                        } else {
                            $findNew[$k]['permissions'] = [];
                        }
                    }
                    return [
                        'success' => true,
                        'team' => $findNew,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Team not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Team not found!',
            ];
        }
    }

    public function actionGetFeatured($pageSize = 3, $user_id = 0, $featured = 1)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'target' => [
                    'asc' => ['target' => SORT_ASC],
                    'desc' => ['target' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['events.id' => SORT_ASC],
                    'desc' => ['events.id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\Events;
        $query = $model->find()->where(['published' => 1]);
        if($user_id){
            $query->andWhere(['user_id' => $user_id]);
        }
        else if($featured){
            $query->andWhere(['featured' => 1]);
        }

        $countQuery = clone $query;

        $query->joinWith(['categoryDetails']);

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        if ($find) {
            $newFind = [];
            foreach ($find as $key => $val) {
                $newFind[$key] = $val;
                $newFind[$key]['description'] = strip_tags($val['description']);
            }
            $response = [
                'success' => true,
                'events' => $newFind,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'events' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    public function actionGetDonorDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\DonorDetails;
                $find = $model->find()->joinWith(['event', 'beneficiaries'])->where(['donor_details.id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'donor' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'It seems donor is not attached with this payment!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Donor not found!',
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Verify Donor~Event~verify-donor~users~users~3
    public function actionVerifyDonor()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\DonorDetails;
                $find = $model->find()->joinWith(['event'])->where(['donor_details.id' => $_POST['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event->id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event->id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    if ($find->verified === 1) {
                        return [
                            'error' => true,
                            'message' => 'Donor already verified.',
                        ];
                    } else {
                        $tmodel = new \app\models\Transactions;
                        $tmodel->event_id = $find->event_id;
                        $tmodel->service_id = $find->service_id;
                        $tmodel->name = $find->donor_first_name . " " . $find->donor_last_name;
                        $tmodel->amount = $find->amount_donated;
                        $tmodel->currency = $find->currency;
                        $tmodel->added_by = Yii::$app->user->identity->id;
                        if ($tmodel->save()) {
                            $pmodel = new \app\models\Payments;
                            $pmodel->event_id = $tmodel->event_id;
                            $pmodel->transaction_id = $tmodel->id;
                            $pmodel->txn_amount = $tmodel->amount;
                            $pmodel->currency = $tmodel->currency;
                            $pmodel->name = $tmodel->name;
                            $pmodel->email = $find->donor_email;
                            $pmodel->donor_id = $find->id;
                            $pmodel->data_dump = serialize([]);
                            $pmodel->user_id = $find->event->user_id;
                            $pmodel->payment_service = $find->service_id;
                            $pmodel->status = 1;
                            $pmodel->received_at = date('Y-m-d H:i:s');
                            $umodel = new \app\models\User;
                            $ufind = $umodel->find()->where(['id' => $find->event->user_id])->one();
                            if ($ufind) {
                                /* $next_transaction_id = $ufind->next_transaction_id;
                                $zero_length = 8 - strlen($next_transaction_id);
                                $pmodel->payment_identifier = $ufind->transaction_prefix.str_pad($next_transaction_id, $zero_length, "0", STR_PAD_LEFT);
                                $ufind->next_transaction_id = $next_transaction_id + 1; */
                                $pmodel->payment_identifier = $ufind->transaction_prefix . mktime();
                                if ($ufind->save()) {
                                    if ($pmodel->save()) {
                                        $find->verified = 1;
                                        if ($find->save()) {
                                            $currency = ['$', '€', '£'];
                                            $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 7])->one();
                                            $eventDetails = \app\models\Events::find()->where(['events.id' => $find->event_id])->one();
                                            $data = [
                                                'case' => 5,
                                                'name' => $find->donor_first_name,
                                                'currency' => $currency[$find->currency],
                                                'amount' => $find->amount_donated,
                                                'date' => date("M d, Y", strtotime($find->added_on)),
                                                'eventDetails' => $eventDetails,
                                                'paymentDetails' => $pmodel,
                                                'templateDetails' => $templateDetails
                                            ];
                                            $compose = Yii::$app->mailer->compose('body', $data);
                                            $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                            $compose->setTo($find->donor_email);
                                            //$compose->setTo('opnsrc.devlpr@gmail.com');
                                            $compose->setSubject(str_replace('[payment-identifier]', $pmodel->payment_identifier, $templateDetails->subject));
                                            if ($compose->send()) {
                                                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 8])->one();
                                                $data = [
                                                    'case' => 4,
                                                    'currency' => $currency[$find->currency],
                                                    'amount' => $find->amount_donated,
                                                    'date' => date("M d, Y", strtotime($find->added_on)),
                                                    'eventDetails' => $eventDetails,
                                                    'donorDetails' => $find,
                                                ];
                                                $compose = Yii::$app->mailer->compose('body', $data);
                                                $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                                                $compose->setTo($eventDetails->user->email);
                                                //$compose->setTo('opnsrc.devlpr@gmail.com');
                                                $compose->setSubject(str_replace('[payment-identifier]', $pmodel->payment_identifier, $templateDetails->subject));
                                                if ($compose->send()) {
                                                    return [
                                                        'success' => true,
                                                        'message' => 'Donor has been verified successfully. Your transaction id is ' . $pmodel->payment_identifier,
                                                    ];
                                                }
                                            }
                                        } else {
                                            return [
                                                'error' => true,
                                                'message' => $find->getErrors(),
                                            ];
                                        }
                                    } else {
                                        return [
                                            'error' => true,
                                            'message' => $pmodel->getErrors(),
                                        ];
                                    }
                                } else {
                                    return [
                                        'error' => true,
                                        'message' => $ufind->getErrors(),
                                    ];
                                }
                            }
                        } else {
                            return [
                                'error' => true,
                                'message' => $tmodel->getErrors(),
                            ];
                        }
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => "Donor does not exist.",
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Order not found!',
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Send Follow Up Email~Event~send-follow-up-email~users~users~3
    public function actionSendFollowUpEmail()
    {
        if (isset($_POST['donor_id']) && !empty($_POST['donor_id'])) {
            try {
                $model = new \app\models\DonorDetails;
                $find = $model->find()->joinWith(['event'])->where(['donor_details.id' => $_POST['donor_id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event->id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event->id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 9])->one();
                    $data = [
                        'case' => 9,
                        'body' => $templateDetails->content,
                        'name' => $find->donor_first_name,
                        'event_organiser_email' => $find->event->email,
                        'event_title' => $find->event->title,
                        'event_url' => Yii::$app->params['siteUrl'] . '/' . $find->event->url . '/' . $find->event->id,

                    ];
                    $compose = Yii::$app->mailer->compose('body', $data);
                    $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                    $compose->setTo($find->donor_email);
                    //$compose->setTo('opnsrc.devlpr@gmail.com');
                    $compose->setSubject($templateDetails->subject);
                    if ($compose->send()) {
                        return [
                            'success' => true,
                            'message' => 'Follow up email has been sent successfully.',
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => "Donor does not exist.",
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Order not found!',
            ];
        }
    }

    public function actionGetCategories()
    {
        try {
            $model = new \app\models\Categories;
            $find = $model->find()->joinWith(['subcategory sc'])->where(['categories.parent_id'=>0])->orderBy('name')->asArray()->all();
            if ($find) {
                return [
                    'success' => true,
                    'categories' => $find,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'Category not found!',
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    public function actionGetSubCategories()
    {
        try {
            $model = new \app\models\SubCategory;
            $find = $model->find()->orderBy('name')->asArray()->all();
            if ($find) {
                return [
                    'success' => true,
                    'categories' => $find,
                ];
            } else {
                return [
                    'error' => true,
                    'message' => 'Category not found!',
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Delete Donor~Event~delete-donor~users~users~2
    public function actionDeleteDonor()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\DonorDetails;
                if (is_array($_POST['id'])) {
                    \app\models\DonorDetails::deleteAll(['id' => $_POST['id']]);
                    return [
                        'success' => true,
                        'message' => 'Donor deleted successfully.'
                    ];
                } else {
                    $find = $model->find()->where(['id' => $_POST['id']])->one();
                    if ($find) {
                        $file_name = $find->file_name;
                        if ($find->delete()) {
                            if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $file_name)) {
                                @unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $file_name);
                            }
                            return [
                                'success' => true,
                                'message' => 'Donor deleted successfully.'
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => 'Donor deleted '
                            ];
                        }
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Donor not found!'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Donor not found!'
            ];
        }
    }

    public function actionGetBasicDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->select(['title', 'image', 'id', 'url'])->where(['id' => $id, 'published' => 1])->one();
                if ($find) {
                    return [
                        'success' => true,
                        'event' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Event not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Event not found!',
            ];
        }
    }

    public function actionSaveMutualAidRequest()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\MutualAidRequests;
                $POST['MutualAidRequests'] = $_POST['fields'];
                if (isset($POST['MutualAidRequests']['reason']) && $POST['MutualAidRequests']['reason'] == "Other") {
                    $POST['MutualAidRequests']['reason'] = $POST['MutualAidRequests']['other_reason'];
                }
                if ($model->load($POST) && $model->save()) {
                    return [
                        'success' => true,
                        'message' => 'Record has been saved successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:List Mutual Aid Requests~Event~list-mutual-aid-requests~users~users~3
    public function actionListMutualAidRequests($event_id)
    {
        $response = [];
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
        if (\app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($event_id, $eventIds)) {
            return [
                'error' => true,
                'message' => 'Unauthorized request found.'
            ];
        }
        $model = new \app\models\MutualAidRequests;
        $query = $model->find()->joinWith(['appname'])->where(['event_id' => $event_id]);

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 50;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $response = [
            'success' => true,
            'mutual_requests' => $find,
            'pages' => $pages,
        ];
        return $response;
    }

    //Label~Module~Action~Url~Icon:Delete Mutual Aid Requests~Event~delete-mutual-requests~users~users~3
    public function actionDeleteMutualAidRequest()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\MutualAidRequests;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    if ($find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'Record deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Record not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Donor not found!'
            ];
        }
    }

    public function actionToggleDonorForm()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Events;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    $checked = filter_var($_POST['checked'], FILTER_VALIDATE_BOOLEAN);
                    $find->disable_donor_form = $checked === true ? 1 : 0;
                    if ($find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Donor form disabled.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Record not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Donor not found!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Update Donor Amount~Event~update-donor-amount~users~users~3
    public function actionUpdateDonorAmount()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\DonorDetails;
                $POST['DonorDetails'] = $_POST['fields'];
                $find = $model->find()->where(['id' => $POST['DonorDetails']['id']])->one();
                if ($find) {
                    $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->event_id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                    if ($find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Amount updated successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Record not found!'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Record not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Donor not found!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:Export Mutual Requests~Event~update-donor-amount~users~users~4
    public function actionExportMutualRequests()
    {
        if (isset($_POST['event_id']) && !empty($_POST['event_id'])) {
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if (\app\models\Events::find()->where(['id' => $_POST['event_id'], 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($_POST['event_id'], $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }
            $model = new \app\models\MutualAidRequests;
            $requests = $model->find()->joinWith(['appname'])->where(['event_id' => $_POST['event_id']])->orderBy("id desc")->all();
            if (!empty($requests)) {
                $file_name = 'mutual-requests-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
                $rows = 1;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'First Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Last Name')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Email')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Amount Requested')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Payment Method')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[6] . $rows, 'Payment Username')->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[7] . $rows, 'Reason for Request of Funds')->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[8] . $rows, 'Submission Date')->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);

                $rows = 2;
                $srno = 1;
                foreach ($requests as $request) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[1] . $rows, $request->first_name);

                    $sheet->setCellValue($cols[2] . $rows, $request->last_name)->getStyle($cols[2] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[3] . $rows, $request->email)->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[4] . $rows, !empty($request->amount) ? (string)$request->amount : "")->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[5] . $rows, !empty($request->appname) ? $request->appname->name : "")->getStyle($cols[5] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[6] . $rows, $request->service_identifier)->getStyle($cols[6] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[7] . $rows, $request->reason);

                    $sheet->setCellValue($cols[8] . $rows, !empty($request->added_on) ? date("M-d-Y", strtotime($request->added_on)) : "")->getStyle($cols[8] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $rows++;
                    $srno++;
                }

                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        }
    }

    public function actionDeleteBeneficiary()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                if (is_array($_POST['id'])) {
                    \app\models\BeneficiaryDetails::deleteAll(['id' => $_POST['id']]);
                    return [
                        'success' => true,
                        'message' => 'Beneficiary deleted successfully.'
                    ];
                } else {
                    $model = new \app\models\BeneficiaryDetails;
                    $find = $model->find()->where(['id' => $_POST['id']])->one();
                    if ($find && $find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'Beneficiary deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Beneficiary not found!'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Beneficiary not found!'
            ];
        }
    }
    public function actionSaveMember()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Members;
                $POST['Members'] = $_POST['fields'];
                $POST['Members']['answer'] = json_encode($POST['Members']['answer']);
                if ($model->load($POST)) {
                    $find = $model->find()->where(['email'=>$POST['Members']['email']])->andWhere(['event_id'=>$model->event_id])->one();
                    if ($find) {
                        return [
                            'exists' => true,
                            'message' => 'You have already registered for this event.',
                            //'dwollaCustomerUrl' => $find->dwolla_customer_url,
                            'memberId' => $find->id
                        ];
                    }
                    if(isset($_POST['fields']['service_id']) && !empty($_POST['fields']['service_id']) && $_POST['fields']['service_id'] == 64){
                        
                        \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                        \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
                        $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
    
                        $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                        $appToken = $tokensApi->token();
                        \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
    
                        
                        $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
                        $customerDetails = $customersApi->create([
                            'firstName' => $model->first_name,
                            'lastName' => $model->last_name,
                            'email' => $model->email,
                            'ipAddress' => Yii::$app->request->userIP
                        ]);
                        $model->dwolla_customer_url = $customerDetails;
                    }
                    
                    if ($model->save()) {
                        if(isset($model->term_accepted) && $model->term_accepted == "1"){
                            Yii::$app->common->registerUser([
                                'email' => $model->email,
                                'first_name' => $model->first_name,
                                'last_name' => $model->last_name,
                                'phone' => $model->phone,
                            ]);
                        }
                        return [
                            'success' => true,
                            'message' => 'You have been registered successfully.',
                            'dwollaCustomerUrl' => $model->dwolla_customer_url,
                            'memberId' => $model->id
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $customerDetails
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    //Label~Module~Action~Url~Icon:List Members~Event~list-members~users~users~3
    public function actionListMembers($event_id = null, $pageSize = 20)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\Members;
        $query = $model->find()->joinwith(['plan', 'form']);
        if (isset($event_id)) {
            $query->where(['members.event_id' => $event_id]);
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if (\app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($event_id, $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }
        } else {
            $query->where(['members.event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])]);
        }

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->event_id)) {
                    $query->andWhere(['event_id' => $search->event_id]);
                }
                if (!empty($search->name)) {
                    $query->andWhere(new \yii\db\Expression('JSON_SEARCH(answer, "all", "%'.$search->name.'%")'));
                }
                if (!empty($search->email)) {
                    $query->andWhere(new \yii\db\Expression('JSON_SEARCH(answer, "all", "%'.$search->email.'%")'));
                }
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        $newFind = [];
        foreach ($find as $key => $val) {
            $newFind[$key] = $val;
            $newFind[$key]['answer'] = json_decode($val['answer']);
            $newFind[$key]['form']['form_data'] = isset($val['form']['form_data']) ? json_decode($val['form']['form_data']) : null;
        }
        return [
            'success' => true,
            'members' => $newFind,
            'pages' => $pages,
        ];
    }

    //Label~Module~Action~Url~Icon:List Members~Event~list-members~users~users~3
    public function actionExportMembers($event_id = null, $pageSize = 20)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\Members;
        $query = $model->find()->joinwith(['plan', 'form']);
        if (isset($event_id)) {
            $query->where(['members.event_id' => $event_id]);
            $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
            if (\app\models\Events::find()->where(['id' => $event_id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($event_id, $eventIds)) {
                return [
                    'error' => true,
                    'message' => 'Unauthorized request found.'
                ];
            }
        } else {
            $query->where(['members.event_id' => \app\models\Events::find()->select('id')->where(['user_id' => Yii::$app->user->identity->id])]);
        }

        /* if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->event_id)) {
                    $query->andWhere(['event_id' => $search->event_id]);
                }
                if (!empty($search->name)) {
                    $query->andWhere([
                        'or',
                        ['LIKE', 'first_name', $search->name],
                        ['LIKE', 'last_name', $search->name],
                    ]);
                }
                if (!empty($search->email)) {
                    $query->andWhere(['LIKE', 'email', $search->email]);
                }
            }
        } */

        $find = $query->orderBy($sort->orders)->asArray()->all();
        
        $newFind = [];
        foreach ($find as $key => $val) {
            $newFind[$key] = $val;
            $newFind[$key]['answer'] = json_decode($val['answer'],true);
        }
        if (!empty($newFind)) {
            
            $file_name = 'members-' . date('m-d-Y');
            $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','o','p','q','r','s','t'];
            $rows = 0;
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            foreach ($cols as $col) {
                /* if($col == 'B'){
                    $sheet->getDefaultColumnDimension()->setWidth(150, 'pt');
                }
                else{
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                } */
                $sheet->getColumnDimension($col)->setAutoSize(true);
            }
            $rows = 1;
            $count = 0;
            foreach ($newFind as $keys => $data) {
                if ($keys==0 && !empty($data['answer'])) {
                    $membershipDueFormData=[];
                    if (!empty($data['event_id'])) {
                        $membershipDueForm =  MembershipDueForms::findOne(['event_id'=>$data['event_id']]);
                        $membershipDueFormData = !empty($membershipDueForm->form_data)?json_decode($membershipDueForm->form_data,true):null;
                    }
                    $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    $sheet->setCellValue($cols[1] . $rows, 'First Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    $sheet->setCellValue($cols[2] . $rows, 'Last Name')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    $sheet->setCellValue($cols[3] . $rows, 'Email')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    $sheet->setCellValue($cols[4] . $rows, 'Phone')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    foreach ($data['answer'] as $label_key =>$value){ 
                        $label_index = $label_key+5;
                        $name = !empty($membershipDueFormData[$label_key])?$membershipDueFormData[$label_key]['label']:$value['name'];
                        $sheet->setCellValue($cols[$label_index] . $rows, $name)->getStyle($cols[$label_index] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $count++;
                    }
                    $count =  !empty($membershipDueFormData)? count($membershipDueFormData):$count;
                }
                $rows++;
                $sheet->setCellValue($cols[0] . $rows, $rows)->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->setCellValue($cols[1] . $rows,$data['first_name'])->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['size' => 14]]);
                $sheet->setCellValue($cols[2] . $rows,$data['last_name'])->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['size' => 14]]);
                $sheet->setCellValue($cols[3] . $rows,$data['email'])->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['size' => 14]]);
                $sheet->setCellValue($cols[4] . $rows,$data['phone'])->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['size' => 14]]);
                foreach ($data['answer'] as $value_key =>$value){
                    if ($count > $value_key) {
                        $value_index = $value_key+5;
                        $valueData = !empty($value['value']) && !is_array($value['value'])?$value['value']:"";
                        $sheet->setCellValue($cols[$value_index] . $rows,$valueData )->getStyle($cols[$value_index] . $rows)->applyFromArray(['font' => ['size' => 14]]);
                    }
                }
            }
            $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
            header('Cache-Control: max-age=0');
            $objectwriter->setOffice2003Compatibility(false);
            $objectwriter->setPreCalculateFormulas(false);
            $objectwriter->save('php://output');

            $spreadsheet->disconnectWorksheets();
            unset($spreadsheet);
            die;
        }
    }
    public function actionDeleteMember()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                if (is_array($_POST['id'])) {
                    \app\models\Members::deleteAll(['id' => $_POST['id']]);
                    return [
                        'success' => true,
                        'message' => 'Member deleted successfully.'
                    ];
                } else {
                    $model = new \app\models\Members;
                    $find = $model->find()->where(['id' => $_POST['id']])->one();
                    if ($find && $find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'Member deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Member not found!'
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Member not found!'
            ];
        }
    }
    public function actionGetEventMembers($event_id = 42, $pageSize = 20, $export = false)
    {
        $response = [];
        $data = [];

        $model =  new Members();
        $query = $model->find()->joinWith(['transaction'])->where(['members.event_id' => $event_id]);
//         $model = new \app\models\Transactions;
//         $query = $model->find()->select(['name','membership_id', 'id', 'sum(amount) as total_amt', 'date_format(added_on, "%M") as month_name'])->where(['event_id' => $event_id]);
        if ($export === "true") {
            $find = $query->all();
            if (!empty($find)) {
                $file_name = 'members-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'];
                $months = ['January','February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    if($col == 'B'){
                        $sheet->getDefaultColumnDimension()->setWidth(150, 'pt');
                    }
                    else{
                        $sheet->getColumnDimension($col)->setAutoSize(true);
                    }
                }
                $rows = 2;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $colIndex = 2;
                foreach($months as $month){
                    $sheet->setCellValue($cols[$colIndex] . $rows, $month)->getStyle($cols[$colIndex] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                    $colIndex++;
                }
                $rows = 3;
                $srno = 1;
                $findData =  \app\models\Transactions::getTransectionData($find,$event_id);
                foreach ($findData as $data) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[1] . $rows, $data['name'])->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $colIndex = 2;
                    foreach($months as $month){
                        foreach ($data['month'] as $dataMonth){
                            if ($month == $dataMonth['name']) {
                                $sign =  $dataMonth['value'] !="Paid"?"$":"";
                                $value = $dataMonth['payment_status']== 1?$sign.$dataMonth['value']:$dataMonth['value'];
                                $color = $dataMonth['payment_status']== 1?'green':'red';
                                $sheet->setCellValue($cols[$colIndex] . $rows,$value )->getStyle($cols[$colIndex] . $rows)->applyFromArray(['font' => ['size' => 14,'color' => array('rgb' => $color)]])->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            }
                        }
                        $colIndex++;
                    }
                    $rows++;
                    $srno++;
                }
                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        }
        $countQuery = clone $query;
        //select name as full_name, sum(amount) as total_amt, date_format(added_on, '%M') as month_name from transactions where event_id = 190 group by name,month_name limit 100
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->all();
        $find =  \app\models\Transactions::getTransectionData($find,$event_id);
        $members = [];
        foreach($find as $f){
            $members[] = $f;
        }
        return [
            'success' => true,
            'members' => $members,
            'pages' => $pages,
        ];
    }
   
    public function actionLinkMember()
    {
        $post =  Yii::$app->request->post();
        if (!empty($post['transaction_id'])) {
            $model = Transactions::findOne($post['transaction_id']);
            if ($model && !empty($post['member_id'])) {
                $memberModel = Members::findOne($post['member_id']);
                if ($memberModel) {
                    $model->member_id = $memberModel->id;
                    $model->membership_id = $memberModel->membership_level;
                    if ($model->save()){
                        return [
                            'success' => true,
                            'message' => 'Member has been linked successfully.'
                        ];
                    }
                }
            }else {
                return [
                    'error' => true,
                    'message' => 'Transaction id is not valid!'
                ];
            }
        }else {
            return [
                'error' => true,
                'message' => 'Transaction id is Required!'
            ];
        }
        return [
            'error' => true,
            'message' => 'Something Went Wrong!'
        ];
    }
    

    public function actionGetPlans($event_id = null, $pageSize = 20)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\MembershipPlans;
        $query = $model->find()->where(['event_id' => $event_id]);

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'plans' => $find,
            'pages' => $pages,
        ];
    }

    public function actionGetTickets($event_id = null, $pageSize = 20)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\EventTickets;
        $query = $model->find()->where(['event_id' => $event_id]);

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'tickets' => $find,
            'pages' => $pages,
        ];
    }
    public function actionGetGroups($event_id = null, $pageSize = 50)
    {
        $response = [];
        $data = [];
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        $model = new \app\models\EventGroups;
        $query = $model->find()->where(['event_id' => $event_id]);
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'groups' => $find,
            'pages' => $pages,
        ];
    }

    public function actionDeletePlan()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\MembershipPlans;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Plan deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Plan not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Plan not found!'
            ];
        }
    }
    public function actionDeleteTicket()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventTickets;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Ticket deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Ticket not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Ticket not found!'
            ];
        }
    }

    public function actionDeleteGroup()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventGroups;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Group deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Group not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Group not found!'
            ];
        }
    }
    public function actionGetPlanDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\MembershipPlans;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'plan' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Plan not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Plan not found!',
            ];
        }
    }

    public function actionGetTicketDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\EventTickets;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'ticket' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Ticket not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Ticket not found!',
            ];
        }
    }

    public function actionGetGroupDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\EventGroups;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'groups' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Group not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Group not found!',
            ];
        }
    }

    public function actionSavePlan()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\MembershipPlans;
                $POST['MembershipPlans'] = $_POST['fields'];
                $POST['MembershipPlans']['user_id'] = Yii::$app->user->identity->id;
                $event = new \app\models\Events;
                $event = $event->find()->where(['id' => $POST['MembershipPlans']['event_id']])->one();

                if (isset($POST['MembershipPlans']['id']) && !empty($POST['MembershipPlans']['id'])) {
                    $find = $model->find()->where(['id' => $POST['MembershipPlans']['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                    
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Plan updated successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => "Plan does not exist."
                        ];
                    }
                } else if ($model->load($POST) && $model->save()) {
                    // Creating Plans on Paypal first.
                    $this->createPaypalProductAndPlan($POST['MembershipPlans'],$event,$model);
                    $model->save();
                    return [
                        'success' => true,
                        'message' => 'Plan saved successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }
    // Function to create Paypal Product
    private function createPaypalProductAndPlan($modelData,$event,$plan_model){
       $access_token = $this->actionGetPaypalToken(true);
       //$access_token = "A21AAMwkt-jldk8JQIDZtTCW0bUsIg0l3a1XcXqMguzNLikis4uluoyF1hUaCxpNWSmR-WT3T40G1C6JVrVbVaHpvQx9wb0Uw";

        $paypal_product_id = $event->paypal_product_id;
        if(trim($event->paypal_product_id) == ""){ // Create product first followed by plan
            $postFields = '{
                "name": "Product for '.$event->title.'",
                "description": "'.$event->title.'",
                "type": "SERVICE",
                "category": "NONPROFIT"
              }';
            $params = [
                CURLOPT_URL => Yii::$app->params['paypalUrl']."v1/catalogs/products",
                //CURLOPT_URL => "https://api-m.sandbox.paypal.com/v1/catalogs/products",
                CURLOPT_POSTFIELDS => $postFields,
                CURLOPT_HTTPHEADER => array("Content-Type: application/json","PayPal-Partner-Attribution-Id: ".Yii::$app->params['BN_Code'],"Authorization: Bearer ".$access_token)
            ];
            $response = Yii::$app->common->setCurlParams($params);
            //print_r($response);
            if ($response['error'] == true) {
                return [
                    'error' => true,
                    'message' => $response['error']
                ];
            }else{
                $event->paypal_product_id = $response['response']->id;
                if($event->save()){
                    $paypal_product_id = $response['response']->id;
                }
            }         
        } 

        //print_r($event);
        if (trim($paypal_product_id)){
            $interval = $modelData['duration'];
            $days_interval = 1; // Monthly 
            if ($interval == 2){
                $days_interval = 90; // Quarterly
            }elseif($interval == 3){
                $days_interval = 180; // Half Yearly
            }elseif($interval == 4){
                $days_interval = 365; // Yearly
            }
            $amount = $modelData['price'];
            $postFields = '{
                "product_id": "'.$paypal_product_id.'",
                "name": "'.$modelData['name'].'",
                "description": "'.$modelData['name'].'",
                "status": "ACTIVE",
                "billing_cycles": [
                    {
                    "frequency": {
                        "interval_unit": "DAY",
                        "interval_count": '. $days_interval.'
                    },
                    "tenure_type": "REGULAR",
                    "sequence": 1,
                    "total_cycles": 0,
                    "pricing_scheme": {
                        "fixed_price": {
                        "value": "'.$amount.'",
                        "currency_code": "USD"
                        }
                    }
                    }
                ],
                "payment_preferences": {
                    "auto_bill_outstanding": true,
                    "setup_fee": {
                    "value": "0",
                    "currency_code": "USD"
                    },
                    "setup_fee_failure_action": "CONTINUE",
                    "payment_failure_threshold": 3
                }
                }';
            $params = [
                CURLOPT_URL => Yii::$app->params['paypalUrl']."v1/billing/plans",
                //CURLOPT_URL => "https://api-m.sandbox.paypal.com/v1/billing/plans",
                CURLOPT_POSTFIELDS => $postFields,
                CURLOPT_HTTPHEADER => array("Content-Type: application/json","PayPal-Partner-Attribution-Id: ".Yii::$app->params['BN_Code'],"Authorization: Bearer ".$access_token)
            ];
            //print_r($params);
            $response = Yii::$app->common->setCurlParams($params);
            //print_r($response);die;
            if ($response['error'] == true) {
                return [
                    'error' => true,
                    'message' => $response['error']
                ];
            }else{
                $plan_model->paypal_plan_id = $response['response']->id;
                $plan_model->paypal_plan_response = json_encode($response['response']);
               // return $response['response']->id;
                
            } 
           // print_r($response);
        }
    }
    public function actionSaveTicket()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\EventTickets;
                $POST['EventTickets'] = $_POST['fields'];
                $POST['EventTickets']['user_id'] = Yii::$app->user->identity->id;
                if (isset($POST['EventTickets']['id']) && !empty($POST['EventTickets']['id'])) {
                    $find = $model->find()->where(['id' => $POST['EventTickets']['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Ticket updated successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => "Ticket does not exist."
                        ];
                    }
                } else if ($model->load($POST) && $model->save()) {
                    return [
                        'success' => true,
                        'message' => 'Ticket saved successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionSaveGroup()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\EventGroups;
                $POST['EventGroups'] = $_POST['fields'];
                if (isset($POST['EventGroups']['id']) && !empty($POST['EventGroups']['id'])) {
                    $find = $model->find()->where(['id' => $POST['EventGroups']['id']])->one();
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Group updated successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => "Group does not exist."
                        ];
                    }
                } else if ($model->load($POST) && $model->save()) {
                    return [
                        'success' => true,
                        'message' => 'Group saved successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionSendMutualEmail()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\MutualAidRequests;
                $find = $model->findOne(['id' => $_POST['id']]);
                if ($find) {
                    /* $templateDetails = \app\models\EmailTemplates::find()->where(['id'=>7])->one();
                    $data = [
                        'case'=>5,
                        'name'=>$find->donor_first_name,
                        'currency'=>$currency[$find->currency],
                        'amount'=>$find->amount_donated,
                        'date'=>date("M d, Y",strtotime($find->added_on)),
                        'eventDetails'=>$eventDetails,
                        'paymentDetails'=>$pmodel,                      
                        'templateDetails'=>$templateDetails
                    ];
                    $compose = Yii::$app->mailer->compose('body', $data);
                    $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                    $compose->setTo($find->donor_email);
                    //$compose->setTo('opnsrc.devlpr@gmail.com');
                    $compose->setSubject(str_replace('[payment-identifier]',$pmodel->payment_identifier,$templateDetails->subject));
                    if($compose->send()){
                        return [
                            'success'=>true,
                            'message'=>'Email has been sent successfully.'
                        ];
                    } */
                } else {
                    return [
                        'error' => true,
                        'message' => 'Request not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetMembers($event_id = null, $pageSize = 20, $details = 0)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'added_on' => [
                    'asc' => ['event_members.added_on' => SORT_ASC],
                    'desc' => ['event_members.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\EventMembers;
        $query = $model->find()->where(['event_id' => $event_id]);
        if ($details == 0) {
            $query->joinWith(['user' => function ($q) {
                $q->select(['email', 'username', 'id']);
            }]);
        } else {
            $query->joinWith(['user' => function ($q) {
                $q->select(['email', 'username', 'id', 'image']);
            }, 'user.services.appname', 'user.qrcodes']);
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'members' => $find,
            'pages' => $pages,
        ];
    }

    public function actionAddMember()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $POST['EventMembers'] = $_POST['fields'];
                $find = \app\models\User::find()->where([
                    'or',
                    ['email' => $POST['EventMembers']['member']],
                    ['username' => $POST['EventMembers']['member']]
                ])->one();
                if (!$find) {
                    return [
                        'error' => true,
                        'message' => "Member does not exist."
                    ];
                } else {
                    if ($find->id == Yii::$app->user->identity->id) {
                        return [
                            'error' => true,
                            'message' => "It seems you are trying to add yourself."
                        ];
                    }
                    $model = new \app\models\EventMembers;
                    $POST['EventMembers']['user_id'] = $find->id;
                    if ($model->load($POST) && $model->save()) {
                        return [
                            'success' => true,
                            'message' => 'Member added successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors()
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionRemoveMember()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventMembers;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find) {
                    if (\app\models\Events::find()->where(['id' => $find->event_id, 'user_id' => Yii::$app->user->identity->id])->count() > 0 && $find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'Member deleted successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Member not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Member not found!'
            ];
        }
    }

    public function actionDeleteTeam()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\TeamAndPermissions;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Team deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Member not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Team not found!'
            ];
        }
    }

    public function actionSaveMembershipDueForm()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\MembershipDueForms;
                $POST['MembershipDueForms'] = $_POST['fields'];
                //$POST['MembershipDueForms']['form_data'] = json_encode($_POST['fields']['form_data']);
                if ($model->load($POST)) {
                    $find = $model->find()->where(['event_id' => $model->event_id])->one();
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Form updated successfully.'
                        ];
                    } else if ($model->load($POST) && $model->save()) {
                        return [
                            'success' => true,
                            'message' => 'Form saved successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => $model->getErrors() ? $model->getErrors() : $find->getErrors()
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetMembershipDueForm($event_id = null)
    {
        if (isset($event_id) && !empty($event_id)) {
            try {
                $model = new \app\models\MembershipDueForms;
                $find = $model->find()->where(['event_id' => $event_id])->asArray()->one();
                if ($find) {
                    $find['form_data'] = json_decode($find['form_data']);
                    return [
                        'success' => true,
                        'form' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'form' => [],
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'form' => [],
            ];
        }
    }

    public function actionGetLocations($location)
    {
        $locations = file_get_contents("https://dev.virtualearth.net/REST/v1/Autosuggest/?query=" . urlencode($location) . "&key=AsRPOoBZ0L_eamYgCmF5wkDSDMMnrj6pkZy2yGUn78uF8hVTlcJ04f64QCbH_J9R");
        $locations = json_decode($locations);
        return [
            'success' => true,
            'locations' => $locations,
            'url' => "https://dev.virtualearth.net/REST/v1/Autosuggest/?query=" . urlencode($location) . "&key=AsRPOoBZ0L_eamYgCmF5wkDSDMMnrj6pkZy2yGUn78uF8hVTlcJ04f64QCbH_J9R"
        ];
    }

    public function actionSaveSoldTicket()
    {
        $transaction = Yii::$app->db->beginTransaction();
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\EventTicketBookings;
                $POST['EventTicketBookings'] = $_POST['fields'];
                if ($model->load($POST)) {
                    $nameArr = explode(' ', $model->name);
                    if ($model->save()) {
                        $booking_id = $model->id;
                        $eventDetails = \app\models\Events::find()->select(['title', 'events.url', 'events.user_id', 'events.id', 'events.allow_custom_price'])->joinWith(['user' => function ($q) {
                            $q->select(['users.id', 'users.name', 'users.email']);
                        }])->where(['events.id' => $model->event_id])->one();
                        if (isset($model->tickets) && !empty($model->tickets)) {
                            $total_amount = 0;
                            foreach ($model->tickets as $ticket) {
                                $etmodel = new EventTickets;
                                $etquery = $etmodel->find();
                                //Start: If vendor is sailing the physical ticket. 
                                if(isset($model->team_id) && $model->team_id !== $eventDetails->user_id){
                                    $etquery->joinWith(['bookings.booking', 'allotments' => function ($q) use($model) {
                                        $q->where(['physical_ticket_allotments.team_id' => $model->team_id]);
                                    }])->where(['event_ticket_bookings.team_id' => $model->team_id]);
                                }
                                //End: If vendor is sailing the physical ticket. 
                                else{
                                    $etquery->joinWith(['bookings.booking']);
                                }
                                $etfind = $etquery->where(['event_tickets.id'=>$ticket['ticket_id']])->one();
                                if($etfind){
                                    //Start: If vendor is sailing the physical ticket. 
                                    if (isset($model->team_id) && $model->team_id !== $eventDetails->user_id) {
                                        $totalBalanceTicket = $etfind->allotments[0]->total_tickets;
                                    }
                                    //End: If vendor is sailing the physical ticket. 
                                    else{
                                        $totalBalanceTicket = $etfind->max_limit - $etfind->physical_ticket_limit;
                                    }
                                    foreach($etfind->bookings as $booking){
                                        if(isset($booking->booking) && !empty($booking->booking) && $booking->booking->status == 1){
                                            $totalBalanceTicket = $totalBalanceTicket - $booking->total_tickets;
                                        }
                                    }
                                    //dd($totalBalanceTicket);
                                    if($ticket['total_tickets'] <= $totalBalanceTicket){
                                        for($i = 0; $i < $ticket['total_tickets']; $i++){
                                            $loadVal['EventTicketBookingDetails'] = $ticket;
                                            $loadVal['EventTicketBookingDetails']['booking_id'] = $model->id;
                                            $loadVal['EventTicketBookingDetails']['total_tickets'] = 1;
                                            $loadVal['EventTicketBookingDetails']['total_price'] =  $ticket['ticket_price'];
                                            $etbdModel = new \app\models\EventTicketBookingDetails;
                                            if($model->is_physical_ticket == 1){
                                                $ptDetails = \app\models\PhysicalTickets::find()->where(['id'=>$POST['EventTicketBookings']['selectedTickets'],'sold'=>0])->one();
                                                if($ptDetails){
                                                    $loadVal['EventTicketBookingDetails']['custom_ticket_id'] =  $ptDetails->custom_ticket_id;
                                                    $loadVal['EventTicketBookingDetails']['qrcode'] =  $ptDetails->qrcode;
                                                }
                                            }
                                            else{
                                                if(isset($ticket['custom_price'])){
                                                    $loadVal['EventTicketBookingDetails']['custom_price'] =  $ticket['custom_price'];
                                                }
                                            }

                                            if ($etbdModel->load($loadVal)) {
                                                if ($etbdModel->save()) {
                                                    if($eventDetails->allow_custom_price == 1){
                                                        if(isset($ticket['custom_price']) && $ticket['custom_price'] > 0){
                                                            $total_amount = $total_amount + $ticket['custom_price'];
                                                        }
                                                        else{
                                                            $total_amount = $total_amount + $ticket['ticket_price'];
                                                        }
                                                    }
                                                    else{
                                                        $total_amount = $total_amount + $ticket['ticket_price'];
                                                    }
                                                    if(!isset($model->is_physical_ticket)){
                                                        $custom_ticket_id = '';
                                                        foreach ($nameArr as $name) {
                                                            $custom_ticket_id .=  strtoupper($name[0]);
                                                        }
                                                        $etbdModel->custom_ticket_id = $custom_ticket_id . $etbdModel->id;
                                                    }
                                                    $etbdModel->save();
                                                    if(isset($model->is_physical_ticket) && $model->is_physical_ticket == 1 && isset($ptDetails)){
                                                        $ptDetails->sold = 1;
                                                        $ptDetails->save();
                                                    }
                                                } else {
                                                    $transaction->rollBack();
                                                    return [
                                                        'error' => true,
                                                        'message' => $etbdModel->getErrors()
                                                    ];
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        $transaction->rollBack();
                                        return [
                                            'error' => true,
                                            'message' => $etfind->name .' has already been sold. Please reach out to event organizers for any questions.'
                                        ];
                                    }
                                }
                            }
                            $model->total_amount = $total_amount + $model->donation_amount + ($total_amount * ($model->service_fee/100));
                            if($model->total_amount == 0){
                                $model->status = 1;
                            }
                            $model->save();
                        }
                        $bookingDetails = \app\models\EventTicketBookingDetails::find()->joinWith(['ticket'])->where(['booking_id' => $model->id])->all();
                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 14])->one();
                        Yii::$app->queue->delay(30 * 60)->push(new \app\components\MailJob([
                            'case' => 14,
                            'name' => $model->first_name,
                            'body' => $templateDetails->content,
                            'eventDetails' => $eventDetails,
                            'ticketDetails' => $bookingDetails,
                            'templateDetails' => $templateDetails,
                            'to' => $model->email,
                            'bookingId' => $model->id
                        ]));

                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 15])->one();
                        $data = [
                            'case' => 15,
                            'name' => $eventDetails->user->name,
                            'buyer' => $model,
                            'body' => $templateDetails->content,
                            'eventDetails' => $eventDetails,
                            'ticketDetails' => $bookingDetails
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                        $compose->setTo($eventDetails->user->email);
                        //$compose->setTo('opnsrc.devlpr@gmail.com');
                        $compose->setSubject($templateDetails->subject);
                        if ($compose->send()) {
                            if(isset($model->term_accepted) && $model->term_accepted == "1"){
                                Yii::$app->common->registerUser([
                                    'email' => $model->email,
                                    'first_name' => $model->first_name,
                                    'last_name' => $model->last_name,
                                    'phone' => $model->phone,
                                ]);
                            }
                            $transaction->commit();
                            return [
                                'success' => true,
                                'booking_id'=>$booking_id,
                                'message' => 'Record has been saved successfully. Please pay the price for the ticket to get confirmation.'
                            ];
                        }
                    } else {
                        $transaction->rollBack();
                        return [
                            'error' => true,
                            'message' => $model->getErrors()
                        ];
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionTestPayment()
    {
        $locations = file_get_contents("https://dev.virtualearth.net/REST/v1/Autosuggest/?query=patna,bihar&key=AsRPOoBZ0L_eamYgCmF5wkDSDMMnrj6pkZy2yGUn78uF8hVTlcJ04f64QCbH_J9R");
        print_r($locations);
        die;
        /* \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
        \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
        $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
        $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
        $appToken = $tokensApi->token();
        \DwollaSwagger\Configuration::$access_token = $appToken->access_token;

        $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
        $myCusties = $customersApi->_list(10);
        print_r($appToken);die; */
    }

    /*public function actionGetBookings($event_id = NULL, $pageSize = 50, $export = false, $format = null)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'first_name' => [
                    'asc' => ['first_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['event_ticket_bookings.added_on' => SORT_ASC],
                    'desc' => ['event_ticket_bookings.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\EventTicketBookings;
        $query = $model->find()->joinWith(['details.ticket','service'])->where(['event_ticket_bookings.event_id' => $event_id])->andWhere("event_ticket_bookings.email != ''");

        $search = new \app\models\SearchForm;
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        
        if ($search->load($GET)) {
            if (isset($search->broadcast_sms) && !empty($search->broadcast_sms)) {
                // Sending SMSs in Queue
                Yii::$app->queue->push(new \app\components\SendSms([
                    'query' => $query,
                    'sms_body' => $search->sms_content,
                    'twilioSid' => Yii::$app->params['twilioSid'], 
                    'twilioToken' => Yii::$app->params['twilioToken'],
                    'twilioFromNumber' => Yii::$app->params['twilioFromNumber']
                ]));
            }
            if (isset($search->broadcast_email) && !empty($search->broadcast_email)) {
                // Sending broadcast Emails in Queue
                Yii::$app->queue->push(new \app\components\SendEmail([
                    'query' => $query,
                    'email_content' => $search->email_content,
                    'email_subject' => $search->email_subject
                ]));
            }
            if (!empty($search->first_name)) {
                $query->andWhere(['LIKE', 'first_name', $search->first_name]);
            }
            if (!empty($search->email)) {
                $query->andWhere(['LIKE', 'email', $search->email]);
            }
            if (!empty($search->last_name)) {
                $query->andWhere(['LIKE', 'last_name', $search->last_name]);
            }
            if (!empty($search->status)) {
                $query->andWhere(['status' => $search->status]);
            }
            if (!empty($search->status_unconfirmed)) {
                $query->andWhere(['status' => 0]);
            }
            if (!empty($search->checked_in)) {
                $query->andWhere(['checked_in' => $search->checked_in]);
            }
            if (!empty($search->ticket_number)) {
                $query->andWhere(['event_ticket_booking_details.custom_ticket_id' => $search->ticket_number]);
            }
            if (!empty($search->ticket_id)) {
                $ticket_id = $search->ticket_id;
                $query->joinWith(['details'=>function($q) use($ticket_id) {$q->where(['event_ticket_booking_details.ticket_id' => $ticket_id]);}])->andWhere(['event_ticket_booking_details.ticket_id' => $search->ticket_id]);
            }
        }
        
        if ($export === "true") {
            $find = $query->groupBy('event_ticket_bookings.id')->orderBy($sort->orders)->asArray()->all();
            if (!empty($find)) {
                $file_name = 'bookings-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    if($col == 'B'){
                        $sheet->getDefaultColumnDimension()->setWidth(150, 'pt');
                    }
                    else{
                        $sheet->getColumnDimension($col)->setAutoSize(true);
                    }
                    
                }
                
                // $sheet->mergeCells("A$rows:K$rows");
                // $sheet->setCellValue($cols[0].$rows, 'Donors')->getStyle($cols[0].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>18]]);
                // $sheet->mergeCells("L$rows:X$rows");
                // $sheet->setCellValue($cols[11].$rows, 'Beneficiaries')->getStyle($cols[11].$rows)->applyFromArray(['font'=>['bold'=>true,'size'=>18]]);

                $rows = 2;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Email')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Phone')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Total Price')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Booked On')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);


                $rows = 3;
                $srno = 1;
                foreach ($find as $data) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    
                    $sheet->setCellValue($cols[1] . $rows, $data['first_name'] . ' ' . $data['last_name'])->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[2] . $rows, $data['email'])->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[3] . $rows, $data['phone'])->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[4] . $rows, '$'.number_format($data['total_amount'], 2))->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[5] . $rows, $data['added_on'])->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    if ($data['details']) {
                        $bsrn = 1;
                        $rows++;
                        $sheet->setCellValue($cols[1] . $rows, '#')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);;
                        $sheet->setCellValue($cols[2] . $rows, 'Ticket')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $sheet->setCellValue($cols[3] . $rows, 'Ticket Category')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $sheet->setCellValue($cols[4] . $rows, 'Price')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);;
                        $sheet->setCellValue($cols[5] . $rows, 'Total Tickets')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $sheet->setCellValue($cols[6] . $rows, 'Sub Total')->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 12]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                        $rows++;
                        foreach ($data['details'] as $data) {
                            $sheet->setCellValue($cols[1] . $rows, $bsrn)->getStyle($cols[1] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[2] . $rows, $data['custom_ticket_id'])->getStyle($cols[2] . $rows)->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            // try {
                            //     $sheet->setCellValue($cols[3] . $rows, ($data['total_price'] / $data['total_tickets']))->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            // }
                            // catch (\Exception $e) {
                            //     print_r($data)
                            // }
                            $sheet->setCellValue($cols[3] . $rows, $data['ticket']['name'])->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            if($data['total_tickets'] != 0){
                                $sheet->setCellValue($cols[4] . $rows, '$'.(number_format($data['total_price'] / $data['total_tickets'], 2)))->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            }
                            else{
                                $sheet->setCellValue($cols[4] . $rows, 0)->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            }
                            
                            $sheet->setCellValue($cols[5] . $rows, $data['total_tickets'])->getStyle($cols[5] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[6] . $rows, '$'.number_format($data['total_price'],2))->getStyle($cols[6] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                            $bsrn++;
                            $rows++;
                        }
                    }
                    $rows++;
                    $srno++;
                }
                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        } else {
            $query->groupBy('event_ticket_bookings.id');
            $countQuery = clone $query;

            $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
            $pages->pageSize = $pageSize;
            $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
            //echo $find->createCommand()->getRawSql();die;
            
            return [
                'fields' => $search,
                'success' => true,
                'bookings' => $find,
                'pages' => $pages,
            ];
        }
    }*/

    public function actionGetBookings($event_id = NULL, $pageSize = 50, $export = false, $format = null)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'first_name' => [
                    'asc' => ['first_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['event_ticket_bookings.added_on' => SORT_ASC],
                    'desc' => ['event_ticket_bookings.added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);

        $model = new \app\models\EventTicketBookings;
        $query = $model->find()->joinWith(['details.ticket','service','team'=>function($q){$q->select(['users.id','users.name','users.username']);}])->where(['event_ticket_bookings.event_id' => $event_id])->andWhere("event_ticket_bookings.email != ''");
        $findEvent = \app\models\Events::find()->where(['id'=>$event_id,'user_id'=>Yii::$app->user->identity->id])->one();
        if(!$findEvent){
            $query->andWhere(['is_physical_ticket' => 1, 'team_id' => Yii::$app->user->identity->id]);
        }

        $search = new \app\models\SearchForm;
        //dd($_GET['fields']);
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        
        if ($search->load($GET)) {
            if (isset($search->broadcast_sms) && !empty($search->broadcast_sms)) {
                // Sending SMSs in Queue
                Yii::$app->queue->push(new \app\components\SendSms([
                    'query' => $query,
                    'sms_body' => $search->sms_content,
                    'twilioSid' => Yii::$app->params['twilioSid'], 
                    'twilioToken' => Yii::$app->params['twilioToken'],
                    'twilioFromNumber' => Yii::$app->params['twilioFromNumber']
                ]));
            }
            if (isset($search->broadcast_email) && !empty($search->broadcast_email)) {
                // Sending broadcast Emails in Queue
                Yii::$app->queue->push(new \app\components\SendEmail([
                    'query' => $query,
                    'email_content' => $search->email_content,
                    'email_subject' => $search->email_subject
                ]));
            }
            if (!empty($search->first_name)) {
                $query->andWhere(['LIKE', 'event_ticket_bookings.first_name', $search->first_name]);
            }
            if (!empty($search->email)) {
                $query->andWhere(['LIKE', 'event_ticket_bookings.email', $search->email]);
            }
            if (!empty($search->last_name)) {
                $query->andWhere(['LIKE', 'event_ticket_bookings.last_name', $search->last_name]);
            }
            if (!empty($search->status)) {
                $query->andWhere(['event_ticket_bookings.status' => $search->status]);
            }
            if (!empty($search->status_unconfirmed)) {
                $query->andWhere(['event_ticket_bookings.status' => 0]);
            }
            if (!empty($search->checked_in)) {
                $query->andWhere(['checked_in' => $search->checked_in]);
            }
            if (!empty($search->ticket_number)) {
                $query->andWhere(['event_ticket_booking_details.custom_ticket_id' => $search->ticket_number]);
            }
            if (!empty($search->ticket_id)) {
                $ticket_id = $search->ticket_id;
                $query->joinWith(['details'=>function($q) use($ticket_id) {$q->where(['event_ticket_booking_details.ticket_id' => $ticket_id]);}])->andWhere(['event_ticket_booking_details.ticket_id' => $search->ticket_id]);
            }
            if (!empty($search->service_id)) {
                $query->andWhere(['event_ticket_bookings.service_id' => $search->service_id]);
            }
        }
        
        if ($export === "true") {
            $find = $query->groupBy('event_ticket_bookings.id')->orderBy($sort->orders)->asArray()->all();
            if (!empty($find)) {
                $file_name = 'bookings-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    if($col == 'B'){
                        $sheet->getDefaultColumnDimension()->setWidth(150, 'pt');
                    }
                    else{
                        $sheet->getColumnDimension($col)->setAutoSize(true);
                    }
                    
                }

                $rows = 2;
                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Email')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Phone')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Total Price')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Booked On')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[6] . $rows, 'Ticket')->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[7] . $rows, 'Ticket Category')->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[8] . $rows, 'Price')->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[9] . $rows, 'Total Tickets')->getStyle($cols[9] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[10] . $rows, 'Sub Total')->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);


                $rows = 3;
                $srno = 1;
                $grand_total = 0;
                foreach ($find as $data) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    
                    $sheet->setCellValue($cols[1] . $rows, $data['first_name'] . ' ' . $data['last_name'])->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->setCellValue($cols[2] . $rows, $data['email'])->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[3] . $rows, $data['phone'])->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[4] . $rows, '$'.number_format($data['total_amount'], 2))->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[5] . $rows, $data['added_on'])->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    
                    if ($data['details']) {
                        foreach ($data['details'] as $data) {
                            $sheet->setCellValue($cols[6] . $rows, $data['custom_ticket_id'])->getStyle($cols[6] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setWrapText(true)->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[7] . $rows, $data['ticket']['name'])->getStyle($cols[7] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            if($data['total_tickets'] != 0){
                                $sheet->setCellValue($cols[8] . $rows, '$'.(number_format($data['total_price'] / $data['total_tickets'], 2)))->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            }
                            else{
                                $sheet->setCellValue($cols[8] . $rows, 0)->getStyle($cols[8] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            }
                            
                            $sheet->setCellValue($cols[9] . $rows, $data['total_tickets'])->getStyle($cols[9] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $sheet->setCellValue($cols[10] . $rows, '$'.number_format($data['total_price'],2))->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['size' => 14]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $grand_total = $grand_total + $data['total_price'];
                            $rows++;
                        }
                    }
                    $srno++;
                }
                
                $sheet->setCellValue($cols[10] . $rows, '$'.number_format($grand_total,2))->getStyle($cols[10] . $rows)->applyFromArray(['font' => ['size' => 20]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $sheet->mergeCells("A$rows:J$rows");
                $sheet->setCellValue($cols[0] . $rows, 'Grand Total')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['size' => 20]])->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        } else {
            $query->groupBy('event_ticket_bookings.id');
            $countQuery = clone $query;

            $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
            $pages->pageSize = $pageSize;
            $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
            //echo $find->createCommand()->getRawSql();die;
            
            return [
                'fields' => $search,
                'success' => true,
                'bookings' => $find,
                'pages' => $pages,
            ];
        }
    }

    public function actionConfirmBooking()
    {
        $transaction = Yii::$app->db->beginTransaction();
        if (isset($_POST['booking_id']) && !empty($_POST['booking_id'])) {
            try {
                $model = new \app\models\EventTicketBookings;
                $find = $model->find()->joinWith(['details.ticket', 'event.user'])->where(['event_ticket_bookings.id' => $_POST['booking_id']])->one();
                if ($find) {
                    $find->status = 1;
                    if ($find->save()) {
                        //Start add payment to transaction table
                        if(empty($find->transaction_id)){
                            $tmodel = new \app\models\Transactions;
                            $tmodel->event_id = $find->event_id;
                            $tmodel->service_id = $find->service_id;
                            $tmodel->name = $find->first_name . ' '. $find->last_name;
                            $tmodel->amount = $find->total_amount;
                            $tmodel->currency = $find->event->currency;
                            $tmodel->added_by = Yii::$app->user->identity->id;
                            $tmodel->check_in = 1;
                            $tmodel->check_in_date = date("Y-m-d H:i:s");
                            if($tmodel->save()){
                                $pmodel = new \app\models\Payments;
                                $pmodel->txn_amount = $tmodel->amount;
                                $pmodel->received_at = date("Y-m-d H:i:s");
                                $pmodel->name = $tmodel->name;
                                $pmodel->payment_service =  $tmodel->service_id;
                                $pmodel->payment_identifier =  'TPH' . mt_rand();
                                $pmodel->type = 1;
                                $pmodel->transaction_id = $tmodel->id;
                                $pmodel->event_id = $tmodel->event_id;
                                $pmodel->user_id = Yii::$app->user->identity->id;
                                $pmodel->status = 1;
                                $pmodel->data_dump = json_encode($tmodel);
                                if($pmodel->save()){
                                    $find->transaction_id = $tmodel->id;
                                    $find->save();
                                }
                                else{
                                    $transaction->rollBack();
                                    return [
                                        'error' => true,
                                        'message' => $pmodel->getErrors()
                                    ];
                                }
                            }
                            else{
                                $transaction->rollBack();
                                return [
                                    'error' => true,
                                    'message' => $tmodel->getErrors()
                                ];
                            }
                        }
                        //End: add payment to transaction table
                        $transaction->commit();
                        // send ticket confirmation email
                        $confirm_ticket_email = Yii::$app->common->sendTicketConfirmationEmail($find);
                        /* $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 13])->one();
                        $data = [
                            'case' => 13,
                            'name' => $find->first_name,
                            'full_name' => $find->first_name . " " . $find->last_name,
                            'total_amount' => $find->total_amount,
                            'body' => $templateDetails->content,
                            'ticketDetails' => $find->details
                        ];

                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                        $compose->setTo($find->email);
                        $compose->setSubject($templateDetails->subject);
                        foreach ($find->details as $ticket) {
                            $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/ticket/' . $ticket->id))->setSize(300)->setMargin(5);
                            $compose->attachContent($qrCode->writeString(), ['fileName' => $ticket->custom_ticket_id . '.png', 'contentType' => $qrCode->getContentType()]);
                        } */
                        if ($confirm_ticket_email == true) {
                            $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 16])->one();
                            $data = [
                                'case' => 16,
                                'name' => $find->event->user->name,
                                'full_name' => $find->first_name . " " . $find->last_name,
                                'total_amount' => $find->total_amount,
                                'body' => $templateDetails->content,
                                'ticketDetails' => $find->details
                            ];

                            $compose = Yii::$app->mailer->compose('body', $data);
                            $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                            $compose->setTo($find->event->user->email);
                            $compose->setSubject($templateDetails->subject);
                            foreach ($find->details as $ticket) {
                                $qrCode = (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/ticket/' . $ticket->id))->setSize(300)->setMargin(5);
                                $compose->attachContent($qrCode->writeString(), ['fileName' => $ticket->custom_ticket_id . '.png', 'contentType' => $qrCode->getContentType()]);
                            }
                            if ($compose->send()) {
                                return [
                                    'success' => true,
                                ];
                            }
                        }
                    }
                } else {
                    $transaction->rollBack();
                    return [
                        'error' => true,
                    ];
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            $transaction->rollBack();
            return [
                'error' => true,
                'message' => 'Invalid request.'
            ];
        }
    }
    //Label~Module~Action~Url~Icon:Check In Booking~Event~view-ticket-details~users~users~1,2
    public function actionViewTicketDetails($id = null, $user_id = 0)
    {
        $model = new \app\models\EventTicketBookingDetails;
        if (isset($id) && !empty($id)) {
            $ticket = $model->find()->joinWith(['booking.event', 'ticket'])->where(['event_ticket_booking_details.id' => $id])->one();
            $ticketResponse = $model->find()->joinWith(['booking.event', 'ticket'])->where(['event_ticket_booking_details.id' => $id])->asArray()->one();
            if ($ticket){
                $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id, $user_id);
                if($ticket->booking->event->user_id == $user_id || in_array($ticket->booking->event->id, $eventIds)){
                    if($ticket->checked_in == 0){
                        $ticket->checked_in = 1;
                        if ($ticket->save()) {
                            $model = new \app\models\EventTicketBookings;
                            $find = $model->find()->joinWith(['event'])->where(['event_ticket_bookings.id' => $ticket->booking_id])->one();

                            $bookingDetails = \app\models\EventTicketBookingDetails::find()->joinWith(['ticket'])->where(['booking_id' => $find->id])->all();
                            $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 17])->one();
                            $data = [
                                'case' => 17,
                                'name' => $find->first_name,
                                'body' => $templateDetails->content,
                                'buyer' => $find,
                                'eventDetails' => $find->event,
                                'ticketDetails' => $bookingDetails
                            ];
                            $compose = Yii::$app->mailer->compose('body', $data);
                            $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                            $compose->setTo($find->email);
                            //$compose->setTo('amrender.kumar1986@gmail.com');
                            $compose->setSubject($templateDetails->subject);
                            if ($compose->send()) {
                                return [
                                    'success' => true,
                                    'alertBox' => true,
                                    'message' => 'This ticket has been checked-in successfully.',
                                    'ticket' => $ticketResponse
                                ];
                            }
                        }
                    }
                    else{
                        return [
                            'error' => true,
                            'alertBox' => true,
                            'message' => 'This ticket has already been checked-in.',
                            'ticket' => $ticketResponse
                        ];
                    }
                }
                else{
                    return [
                        'success' => true,
                        'alertBox' => false,
                        'ticket' => $ticketResponse
                    ];
                }
            }  
            else{
                return [
                    'error' => true,
                    'alertBox' => true,
                    'message' => 'Booking not found!'
                ];
            } 
        } else {
            return [
                'error' => true,
                'alertBox' => true,
                'message' => 'Booking not found!'
            ];
        }
    }
    public function actionDeleteBooking()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventTicketBookings;
                $find = $model->find()->where(['id' => $_POST['id'], 'event_id' => $_POST['event_id']])->one();
                if ($find && $find->delete()) {
                    \app\models\EventTicketBookingDetails::deleteAll(['booking_id' => $_POST['id']]);
                    return [
                        'success' => true,
                        'message' => 'Booking deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Booking not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Booking not found!'
            ];
        }
    }

    public function actionSaveGroupPayment()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\EventGroupPayments;
                $POST['EventGroupPayments'] = $_POST['fields'];
                if (isset($POST['EventGroupPayments']['id']) && !empty($POST['EventGroupPayments']['id'])) {
                    $find = $model->find()->where(['id' => $POST['EventGroupPayments']['id']])->one();
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Record updated successfully.'
                        ];
                    } else {
                        return [
                            'error' => true,
                            'message' => "Record does not exist."
                        ];
                    }
                } else if ($model->load($POST) && $model->save()) {
                    $find = \app\models\EventGroupPayments::find()->joinWith(['event'])->where(['event_group_payments.id' => $model->id])->one();
                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 3])->one();
                    $data = [
                        'case' => 3,
                        'body' => $templateDetails->content,
                        'name' => $find->first_name,
                        'event_title' => $find->event->title,
                        'event_url' => Yii::$app->params['siteUrl'] . '/' . $find->event->url . '/' . $find->event->id,
                    ];
                    $compose = Yii::$app->mailer->compose('body', $data);
                    $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                    $compose->setTo($find->email);
                    $compose->setSubject($templateDetails->subject);
                    if ($compose->send()) {
                        return [
                            'success' => true,
                            'message' => 'Record saved successfully.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors()
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetGroupPayments($event_id = NULL, $pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'first_name' => [
                    'asc' => ['first_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);
        
        $model = new \app\models\EventGroupPayments;
        $query = $model->find()->joinWith(['group'])->where(['event_group_payments.event_id' => $event_id]);

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->first_name)) {
                    $query->andWhere(['LIKE', 'first_name', $search->first_name]);
                }
                if (!empty($search->email)) {
                    $query->andWhere(['LIKE', 'email', $search->email]);
                }
                if (!empty($search->last_name)) {
                    $query->andWhere(['LIKE', 'last_name', $search->last_name]);
                }
                if (!empty($search->phone)) {
                    $query->andWhere(['LIKE', 'phone', $search->phone]);
                }
                if (!empty($search->status)) {
                    $query->andWhere(['status' => $search->status]);
                }
            }
        }
        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'groupPayments' => $find,
            'pages' => $pages,
        ];
    }

    public function actionGetReligiousPayments($event_id = NULL, $pageSize = 50, $export = false)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'amount' => [
                    'asc' => ['payments.amount' => SORT_ASC],
                    'desc' => ['payments.amount' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['payments.received_at' => SORT_ASC],
                    'desc' => ['payments.received_at' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);
        
        $model = new \app\models\Payments;
        $query = $model->find()->innerJoinWith(['transaction.program'])->where(['payments.event_id' => $event_id]);

        if (isset($_GET['fields']) && !empty($_GET['fields'])) {
            $search = new \app\models\SearchForm;
            $GET['SearchForm'] = json_decode($_GET['fields'], true);
            if ($search->load($GET)) {
                if (!empty($search->name)) {
                    $query->andWhere(['LIKE', 'payments.name', $search->name]);
                }
                if (!empty($search->email)) {
                    $query->andWhere(['LIKE', 'email', $search->email]);
                }
                if (!empty($search->program_id)) {
                    $query->andWhere(['program_id' => $search->program_id]);
                }
            }
        }
        if ($export == 'true') {
            $find = $query->all();
            if (!empty($find)) {
                $file_name = 'payments-' . date('m-d-Y');
                $cols = ['A', 'B', 'C', 'D', 'E', 'F'];
                $rows = 1;
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                foreach ($cols as $col) {
                    $sheet->getColumnDimension($col)->setAutoSize(true);
                }
                $sheet->setCellValue($cols[0] . $rows, 'First Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);

                $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[1] . $rows, 'Name')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[2] . $rows, 'Email')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[3] . $rows, 'Program')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[4] . $rows, 'Amount')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                $sheet->setCellValue($cols[5] . $rows, 'Added On')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);

                $rows = 2;
                $srno = 1;
                foreach ($find as $data) {
                    $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[1] . $rows, $data->name)->getStyle($cols[1] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[2] . $rows, $data->email)->getStyle($cols[2] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[3] . $rows, $data->transaction->program->name)->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[4] . $rows, $data->txn_amount)->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $sheet->setCellValue($cols[5] . $rows, date("M d, Y", strtotime($data->received_at)))->getStyle($cols[5] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                    $rows++;
                    $srno++;
                }
                $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objectwriter->setOffice2003Compatibility(false);
                $objectwriter->setPreCalculateFormulas(false);
                $objectwriter->save('php://output');

                $spreadsheet->disconnectWorksheets();
                unset($spreadsheet);
                exit;
            }
        }
        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'records' => $find,
            'pages' => $pages,
        ];
    }

    public function actionDeleteGroupPayment()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventGroupPayments;
                $find = $model->find()->where(['id' => $_POST['id'], 'event_id' => $_POST['event_id']])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Payment deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Payment not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Payment not found!'
            ];
        }
    }
    public function actionCheckInBooking()
    {
        if (isset($_POST['ticket_id']) && !empty($_POST['ticket_id'])) {
            try {
                $model = new \app\models\EventTicketBookingDetails;
                $find = $model->find()->joinWith(['booking.event','ticket'])->where(['event_ticket_booking_details.id' => $_POST['ticket_id']])->one();
                if ($find) {
                    /* $eventIds = Yii::$app->common->getAssingedEvents(Yii::$app->controller->action->id);
                    if (\app\models\Events::find()->where(['id' => $find->booking->event->id, 'user_id' => Yii::$app->user->identity->id])->count() == 0 && !in_array($find->booking->event->id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    } */
                    $find->checked_in = 1;
                    if ($find->save()) {
                        $bookingDetails = \app\models\EventTicketBookingDetails::find()->joinWith(['ticket'])->where(['event_ticket_booking_details.id' => $find->id])->all();
                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 17])->one();
                        $data = [
                            'case' => 17,
                            'name' => $find->booking->first_name,
                            'body' => $templateDetails->content,
                            'buyer' => $find->booking,
                            'eventDetails' => $find->booking->event,
                            'ticketDetails' => $bookingDetails
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                        $compose->setTo($find->booking->email);
                        //$compose->setTo('opnsrc.devlpr@gmail.com');
                        $compose->setSubject($templateDetails->subject);
                        if ($compose->send()) {
                            return [
                                'success' => true,
                            ];
                        }
                    }
                } else {
                    return [
                        'error' => true,
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request.'
            ];
        }
    }

    public function actionCheckOutBooking()
    {
        if (isset($_POST['ticket_id']) && !empty($_POST['ticket_id'])) {
            try {
                $model = new \app\models\EventTicketBookingDetails;
                $find = $model->find()->where(['id' => $_POST['ticket_id']])->one();
                if ($find) {
                    $find->checked_in = 0;
                    if ($find->save()) {
                        return [
                            'success' => true,
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request.'
            ];
        }
    }

    public function actionGetTicketsWithDetails($event_id = null, $physical_ticket = 0, $sale = 0, $user_id = null)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        $model = new \app\models\EventTickets;
        $query = $model->find()->where(['event_tickets.event_id' => $event_id]);
        if($sale){
            $findEvent = \app\models\Events::find()->where(['id'=>$event_id,'user_id'=>$user_id])->one();
            if(!$findEvent){
                $query->andWhere(['event_tickets.id'=>\app\models\PhysicalTicketAllotments::find()->select(['ticket_id'])->where(['team_id'=>$user_id])]);
            }
        }
        if($physical_ticket){
            $query->andWhere(['>', 'physical_ticket_limit', 0]);
            if(!$sale){
                $query->joinWith(['allotments.team' => function ($q) {
                    $q->select(['users.id', 'users.name', 'users.username']);
                }, 
                'bookings.booking']);
            }
            else{
                $query->joinWith(['allotments.tickets','bookings.booking']);
            }
        }
        else{
            $query->joinWith(['bookings.booking']);
        }
        $find = $query->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'tickets' => $find,
        ];
    }

    public function actionFollowUpBuyer()
    {
        if (isset($_POST['booking_id']) && !empty($_POST['booking_id'])) {
            try {
                $model = new \app\models\EventTicketBookings;
                $find = $model->find()->joinWith(['event.user'])->where(['event_ticket_bookings.id' => $_POST['booking_id']])->one();
                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 9])->one();
                $data = [
                    'case' => 9,
                    'body' => $templateDetails->content,
                    'name' => $find->first_name,
                    'event_organiser_email' => $find->event->user->email,
                    'event_title' => $find->event->title,
                    'event_url' => Yii::$app->params['siteUrl'] . '/' . $find->event->url . '/' . $find->event->id,
                ];
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                $compose->setTo($find->email);
                $compose->setSubject($templateDetails->subject);

                if ($compose->send()) {
                    return [
                        'success' => true,
                        'message' => 'Follow up email has been sent successfully.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Buyer not found!',
            ];
        }
    }

    public function actionGetGroupPaymentDetails($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\EventGroupPayments;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'payment' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Payment not found.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Payment not found!',
            ];
        }
    }

    public function actionFollowUpGroupDonor()
    {
        if (isset($_POST['payment_id']) && !empty($_POST['payment_id'])) {
            try {
                $model = new \app\models\EventGroupPayments;
                $find = $model->find()->joinWith(['event.user'])->where(['event_group_payments.id' => $_POST['payment_id']])->one();
                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 9])->one();
                $data = [
                    'case' => 9,
                    'body' => $templateDetails->content,
                    'name' => $find->first_name,
                    'event_organiser_email' => $find->event->user->email,
                    'event_title' => $find->event->title,
                    'event_url' => Yii::$app->params['siteUrl'] . '/' . $find->event->url . '/' . $find->event->id,
                ];
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                $compose->setTo($find->email);
                $compose->setSubject($templateDetails->subject);

                if ($compose->send()) {
                    return [
                        'success' => true,
                        'message' => 'Follow up email has been sent successfully.',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Buyer not found!',
            ];
        }
    }

    public function actionConfirmGroupDonor()
    {
        if (isset($_POST['payment_id']) && !empty($_POST['payment_id'])) {
            try {
                $model = new \app\models\EventGroupPayments;
                $find = $model->find()->joinWith(['event'])->where(['event_group_payments.id' => $_POST['payment_id']])->one();
                if ($find) {
                    $find->status = 1;
                    if ($find->save()) {
                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 18])->one();
                        $data = [
                            'case' => 18,
                            'name' => $find->first_name,
                            'event' => "<a href='".Yii::$app->params['siteUrl']."/".$find->event->url."/".$find->event->id."'>".$find->event->title."</a>",
                            'body' => $templateDetails->content,
                        ];

                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom([$templateDetails->from_email => $templateDetails->from_label]);
                        $compose->setTo($find->email);
                        $compose->setSubject($templateDetails->subject);

                        $content = $this->renderPartial('certificate', ['record'=>$find]);

                        $pdf = new \kartik\mpdf\Pdf([
                            // set to use core fonts only
                            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
                            // A4 paper format
                            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
                            // portrait orientation
                            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
                            // stream to browser inline
                            //'destination' => \kartik\mpdf\Pdf::DEST_BROWSER, 
                            //'destination' => \kartik\mpdf\Pdf::DEST_STRING,
                            'destination' => \kartik\mpdf\Pdf::DEST_FILE,
                            'filename' => $_SERVER['DOCUMENT_ROOT'] . "/api/web/certificates/" . $find->id . ".pdf",
                            // your html content input
                            'content' => $content,
                            // format content from your own css file if needed or use the
                            // enhanced bootstrap css built by Krajee for mPDF formatting 
                            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                            // any css to be embedded if required
                            //'defaultFontSize' => '1',
                            'cssInline' => '.outer-border{width:800px; height:650px; padding:20px; text-align:center; border: 10px solid #673AB7;} .inner-dotted-border{width:750px; height:600px; padding:20px; text-align:center; border: 5px solid #673AB7;border-style: dotted;} .certification{font-size:40px; font-weight:bold;color: #663ab7;} .certify{font-size:25px;}.name{font-size:30px;color: green;} .fs-30 {font-size: 30px;}',
                            // set mPDF properties on the fly
                            'options' => ['title' => 'Krajee Report Title'],
                            // call mPDF methods on the fly
                            'methods' => [
                                'SetTitle' => 'Certificate - TipHub.co',
                                'SetAuthor' => 'Mitiz Technologies',
                                'SetCreator' => 'Mitiz Technologies',
                                //'SetHeader' => ['iMagDent - ' . $title],
                                //'SetFooter' => ['{PAGENO}'],
                            ]
                        ]);
                        $pdf->render();

                        $compose->attachContent(file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/api/web/certificates/" . $find->id . ".pdf"), ['fileName' => 'certificate.pdf', 'contentType' => 'application/pdf']);
                        
                        if ($compose->send()) {
                            return [
                                'success' => true,
                            ];
                        }
                    }
                } else {
                    return [
                        'error' => true,
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request.'
            ];
        }
    }

    public function actionGetLeadershipBoard($event_id = null){
        $model = new \app\models\EventGroupPayments;
        $find = $model->find()->select(['sum(event_group_payments.amount) as total', 'event_group_payments.group_id'])->joinWith(['group','donors dns'=>function($q){$q->where(['dns.status'=>1]);}])->where(['event_group_payments.event_id' => $event_id,'event_group_payments.status'=>1])->groupBy(['group_id'])->orderBy('total desc')->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
       // 'event_group_payments.first_name'
        foreach($find as $key=>$val){
            $total = 0;
            $donors = [];
            foreach($val['donors'] as $donor){
                $total = $total + $donor['amount'];
                $full_name = strtolower($donor['first_name']." ".$donor['last_name']);
                if(array_key_exists($full_name,$donors)){
                    $donors[$full_name]['amount'] += $donor['amount'];
                }else {
                    $donors[$full_name]  = $donor;
                }
            }
            $find[$key]['total'] = $total;
            $find[$key]['donors'] = array_values($donors);
        }
        return [
            'success' => true,
            'records' => $find,
        ];
    }


    public function actionDeleteSupporter()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Transactions;
                $find = $model->find()->joinWith(['event'])->where(['transactions.id' => $_POST['id']])->one();
                if ($find) {
                    if ($find->event->user_id == Yii::$app->user->identity->id) {
                        $transaction_id = $find->id;
                        if ($find->delete()) {
                            \app\models\Payments::deleteAll(['transaction_id' => $transaction_id]);
                            return [
                                'success' => true,
                                'message' => 'Event deleted successfully.'
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors()
                            ];
                        }
                    } else if (!in_array($find->id, $eventIds)) {
                        return [
                            'error' => true,
                            'message' => 'Unauthorized request found.'
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Supporter not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Supporter not found!'
            ];
        }
    }

    public function actionSendPaymentReminder()
    {
        if (!empty($_POST['selectedRecords'])) {
                $model = new \app\models\Members();
                $phoneNumbers = $model->find()->where(['id' => $_POST['selectedRecords']])->select('phone')->all();
                if(!empty($phoneNumbers)){
                    Yii::$app->queue->push(new \app\components\SendSms([
                        'number' => $phoneNumbers,
                        'sms_body' => $_POST['sms_content'],
                        'twilioSid' => Yii::$app->params['twilioSid'],
                        'twilioToken' => Yii::$app->params['twilioToken'],
                        'twilioFromNumber' => Yii::$app->params['twilioFromNumber']
                    ]));
                    return [
                        'success' => true,
                        'message' => 'Sms sent successfully.',
                        'find' => $phoneNumbers
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Phone number is not available or incorrect format.',
                    ];
                }
        } else {
            return [
                'error' => true,
                'message' => 'Supporter not found!'
            ];
        }
    }
    

    // Function to get access token and linking URL of Paypal
    public function actionGetPaypalToken($token = false)
    {

        // Yii::$app->common->getPaypalAccessToken();
        $params = [
            CURLOPT_URL => Yii::$app->params['paypalUrl']."v1/oauth2/token",
            CURLOPT_USERPWD => Yii::$app->params['clientId'].":".Yii::$app->params['clientSecret'],
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        ];
        $response = Yii::$app->common->setCurlParams($params);
        
        if ($response['error'] == true) {
            return [
                'error' => true,
                'message' => $response['error']
            ];
        }
        else
        {
           //print_r($response['response']);die;
           $access_token = $response['response']->access_token;
          
            if($access_token != ''){
               if ($token == true) {
                return $access_token;
               }
               if ($_POST){
                    $redirectUrl = $_POST['redirect_url'];
                }
                $tracking_id = Yii::$app->user->identity->id."|".$redirectUrl;
            
                $returnURL = Yii::$app->params['siteUrl']."/api/event/paypal-return-response?access-token=".Yii::$app->user->identity->access_token;
                // Hitting API for getting account linking URL
                $postFields = '{
                    "tracking_id": "'.$tracking_id.'",
                    "operations": [
                      {
                        "operation": "API_INTEGRATION",
                        "api_integration_preference": {
                          "rest_api_integration": {
                            "integration_method": "PAYPAL",
                            "integration_type": "THIRD_PARTY",
                            "third_party_details": {
                              "features": [
                                "PAYMENT",
                                "REFUND"
                             ]
                            }
                          }
                        }
                      }
                    ],
                    "products": [
                      "EXPRESS_CHECKOUT"
                    ],
                    "legal_consents": [
                      {
                        "type": "SHARE_DATA_CONSENT",
                        "granted": true
                      }
                    ],
                    "partner_config_override": {
                        "return_url": "'.$returnURL.'",
                        "return_url_description": "TipHub Event Description"
                    }
                }';
                $params = [
                    CURLOPT_URL => Yii::$app->params['paypalUrl']."v2/customer/partner-referrals",
                    CURLOPT_POSTFIELDS => $postFields,
                    CURLOPT_HTTPHEADER => array("Content-Type: application/json","PayPal-Partner-Attribution-Id: ".Yii::$app->params['BN_Code'],"Authorization: Bearer ".$access_token)
                ];
                //print_r($params);die;
                $response = Yii::$app->common->setCurlParams($params);
        
                if ($response['error'] == true) {
                    return [
                        'error' => true,
                        'message' => $response['error']
                    ];
                }else {
                    return [
                        'success' => true,
                        'token' => $access_token,
                        'linking_data' => $response['response']
                    ];
                }
            }
            return [
                'success' => true,
                'token' => $access_token
            ];
        }
        
    }

    // https://tiphub.co/api/event/paypal-return-response?merchantId=88|414&merchantIdInPayPal=ZNEW6RYYX5U2Q&permissionsGranted=true&consentStatus=true&productIntentId=addipmt&productIntentID=addipmt&isEmailConfirmed=true&accountStatus=BUSINESS_ACCOUNT
    // Return URL for Paypal user attachment
    function actionPaypalReturnResponse(){
        if(count($_GET)) {
            //print_r($_GET);die;
            $tracking_id = isset($_GET['merchantId']) ? $_GET['merchantId'] : "";
            $merchantIdInPayPal = isset($_GET['merchantIdInPayPal']) ? $_GET['merchantIdInPayPal'] : ""; 
            unset($_GET['access-token']);
            $response = json_encode($_GET);

            $tracking_id_arr = @explode('|',$tracking_id);
            $user_id = isset($tracking_id_arr[0]) ? $tracking_id_arr[0] : "";
            $redirectUrl = isset($tracking_id_arr[1]) ? $tracking_id_arr[1]."#paypal" : "";
           
            if (Yii::$app->user->identity->id != '') {
                $model = new \app\models\UsersPaypalConfigs;
                $find = $model->find()->where(['user_id' => Yii::$app->user->identity->id,'paypal_merchant_id' => $merchantIdInPayPal])->one();
                if(!$find) {
                    $model = new \app\models\UsersPaypalConfigs;
                    $model->user_id = Yii::$app->user->identity->id;
                    $model->paypal_merchant_id = $merchantIdInPayPal;
                    if(isset($_GET['permissionsGranted'])){
                        $model->permissions_granted = $_GET['permissionsGranted'];
                    }
                    else{
                        $model->permissions_granted = false;
                    }
                    if(isset($_GET['consentStatus'])){
                        $model->consent_status = $_GET['consentStatus'];
                    }
                    else{
                        $model->consent_status = false;
                    }
                    $model->gateway_response = $response;
                    $model->added_on = date('Y-m-d H:i:s');
                    if($model->save()){
                        $this->redirect($redirectUrl);
                    }else{
                        print_r($model->getErrors());die;
                    }
                }else {
                    $this->redirect($redirectUrl);
                }
            }
           
            
        }
    }

    // Function to check if a user has added Creditcard as Payment method 
    function actionCheckPaymentApp(){
        $send_otp = $_POST['send_otp'];
        $userdata = Yii::$app->user->identity;
        $model = new \app\models\EventApps;
        $eventApps = $model->find()->where(['service_id' => 73,'user_id' => $userdata->id])->one();
        $userApps = \app\models\Services::find()->where(['service_id' => 73,'user_id' => $userdata->id])->one();
        if (empty($eventApps) && empty($userApps) && !empty($userdata->parent_id)) {
            $eventApps = $model->find()->where(['service_id' => 73,'user_id' => $userdata->parent_id])->one();
            $userApps = \app\models\Services::find()->where(['service_id' => 73,'user_id' => $userdata->parent_id])->one();
        }
        if ($eventApps || $userApps ) { //|| $event_id > 0
            if ($send_otp == 'Y') {
                return $this->sendEmailOtp();
            }else{
                return [
                    'success' => true,
                    'is_cc_added' => true,
                    'message' => "An OTP will be sent to your registered email."
                ];
            }
            
        }else {
            return [
                'success' => true,
                'is_cc_added' => false
            ];
        }
    }

    // Function to send 2FA OTP     
    private function sendEmailOtp(){
        
        // Sending email to the registered email
        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 25])->one();
        
        $body = $templateDetails->content;
        $otp = mt_rand(111111, 999999);
        $body = str_replace('[name]',ucwords(Yii::$app->user->identity->first_name),$body);
        $body = str_replace('[OTP_VALUE]',$otp,$body);
        $to = Yii::$app->user->identity->email;
        //$to = "amrender.kumar1986@gmail.com";
        Yii::$app->common->sendSmtpEmail($to,$body,$templateDetails->subject);
        $find = \app\models\User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        
        if ($find) {
           
            $otpArr['otp'] = $otp;  
            $otpArr['otp_expiry'] = Carbon::now('UTC')->addMinute(15);
            // Updating verification code
            $find->verification_code = json_encode($otpArr);
            
            if($find->save()){
                return [
                        'success' => true,
                        'is_cc_added' => true,
                        'message' => "OTP sent to your registered email."
                    ];
            }else{
                return [
                    'error' => true,
                    'message' => $find->getErrors()
                ];
            }
             
        }else {
            return [
                'error' => true,
                'message' => "User does not exist."
            ];
        }
    }

    function actionVerifyOtp(){
        $otp = $_POST['otp'];
        $find = \app\models\User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        
        if ($find) {
            $otp_details = $find->verification_code;
            if($otp_details != ''){
                $otp_details = json_decode($otp_details);
                
                if (isset($otp_details->otp_expiry) && ($otp == $otp_details->otp && Carbon::now('UTC') <= $otp_details->otp_expiry)){
                    $find->is_2fa_verified = 1; // Setting to Verified
                    $find->verification_code = "";
                    if($find->save()){
                        return [
                                'success' => true,
                                'message' => "Payment App verified",
                            ];
                    }else{
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                        
                    }
                }
                else{
                    return [
                        'error' => true,
                        'message' => "Incorrect OTP or it's expired.Try again."
                    ];
                    
                }
            }
            else{
                return [
                    'error' => true,
                    'message' => "No OTP details found."
                ];
                
            }
        }
    }

    // Function to check if a user has configured Paypal
    function actionIsPaypalLinked(){
        $model = new \app\models\UsersPaypalConfigs;
        $find = $model->find()->where(['user_id' =>  Yii::$app->user->identity->id])->one();
        
        if ($find) {
             return [
                    'success' => true,
                    'message' => "Paypal linked already.",
                ];
        }else{
            return [
                'error' => true,
                'message' => "Not linked."
            ];
            
        }    
    }

    function actionIsEdgepayLinked(){
        $model = new \app\models\UsersEdgePayConfigs;
        $find = $model->find()->where(['user_id' =>  Yii::$app->user->identity->id])->one();
        
        if ($find) {
             return [
                    'success' => true,
                    'message' => "EdgePay already linked.",
                ];
        }else{
            return [
                'error' => true,
                'message' => "Not linked."
            ];
            
        }    
    }
    
    public function actionGetPrograms($event_id = null, $pageSize = 20)
    {
        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        $model = new \app\models\EventPrograms();
        $query = $model->find()->where(['event_id' => $event_id]);
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        //echo $find->createCommand()->getRawSql();die;
        return [
            'success' => true,
            'programs' => $find,
            'pages' => $pages,
        ];
    }
    public function actionSaveProgram()
    {
        $errorMessage = "";
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\EventPrograms();
                $POST['EventPrograms'] = $_POST['fields'];
                if (is_array($POST['EventPrograms']['image'])) {
                    $temp_file_name = $POST['EventPrograms']['image']['file_name'];
                    if (strpos($POST['EventPrograms']['image']['file_name'], "temp") >= 0) {
                        $new_file_name = mt_rand(111111, 999999) . "." . $POST['EventPrograms']['image']['extension'];
                        if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $POST['EventPrograms']['image']['file_name'])) {
                            copy($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" .$POST['EventPrograms']['image']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name);
                            $POST['EventPrograms']['image'] = $new_file_name;
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/temp/" . $temp_file_name);
                        }
                    }
                }
                if (isset($POST['EventPrograms']['id']) && !empty($POST['EventPrograms']['id'])) {
                    $find = $model->find()->where(['id' => $POST['EventPrograms']['id']])->one();
                    if ($find && $find->load($POST) && $find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Programs updated successfully.'
                        ];
                    } else {
                        $errorMessage = "Programs does not exist.";
                    }
                } else if ($model->load($POST) && $model->save()) {
                    return [
                        'success' => true,
                        'message' => 'Programs saved successfully.'
                    ];
                } else {
                    $errorMessage = $model->getErrors();
                }
            } catch (\Exception $e) {
                $errorMessage = Yii::$app->common->returnException($e);
            }
        } else {
            $errorMessage ='Invalid request!';
        }
        return [
            'error' => true,
            'message' => $errorMessage
        ];
    }
    public function actionDeleteProgram()
    {
        $error_message = "";
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\EventPrograms();
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Program deleted successfully.'
                    ];
                } else {
                    $error_message = 'Program not found!';
                }
            } catch (\Exception $e) {
                $error_message = Yii::$app->common->returnException($e);
            }
        } else {
            $error_message = 'Program not found!';
            
        }
        return [
            'error' => true,
            'message' => $error_message
        ];
    }
    public function actionGetProgramDetails($id)
    {
        $error_message = '';
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\EventPrograms();
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'program' => $find,
                    ];
                } else {
                    $error_message = 'Program not found!';
                }
            } catch (\Exception $e) {
                $error_message = Yii::$app->common->returnException($e);
            }
        } else {
            $error_message = 'Program not found!';
        }
        return [
            'error' => true,
            'message' => $error_message,
        ];
    }
    public function actionGetAllEventMembers($event_id) 
    {
        $error_message = "";
        if ($event_id) {
            $data =  Members::find()->where(['event_id'=>$event_id])->asArray()->all();
            return [
                'success' => true,
                'members' => $data,
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }
    public function actionSendMailPaymentReminder()
    {
        $post =  Yii::$app->request->post();
        if (!empty($post)) {
            if(isset($post['emails']) && !empty($post['emails'])){
                $emails = $post['emails'];
            }
            else{
                $ids = $post['selectedRecords'];
                $emails = Members::find()->where(['id'=>$ids])->select('email')->column(); 
            }
            
            $subject = $post['email_subject'];
            $data = ['body'=>$post['email_content']];
            if (!empty($emails)) {
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                $compose->setTo($emails);
                $compose->setSubject($subject);
                if ($compose->send()) {
                    return [
                        'success' => true,
                        'message' => "Email sent successfully!",
                    ];
                } 
            } else {
                return [
                    'error' => true,
                    'message' => 'Email can not be blank!',
                ];
            }
        }
        return [
            'error' => true,
            'message' => 'Something went wrong!',
        ];
    }
    
    public function actionAllotPhysicalTickets(){
        if (isset($_POST) && !empty($_POST)) {
            try {
                $POST = $_POST['fields'];
                if(isset($POST['tickets']) && !empty($POST['tickets'])){
                    $counter = 0;
                    foreach($POST['tickets'] as $ticket){
                        $tquery = new \app\models\EventTickets;
                        $fallotment = $tquery->find()->joinWith(['allotments'])->where(['event_tickets.id' => $ticket['ticket_id']])->one();
                        if($fallotment){
                            $total_allotments = 0;
                            foreach($fallotment->allotments as $allotment){
                                $total_allotments = $total_allotments + $allotment->total_tickets;
                            }
                            $actual_allotments = $total_allotments + $ticket['total_tickets'];
                            if($actual_allotments > $fallotment->physical_ticket_limit){
                                $remaining_balance = $fallotment->physical_ticket_limit - $total_allotments;
                                return [
                                    'error' => true,
                                    'message' => 'Alloted tickets should be lesser than ' . $remaining_balance . ' for ' . $fallotment->name
                                ];
                            }
                            else{
                                $model = new \app\models\PhysicalTicketAllotments;
                                $model->event_id = $POST['event_id'];
                                $model->team_id = $POST['team_id'];
                                $model->user_id = Yii::$app->user->identity->id;
                                $model->total_tickets = $ticket['total_tickets'];
                                $model->ticket_id = $ticket['ticket_id'];
                                if($model->save()){
                                    for($i = 0; $i < $model->total_tickets; $i++){
                                        $pmodel = new \app\models\PhysicalTickets;
                                        $pmodel->allotment_id = $model->id;
                                        $pmodel->qrcode =  (new \Da\QrCode\QrCode(Yii::$app->params['siteUrl'] . '/ticket/' . $model->event_id))->setSize(300)->setMargin(5)->writeDataUri();
                                        if($pmodel->save()){
                                            $nameArr = explode(' ', $POST['name']);
                                            $custom_ticket_id = '';
                                            foreach ($nameArr as $name) {
                                                $custom_ticket_id .=  strtoupper($name[0]).'PT';
                                            }
                                            $pmodel->custom_ticket_id = $custom_ticket_id.$pmodel->id;
                                            $pmodel->save();
                                        }
                                    }
                                    $counter++;
                                }
                                else{
                                    return [
                                        'error' => true,
                                        'message' => [
                                            $ticket['ticket_id'] => $model->getErrors()
                                        ],
                                    ];
                                }
                            }
                        }
                    }
                    if($counter > 0){
                        return [
                            'success' => true,
                            'message' => 'Tickets have been alloted successfully.'
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message' => 'Invalid request!'
                        ];
                    }
                }
                else{
                    return [
                        'error' => true,
                        'message' => 'Invalid request!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetAllotments($event_id) 
    {
        if ($event_id) {
            $find =  \app\models\PhysicalTicketAllotments::find()->joinWith(['ticket','team','bookings.details'])->where(['physical_ticket_allotments.event_id'=>$event_id])->asArray()->all();
            return [
                'success' => true,
                'allotments' => $find,
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }
    public function actionGetVendors($event_id) 
    {
        if ($event_id) {
            $find =  \app\models\User::find()->joinWith(['allotments'=>function($q) use ($event_id) {$q->andOnCondition(['physical_ticket_allotments.event_id'=>$event_id]);}])->joinWith(['allotments.ticket.bookings'])->where(['parent_id'=>Yii::$app->user->identity->id])->asArray()->all();
            //echo $find->createCommand()->getRawSql();die;
            return [
                'success' => true,
                'vendors' => $find,
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }

    public function actionDeleteAllotment()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\PhysicalTicketAllotments;
                $find = $model->find()->where(['id' => $_POST['id'], 'user_id' => Yii::$app->user->identity->id])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Allotment deleted successfully.'
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Allotment not found!'
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Allotment not found!'
            ];
        }
    }

    public function actionCheckEventVendor($event_id) 
    {
        if ($event_id) {
            return [
                'success' => true,
                'totalTicketAllotments' => \app\models\PhysicalTicketAllotments::find()->where(['event_id'=>$event_id,'team_id'=>Yii::$app->user->identity->id])->count(),
                'vendorApps'=>\app\models\Services::find()->joinWith(['appname'])->where(['services.user_id'=>Yii::$app->user->identity->id])->asArray()->all()
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }

    public function actionGetTicketAnalytics($event_id, $export = 0) 
    {
        if ($event_id) {
            //$find = \app\models\PhysicalTicketAllotments::find()->joinWith(['ticket','bookings'=>function($q) use ($event_id) {$q->andOnCondition(['event_ticket_bookings.event_id'=>$event_id]);},'bookings.details'])->where(['physical_ticket_allotments.event_id'=>$event_id,'physical_ticket_allotments.team_id'=>Yii::$app->user->identity->id])->asArray()->all();
            $tickets = \app\models\PhysicalTicketAllotments::find()->joinWith(['ticket'])->where(['physical_ticket_allotments.event_id'=>$event_id,'physical_ticket_allotments.team_id'=>Yii::$app->user->identity->id])->asArray()->all();
            if($tickets){
                foreach($tickets as $key=>$val){
                    $tickets[$key]['bookings'] = \app\models\EventTicketBookingDetails::find()->where(['ticket_id'=>$val['ticket_id'], 'booking_id'=>\app\models\EventTicketBookings::find()->select(['booking_id'])->where(['team_id'=>$val['team_id'],'event_id'=>$event_id])])->asArray()->all();
                }
                if($export){
                    if (!empty($tickets)) {
                        $file_name = 'ticket-analytics-' . date('m-d-Y');
                        $cols = ['A', 'B', 'C', 'D', 'E', 'F'];
        
                        $spreadsheet = new Spreadsheet();
                        $sheet = $spreadsheet->getActiveSheet();
                        foreach ($cols as $col) {
                            $sheet->getColumnDimension($col)->setAutoSize(true);
                        }
                        $rows = 1;
                        $sheet->setCellValue($cols[0] . $rows, '#')->getStyle($cols[0] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $sheet->setCellValue($cols[1] . $rows, 'Ticket')->getStyle($cols[1] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $sheet->setCellValue($cols[2] . $rows, 'Allotted Tickets')->getStyle($cols[2] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $sheet->setCellValue($cols[3] . $rows, 'Sold Tickets')->getStyle($cols[3] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $sheet->setCellValue($cols[4] . $rows, 'Balance')->getStyle($cols[4] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
                        $sheet->setCellValue($cols[5] . $rows, 'Amount')->getStyle($cols[5] . $rows)->applyFromArray(['font' => ['bold' => true, 'size' => 15]]);
        
                        $rows = 2;
                        $srno = 1;
                        foreach ($tickets as $ticket) {
                            $sheet->setCellValue($cols[0] . $rows, $srno)->getStyle($cols[0] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        
                            $sheet->setCellValue($cols[1] . $rows, $ticket['ticket']['name']);
        
                            $sheet->setCellValue($cols[2] . $rows, $ticket['total_tickets'])->getStyle($cols[2] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        
                            $sheet->setCellValue($cols[3] . $rows, count($ticket['bookings']))->getStyle($cols[3] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        
                            $sheet->setCellValue($cols[4] . $rows, ($ticket['total_tickets'] - count($ticket['bookings'])))->getStyle($cols[4] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
        
                            $sheet->setCellValue($cols[5] . $rows, 0)->getStyle($cols[5] . $rows)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                            $rows++;
                            $srno++;
                        }
        
                        $objectwriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        
                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment;filename="' . $file_name . '.xlsx"');
                        header('Cache-Control: max-age=0');
                        $objectwriter->setOffice2003Compatibility(false);
                        $objectwriter->setPreCalculateFormulas(false);
                        $objectwriter->save('php://output');
        
                        $spreadsheet->disconnectWorksheets();
                        unset($spreadsheet);
                        die();
                    }
                }
            }
            return [
                'success' => true,
                'tickets' => $tickets
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }

    public function actionGetTicketsToPrint(){
        if(isset($_POST['booking_id']) && !empty($_POST['booking_id'])){
            $find = \app\models\EventTicketBookings::find()->joinWith(['details.ticket','event'])->where(['event_ticket_bookings.id'=>$_POST['booking_id']])->asArray()->all();
            if($find){
                $tickets = [];
                $i = 0;
                foreach($find as $key=>$val){
                    foreach($val['details'] as $k=>$v){
                        $tickets[$i] = $v;
                        $tickets[$i]['event'] = $val['event'];
                        $tickets[$i]['ticket'] = $v['ticket'];
                        $i++;
                    }
                }
                return [
                    'success' => true,
                    'tickets' => $tickets
                ];
            }
        }
        else if(isset($_POST['allotments']) && !empty($_POST['allotments'])){
            $find = \app\models\PhysicalTicketAllotments::find()->joinWith(['event','tickets','ticket'])->where(['physical_ticket_allotments.id'=>$_POST['allotments']])->asArray()->all();
            if($find){
                $tickets = [];
                $i = 0;
                foreach($find as $key=>$val){
                    foreach($val['tickets'] as $k=>$v){
                        $tickets[$i] = $v;
                        $tickets[$i]['event'] = $val['event'];
                        $tickets[$i]['ticket'] = $val['ticket'];
                        $i++;
                    }
                }
                return [
                    'success' => true,
                    'tickets' => $tickets
                ];
            }
            else{
                return [
                    'error' => true,
                    'message' => 'Allotment not found.'
                ];
            }
        }
        else{
            return [
                'error' => true,
                'message' => 'Invalid request.'
            ];
        }
    }

    public function actionSaveEdgepayConfigs()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\UsersEdgePayConfigs;
                $POST['UsersEdgePayConfigs'] = $_POST['fields'];
                $find = $model->find()->where(['user_id'=>Yii::$app->user->identity->id])->one();
                if($find && $find->load($POST)){
                    if($find->save()){
                        return [
                            'success' => true,
                            'message' => 'Record has been saved successfully.'
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message' => $find->getErrors()
                        ];
                    }
                }
                else if($model->load($POST)){
                    $model->user_id = Yii::$app->user->identity->id;
                    if($model->save()){
                        return [
                            'success' => true,
                            'message' => 'Record has been saved successfully.'
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message' => $model->getErrors()
                        ];
                    }
                }
                else{
                    return [
                        'error' => true,
                        'message' => 'Invalid reqest.'
                    ];
                }
                
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Invalid request!'
            ];
        }
    }

    public function actionGetTicketAnalyticDetails($event_id) 
    {
        if ($event_id) {
            $find = \app\models\EventTickets::find()->joinWith(['bookings'=>function($q) use($event_id) {
                $q->where(['booking_id'=>\app\models\EventTicketBookings::find()->select(['id'])->where(['event_id'=>$event_id, 'status'=>1])]);
            }])->where(['event_id'=>$event_id])->asArray()->all();

            $paymentFind = \app\models\Payments::find()->select(['payment_service', 'sum(txn_amount) as total_txn_amount', 'sum(gateway_charges) as total_gateway_charges', 'sum(platform_charges) as total_platform_charges', '(sum(txn_amount) - (sum(platform_charges) + sum(gateway_charges))) as total_net_amount'])->joinWith(['service'])->where(['event_id'=>$event_id])->groupBy('payment_service')->asArray()->all();
            
            return [
                'success' => true,
                'tickets' => $find,
                'payments' => $paymentFind
            ];
        } 
        return [
            'error' => true,
            'message' => 'Event id is not valid!',
        ];
    }

    
}
