<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use linslin\yii2\curl;

use app\models\Transactions;
use app\models\Events;
use app\models\PaymentServiceNames;

class TestController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action){
        if (parent::beforeAction($action)) {
			
            if ($action->id=='error'){
                if(!Yii::$app->user->isGuest){
                    $this->layout ='admin';
                    return true;
                }
            }
           
            $this->layout = 'login';
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //phpinfo();
        return $this->render('index');
    }
    public function actionCheckJob(){
        Yii::$app->queue->push(new \app\components\MailJob());
        //Yii::$app->queue->delay(1 * 60)->push(new \app\components\MailJob());
        //Yii::$app->common->sendTestEmail();
        
        
    }
    
}
