<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use yii\web\Response;

class SettingsController extends Controller
{

    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => [],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    //QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        //return Yii::$app->common->checkPermission('Settings', $action->id);
                        return true;
                    },
                ],
            ],
        ];
        return $behaviors;
    }

    public function actionAddBanner()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Banners;
                $POST['Banners'] = $_POST['fields'];
                if (isset($POST['Banners']['id']) && !empty($POST['Banners']['id'])) {
                    $find = $model->find()->where(['id'=>$POST['Banners']['id']])->one();
                    $find->load($POST);
                    if (isset($POST['Banners']['file']) && !empty($POST['Banners']['file'])) {
                        if(strpos($POST['Banners']['file']['file_name'], "temp") != false){
                            $new_file_name = mt_rand(111111, 999999) . "." . $POST['Banners']['file']['extension'];
                            copy($_SERVER['DOCUMENT_ROOT'] . "/images/temp/" . $POST['Banners']['file']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/images/" . $new_file_name);
                            $find->file_name = $new_file_name;
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/images/temp/" . $POST['Banners']['file']['file_name']);
                        }
                    }
                    if ($find->save()) {
                        return [
                            'success' => true,
                            'message' => 'Banner image has been updated successfully.',
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message' => $find->getErrors(),
                        ];
                    }
                }
                else if ($model->load($POST)){
                    if (isset($POST['Banners']['file']) && !empty($POST['Banners']['file'])) {
                        if(strpos($POST['Banners']['file']['file_name'], "temp") != false){
                            $new_file_name = mt_rand(111111, 999999) . "." . $POST['Banners']['file']['extension'];
                            copy($_SERVER['DOCUMENT_ROOT'] . "/images/temp/" . $POST['Banners']['file']['file_name'], $_SERVER['DOCUMENT_ROOT'] . "/images/" . $new_file_name);
                            $model->file_name = $new_file_name;
                            unlink($_SERVER['DOCUMENT_ROOT'] . "/images/temp/" . $POST['Banners']['file']['file_name']);
                        }
                    }
                    if ($model->save()) {
                        return [
                            'success' => true,
                            'message' => 'Banner image has been added successfully.',
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message' => $model->getErrors(),
                        ];
                    }
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionListBanners($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'sequence' => [
                    'asc' => ['sequence' => SORT_ASC],
                    'desc' => ['sequence' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['sequence' => SORT_ASC],
        ]);

        $model = new \app\models\Banners;
        $query = $model->find();

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        if ($find) {
            $response = [
                'success' => true,
                'banners' => $find,
                'pages' => $pages,
            ];
        } else {
            $response = [
                'success' => true,
                'banners' => [],
                'pages' => $pages,
            ];
        }
        return $response;
    }

    public function actionGetBanner($id)
    {
        if (isset($id) && !empty($id)) {
            try {
                $model = new \app\models\Banners;
                $find = $model->find()->where(['id' => $id])->asArray()->one();
                if ($find) {
                    return [
                        'success' => true,
                        'banner' => $find,
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Plan not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Plan not found!',
            ];
        }
    }

    public function actionDelete()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Plans;
                $find = $model->find()->where(['id' => $_POST['id']])->one();
                if ($find && $find->delete()) {
                    return [
                        'success' => true,
                        'message' => 'Plan has been deleted successfully.',
                    ];
                } else {
                    return [
                        'error' => true,
                        'message' => 'Plan not found!',
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        } else {
            return [
                'error' => true,
                'message' => 'Plan not found!',
            ];
        }
    }

    /* public function actionSaveBeneficial(){
        if (isset($_POST) && !empty($_POST)) {
            try {
                \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];

                $apiClient = new \DwollaSwagger\ApiClient("https://api-sandbox.dwolla.com");
                // For production
                // $apiClient = new DwollaSwagger\ApiClient("https://api.dwolla.com");
                $customersApi = new DwollaSwagger\CustomersApi($apiClient);
                $verified_customer = 'https://api-sandbox.dwolla.com/customers/81696e5d-a593-45a6-8863-3c20ad634de5';
                
                $addOwner = $customersApi->addBeneficialOwner([
                      'firstName' => 'document',
                      'lastName'=> 'owner',
                      'dateOfBirth' => '1990-11-11',
                      'ssn' => '123-34-9876',
                      'address' =>
                      [
                          'address1' => '18749 18th st',
                          'address2' => 'apt 12',
                          'address3' => '',
                          'city' => 'Des Moines',
                          'stateProvinceRegion' => 'IA',
                          'postalCode' => '50265',
                          'country' => 'US'
                      ],
                  ], $verified_customer);
                

                $model = new \app\models\Beneficials;
                $POST['Beneficials'] = $_POST['fields'];
                $POST['Beneficials']['country'] = $_POST['fields']['country']['value'];
                $POST['Beneficials']['stateProvinceRegion'] = isset($_POST['fields']['stateProvinceRegion']['value']) ? $_POST['fields']['stateProvinceRegion']['value'] : NULL;
                $POST['Beneficials']['passport_country'] = isset($_POST['fields']['passport_country']['value']) ? $_POST['fields']['passport_country']['value'] : NULL;
                $POST['Beneficials']['user_id'] = Yii::$app->user->identity->id;
                $POST['Beneficials']['dateOfBirth'] = date('Y-m-d',strtotime($POST['Beneficials']['dateOfBirth']));
                if(isset($POST['Beneficials']['id']) && !empty($POST['Beneficials']['id'])){
                    $find = \app\models\Beneficials::find()->where(['id'=>$POST['Beneficials']['id']])->one();
                    if($find && $find->load($POST) && $find->save()){
                        return [
                            'success' => true,
                            'message' => 'Transaction updated successfully.',
                        ];
                    }
                    else {
                        return [
                            'error' => true,
                            'message' => $find->getErrors(),
                        ];
                    }
                }
                else if($model->load($POST) && $model->save()){
                    return [
                        'success' => true,
                        'message' => 'Transaction added successfully.',
                    ];
                } 
                else{
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }             
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    } */

    public function actionSaveBeneficial(){
        if (isset($_POST) && !empty($_POST)) {
            try {
                $model = new \app\models\Beneficials;
                $POST['Beneficials'] = $_POST['fields'];
                $POST['Beneficials']['stateProvinceRegion'] = isset($POST['Beneficials']['stateProvinceRegion']) ? $POST['Beneficials']['stateProvinceRegion']['value'] : NULL;
                $POST['Beneficials']['cStateProvinceRegion'] = isset($POST['Beneficials']['cStateProvinceRegion']) ? $POST['Beneficials']['cStateProvinceRegion']['value'] : NULL;
                $POST['Beneficials']['user_id'] = Yii::$app->user->identity->id;
                $POST['Beneficials']['ipAddress'] = Yii::$app->request->userIP;
                $POST['Beneficials']['dateOfBirth'] = date('Y-m-d',strtotime($POST['Beneficials']['dateOfBirth']));
                $POST['Beneficials']['cDateOfBirth'] = isset($POST['Beneficials']['cDateOfBirth']) ? date('Y-m-d',strtotime($POST['Beneficials']['cDateOfBirth'])) : NULL;
                $POST['Beneficials']['cCountry'] = isset($POST['Beneficials']['cCountry']) ? $POST['Beneficials']['cCountry']['value'] : NULL;

                //$POST['Beneficials']['type'] = 'business';
                if(isset($POST['Beneficials']['id']) && !empty($POST['Beneficials']['id'])){
                    $addBank = false;
                    $find = \app\models\Beneficials::find()->where(['id'=>$POST['Beneficials']['id']])->one();
                    if($find){
                        if($find->funding_source_url == null){
                            //$addBank = true;
                            \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                            \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
            
                            $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
                            $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                            $appToken = $tokensApi->token();
                            \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
                            //\DwollaSwagger\Configuration::$debug = 1;

                            $fsApi = new \DwollaSwagger\FundingsourcesApi($apiClient);
                            $fundingSources = $fsApi->getCustomerFundingSources($find->dwolla_customer_url);
                            $find->funding_source_url = $fundingSources->_embedded->{'funding-sources'}[1]->_links->{'with-available-balance'}->href;
                            $find->save();
                        }
                        return [
                            'success' => true,
                            'dwollaCustomerUrl' => $find->dwolla_customer_url,
                            'message' => 'Account already verified.',
                            //'addBank' => $addBank
                        ];
                    }
                }
                else if($model->load($POST) && $model->save()){
                    \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                    \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
    
                    $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
                    $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                    $appToken = $tokensApi->token();
                    \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
                    //\DwollaSwagger\Configuration::$debug = 1;

                    $customersApi = new \DwollaSwagger\CustomersApi($apiClient);

                    if($POST['Beneficials']['type'] == 'personal'){
                        $details = [
                            'firstName' => $model->firstName,
                            'lastName' =>  $model->lastName,
                            'email' =>  $model->email,
                            'type' => $POST['Beneficials']['type'],
                            'address1' =>  $model->address1,
                            'city' =>  $model->city,
                            'state' =>  $model->stateProvinceRegion,
                            'postalCode' => $model->postalCode,
                            'dateOfBirth' => $model->dateOfBirth,
                            'ssn' => $model->ssn
                        ];
                    }
                    else if($POST['Beneficials']['type'] == 'business'){
                        if($POST['Beneficials']['businessType'] == 'soleProprietorship'){
                            $details = [
                                'firstName' => $model->firstName,
                                'lastName' =>  $model->lastName,
                                'email' =>  $model->email,
                                'ipAddress' => Yii::$app->request->userIP,
                                'type' => $POST['Beneficials']['type'],
                                'dateOfBirth' => $model->dateOfBirth,
                                'ssn' => $model->ssn,
                                'address1' =>  $model->address1,
                                'city' =>  $model->city,
                                'state' =>  $model->stateProvinceRegion,
                                'postalCode' => $model->postalCode,
                                'businessClassification' => $model->businessClassification,
                                'businessType' => $model->businessType,
                                'businessName' => $model->businessName,
                                'ein' => $model->ein,
                            ];
                        }
                        else if($POST['Beneficials']['businessType'] == 'corporation' || $POST['Beneficials']['businessType'] == 'llc' || $POST['Beneficials']['businessType'] == 'partnership'){
                            $details = [
                                'firstName' => $model->firstName,
                                'lastName' =>  $model->lastName,
                                'email' =>  $model->email,
                                'type' => $POST['Beneficials']['type'],
                                'address1' =>  $model->address1,
                                'city' =>  $model->city,
                                'state' =>  $model->stateProvinceRegion,
                                'postalCode' =>  $model->postalCode,
                                'controller' => [
                                    'firstName' => $model->cFirstName,
                                    'lastName' =>  $model->cLastName,
                                    'title' =>  $model->title,
                                    'dateOfBirth' =>  $model->cDateOfBirth,
                                    'ssn' =>  $model->cSsn,
                                    'address' =>  [
                                        'address1' => $model->cAddress1,
                                        'address2' => $model->cAddress2,
                                        'city' => $model->cCity,
                                        'stateProvinceRegion' => $model->cStateProvinceRegion,
                                        'postalCode' => $model->cPostalCode,
                                        'country'=>$model->cCountry,
                                    ]
                                ],
                                'phone' => $model->phone,
                                'businessClassification' => $model->businessClassification,
                                'businessType' => $model->businessType,
                                'businessName' => $model->businessName,
                                'ein' => $model->ein,
                            ];
                        }
                    }
                    //print_r($details);die;
                    $customer = $customersApi->create($details);
                    $model->dwolla_customer_url = $customer;

                    $fsApi = new \DwollaSwagger\FundingsourcesApi($apiClient);
                    $fundingSources = $fsApi->getCustomerFundingSources($customer);
                    $model->funding_source_url = $fundingSources->_embedded->{'funding-sources'}[0]->_links->{'with-available-balance'}->href;
                    if($model->save()){
                        return [
                            'success' => true,
                            'dwollaCustomerUrl'=>$customer,
                            'message' => 'Beneficial added successfully.',
                            //'addBank' => true
                        ];
                    }
                } 
                else{
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }             
           } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }
        }
    }

    public function actionCheckFundingSource()
    {
        try {
            $model = new \app\models\User;
            $find = $model->find()->where(['id' => Yii::$app->user->identity->id])->one();
            if ($find) {
                if($find->dwolla_customer_url == null){
                    if(empty($find->first_name) || empty($find->last_name)){
                        return [
                            'error' => true,
                            'message' => 'First name and last is missing.'
                        ];
                    }
                    \DwollaSwagger\Configuration::$username = Yii::$app->params['dwollaUsername'];
                    \DwollaSwagger\Configuration::$password = Yii::$app->params['dwollaPassword'];
                    $apiClient = new \DwollaSwagger\ApiClient(Yii::$app->params['dwollaApiUrl']);
                    
                    $tokensApi = new \DwollaSwagger\TokensApi($apiClient);
                    $appToken = $tokensApi->token();
                    \DwollaSwagger\Configuration::$access_token = $appToken->access_token;
                    
                    $customersApi = new \DwollaSwagger\CustomersApi($apiClient);
                    $customerDetails = $customersApi->create([
                        'firstName' => Yii::$app->user->identity->first_name,
                        'lastName' => Yii::$app->user->identity->last_name,
                        'email' => Yii::$app->user->identity->email,
                        'ipAddress' => Yii::$app->request->userIP
                    ]);
                    $find->dwolla_customer_url = $customerDetails;
                    if($find->save()){
                        return [
                            'success' => true,
                            'addFundingSource'=>true
                        ];
                    }
                    else{
                        return [
                            'error' => true,
                            'message'=>$find->getErrors()
                        ];
                    }
                }
                else if($find->funding_source_url == null){
                    return [
                        'success' => true,
                        'addFundingSource'=>true,
                        'dwollaCustomerUrl'=>$find->dwolla_customer_url
                    ];
                }
                else{
                    return [
                        'success' => true,
                        'addFundingSource'=>false
                    ];
                }
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }
}
