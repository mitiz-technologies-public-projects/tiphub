<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\Response;

class WidgetController extends Controller
{
    public $layout = "blank";
    
    public function actionIndex($username)
    {
        $data = [];
        $data['servicesLinkArr'] = ["","https://cash.app/$", "https://venmo.com/code?user_id=", "https://www.patreon.com/", "https://www.paypal.com/paypalme/"];
        $data['servicesArr'] = ["","CashApp", "Venmo", "Patreon", "Paypal.me"];
        if (isset($username) && !empty($username)) {
            try {
                $model = new \app\models\User;
                $find = $model->find()->select(Yii::$app->params['user_table_select_columns'])->joinWith(['services'=>function($q){$q->orderby('sequence');},'qrcodes','sociallinks'])->where(['users.id' => $username])->orWhere(['users.username' => $username])->one();
                if ($find) {
                    $data['record'] = $find;
                } 
            } catch (\Exception $e) {
                $data['error'] = Yii::$app->common->returnException($e);
            }
        } 
        return $this->render('index', $data);
    }
}
