<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\Response;
use xstreamka\mobiledetect\Device;
use Carbon\Carbon;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class CronController extends Controller
{

    public $enableCsrfValidation = false;
    private $apiMode = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
        //$_GET['fields'] = json_decode($_GET['fields'], true);
       
    }

    // public function __construct(){
    //    // $this->apiMode =  \net\authorize\api\constants\ANetEnvironment::SANDBOX;
    // }
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
            $behaviors['authenticator'] = [
                'except' => ['check-membership','data-base-backup'],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    //QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'except' => ['data-base-backup'],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        //return Yii::$app->common->checkPermission('Users', $action->id);
                        return true;
                    },
                ],
            ],
        ];
        return $behaviors;
    }

    // Function to set the cronjob for users Membership
    function actionCheckMembership(){
        try{
            $users = new \app\models\User;
            $users = $users->find()->joinWith(['usersMembership'])
            ->where(['users_membership.status' => 1,'users.status' => 'Y'])
            
            ->andFilterWhere(['or',
                ['=','DATE(trial_period_ends)',date('Y-m-d')], // if it's expired today
                ['=','DATE(plan_ends_on)',date('Y-m-d')]])
            ->asArray()->all();
            
            // Loop through all users and mark the payments due if the current plan is expired
            if(count($users)){
                foreach($users as $user){
                    $membership_id = $user['usersMembership'];
                    return $membership_id;
                }
            }
            // print_r($users);
            // return [
            //     'success'=>true,
            //     'membership'=>$users
            // ];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

     // function to save CC details for users
     public function actionSaveBillingDetails()
     {
         try{
            //$this->createAuthNetSubscription();die;
            $apiMode = \net\authorize\api\constants\ANetEnvironment::SANDBOX;
            $model = new \app\models\UsersPaymentProfile;
            $POST['UsersPaymentProfile'] = $_POST['fields']; 
            $model->user_id = Yii::$app->user->identity->id;
            $model->added_on = date('Y-m-d H:i:s');
            if($model->load($POST) && $model->validate()){
                /* Create a merchantAuthenticationType object with authentication details
                retrieved from the constants file */
                //echo Yii::$app->params['authorizeUsername']."=>>>>>>".Yii::$app->params['authorizeKey'];die;
                $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
                $merchantAuthentication->setName(Yii::$app->params['authorizeUsername']);
                $merchantAuthentication->setTransactionKey(Yii::$app->params['authorizeKey']);
                
               
                // Create a Customer Profile Request
                // Submit a CreateCustomerProfile Request
                // Validate Profile ID returned

                // Set credit card information for payment profile
                $creditCard = new AnetAPI\CreditCardType();
                $creditCard->setCardNumber($POST['UsersPaymentProfile']['cc_no']); //4242424242424242
                $creditCard->setExpirationDate($POST['UsersPaymentProfile']['expiry']); //"2038-12"
                $creditCard->setCardCode($POST['UsersPaymentProfile']['ccv']); //"142"
                $paymentCreditCard = new AnetAPI\PaymentType();
                $paymentCreditCard->setCreditCard($creditCard);

                 // Create the Bill To info for new payment type
                $billTo = new AnetAPI\CustomerAddressType();
                $billTo->setFirstName($POST['UsersPaymentProfile']['first_name']);
                $billTo->setLastName($POST['UsersPaymentProfile']['last_name']);
                $billTo->setCompany($POST['UsersPaymentProfile']['company']);
                $billTo->setAddress($POST['UsersPaymentProfile']['address']);
                $billTo->setCity($POST['UsersPaymentProfile']['city']);
                $billTo->setState($POST['UsersPaymentProfile']['state']);
                $billTo->setZip($POST['UsersPaymentProfile']['zip']);
                $billTo->setCountry($POST['UsersPaymentProfile']['country']);
                $billTo->setPhoneNumber($POST['UsersPaymentProfile']['phone']);
                $billTo->setfaxNumber($POST['UsersPaymentProfile']['phone']);

                // Create a new CustomerPaymentProfile object
                $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
                $paymentProfile->setCustomerType('individual');
                $paymentProfile->setBillTo($billTo);
                $paymentProfile->setPayment($paymentCreditCard);
                $paymentProfiles[] = $paymentProfile;

                $email = Yii::$app->user->identity->email;//"ritesh@mitiztechnologies.com";
                // Create a new CustomerProfileType and add the payment profile object
                $customerProfile = new AnetAPI\CustomerProfileType();
                $customerProfile->setDescription($POST['UsersPaymentProfile']['first_name']);
                $customerProfile->setMerchantCustomerId(Yii::$app->user->identity->id);
                $customerProfile->setEmail($email);
                $customerProfile->setpaymentProfiles($paymentProfiles);
                //$customerProfile->setShipToList($shippingProfiles);

                // Assemble the complete transaction request
                $request = new AnetAPI\CreateCustomerProfileRequest();
                $request->setMerchantAuthentication($merchantAuthentication);
                $request->setRefId(Yii::$app->user->identity->id); // Setting User id as reference
                $request->setProfile($customerProfile);
               
                // Save the data to database
                if($model->save()){
                    // Create the controller and get the response
                    $controller = new AnetController\CreateCustomerProfileController($request);
                    $response = $controller->executeWithApiResponse($apiMode);

                    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
                         
                            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
                            //echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
                            $customerProfileId = $response->getCustomerProfileId();
                            $model->profile_id = $customerProfileId;
                            $model->payment_profile_id	 = $paymentProfiles[0];
                            $model->company	 = $POST['UsersPaymentProfile']['company'];
                            $model->cc_no = "XXXXXXXX".substr($POST['UsersPaymentProfile']['cc_no'],-4);
                            $model->save();

                            // Creating Subscription of the user on Auth.net
                            $this->createAuthNetSubscription($merchantAuthentication,$customerProfileId,$paymentProfiles[0]);
                        return [
                            'success' => true,
                            'message'=> 'Billing profile added successfully.'
                        ];

                    }else {
                        //echo "ERROR :  Invalid response\n";
                        $model->delete();
                        $errorMessages = $response->getMessages()->getMessage();
                        $msg = "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
                        return [
                            'error' => true,
                            'message' => $msg,
                        ];
                    }
                }
                else{
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }

                  
            }
            else {
                return [
                    'error' => true,
                    'message' => $model->getErrors(),
                ];
            }
          } catch (\Exception $e) {
                 return [
                     'error' => true,
                     'message' => Yii::$app->common->returnException($e),
                 ];
        } 
     }

     // Function to create subscription on Auth.net
     private function createAuthNetSubscription($merchantAuthentication ="", $customerProfileId = "",$customerPaymentProfileId =""){
        $user = new \app\models\UsersMembership;
        $user = $user->find()
        ->joinWith('plan')
        ->where(['users_membership.status' => 1,'user_id' => Yii::$app->user->identity->id])
        ->andWhere(['>','plan_price',0])
        ->one();
        
      
        // print_r($user->plan->plan);
        // die;
        if($user){
            $plan_name = $user->plan->plan;
            $intervalLength = $user->plan_cycle;
            $trial_ends_dt = $user->trial_period_ends;
            $amount = $user->plan_price;
            $plan_start_date = date('Y-m-d'); 
            if ($trial_ends_dt >= date('Y-m-d')){ // If the trial is ended or about to end or the date has been passed already, start the plan on that date, else start from today
                $plan_start_date = $trial_ends_dt;
            } 
          // echo  $plan_end_date = Carbon::parse($plan_start_date)->addDays($intervalLength);die;
            // Subscription Type Info
            $subscription = new AnetAPI\ARBSubscriptionType();
            $subscription->setName($plan_name);

            $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
            $interval->setLength($intervalLength);
            $interval->setUnit("days");

            $paymentSchedule = new AnetAPI\PaymentScheduleType();
            $paymentSchedule->setInterval($interval);
            $paymentSchedule->setStartDate(new \DateTime($plan_start_date));
            $paymentSchedule->setTotalOccurrences("36"); //setting to 3 years
            //$paymentSchedule->setTrialOccurrences("1");

            $subscription->setPaymentSchedule($paymentSchedule);
            $subscription->setAmount($amount);
            //$subscription->setTrialAmount("0.00");
            
            $profile = new AnetAPI\CustomerProfileIdType();
            $profile->setCustomerProfileId($customerProfileId);
            $profile->setCustomerPaymentProfileId($customerPaymentProfileId);
           // $profile->setCustomerAddressId($customerAddressId);

            $subscription->setProfile($profile);

            $refId = time();
            $request = new AnetAPI\ARBCreateSubscriptionRequest();
            $request->setmerchantAuthentication($merchantAuthentication);
            $request->setRefId($refId);
            $request->setSubscription($subscription);
            $controller = new AnetController\ARBCreateSubscriptionController($request);

            $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
            
            if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
            {
                //echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
                $plan_end_date = Carbon::parse($plan_start_date)->addDays($intervalLength);
                 $user->updateAttributes(['authnet_subscription_id' => $response->getSubscriptionId(),'plan_starts_on' => $plan_start_date, 'plan_ends_on' =>  $plan_end_date]);
                return true;
            }
            else
            {
                echo "ERROR :  Invalid response\n";
                $errorMessages = $response->getMessages()->getMessage();
                echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
            }
        }
     }
     public function actionDataBaseBackup() {
         Yii::$app->queue->delay(0)->push(new \app\components\DataBaseBackup());
     }
}
