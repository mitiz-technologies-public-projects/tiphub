<?php

namespace app\controllers;

use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use yii\web\Response;

/**

 * Service controller for the `Services` module

 */

class ServiceController extends Controller
{

    public $enableCsrfValidation = false;

    public function init()
    {

        parent::init();

        Yii::$app->response->format = Response::FORMAT_JSON;

        \Yii::$app->user->enableSession = false;

        $_POST = json_decode(file_get_contents('php://input'), true);

    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();

        if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {

            $behaviors['authenticator'] = [

                'except' => ['get-payment-service-names'],

                'class' => CompositeAuth::className(),

                'authMethods' => [

                    HttpBearerAuth::className(),

                ],

            ];

        }

        $behaviors['corsFilter'] = [

            'class' => \yii\filters\Cors::className(),

        ];

        $behaviors['access'] = [

            'class' => \yii\filters\AccessControl::className(),

            'only' => [],

            'rules' => [

                [

                    'allow' => true,

                    'matchCallback' => function ($rule, $action) {

                        //return Yii::$app->common->checkPermission('Services', $action->id);
                        return true;

                    },

                ],

            ],

        ];

        return $behaviors;

    } 
    //https://venmo.com/test?txn=pay&amount=13.55

    //Label~Module~Action~Url~Icon:Add/Edit Service~Services~add~services~hand-holding-usd
    public function actionAdd()
    {
        if (isset($_POST) && !empty($_POST)) {
            try {
                $servicesLinkArr = ["","https://cash.app/$", "https://venmo.com/", "https://www.patreon.com/", "https://www.paypal.com/paypalme/"];
                $model = new \app\models\Services;

                $POST['Services'] = $_POST['fields'];
                $POST['Services']['user_id'] = Yii::$app->user->identity->id;
                if (isset($POST['Services']['id']) && !empty($POST['Services']['id'])) {
                    $find = $model->find()->where(['id' => $POST['Services']['id']])->one();
                    if ($find && $find->load($POST)) {
                        if($find->service_id == "other"){
                            $psnmodel = new \app\models\PaymentServiceNames;
                            $psnfind = $psnmodel->findOne(['name'=>$find->payment_service_name]);
                            if($psnfind){
                                $find->service_id = $psnfind->id;
                            }
                            else{
                                $psnmodel->name = $find->payment_service_name;
                                $psnmodel->icon = "other.svg";
                                if($psnmodel->save()){
                                    $find->service_id = $psnmodel->id;
                                }
                            }
                        }
                        if ($find->save()) {
                            return [
                                'success' => true,
                                'services'=> \app\models\Services::find()->joinWith(['appname'])->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->all(),
                                'message' => 'Service has been updated successfully.',
                            ];
                        } else {
                            return [
                                'error' => true,
                                'message' => $find->getErrors(),
                            ];
                        }

                    } else {
                        return [
                            'error' => true,
                            'message' => "Service not found.",
                        ];
                    }
                } else if ($model->load($POST) && $model->validate()) {
                    if($model->service_id == "other"){
                        $psnmodel = new \app\models\PaymentServiceNames;
                        $psnfind = $psnmodel->findOne(['name'=>$model->payment_service_name,'user_id'=>Yii::$app->user->identity->id]);
                        if($psnfind){
                            $model->service_id = $psnfind->id;
                        }
                        else{
                            $psnmodel->name = $model->payment_service_name;
                            $psnmodel->icon = "other.svg";
                            $psnmodel->user_id = Yii::$app->user->identity->id;
                            if($psnmodel->save()){
                                $model->service_id = $psnmodel->id;
                            }
                        }
                    }
                    else if($model->service_id <= 4){
                        $model->url = $servicesLinkArr[$model->service_id].$model->username;
                        if(isset($model->default_amount) && !empty($model->default_amount)){
                            $model->url = $servicesLinkArr[$model->service_id].$model->username."/".$model->default_amount;
                        }
                        if($model->service_id == 2){
                            $model->url = $servicesLinkArr[$model->service_id].$model->username."?txn=pay";
                            if(isset($model->default_amount) && !empty($model->default_amount)){
                                $model->url = $model->url."&amount=".$model->default_amount;
                            }
                        }
                    }
                    else if($model->service_id == 12){
                        $model->url = "https://onlyfans.com/".$model->username;
                        /* if(isset($model->default_amount) && !empty($model->default_amount)){
                            $model->url = $servicesLinkArr[$model->service_id].$model->username."/".$model->default_amount;
                        } */
                    }
                    if ($model->save()) {
                        return [
                            'success' => true,
                            'services'=> \app\models\Services::find()->joinWith(['appname'])->where(['services.user_id'=>Yii::$app->user->identity->id])->asArray()->all(),
                            'message' => 'Service has been added successfully.',
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => $model->getErrors(),
                    ];
                }
            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e)
                ];

            }

        }

    }

    //Label~Module~Action~Url~Icon:List Services~Services~services~services~hand-holding-usd
    public function actionList($pageSize = 50)
    {

        $response = [];

        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['services.name' => SORT_ASC],
                    'desc' => ['services.name' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['name' => SORT_ASC],
        ]);

        $model = new \app\models\Services;

        $query = $model->find()->joinWith(['locations','parent p']);//->where(['!=','services.parent_id',0]);

        $search = new \app\models\SearchForm;

        $GET['SearchForm'] = json_decode($_GET['fields'], true);

        if ($search->load($GET)) {

            
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);

        $pages->pageSize = $pageSize;

        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();

        if ($find) {

            $response = [

                'success' => true,

                'services' => $find,

                'pages' => $pages,

            ];

        } else {

            $response = [

                'success' => true,

                'services' => [],

                'pages' => $pages,

            ];

        }

        return $response;

    }

    public function actionGetPaymentServiceNames($pageSize = 50)
    {
        $response = [];
        $model = new \app\models\PaymentServiceNames;
        $find = $model->find()->where(['OR',
            //['user_id'=>Yii::$app->user->identity->id],
            ['user_id'=>0]
        ])->orderBy("name")->asArray()->all();
        $response = [
            'success' => true,
            'names' => $find,
        ];
        return $response;
    }

    //Label~Module~Action~Url~Icon:Get Service Details~Services~get-one~services~hand-holding-usd
    public function actionGetOne($id)
    {

        if (isset($id) && !empty($id)) {

            try {

                $model = new \app\models\Services;

                $find = $model->find()->where(['id' => $id])->asArray()->one();

                if ($find) {

                    return [

                        'success' => true,

                        'service' => $find,

                    ];

                } else {

                    return [

                        'error' => true,

                        'message' => 'Service not found!',

                    ];

                }

            } catch (\Exception $e) {

                return [

                    'error' => true,

                    'message' => Yii::$app->common->returnException($e),

                ];

            }

        }

    }

    //Label~Module~Action~Url~Icon:Delete Service~Services~delete~services~hand-holding-usd
    public function actionDelete()
    {

        if (isset($_POST['id']) && !empty($_POST['id'])) {
            try {
                $model = new \app\models\Services;
                $find = $model->find()->where(['id' => $_POST['id'],'user_id'=>Yii::$app->user->identity->id])->one();
                if ($find) {
                    if ($find->delete()) {
                        return [
                            'success' => true,
                            'message' => 'Service deleted successfully.',
                        ];
                    }
                } else {
                    return [
                        'error' => true,
                        'message' => 'Service not found!',
                    ];
                }

            } catch (\Exception $e) {

                return [

                    'error' => true,

                    'message' => Yii::$app->common->returnException($e),

                ];

            }

        } else {

            return [

                'error' => true,

                'message' => 'Service not found!',

            ];

        }

    }

    public function actionUpdateSequence()
    {
        //print_r($_POST);die();
        if (isset($_POST['services']) && !empty($_POST['services'])) {
            try {
                $update = 0;
                foreach($_POST['services'] as $key=>$val){
                    $model = new \app\models\Services;
                    $find = $model->find()->where(['id' => $val['id'],'user_id'=>Yii::$app->user->identity->id])->one();
                    if($find){
                        $find->sequence = $key;
                        if($find->save()){
                            $update++;
                        }
                    }
                }
                if($update > 0){
                    return [
                        'success' => true,
                        'message' => 'Service updated successfully.',
                    ];
                }

            } catch (\Exception $e) {
                return [
                    'error' => true,
                    'message' => Yii::$app->common->returnException($e),
                ];
            }

        } else {
            return [
                'error' => true,
                'message' => 'Service not found!',
            ];
        }

    }

    public function actionCheckDwollaSetting()
    {
        try {
            $model = new \app\models\Beneficials;
            $find = $model->find()->where(['user_id' => Yii::$app->user->identity->id])->one();
            if($find){
                return [
                    'success'=>true,
                ];
            }
            else{
                return [
                    'error'=>true,
                ];
            }
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => Yii::$app->common->returnException($e),
            ];
        }
    }

}
