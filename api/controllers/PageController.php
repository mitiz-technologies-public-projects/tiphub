<?php

namespace app\controllers;

use Yii;
use yii\base\ErrorException;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Controller;
use yii\web\Response;

class PageController extends Controller
{

    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
        \Yii::$app->user->enableSession = false;
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    public function behaviors(){
        $behaviors = parent::behaviors();
        if($_SERVER['REQUEST_METHOD'] != 'OPTIONS'){
            $behaviors['authenticator'] = [
                'except' => [],
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    //QueryParamAuth::className(),
                ],
            ];
        }
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];				
		$behaviors['access'] = [
			'class' => \yii\filters\AccessControl::className(),
			'only' => [],
			'rules' => [
				[				
					'allow' => true,
					'matchCallback' => function ($rule, $action) {												
						return Yii::$app->common->checkPermission('Static Pages',$action->id);						
					}
				],
			],
		];		
		return $behaviors;
    }

    function actionAdd(){							
		try{
			$model = new \app\models\Page;			
			if(isset($_POST) && !empty($_POST)){				
                $POST['Page'] = $_POST['fields'];                
                $POST['Page']['content'] = $_POST['content'];
                $POST['Page']['left_menu'] = $_POST['left_menu']?'Y':'N';
                if(isset($_POST['id']) && !empty($_POST['id'])){                      
                    $find = $model->findOne($_POST['id']);                                        
                    if($find){     
                        $find->load($POST);
                        if($find->save()){
                            return [
                                'success'=>true,
                                'message'=>'Page updated successfully.'
                            ];
                        }
                        else{
                            return [
                                'error'=>true ,
                                'message'=> $find->getErrors()
                            ];
                        }                        
                    }
                    else{
                        return [
                            'error'=>true,
                            'message'=> "Page not found."
                        ];
                    }											
                }
                else{                   				
                    $model->load($POST);                   
                    if($model->save()){
                        return [
                            'success'=>true,
                            'message'=>'Page added successfully.'
                        ];
                    }
                    else{
                        return [
                            'error'=>true,
                            'message'=> $model->getErrors()
                        ];
                    }						
                }				
			}						
		}
		catch(\Exception $e){
			return [
                'error'=>true,
                'message' => Yii::$app->common->returnException($e),
            ];
		}		
	}

    public function actionList($pageSize = 50)
    {
        $response = [];
        $data = [];

        $sort = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['id' => SORT_ASC],
                    'desc' => ['id' => SORT_DESC],
                    'default' => SORT_DESC,                    
                ],
                'name' => [
                    'asc' => ['name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC],
                    'default' => SORT_DESC,                    
                ],
                
            ],
            'defaultOrder' => ['name' => SORT_ASC],
        ]);

        $model = new \app\models\Page;
        $query = $model->find()->where("id != 0");
        
        $search = new \app\models\SearchForm;
        $GET['SearchForm'] = json_decode($_GET['fields'], true);
        if ($search->load($GET)) {
            if (!empty($search->name)) {
                $query->andWhere(['LIKE', 'name', $search->name]);
            }
        }

        $countQuery = clone $query;

        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = $pageSize;
        $find = $query->offset($pages->offset)->limit($pages->limit)->orderBy($sort->orders)->asArray()->all();
        if($find){
            $response = [
                'success'=>true,
                'contents'=>$find,
                'pages'=>$pages
            ];           
        }
        else{
            $response = [
                'success'=>true,
                'contents'=>[],  
                'pages'=>$pages              
            ];
        }
        return $response;
    }

    public function actionGetOne($id)
    {        
        if(isset($id) && !empty($id)){
            try{
                $model = new \app\models\Page;
                $find = $model->find()->where(['id'=>$id])->asArray()->one();
                if($find){
                    return [
                        'success'=>true,
                        'content'=>$find
                    ];
                }
                else{
                    return [
                        'error'=>true,
                        'message'=>'Page not found!'
                    ];
                }
            }
			catch(\Exception $e){
                return [
                    'error'=>true,
                    'message' => Yii::$app->common->returnException($e),
                ];													
			}
        }
        else{
            return [
                'error'=>true,
                'message'=>'Page not found!'
            ];
        }
    }

    public function actionGetOneByName($url)
    {        
        if(isset($url) && !empty($url)){
            try{
                $model = new \app\models\Page;
                $find = $model->find()->where(['url'=>$url])->asArray()->one();
                if($find){
                    return [
                        'success'=>true,
                        'content'=>$find
                    ];
                }
                else{
                    return [
                        'error'=>true,
                        'message'=>'Page not found!'
                    ];
                }
            }
			catch(\Exception $e){
                return [
                    'error'=>true,
                    'message' => Yii::$app->common->returnException($e),
                ];													
			}
        }
        else{
            return [
                'error'=>true,
                'message'=>'Page not found!'
            ];
        }
    }

    public function actionDelete()
    {               
        if(isset($_POST['id']) && !empty($_POST['id'])){            
            try{
                $model = new \app\models\Page;
                $find = $model->find()->where(['id'=>$_POST['id']])->one();
                if($find){                    
                    if($find->delete()){
                        return [
                            'success'=>true,
                            'message'=>'Page has been deleted successfully.'
                        ];
                    }
                    else{
                        return [
                            'error'=>true,
                            'message'=>$find->getErrors()
                        ];
                    }                    
                }
                else{
                    return [
                        'error'=>true,
                        'message'=>'Page not found!'
                    ];
                }
            }
			catch(\Exception $e){
                return [
                    'error'=>true,
                    'message' => Yii::$app->common->returnException($e),
                ];													
			}
        }
        else{
            return [
                'error'=>true,
                'message'=>'Category not found!'
            ];
        }
    }
      
    
    public function actionCopyData(){
        $query = new \yii\db\Query;
        $rows = $query->from("elgg_users_entity")->all();
        foreach($rows as $row){            
            $model = new \app\models\User;
            $find = $model->findOne(['username'=>$row['username']]);
            if(!$find){
                $model->name = $row['name'];
                $model->username = $row['username'];
                $model->password_hash = $row['password_hash'];
                $model->email = $row['email'];
                $model->role_id = 2; 
                if($model->validate())               {
                    $model->save();
                }
            }
            else{
                $find->status = "Y";
                $find->save();
            }
        }
    }

    

}
