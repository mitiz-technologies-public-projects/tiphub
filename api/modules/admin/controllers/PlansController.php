<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;

/**
 * Default controller for the `admin` module
 */
class PlansController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->request->isAjax) {
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
            } else {
                Yii::$app->user->loginUrl = ['/site/login'];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit;
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        if (! empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 1) {
                            return true;
                        }
                    }
                ],
            ],
        ];
        return $behaviors;
    }
    /**
     * This function is used to list all Roles	 
	  
     * @throws Exception If something interesting cannot happen
     * @author Vikas
     * Label~Module~Action:List Plans~Plans~index
     */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = false;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'plan' => [
                    'asc' => ['plan' => SORT_ASC,],
                    'desc' => ['plan' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Plan',
                ],
                'monthly_price' => [
                    'asc' => ['monthly_price' => SORT_ASC,],
                    'desc' => ['monthly_price' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Monthly Price',
                ],
                'yearly_price' => [
                    'asc' => ['yearly_price' => SORT_ASC,],
                    'desc' => ['yearly_price' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Yearly Price',
                ],

            ],
            'defaultOrder' => ['plan' => SORT_ASC]
        ]);

        $data['search'] = $search = new \app\models\SearchForm;

        $model = new \app\models\Plans;
        $query = $model->find();
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (!empty($search->plan)) {
                $query->andWhere([
                    'or',
                    ['LIKE', 'plan', $search->plan],
                ]);
            }
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)->limit($pages->limit)->orderBy($data['sort']->orders)->all();
        $data['pages'] = $pages;
        return $this->render('plans', $data);
    }

    /**
     * This function is used to add/edit plan	 
	  
     * @throws Exception If something interesting cannot happen
     * @author Ritesh
     * Label~Module~Action:Add/edit Plan~Plans~add-plan
     */
    function actionAddPlan($id = NULL)
    {
        $data = $response = [];
        try {
            $data['model'] = $model = new \app\models\Plans;
            if ($model->load(Yii::$app->request->post())) {
                try {
                    $id = $_POST['Plans']['id'];
                    if (isset($id) && !empty($id)) {
                        if ($find = $model->findOne(['id' => $id])) {
                            $find->load(Yii::$app->request->post());

                            if ($find->validate() && $find->save()) {
                                Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Plan updated successfully.');
                            } else {
                                Yii::$app->getSession()->setFlash('error', $find->getErrors());
                            }
                        }
                    } else {
                        if ($model->validate() && $model->save()) {
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Plan has been added successfully.');
                        } else {
                            $erros = "";
                            foreach ($model->getErrors() as $key => $error) {
                                $erros .= $error[0] . "<br>";
                            }
                            Yii::$app->getSession()->setFlash('error', $erros);
                        }
                    }
                } catch (\Exception $e) {
                    Yii::$app->getSession()->setFlash('error', $e->getMessage());
                }
                return $this->redirect("index");
            } else {
                if ($id != NULL) {
                    $find = $model->find()->where(['id' => $id])->count();
                    if ($find) {
                        $data['model'] = $model->findOne(['id' => $id]);
                    }
                }
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
        }
        return $this->render('add-plan', $data);
    }

    /**
     * This function is used to delete Template	 
	  
     * @throws Exception If something interesting cannot happen
     * @author Ritesh
     * Label~Module~Action:Delete Plan~Plans~delete
     */
    public function actionDelete($id)
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $model = new \app\models\Plans;
            if ($find = $model->findOne(['id' => $_GET['id']])) {
                if ($find->delete()) {
                    Yii::$app->getSession()->setFlash('success', 'Plan has been deleted successfully.');
                } else {
                    Yii::$app->getSession()->setFlash('success', $find->getErrors());
                }
            } else {
                Yii::$app->getSession()->setFlash('error', "Plan not found.");
            }
        }
        $this->redirect("index");
    }


    
}
