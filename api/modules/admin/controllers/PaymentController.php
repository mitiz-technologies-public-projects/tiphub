<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

/**
 * Payment controller for the `Event` module
 */
class PaymentController extends Controller
{

    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            if (! Yii::$app->request->isAjax) {
                Yii::$app->user->loginUrl = [
                    '/site/login',
                    'return' => \Yii::$app->request->url
                ];
            } else {
                Yii::$app->user->loginUrl = [
                    '/site/login'
                ];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit();
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'actions' => [
                        'index',
                        'add',
                        'delete'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {
                        if (! empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 1) {
                            return true;
                        }
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    /**
     * This function is used to list all events
     *
     * @throws Exception If something interesting cannot happen
     * @author Sibananda
     * Label~Module~Action:List Events~Event~index
     */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'email' => [
                    'asc' => ['email' => SORT_ASC],
                    'desc' => ['email' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'amount' => [
                    'asc' => ['txn_amount' => SORT_ASC],
                    'desc' => ['txn_amount' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'event' => [
                    'asc' => ['events.title' => SORT_ASC],
                    'desc' => ['events.title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'received_at' => [
                    'asc' => ['received_at' => SORT_ASC],
                    'desc' => ['received_at' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['received_at' => SORT_DESC],
        ]);
        $data['search'] = $search = new \app\models\SearchForm();
        $model = new \app\models\Payments();
        $query = $model->find()->joinWith(['event','service'])->where(['payments.type'=>1, 'status'=>1])->andWhere(['!=', 'payments.event_id', 'NULL']);
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (! empty($search->date_range)) {
                list($start_date, $end_date) = explode(' - ', $search->date_range);
                $query->andWhere([
                    'AND',
                    [
                        '>=',
                        'DATE(received_at)',
                        $start_date
                    ],
                    [
                        '<=',
                        'DATE(received_at)',
                        $end_date
                    ]
                ]);
            }
            if (! empty($search->title)) {
                $query->andWhere(['LIKE', 'events.title', $search->title]);
            }
            if (! empty($search->name)) {
                $query->andWhere(['LIKE', 'payment_service_names.name', $search->name]);
            }
        }
        else{
            $query->andWhere([
                'AND',
                [
                    '>=',
                    'DATE(received_at)',
                    date('Y-m-d')
                ],
                [
                    '<=',
                    'DATE(received_at)',
                    date('Y-m-d')
                ]
            ]);
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination([
            'totalCount' => $countQuery->count()
        ]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($data['sort']->orders)
            ->all();
        //echo $query->createCommand()->getRawSql();die;
        $data['pages'] = $pages;
        return $this->render('index', $data);
    }
}
