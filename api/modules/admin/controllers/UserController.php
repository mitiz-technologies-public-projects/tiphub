<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;
use app\models\User;
use yii\web\UploadedFile;

/**
 * User controller for the `User` module
 */
class UserController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest){
            if(!Yii::$app->request->isAjax){
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
            }else{
                Yii::$app->user->loginUrl = ['/site/login'];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit;
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        if (! empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 1) {
                            return true;
                        }
                    }
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * This function is used to list all users
     * @throws Exception If something interesting cannot happen
     * @author Vikas
     * Label~Module~Action:List Users~User~index 
    */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['users.id' => SORT_ASC, ],
                    'desc' => ['users.id' => SORT_DESC, ],
                    'default' => SORT_DESC,
                    'label' => 'ID', 
                ],
                'email' => [
                    'asc' => ['users.email' => SORT_ASC, ],
                    'desc' => ['users.email' => SORT_DESC, ],
                    'default' => SORT_DESC,
                    'label' => 'Email', 
                ],
                'phone' => [
                    'asc' => ['users.phone' => SORT_ASC, ],
                    'desc' => ['users.phone' => SORT_DESC, ],
                    'default' => SORT_DESC,
                    'label' => 'Phone', 
                ],
                 'name' => [
                    'asc' => ['users.name' => SORT_ASC, ], 
                    'desc' => ['users.name' => SORT_DESC, ], 
                    'default' => SORT_DESC,
                    'label' => 'Name', 
                ],
                'username' => [
                    'asc' => ['users.username' => SORT_ASC, ],
                    'desc' => ['users.username' => SORT_DESC, ],
                    'default' => SORT_DESC, 
                    'label' => 'Username',
                ], 
                'added_on' => [
                    'asc' => ['users.added_on' => SORT_ASC, ],
                    'desc' => ['users.added_on' => SORT_DESC, ],
                    'default' => SORT_DESC,
                    'label' => 'Added On',
                ], 
            ], 
            'defaultOrder' => ['id' => SORT_DESC]]);

        $data['search'] = $search = new \app\models\SearchForm;

        $model = new \app\models\User;
        $query = $model->find();
        if ($search->load(Yii::$app->request->get()))
        {
            $data['search_form'] = true;
            if (!empty($search->name))
            {
                $query->andWhere(['or', ['LIKE', 'users.name', $search->name], ['LIKE', 'users.email', $search->name], ['LIKE', 'users.username', $search->name]]);
            }
            
            if (!empty($search->role))
            {
                $query->joinWith(['roles' => function ($j_query) use($search) {
                    return $j_query->andWhere(['assigned_roles.role_id' => $search->role]);
                }]);
            }
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count() ]);
        $pages->pageSize = 20;
        $data['records'] = $query->andWhere(['role_id'=>'0'])
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($data['sort']->orders)
            ->all();
        $data['pages'] = $pages;
        // echo $query->createCommand()->getRawSql();die;
        return $this->render('index', $data);
    }

public function actionUpdate(){
        $featured = $_POST['featured'];
        $id = $_POST['id'];
        if (isset($id) && !empty($id)){
            $model = new \app\models\User;
            if ($model = $model->findOne(['id' => $id])){

                $model->featured = $featured;
                $model->save();
                //print_r($find);exit;
                /*
                if ($model->save()){
                    //echo "done";die;
                    Yii::$app->getSession()->setFlash('success', 'Changes has been successfully made.');
                }else{
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
                */
            }else{
                Yii::$app->getSession()->setFlash('error', "Event not found.");
            }
        }
        //$this->redirect(Yii::$app->request->referrer);
    }
    /**
     * This function is used to add and update user
     * @throws Exception If something interesting cannot happen
     * @author Vikas
     * Label~Module~Action:Add/edit User~User~add
    */
    public function actionAdd($id = NULL)
    {
        $data = $response = [];
        $title = 'Add New User';
        try
        {
            $data['model'] = $model = new \app\models\User();
            if ($model->load(Yii::$app->request->post()))
            {
                try
                {
                    //$roles = $_POST['User']['role'];
                    $id = $_POST['User']['id'];
                    if (isset($id) && !empty($id))
                    {
                        if ($find = $model->findOne(['id' => $id]))
                        {
                            
                            $find->load(Yii::$app->request->post());
                            if (!empty($model->password) && $model->password != null)
                            {
                                $find->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                            }

                            if ($find->validate() && $find->save())
                            {
                                Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> User updated successfully.');
                                $response['success'] = true;
                            }
                            else
                            {
                                $response['error'] = $find->getErrors();
                            }
                        }
                    }
                    else
                    {
                        if (!empty($model->password) && $model->password != null)
                        {
                            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
                        }

                        $model->added_by = Yii::$app->user->identity->id;
                        $model->name = ucwords($_POST['User']['first_name']) . " " . ucwords($_POST['User']['last_name']);

                        if ($model->validate() && $model->save())
                        {
                            $data['user_id'] = $model->id;

                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> User has been added successfully.');
                            
                            $response['success'] = true;
                        }
                        else
                        {
                            $response['error'] = $model->getErrors();
                        }
                    }
                    return json_encode($response);
                }
                catch(\Exception $e)
                {
                    return \app\widgets\Popup::widget(['popup_id' => 'add-edit-window', 'title' => 'Error', 'message' => '<p align="center" style="padding:20px;color:red">' . $e->getMessage() . '</p>', 'padding' => '20px', ]);
                }
            }
            else
            {
                if ($id != NULL)
                {
                    $title = 'Update User';
                    $find = $model->find()->where(['id' => $id])->count();
                    if ($find)
                    {
                        $data['model'] = $model->findOne(['id' => $id]);
                    }
                }
                return \app\widgets\Popup::widget(['popup_id' => 'add-edit-window', 'title' => $title,
                //'size'=>'modal-sm',
                'padding' => '20px', 'message' => $this->renderAjax('add-edit-user', $data) , ]);
            }
        }
        catch(\Exception $e)
        {
            return \app\widgets\Popup::widget(['popup_id' => 'add-edit-window', 'title' => 'Error', 'message' => '<p align="center" style="padding:20px;color:red">' . $e->getMessage() . '</p>', 'padding' => '10px', ]);
        }
    }

    /**
     * This function is used to delete User
     * @throws Exception If something interesting cannot happen
     * @author Vikas
     * Label~Module~Action:Delete User~User~delete-user
    */
    public function actionDeleteUser($id)
    {
        if (isset($id) && !empty($id)){
            $model = new \app\models\User;
            if ($find = $model->findOne(['id' => $id])){
                if ($find->delete()){
                    Yii::$app->getSession()->setFlash('success', 'User has been deleted successfully.');
                    \app\models\AssignedRoles::deleteAll(['user_id' => $id]);
                    \app\models\UserEmails::deleteAll(['user_id'=> $id]);
                    \app\models\ProfileVisitors::deleteAll(['user_id' => $id]);
                    \app\models\Services::deleteAll(['user_id' => $id]);
                    \app\models\SocialLinks::deleteAll(['user_id' => $id]);
                    \app\models\UserAppVisitors::deleteAll(['user_id' => $id]);
                    \app\models\Qrcodes::deleteAll(['user_id' => $id]);

                }else{
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
            }else{
                Yii::$app->getSession()->setFlash('error', "User not found.");
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }
    public function actionShadowUser($id) {
        $user =  User::findOne($id);
        if (empty($user->access_token)) {
            $user->access_token =  Yii::$app->security->generateRandomString();
            $user->save();
        }
        if (!empty($user->access_token)){
            Yii::$app->user->logout();
            $url = Yii::$app->params['siteUrl']."/force-login/".$user->access_token;
            $this->redirect($url);
        }
    }
}