<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;
use yii\helpers\ArrayHelper;
use app\models\Settings;

/**
 * Settings controller for the `Settings` module
 */
class SettingsController extends Controller
{

    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            Yii::$app->user->loginUrl = [
                '/site/login',
                'return' => \Yii::$app->request->url
            ];
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit();
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                    if (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 2) {
                            return true;
                    }else{
                        return Yii::$app->common->checkPermission('Settings', $action->id);}
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    /**
     * This function is used to update password
     *
     * @throws Exception If something interesting cannot happen
     * @author Vikas
     *         Label~Module~Action:Profile Update~Settings~profile
     */
    public function actionProfile()
    {
        $data = [];
        $data['password'] = $password = new \app\models\ChangePasswordForm();
        $data['user'] = $user = new \app\models\User();
        if ($password->load(Yii::$app->request->post()) && $password->checkUser()) {
            try {
                if ($password->validate()) {
                    $user = new \app\models\User();
                    $user = $user->findOne([
                        'id' => Yii::$app->user->identity->id
                    ]);
                    $user->password = $password->new_password;
                    $user->password_hash = Yii::$app->security->generatePasswordHash($password->new_password);
                    $user->add();
                    Yii::$app->getSession()->setFlash('success', 'Your password has been changed successfully.');
                } else {

                    $errors = $password->getErrors();
                    foreach ($errors as $key => $error) {
                        $data['password']->addError($key, $error[0]);
                    }
                }
            } catch (\Exception $e) {
                Yii::$app->getSession()->setFlash('error', "Something went through with this request. Please try again later.");
            }
            return $this->redirect("profile");
            exit();
        }

        if ($user->load(Yii::$app->request->post())) {
            $user = $user->findOne([
                'id' => Yii::$app->user->identity->id
            ]);
            $prvis_image = $user->image;
            $user->load(Yii::$app->request->post());
            $new_image = $user->image;
            if ($user->validate()) {
                $user = $user->add();

                Yii::$app->getSession()->setFlash('success', 'Profile updated successfully.');
                return $this->redirect("profile");
                exit();
            } else {
                $errors = $user->getErrors();
                foreach ($errors as $key => $error) {
                    $data['user']->addError($key, $error[0]);
                }
            }
        }
        return $this->render('profile', $data);
    }
}
   
