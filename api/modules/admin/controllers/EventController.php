<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;
use app\models\User;
use app\models\Events;
use yii\web\UploadedFile;
use app\models\Categories;
use yii\helpers\ArrayHelper;
use app\models\EventTicketBookings;
use app\models\EventInvoice;
use app\models\InvoicePayment;
use app\models\PaymentServiceNames;
use app\models\EventTicketBookingDetails;
use app\models\SubCategory;

/**
 * Event controller for the `Event` module
 */
class EventController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->request->isAjax) {
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
            } else {
                Yii::$app->user->loginUrl = ['/site/login'];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit;
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'actions' => [],
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        if (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 1) {
                            return true;
                        }
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    /**
     * This function is used to list all events
     * @throws Exception If something interesting cannot happen
     * @author Sibananda
     * Label~Module~Action:List Events~Event~index 
     */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'id' => [
                    'asc' => ['events.id' => SORT_ASC,],
                    'desc' => ['events.id' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'ID',
                ],
                'title' => [
                    'asc' => ['events.title' => SORT_ASC,],
                    'desc' => ['events.title' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Title',
                ],
                'target_amount' => [
                    'asc' => ['events.target_amount' => SORT_ASC,],
                    'desc' => ['events.target_amount' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Target Amount',
                ],
                'start_date' => [
                    'asc' => ['events.start_date' => SORT_ASC,],
                    'desc' => ['events.start_date' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Start Date',
                ],
                'end_date' => [
                    'asc' => ['events.end_date' => SORT_ASC,],
                    'desc' => ['events.end_date' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'End Date',
                ],
                'added_on' => [
                    'asc' => ['events.added_on' => SORT_ASC,],
                    'desc' => ['events.added_on' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Added On',
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC]
        ]);

        $data['search'] = $search = new \app\models\SearchForm;

        $model = new \app\models\Events;
        $query = $model->find();
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (!empty($search->name)) {
                $query->andWhere(['or', ['LIKE', 'title', $search->name]]);
            }
            if (!empty($search->category)) {
                $query->andWhere(['category' => $search->category]);
            }
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)->limit($pages->limit)->orderBy($data['sort']->orders)->all();
        $data['pages'] = $pages;
        $categories["0"] = "Parent Category";

        $categories = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
        $categories = array("0" => "Parent Category") + $categories;
        $data['categories'] = $categories;
        // echo $query->createCommand()->getRawSql();die;
        return $this->render('index', $data);
    }

    public function actionAdd($id = NULL)
    {
        $data = $response = [];
        try {
            $data['model'] = $model = new \app\models\Events;
            $data['categorylist'] = ArrayHelper::map(Categories::find()->where(['parent_id'=>0])->all(), "id", "name");
            if ($model->load(Yii::$app->request->post())) {
                try {
                    if (isset($id) && !empty($id)) {
                        if ($find = $model->findOne(['id' => $id])) {
                            $find->load(Yii::$app->request->post());
                            $find->url = preg_replace('/\s+/', '-', strtolower(trim($find->title)));
                            if ($find->save()) {
                                /* $imageDetails = \yii\web\UploadedFile::getInstance($find, 'image');
                                $new_file_name = mt_rand(111111, 999999) . "." . $imageDetails->extension;
                                if(\yii\web\UploadedFile::saveAs($_SERVER['DOCUMENT_ROOT'] . "/api/web/events/" . $new_file_name)){

                                } */
                                if ($find->approved == 1) {
                                    $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 23])->one();
                                    $data = [
                                        'case' => 23,
                                        'name' => $find->user['name'],
                                        'event' => "<a href='" . Yii::$app->params['siteUrl'] . "/" . $find->url . "/" . $find->id . "'>" . $find->title . "</a>",
                                        'body' => $templateDetails->content,
                                    ];
                                    $compose = Yii::$app->mailer->compose('body', $data);
                                    $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                                    $compose->setTo($find->user['email']);
                                    $compose->setSubject($templateDetails->subject);
                                    if ($compose->send()) {
                                        Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Event updated successfully.');
                                        return $this->redirect("index");
                                    }
                                }
                                Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Event updated successfully.');
                                return $this->redirect("index");
                            } else {
                                $erros = "";
                                foreach ($find->getErrors() as $key => $error) {
                                    $erros .= $error[0] . "<br>";
                                }
                                Yii::$app->getSession()->setFlash('error', $erros);
                            }
                        }
                    } else if ($model->validate() && $model->save()) {
                        //send email to admin
                        $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 21])->one();
                        $data = [
                            'case' => 21,
                            'body' => $templateDetails->content,
                            'event' => $model->title,
                            'name' => $model->user['name'],
                            'username' => $model->user['username'],
                            'email' => $model->user['email'],
                        ];
                        $compose = Yii::$app->mailer->compose('body', $data);
                        $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
                        $compose->setTo(Yii::$app->params['adminEmail']);
                        $compose->setSubject($templateDetails->subject);
                        if ($compose->send()) {
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Event has been added successfully.');
                        }
                    } else {
                        $erros = "";
                        foreach ($model->getErrors() as $key => $error) {
                            $erros .= $error[0] . "<br>";
                        }
                        Yii::$app->getSession()->setFlash('error', $erros);
                    }
                } catch (\Exception $e) {
                    Yii::$app->getSession()->setFlash('error', $e->getMessage());
                }
            } else {
                if ($id != NULL) {
                    $data['model'] = $model->findOne(['id' => $id]);
                }
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
        }

        return $this->render('add', $data);
    }

    public function actionUpdate()
    {
        $featured = $_POST['featured'];
        $id = $_POST['id'];
        if (isset($id) && !empty($id)) {
            $model = new \app\models\Events;
            if ($model = $model->findOne(['id' => $id])) {

                $model->featured = $featured;
                $model->save();
                //print_r($find);exit;
                /*
                if ($model->save()){
                    //echo "done";die;
                    Yii::$app->getSession()->setFlash('success', 'Changes has been successfully made.');
                }else{
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
                */
            } else {
                Yii::$app->getSession()->setFlash('error', "Event not found.");
            }
        }
        //$this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdateDiscovered()
    {
        $discovered = $_POST['discovered'];

        $id = $_POST['id'];
        if (isset($id) && !empty($id)) {
            $model = new \app\models\Events;
            if ($model = $model->findOne(['id' => $id])) {

                $model->discovered = $discovered;
                $model->save();
                //print_r($find);exit;
                /*
                if ($model->save()){
                    //echo "done";die;
                    Yii::$app->getSession()->setFlash('success', 'Changes has been successfully made.');
                }else{
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
                */
            } else {
                Yii::$app->getSession()->setFlash('error', "Event not found.");
            }
        }

        //$this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete($id)
    {
        if (isset($id) && !empty($id)) {
            $model = new \app\models\Events;
            if ($find = $model->findOne(['id' => $id])) {
                if ($find->delete()) {
                    Yii::$app->getSession()->setFlash('success', 'Event has been deleted successfully.');
                    \app\models\Transactions::deleteAll(['event_id' => $id]);
                    $paymentIds = \app\models\Payments::find()->select('id')->where(['event_id' => $id])->column();
                    \app\models\Payments::updateAll(['event_id' => NULL, 'transaction_id' => NUll, 'status' => 0], ['id' => $paymentIds]);
                    \app\models\DonorDetails::deleteAll(['event_id' => $id]);
                } else {
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
            } else {
                //echo "failed case";die;
                Yii::$app->getSession()->setFlash('error', "Event not found.");
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }
    public function actionShowInvoice($id) {
        $data = [];
        $title = 'Send Invoice';
        $model = Events::findOne(['id'=>$id]);
        if (!empty($model)) {
            $data['id'] = $id;
            $data['model']= $model;
            $data = $model->getInvoiceValue($data);
        }
        return \app\widgets\Popup::widget([
            'popup_id'=> 'send-invoice',
            'title'=> $title,
            'padding'=>'20px',
            'message'=> $this->renderAjax('send_invoice_details', $data),
        ]);
    }
    public function actionSendInvoice($id) {
        $event = Events::findOne(['id' => $id]);
        $userdata = $event->user;
        if (!empty($userdata->email)) {
            $eventData = $event->getInvoiceValue();
            $sold_ticket = !empty($eventData['total_tickets_sold'])?$eventData['total_tickets_sold']:0;
            $exsists = EventInvoice::find()->where(["event_id"=>$id])->one();
            $message = "Invoice has been sent successfully.";
            if (!empty($exsists)) {
                $model =  $exsists;
                $invoice_number = $model->invoice_id;
                $model->total_sold_amount = !empty($eventData['total_amount'])?$eventData['total_amount']:0;
                $model->service_charge = $event->service_fee;
                $model->amount = !empty($eventData['total_service_fee'])?$eventData['total_service_fee']:0;
                $message = "Invoice has been Re-sent successfully.";
                
            } else {
                $count =  EventInvoice::find()->max('invoice_id');
                if (empty($count)) {
                    $count = 100;
                }
                $count++;
                $invoice_number = sprintf('%05d', $count);
                $model= new EventInvoice();
                $model->event_id = $id;
                $model->invoice_id = $invoice_number;
                $model->total_sold_amount = !empty($eventData['total_amount'])?$eventData['total_amount']:0;
                $model->service_charge = $event->service_fee;
                $model->amount = !empty($eventData['total_service_fee'])?$eventData['total_service_fee']:0;
                $model->created_on = date("Y-m-d H:i:s");
            }
            if ($model->save()) {
                $fullname  = "";
                if ($userdata) {
                    $fullname = $userdata->first_name . " ".$userdata->last_name;
                    $fileName =  $userdata->first_name;
                }
                $templateDetails = \app\models\EmailTemplates::find()->where(['id' => 26])->one();
                $data = [
                    'case' => 26,
                    'name' => $fullname,
                    'event_name' => !empty($event)?$event->title:"N/A",
                    'total_tickets_sold'=>!empty($eventData['total_tickets_sold'])?$eventData['total_tickets_sold']:0,
                    'total_amount'=>"$".$model->total_sold_amount,
                    'service_charge_percentage'=>$model->service_charge."%",
                    'service_charge'=>"$".$model->amount,
                    'body' => $templateDetails->content,
                ];
                $compose = Yii::$app->mailer->compose('body', $data);
                $compose->setFrom(["no-reply@tiphub.co" => 'Tiphub.co']);
//              $compose->setTo(['lullmen@gmail.com','opnsrc.devlpr@gmail.com']);
//                 $compose->setTo('rahulkanhaiyaranjan@gmail.com');
                $compose->setTo($userdata->email);
                $compose->setCc(Yii::$app->params['adminEmail']);
                $compose->setSubject("Your Invoice Details");
                $content = $this->renderPartial('email_invoice_formate', ['event'=>$event,'eventInvoice'=>$model,"user"=>$userdata,'sold_ticket'=>$sold_ticket]);
                $fileName = $fileName."-".$invoice_number . ".pdf";
                $pdf = new \kartik\mpdf\Pdf([
                    'mode' => \kartik\mpdf\Pdf::MODE_CORE,
                    'format' => \kartik\mpdf\Pdf::FORMAT_A4,
                    'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
                    'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
                    'cssInline' => '.invoice-box { max-width: 800px; margin: auto; padding: 30px; border: 1px solid #eee; box-shadow: 0 0 10px rgba(0, 0, 0, 0.15); font-size: 16px; line-height: 24px; color: #555; } .invoice-box table { width: 100%; line-height: inherit; text-align: left; } .invoice-box table td { padding: 15px; vertical-align: top; } .invoice-box table tr td:nth-child(2) { text-align: right; } .invoice-box table tr.top table td { padding-bottom: 20px; } .invoice-box table tr.information table td { padding-bottom: 40px; } .invoice-box table tr.heading td { background: #eee; border-bottom: 1px solid #ddd; font-weight: bold; } .company-logo { text-align: center; } .invoice-box table tr.item td { border-bottom: 3px solid #eee; }',
                    'filename' => $fileName,
                    'content' => $content,
                    'options' => ['title' => 'Invoice Details'],
                    'methods' => [
                        'SetTitle' => 'Event-Invoice - TipHub.co',
                        'SetAuthor' => 'Mitiz Technologies',
                        'SetCreator' => 'Mitiz Technologies',
                    ]
                ]);
                $compose->attachContent($pdf->render(), ['fileName' => $fileName, 'contentType' => 'application/pdf']);
                if($compose->send()){
                    Yii::$app->session->setFlash('success', $message);
                }
                $this->redirect(Yii::$app->request->referrer);
            }
        }else{
            Yii::$app->session->setFlash('success', "User Email Id Does not Exists!!!");
            $this->redirect(Yii::$app->request->referrer);
        }
       
    }
    public function actionAddPayment($id) {
        
        $post =  Yii::$app->request->post();
        $model = new InvoicePayment();
        $model->event_id = $id;
        $data = [];
        $title = 'Add payment';
        if (!empty($post) && $model->load($post)) {
            if ($model->save()) {
                $total_amountPaid =  InvoicePayment::find()->where(['event_id'=>$id])->sum('amount');
                if ($total_amountPaid >= $model->eventinvoice->amount) {
                    $model->eventinvoice->status = 1;
                    $model->eventinvoice->save();
                }
                Yii::$app->getSession()->setFlash('success', "payment Added Successfull.");
                $this->redirect('index');
            }else{
                Yii::$app->getSession()->setFlash('error', $model->getErrors());
            }
        }
        $categories =ArrayHelper::map(PaymentServiceNames::find()->where(['user_id'=>0])->orderBy(['name'=>SORT_ASC])->all(), 'id', 'name');
        $model->invoice_id = !empty($model->eventinvoice)?sprintf('%05d',$model->eventinvoice->invoice_id):"";
        $data['model'] = $model;
        $data['categories']=$categories;
        return \app\widgets\Popup::widget([
            'popup_id'=> 'add-payment',
            'title'=> $title,
            'padding'=>'20px',
            'message'=> $this->renderAjax('add-payment', $data),
        ]);
    }
    Public function actionAllTransactions() 
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['transactions.name' => SORT_ASC,],
                    'desc' => ['transactions.name' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Name',
                ],
                'event_id' => [
                    'asc' => ['transactions.event_id' => SORT_ASC,],
                    'desc' => ['transactions.event_id' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Event Name',
                ],
                'amount' => [
                    'asc' => ['transactions.amount' => SORT_ASC,],
                    'desc' => ['transactions.amount' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Amount',
                ],
                'added_on' => [
                    'asc' => ['transactions.added_on' => SORT_ASC,],
                    'desc' => ['transactions.added_on' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Added On',
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC]
        ]);
        $data['search'] = $search = new \app\models\SearchForm;
        
        $model = new \app\models\Transactions();
        $query = $model->find()->joinWith(['event']);
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (!empty($search->name)) {
                $query->andWhere(['or', ['LIKE', 'name', $search->name]]);
            }
            if (!empty($search->event_id)) {
                $query->andWhere(['event_id' => $search->event_id]);
            }
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)->limit($pages->limit)->orderBy($data['sort']->orders)->all();
        $data['pages'] = $pages;
        $categories = ArrayHelper::map(Events::find()->all(), 'id', 'title');
        $data['categories'] = array("0" => "Select Event") + $categories;
        return $this->render('transactions', $data);
    }
    public function actionGetSubCateory($id)
    {
        \Yii::$app->response->format = 'json';
        $response['status']="NOK";
        if ($id) {
            $data = ArrayHelper::map(Categories::find()->where(['parent_id'=>$id])->all(), 'id', 'name');
            if ($data) {
                $response['status']="OK";
                $response['data']=$data;
            }
        }
        return $response;
    }
}
