<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\swiftmailer\Mailer;
use app\models\Events;
use app\models\Transactions;
use app\models\EventTicketBookingDetails;
use app\models\EventTicketBookings;


/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public function init(){	
        parent::init();	
		if(Yii::$app->user->isGuest){			
            Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
            return $this->redirect(Yii::$app->user->loginUrl)->send();exit;
        }		
	}

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        if (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 2) {
            return $this->redirect(Yii::$app->request->baseUrl.'/admin/blog');
        }
        $data = [];
        $today = date('Y-m-d H:i:s');
        $data['transactions'] =$transactions = new \app\models\Transactions();

        $model = new \app\models\Transactions;
        $query = $model->find()->select('DATE(added_on) as added_date,sum(amount) as total');
        $end_date = date('Y-m-d');
        $start_date=date('Y-m-01', strtotime($end_date));
        $fund_date = $query->Where(['between', 'DATE(added_on)', $start_date, $end_date])
        ->groupBy('added_date')
        ->orderBy('added_date')
        ->asArray()
        ->all();
        //print_r($fund_date);die;

        
        $total_camp = Events::find()->count();
        $total_money = Transactions::find()->sum('amount');
        //$transactionIds = Transactions::find()->select('event_id')->where(['not',['amount'=>'0.00']])->groupBy('event_id')->column();
        $funded_camp = Transactions::find()->select('event_id')->where(['not',['amount'=>'0.00']])->groupBy('event_id')->count();
        $active_camp = Events::find()->where(['start_date' => NULL,'end_date'=> NUll])->orWhere(['<=','end_date',$today])->count();

        $ticketed_events =  Events::find()->select("id")->where(["category"=>[59,71]])->column();
        $ticketed_event_booking_id = EventTicketBookings::find()->select("id")->where(['event_id'=>$ticketed_events,"status"=>1])->column();
        $countTicketSold =  EventTicketBookingDetails::find()->sum("total_tickets");
        $totalTicketPrice =  EventTicketBookingDetails::find()->where(["booking_id"=>$ticketed_event_booking_id])->sum("total_price");
        $data['ticketed_events']=!empty(count($ticketed_events))?count($ticketed_events):0;
        $data['countTicketSold']=!empty($countTicketSold)?$countTicketSold:0;
        $data['totalTicketPrice']=!empty($totalTicketPrice)?$totalTicketPrice:0;
        $data['total_campaign'] = $total_camp;
        $data['funded_campaign'] = $funded_camp; 
        $data['total_money'] = number_format($total_money);
        $data['active_campaign'] = $active_camp;
        $data['fund'] = $fund_date;
        
        return $this->render('index',$data);
    }

    
    public function actionAjax(){
        $data = [];
        $today = date('Y-m-d H:i:s');
        if (isset($_POST['date_range']) && !empty($_POST['date_range'])){
            $daterange = $_POST['date_range'];
           // echo $daterange;die;
            list($start_date, $end_date) = explode(' - ', $daterange);
            //echo $start_date."and".$end_date;die;
        
            $model = new \app\models\Transactions;
        $query = $model->find()->select('DATE(added_on) as added_date,sum(amount) as total');
        $fund_date = $query->Where(['between', 'DATE(added_on)', $start_date, $end_date])
        ->groupBy('added_date')
        ->orderBy('added_date')
        ->asArray()
        ->all();

        $total_camp = Events::find()->count();
        $total_money = Transactions::find()->sum('amount');
        //$transactionIds = Transactions::find()->select('event_id')->where(['not',['amount'=>'0.00']])->groupBy('event_id')->column();
        $funded_camp = Transactions::find()->select('event_id')->where(['not',['amount'=>'0.00']])->groupBy('event_id')->count();
        $active_camp = Events::find()->where(['start_date' => NULL,'end_date'=> NUll])->orWhere(['<=','end_date',$today])->count();

        $data['total_campaign'] = $total_camp;
        $data['funded_campaign'] = $funded_camp; 
        $data['total_money'] = number_format($total_money);
        $data['active_campaign'] = $active_camp;
        $data['fund'] = $fund_date;
       // echo "<pre>";print_r($data);die;    
        $html = $this->renderAjax('chart',$data);
        echo $html;
       
        }
       
    }

    public function actionAjaxUpdate()
    {
        
        $today = date('Y-m-d H:i:s');
    //print_r($model);die;
    if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //if ($model->load(Yii::$app->request->post()) ) {
            if (isset($_POST['date_range']) && !empty($_POST['date_range'])){
                list($start_date, $end_date) = explode(' - ', $_POST['date_range']);
                $model = new \app\models\Transactions;
                $query = $model->find()->select('DATE(added_on) as added_date,sum(amount) as total');
                $fund_date = $query->Where(['between', 'DATE(added_on)', $start_date, $end_date])
                ->groupBy('added_date')
                ->orderBy('added_date')
                ->asArray()
                ->all();
                $totalCollectedAmount = 0;
                if($fund_date){
                    foreach($fund_date as $fd){
                        $totalCollectedAmount = $totalCollectedAmount + $fd['total'];
                    }
                }
            return [
                    'success' => true,
                    'model' => $fund_date ,
                    'totalCollectedAmount'=>$totalCollectedAmount    
                   ];
        } else {
            return [
                'data' => [
                    'success' => false,
                    'model' => null,
                    'message' => 'An error occured.',
                ],
                'code' => 1, // Some semantic codes that you know them for yourself
            ];
        }
    }
    }

}
