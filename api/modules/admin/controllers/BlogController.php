<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;
use app\models\User;
use app\models\Categories;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Blog;

/**
 * Event controller for the `Event` module
 */
class BlogController extends Controller
{

    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            if (! Yii::$app->request->isAjax) {
                Yii::$app->user->loginUrl = [
                    '/site/login',
                    'return' => \Yii::$app->request->url
                ];
            } else {
                Yii::$app->user->loginUrl = [
                    '/site/login'
                ];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit();
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'actions' => [
                        'index',
                        'add',
                        'delete'
                    ],
                    'allow' => true,
                    'matchCallback' => function () {
                        if (! empty(Yii::$app->user->identity) && (Yii::$app->user->identity->role_id == 1 || Yii::$app->user->identity->role_id == 2)) {
                            return true;
                        }
                    }
                ]
            ]
        ];
        return $behaviors;
    }

    /**
     * This function is used to list all events
     *
     * @throws Exception If something interesting cannot happen
     * @author Sibananda
     *         Label~Module~Action:List Events~Event~index
     */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
                'added_on' => [
                    'asc' => ['added_on' => SORT_ASC],
                    'desc' => ['added_on' => SORT_DESC],
                    'default' => SORT_DESC,
                ],
            ],
            'defaultOrder' => ['added_on' => SORT_DESC],
        ]);
        $data['search'] = $search = new \app\models\SearchForm();
        $model = new \app\models\Blog();
        $query = $model->find();
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (! empty($search->title)) {
                $query->andWhere([
                    'or',
                    [
                        'LIKE',
                        'title',
                        $search->title
                    ]
                ]);
            }
            if (! empty($search->tag)) {
                $query->andWhere([
                    'or',
                    [
                        'LIKE',
                        'tag',
                        $search->tag
                    ]
                ]);
            }
        }
        $countQuery = clone $query;
        $pages = new \yii\data\Pagination([
            'totalCount' => $countQuery->count()
        ]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy($data['sort']->orders)
            ->all();
        $data['pages'] = $pages;
        return $this->render('index', $data);
    }

/*     public function actionAdd($id = NULL)
    {
        $data = $response = [];
        $title = 'Add New Blog';
        try {
            $data['model'] = $model = new \app\models\Blog();
            if ($model->load(Yii::$app->request->post())) {
                try {
                    $id = $_POST['Blog']['id'];
                    if (! empty($id)) {
                        $find = $model->findOne($id);
                        if ($find) {
                            $old_image = $find->image;
                            if ($find->load(Yii::$app->request->post())) {
                                $find->url = preg_replace('/\s+/', '-', strtolower(trim($find->title)));
                                if (! empty($_FILES['Blog']['name']['image'])) {
                                    if (! empty($find->image) && is_file(Yii::getAlias('@webroot') . '/web/blogs/' . $find->id . "_" . $find->image)) {
                                        unlink(Yii::getAlias('@webroot') . '/web/blogs/' . $find->id . "_" . $find->image);
                                    }
                                    $find->image = $_FILES['Blog']['name']['image'];
                                } else if (! empty($old_image)) {
                                    $find->image = $old_image;
                                }
                                if ($find->validate() && $find->save()) {
                                    $model->FileSave($_FILES);
                                    Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Blog updated successfully.');
                                    $this->redirect(Yii::$app->request->referrer);
                                } else {
                                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                                    $this->redirect(Yii::$app->request->referrer);
                                }
                            }
                        }
                    } else {
                        $model->url = preg_replace('/\s+/', '-', strtolower(trim($model->title)));
                        if (! empty($_FILES['Blog']['name']['image'])) {
                            $model->image = $_FILES['Blog']['name']['image'];
                        }
                        if ($model->validate() && $model->save()) {
                            $model->FileSave($_FILES);
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Blog has been added successfully.');
                            $this->redirect(Yii::$app->request->referrer);
                        } else {
                            Yii::$app->getSession()->setFlash('error', $model->getErrors());
                            $this->redirect(Yii::$app->request->referrer);
                        }
                    }
                    return json_encode($response);
                } catch (\Exception $e) {
                    return \app\widgets\Popup::widget([
                        'popup_id' => 'add-edit-window',
                        'title' => 'Error',
                        'message' => '<p align="center" style="padding:20px;color:red">' . $e->getMessage() . '</p>',
                        'padding' => '20px'
                    ]);
                }
            } else {

                if (! empty($id)) {
                    $find = $model->findOne($id);
                    if (! empty($find)) {
                        $title = 'Update Blog';
                        $data['model'] = $find;
                    }
                }
                return \app\widgets\Popup::widget([
                    'popup_id' => 'add-edit-window',
                    'title' => $title,
                    'size' => 'modal-sm',
                    'padding' => '20px',
                    'message' => $this->renderAjax('add-edit-blog', $data)
                ]);
            }
        } catch (\Exception $e) {
            return \app\widgets\Popup::widget([
                'popup_id' => 'add-edit-window',
                'title' => 'Error',
                'message' => '<p align="center" style="padding:20px;color:red">' . $e->getMessage() . '</p>',
                'padding' => '10px'
            ]);
        }
    } */

    function actionAdd($id = NULL)
    {
        $data = [];
        try {
            $data['model'] = $model = new \app\models\Blog;
            if ($model->load(Yii::$app->request->post())) {
                try {
                    $id = $_POST['Blog']['id'];
                    if (isset($id) && !empty($id)) {
                        if ($find = $model->findOne(['id' => $id])) {
                            $old_image = $find->image;
                            $find->load(Yii::$app->request->post());
                            $find->url = Yii::$app->common->removeSpecialChar($find->title);
                            if (!empty($_FILES['Blog']['name']['image'])) {
                                if (!empty($find->image) && is_file(Yii::getAlias('@webroot') . '/web/blogs/' . $find->id . "_" . $find->image)) {
                                    unlink(Yii::getAlias('@webroot') . '/web/blogs/' . $find->id . "_" . $find->image);
                                }
                                $find->image = $_FILES['Blog']['name']['image'];
                            } else if (!empty($old_image)) {
                                $find->image = $old_image;
                            }
                            if ($find->validate() && $find->save()) {
                                $model->FileSave($_FILES);
                                Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Blog updated successfully.');
                            } else {
                                Yii::$app->getSession()->setFlash('error', $find->getErrors());
                            }
                        }
                    } else {
                        $model->url = Yii::$app->common->removeSpecialChar($model->title);
                        if (!empty($_FILES['Blog']['name']['image'])) {
                            $model->image = $_FILES['Blog']['name']['image'];
                        }
                        if ($model->validate() && $model->save()) {
                            $model->FileSave($_FILES);
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Blog has been added successfully.');
                        } else {
                            $erros = "";
                            foreach ($model->getErrors() as $key => $error) {
                                $erros .= $error[0] . "<br>";
                            }
                            Yii::$app->getSession()->setFlash('error', $erros);
                        }
                    }
                } catch (\Exception $e) {
                    Yii::$app->getSession()->setFlash('error', $e->getMessage());
                }
                return $this->redirect("index");
            } else {
                if ($id != NULL) {
                    $data['model'] = $model->findOne(['id' => $id]);
                }
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', $e->getMessage());
        }
        return $this->render('add-edit-blog', $data);
    }

    public function actionDelete($id)
    {
        if (! empty($id)) {

            $find = Blog::findOne($id);
            if (! empty($find)) {
                if ($find->delete()) {
                    Yii::$app->getSession()->setFlash('success', 'Blog has been deleted successfully.');
                } else {
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
            } else {
                // echo "failed case";die;
                Yii::$app->getSession()->setFlash('error', "Blog not found.");
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }
}
