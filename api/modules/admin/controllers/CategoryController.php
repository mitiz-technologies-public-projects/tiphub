<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\base\ErrorException;
use yii\filters\AccessControl;
use yii\web\Session;
use yii\swiftmailer\Mailer;
use app\models\User;
use app\models\Categories;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * Event controller for the `Event` module
 */
class CategoryController extends Controller
{
    public function init()
    {
        parent::init();
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->request->isAjax) {
                Yii::$app->user->loginUrl = ['/site/login', 'return' => \Yii::$app->request->url];
            } else {
                Yii::$app->user->loginUrl = ['/site/login'];
            }
            return $this->redirect(Yii::$app->user->loginUrl)->send();
            exit;
        }
    }

    public function behaviors()
    {
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'only' => [],
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        if (! empty(Yii::$app->user->identity) && Yii::$app->user->identity->role_id == 1) {
                            return true;
                        }
                    }
                ],
            ],
        ];
        return $behaviors;
    }

    /**
     * This function is used to list all events
     * @throws Exception If something interesting cannot happen
     * @author Sibananda
     * Label~Module~Action:List Events~Event~index 
     */
    public function actionIndex()
    {
        $data = [];
        $data['search_form'] = true;
        $data['sort'] = new \yii\data\Sort([
            'attributes' => [
                'name' => [
                    'asc' => ['categories.name' => SORT_ASC,],
                    'desc' => ['categories.name' => SORT_DESC,],
                    'default' => SORT_DESC,
                    'label' => 'Name',
                ],
            ],
            'defaultOrder' => ['name' => SORT_ASC]
        ]);

        $data['search'] = $search = new \app\models\SearchForm;

        $model = new \app\models\Categories;
        $query = $model->find();
        if ($search->load(Yii::$app->request->get())) {
            $data['search_form'] = true;
            if (!empty($search->name)) {
                $query->andWhere(['or', ['LIKE', 'name', $search->name]]);
            }
        }

        $get = Yii::$app->request->get();

        if(isset($get['parent_id'])){
            $query->where(["parent_id" => $get['parent_id']]);
        }

        $countQuery = clone $query;
        $pages = new \yii\data\Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data['records'] = $query->offset($pages->offset)->limit($pages->limit)->orderBy($data['sort']->orders)->all();
        $data['pages'] = $pages;
        // echo $query->createCommand()->getRawSql();die;
        return $this->render('index', $data);
    }

    public function actionAdd($id = NULL){		
		$data = $response = [];		
		$title = 'Add New Category';
		try{
			$data['model'] = $model = new \app\models\Categories;						
			if($model->load(Yii::$app->request->post())){
				try{
                    $id = $_POST['Categories']['id'];
					if(isset($id) && !empty($id)){
						if($find = $model->findOne(['id'=>$id])){													
							$find->load(Yii::$app->request->post());
							if($find->validate() && $find->save()){	
								Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Category updated successfully.');
                                $response['success'] = true;
                                
							}
							else{
								$response['errors'] = $find->getErrors();
							}													
						}												
					}
					else{		                        
                        if($model->validate() && $model->save()){								
                            Yii::$app->getSession()->setFlash('success', '<i class="fa fa-check"></i> Category has been added successfully.');
                            $response['success'] = true;
                        }
                        else{
                            $response['errors'] = $model->getErrors();
                        }
					}
					return json_encode($response);
				}
				catch(\Exception $e){					
					return \app\widgets\Popup::widget([				
						'popup_id'=> 'add-edit-window',
						'title'=> 'Error',								
						'message'=> '<p align="center" style="padding:20px;color:red">'.$e->getMessage().'</p>',
						'padding'=>'20px',													
					]);					
				}				
			}
			else{
                $categories["0"] = "Parent Category";

                $categories = ArrayHelper::map(Categories::find()->all(), 'id', 'name');
                $categories = array("0" => "Parent Category") + $categories; 
                $data['categories'] = $categories;

				if($id != NULL){
					$title = 'Update Category';
					$find = $model->find()->where(['id'=>$id])->count();
					if($find){
						$data['model'] = $model->findOne(['id'=>$id]);
					}					
                }
				return \app\widgets\Popup::widget([				
					'popup_id'=> 'add-edit-window',
					'title'=> $title,
					'size'=>'modal-sm',					
					'padding'=>'20px',					
					'message'=> $this->renderAjax('add-edit-category', $data),								
				]);
			}				
		}
		catch(\Exception $e){
			return \app\widgets\Popup::widget([				
				'popup_id'=> 'add-edit-window',
				'title'=> 'Error',								
				'message'=> '<p align="center" style="padding:20px;color:red">'.$e->getMessage().'</p>',
				'padding'=>'10px',													
			]);
		}		
	}

    public function actionDelete($id)
    {
        if (isset($id) && !empty($id)) {

            $find = Categories::findOne(['id' => $id]);
            if (!empty($find)) {
                if ($find->delete()) {
                    Yii::$app->getSession()->setFlash('success', 'Category has been deleted successfully.');
                } else {
                    Yii::$app->getSession()->setFlash('error', $find->getErrors());
                }
            } 
            else {
                //echo "failed case";die;
                Yii::$app->getSession()->setFlash('error', "Category not found.");
            }
        }
        $this->redirect(Yii::$app->request->referrer);
    }
}
