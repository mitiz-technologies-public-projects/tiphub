<?php

namespace app\modules\admin;

use Yii;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
    public $layout = '@app/views/layouts/admin';
    
    /**
     * @inheritdoc
     */
    public function init(){
        parent::init();
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->role_id != 1 && Yii::$app->user->identity->role_id != 2){
                die('You are not authorized to access this page.');
            }          
        }
    }
}
