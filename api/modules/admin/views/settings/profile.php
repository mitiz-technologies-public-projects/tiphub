<?php
use yii\helpers\Html;
use app\assets\AdminAsset;
//use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
$this->title = 'Profile';
AdminAsset::register($this);
?>
<img src="<?=Yii::$app->request->baseUrl?>/assets/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
<section class="content-header">
    <h1><?=$this->title?></h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="fa fa-user"></i><?=$this->title?></a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <?=\app\widgets\Alert::widget()?>
    <!---row---->
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading"><strong style="font-size:18px;">User Details</strong></div>
                <div class="panel-body">
                    <?php
                        $form = ActiveForm::begin([
                            'id' => 'upload-user-image-form',
                            'options' => ['class' => 'form','enctype' => 'multipart/form-data'],
                            'validateOnBlur'=>true,
                            'enableClientValidation'=>true,
                        ]);
                        echo $form->field($user, 'username')->textInput(['value'=>Yii::$app->user->identity->username]);
                        echo $form->field($user, 'email')->textInput(['value'=>Yii::$app->user->identity->email]);
                    ?>
                    <?php
                        echo Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'login-button']);
                        ActiveForm::end();
                    ?>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading"><strong style="font-size:18px;">Change Password</strong></div>
                <div class="panel-body">
                    <?php
                        $form = ActiveForm::begin([
                            'id' => 'change-password-form',
                            //'action'=>Yii::$app->request->baseUrl.'/account/index?tab=3',
                            'options' => ['class' => 'login'],
                            'validateOnBlur'=>true,
                            'enableClientValidation'=>true,
                            'fieldConfig' => [
                                'template' => "<div class=\"form-row form-row-wide\">{label}{input}{error}</div>",
                                'labelOptions' => [],
                            ],
                        ]);
                        echo $form->field($password, 'current_password')->passwordInput();
                        echo $form->field($password, 'new_password')->passwordInput();
                        echo $form->field($password, 'confirm_password')->passwordInput();
                        echo Html::submitButton('Save', ['class' => 'btn btn-success', 'name' => 'login-button']);
                        ActiveForm::end();
                    ?>
                </div>
            </div>
        </div>

    </div>
    <!---/.row--->
</section>
<!-- /.content -->
<?php
$this->registerCss('
    .filepond--root.user_image.filepond--hopper {margin-bottom: 0px;}
    .padding_left{padding-left:0px; clear:left;}
    .padding_right{padding-right:0px;}	
');

/*
$this->registerJsFile(
    Yii::$app->request->baseUrl . '/web/site/file-pond/filepond.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    Yii::$app->request->baseUrl . '/web/site/file-pond/filepond.jquery.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
    Yii::$app->request->baseUrl . '/web/site/file-pond/filepond-plugin-image-preview.min.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerCssFile(Yii::$app->request->baseUrl . '/web/site/file-pond/filepond.min.css', [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$this->registerCssFile(Yii::$app->request->baseUrl . '/web/site/file-pond/filepond-plugin-image-preview.css', [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);*/

$this->registerJs('
$("#user-username").keypress(function( e ) {
    if(e.which === 32)
        return false;
});

$(function(){
    // var prv_user_image = $("#user-image").val();
    // var prv_user_image_src = $(".user_image_img").attr("src");
    // $(".user_image").filepond({
    //     allowImagePreview:true,
    //     imagePreviewMinHeight:50,
    //     server: {
    //         process: {
    //             url:"' . Yii::$app->request->baseUrl . '/admin/page/user-image-upload",
    //             onload: (response) => {

    //                 let response_josn = JSON.parse(response);
    //                 if (response_josn.success) {
    //                     $("#user-image").val(response_josn.file_name);
    //                     $(".user_image_img").attr("src", FULL_PATH + "/web/site/images/user_image/temp/" + response_josn.file_name );
    //                     $(".user_image_img").show();
    //                 }
    //             },
    //             onerror: response => {
    //                 alert();
    //             }
    //         }
    //     },
    //     labelIdle:"Drag & Drop your <strong>Profile Image</strong> here or Browse"
    // });
    // $(".user_image").on("FilePond:removefile", function(e) {
    //     if(prv_user_image_src != ""){
    //         $(".user_image_img").attr("src", prv_user_image_src);
    //         $("#user-image").val(prv_user_image);
    //     }else{
    //         $(".user_image_img").hide();
    //         $("#user-image").val(prv_user_image);
    //     }
    // });
});

',\yii\web\VIEW::POS_END);
