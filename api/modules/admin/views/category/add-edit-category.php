<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Url;

	$form = ActiveForm::begin([					
		'id' => 'add-update-form',
		'options' => ['class' => 'form',],
		'validateOnBlur'=>true,		      
		'enableClientValidation'=>true,		
	]);

	echo $form->field($model, 'id')->hiddenInput(['value'=>$model->id])->label(false)->error(false);
	
?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?=$form->field($model, 'parent_id')->dropdownList($categories, [])->label("Select Category");?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?php
			echo $form->field($model, 'name');	
		?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'description')->textArea();	
		?>
	</div>		
</div>
<div class="frm-btn">
	<div class="popup-message pull-left"></div>
	<div class="text-right ">						
		<?php			
			echo Html::Button('Cancel', ['class' => 'btn btn-danger', 'data-dismiss'=>'modal']);		
			echo "&nbsp;&nbsp;".Html::submitButton(($model->id)?'Update':'Save', ['class' => 'btn btn-success add-update-submit ladda-button', 'name' => 'add-update-submit','data-style'=>'expand-left']);
		?>
	</div>
</div>
<?php ActiveForm::end();?>

<?php $this->registerJs('	
	$("#add-update-form").on("submit",function(e) {
		e.preventDefault();
	});
	$("#add-update-form" ).on("beforeSubmit", function(event, jqXHR, settings){		        
		saveCategory(this);
	});

    function saveCategory(t){
		var loading_btn = Ladda.create(document.querySelector(".add-update-submit"));		
		$.ajax({
			type: "POST",
			url: FULL_PATH + "/admin/category/add",
			data: $(t).serialize(),
			dataType:"json",
			beforeSend: function(){
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				if(response.success){
					window.location.href = document.URL;
				}
				else if(response.errors){
					for (control in response.errors) {
						$(".field-categories-" + control).removeClass("has-success").addClass("has-error");
						$(".field-categories-" + control + " .help-block-error").text(response.errors[control]);
					}
				}
				else if(response.error){
					if(response.error.name){
                        $("#clients-name").parent().addClass("has-error").find("p").text(response.error.name);
                    }
				}	
			}
		});	
	}
',
\yii\web\VIEW::POS_END);
