
<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;
	use dosamigos\ckeditor\CKEditor;
	use wbraganca\tagsinput\TagsinputWidget;
	/* @var $this yii\web\View */
	$this->title = isset($model->id)?'Update Blog':'Add Blog';
?>
<style>
.bootstrap-tagsinput {
    width: 100% !important;
}  
</style>
<!-- content goes here -->

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?=Yii::$app->request->baseUrl?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-6 col-sm-6">
                            <div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?=$this->title?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-6 col-sm-6 text-right" style="padding:8px 10px;">
                            <a class="btn btn-primary" href="<?=Yii::$app->request->baseUrl?>/admin/email/templates"><i class="fa fa-undo"></i> Back</a>
                        </div>
                    </div>
                </div>
            </nav> 
			 
            <?=\app\widgets\Alert::widget()?>           
            <div class="box box-info coustum-box">
                <div class="box-body">
					<?php 						
						$form = ActiveForm::begin([
							'id' => 'add-email-content-form',
							'options' => ['class' => 'form', 'enctype' => 'multipart/form-data'],
							'validateOnBlur'=>true,		      
							'enableClientValidation'=>true,
							'fieldConfig' => [
								'template' => "<div class=\"form-group\">{label}{input}{error}</div>",            
							],
						]);
						
						echo $form->field($model, 'id')->hiddenInput(['value'=>$model->id])->label(false)->error(false);
						echo $form->field($model, 'title');
						echo $form->field($model, 'meta_title');
						echo $form->field($model, 'meta_keyword');
                        echo $form->field($model, 'meta_discription');
						echo $form->field($model, 'other_meta_tag');
						echo $form->field($model, 'tag')->widget(TagsinputWidget::classname(), [
							'clientOptions' => [
								'trimValue' => true,
								'allowDuplicates' => false,
							]
						])->label('Tags');
						echo $form->field($model, 'image')->fileInput();
						echo $form->field($model, 'meta_discription');
						echo $form->field($model, 'meta_discription');
						echo $form->field($model, 'meta_discription');
						echo $form->field($model, 'description')->widget(CKEditor::className(), [
							'options' => ['rows' => 10],
							'preset' => 'custom',
							'clientOptions' => [								
								'height'=>"300",
								'filebrowserUploadUrl' => Yii::$app->request->baseUrl.'/admin/page/editor-image-upload',
								'toolbarGroups' => [	
									['name' => 'basicstyles', 'groups' => ['basicstyles']],	
									['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align','bidi']],
									['name' => 'styles',],
									['name' => 'links', 'groups' => ['links', 'insert']],	
									['name' => 'document', 'groups' => ['mode']],
									
								]
							]
						]);
						echo Html::submitButton(($model->id)?'Update':'Save', ['class' => 'btn btn-success pull-right', 'name' => 'login-button']);						
						ActiveForm::end();
					?>
                </div>
            </div>
            
        </div>
    </div>      
</section>

