<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TicketPositions;
use app\models\Cities;
use \app\models\Roles;
//use yii\helpers\Url;


/* @var $this yii\web\View */

$this->title = 'Blogs';
Pjax::begin();

?>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?= Yii::$app->request->baseUrl ?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-12 col-sm-12">
                            <div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?= $this->title ?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-12 col-sm-12 text-right" style="padding:8px 10px;">
                            <a class="btn btn-primary" href="<?=Yii::$app->request->baseUrl?>/admin/blog/add">Add New Blog</a>
                            <button name="search-button" class="btn btn-warning search-button" type="button" onclick="expandSearchPanale(this)"><i class="fa 
                            <?php if ($search_form) echo 'fa-minus'; else echo 'fa-plus' ?>"></i> Search</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="panel panel-default search-panel" style="display:<?php if ($search_form) echo 'block';
                                                                            else echo 'none' ?>">
                <div class="panel-body">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'action' => Yii::$app->request->baseUrl . "/admin/blog",
                        'method' => 'GET',
                        'options' => ['class' => ''],
                        'validateOnBlur' => true,
                        'enableClientValidation' => true,
                        'fieldConfig' => [
                            'template' => "<div class=\"col-sm-4\">{label}{input}</div>",
                        ],
                    ]);
                    echo $form->field($search, 'title')->label("Title");
                    echo $form->field($search, 'tag')->label("Tags");
                    //echo $form->field($search, 'role')->dropDownList(ArrayHelper::map(Roles::find()->asArray()->all(), 'id', 'name'),['prompt'=>'-- Select User Type --']);

                    echo '<div class="col-sm-12 text-right">';
                    echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-right:5px;']);
                    echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record', 'onclick' => 'clearSearch(this)']);
                    echo '</div>';
                    ActiveForm::end();
                    ?>
                </div>
            </div>
            <?= \app\widgets\Alert::widget() ?>
            <div class="box box-info coustum-box">
                <div class="box-body table-responsive">
                    <table class="table table-hover users" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $sort->link('title'); ?></th>
                                <th>Url</th>
                                <th>Image</th>
                                <th>Tag</th>
                                <th width="25%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($pages->page) && !empty($pages->page)) {
                                $i = (($pages->page + 1) - 1) * $pages->defaultPageSize;
                            } else {
                                $i = 0;
                            }
                            if ($records) {
                                foreach ($records as $record) { ?>
                                    <tr>
                                        <td><?= ++$i ?>.</td>
                                        <td><?= ($record->title) ? $record->title : "N/A"; ?></td>
                                        <td><?= ($record->url) ? $record->url : "N/A"; ?></td>
                                        <td><?php if (!empty($record->image)) {?>
                                        	<img src="<?= Yii::$app->request->baseURL.'/web/blogs/'.$record->id."_".$record->image?>" class='responsive' style='width:120px; height:88px;'>
                                        <?php }?>
                                        </td>
                                        <td><?= $record->tag?></td>
                                        <td class="text-center"><a class="btn btn-primary btn-xs" href="<?=Yii::$app->request->baseUrl?>/admin/blog/add?id=<?=$record->id?>"><i class="fa fa-edit"></i> Edit</a></td>
                                        <td class="text-center"><a class="btn btn-danger btn-xs" href="<?=Yii::$app->request->baseUrl?>/admin/blog/delete?id=<?=$record->id?>" onclick="return confirm('Are you sure to delete this email template?')"><i class="fa fa-trash"></i> Delete</a></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="4" style="padding:2px;">
                                        <div class="pull-right" style="border-top:none;height:50px;">
                                            <?php
                                            if (isset($pages)) {
                                                echo \yii\widgets\LinkPager::widget(['pagination' => $pages,]);
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4" class="text-center" style="color:red">Result not found!</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

<?php Pjax::end();
