<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
$this->title = 'Plans';
Pjax::begin();
?>

<!-- content goes here -->

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?=Yii::$app->request->baseUrl?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-6 col-sm-6">
                            <div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?=$this->title?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-6 col-sm-6 text-right" style="padding:8px 10px;">
                            <a class="btn btn-primary" href="<?=Yii::$app->request->baseUrl?>/admin/plans/add-plan">Add New Plan</a>
                             <!-- <button name="search-button" class="btn btn-warning search-button" type="button"><i class="fa <?php if($search_form) echo 'fa-minus';else echo 'fa-plus'?>"></i> Search</button> 
                             <button name="search-button" class="btn btn-warning search-button" type="button" onclick="expandSearchPanale(this)"><i class="fa <?php if ($search_form) echo 'fa-minus';
                                                                                                                                                                else echo 'fa-plus' ?>"></i> Search</button>-->
                        </div>
                    </div>
                </div>
            </nav> 
			 <div class="panel panel-default search-panel" style="display:<?php if($search_form) echo 'block';else echo 'none'?>">	
                <div class="panel-body">
                    <?php 			
                        $form = ActiveForm::begin([
                            'id' => 'search-form',                            
                            'method' => 'GET',				
                            'options' => ['class' => ''],
                            'validateOnBlur'=>true,		      
                            'enableClientValidation'=>true,	
                            'fieldConfig' => [
                                'template' => "<div class=\"col-sm-4\">{label}{input}</div>",            
                            ],			
                        ]); 
                        //echo $form->field($search, 'plan');
                        echo '<div class="col-sm-12 text-right">';
                        echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button','style'=>'margin-right:5px;']);            
                        echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record','onclick'=>'clearSearch(this)']);
                        echo '</div>';
                        ActiveForm::end();
                    ?> 		                                     
                </div>
            </div> 
            <?= \app\widgets\Alert::widget() ?>        
            <div class="box box-info coustum-box">
                <div class="box-body">
                    <table class="table table-hover table-responsive" width="100%">
                        <thead>
                            <tr>
                                <th width="5%"></th>
                                <th><?=$sort->link('plan');?></th>
                                <th><?=$sort->link('monthly_price');?></th>
                                <th><?=$sort->link('yearly_price');?></th>
                                <th colspan="3" class="text-center" width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php if($records){$i=1;foreach($records as $record){?>
                            <tr>                                
                                <td><?=$i++?>.</td>
                                <td><?=$record->plan?></td>		
                                <td>$<?=$record->monthly_price?></td>	
                                <td>$<?=$record->yearly_price?></td>							
                                <td class="text-center"><a class="btn btn-primary btn-xs" href="<?=Yii::$app->request->baseUrl?>/admin/plans/add-plan?id=<?=$record->id?>"><i class="fa fa-edit"></i> Edit</a></td>
                                 <td class="text-center"><a class="btn btn-danger btn-xs" href="<?=Yii::$app->request->baseUrl?>/admin/plans/delete?id=<?=$record->id?>" onclick="return confirm('Are you sure to delete this plan?')"><i class="fa fa-trash"></i> Delete</a></td>
                            </tr>
							
                        <?php } ?>
                            <tr>
                                <td colspan="10" style="padding:0px;">
                                    <div class="pull-right" style="border-top:none;height:54px;">
                                        <?php
                                            if(isset($pages)){
                                                echo \yii\widgets\LinkPager::widget(['pagination' => $pages,]);
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <?php }else{?>
                            <tr>
                                <td colspan="7" class="text-center" style="color:red">Result not found!</td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>      
</section>

<!-- /.content -->
<?php
Pjax::end();  