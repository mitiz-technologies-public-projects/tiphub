<?php
use dosamigos\ckeditor\CKEditor;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use wbraganca\tagsinput\TagsinputWidget;

$form = ActiveForm::begin([
    'id' => 'add-update-form',
    'options' => [
        'class' => 'form',
        'enctype' => 'multipart/form-data'
    ],
    'validateOnBlur' => true,
    'enableClientValidation' => true
]);
?>
<style>
.modal-content {
    width: 600px;
    margin-left: -133px;
}
.bootstrap-tagsinput {
    width: 100% !important;
}  
</style>

<?=$form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false)->error(false);?>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'title');?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'meta_title');?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'meta_keyword');?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'meta_discription');?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?= $form->field($model, 'other_meta_tag')->textarea()->label('Other Meta Tags');?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<?= $form->field($model, 'image')->fileInput();?>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<?php if (!empty($model->image)) {?>
	    	<img src="<?= Yii::$app->request->baseURL.'/web/blogs/'.$model->id."_".$model->image?>" class='responsive' style='width:120px; height:88px;'>
		<?php }?>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
	<?= $form->field($model, 'tag')->widget(TagsinputWidget::classname(), [
    'clientOptions' => [
        'trimValue' => true,
        'allowDuplicates' => false,
    ]
])->label('Tags')?>
        </div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<?=$form->field($model, 'description')->label('Description')->widget(CKEditor::className(), ['options' => ['rows' => 5],'preset' => 'custom','clientOptions' => ['allowedContent' => true,'height' => "200",'toolbarGroups' => [['name' => 'basicstyles','groups' => ['basicstyles']],['name' => 'paragraph','groups' => ['list','indent','blocks','align']],['name' => 'styles']]]]);?>
	</div>
</div>
<div class="frm-btn">
	<div class="popup-message pull-left"></div>
	<div class="text-right ">						
		<?=Html::Button('Cancel', ['class' => 'btn btn-danger','data-dismiss' => 'modal']);echo "&nbsp;&nbsp;" . Html::submitButton(($model->id) ? 'Update' : 'Save', ['class' => 'btn btn-success add-update-submit ladda-button','name' => 'add-update-submit','data-style' => 'expand-left']);?>
	</div>
</div>
<?php ActiveForm::end();?>

