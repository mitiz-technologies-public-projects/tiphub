<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TicketPositions;
use app\models\Cities;
use \app\models\Roles;
//use yii\helpers\Url;


/* @var $this yii\web\View */

$this->title = 'Payments';
Pjax::begin();

?>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?= Yii::$app->request->baseUrl ?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-12 col-sm-12">
                            <div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?= $this->title ?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-12 col-sm-12 text-right" style="padding:8px 10px;">
                            <button name="add-blog-button" class="btn btn-primary add-blog-button ladda-button" type="button" data-style="expand-left" data-parent-id="0">Add New Blog</button>
                            <button name="search-button" class="btn btn-warning search-button" type="button" onclick="expandSearchPanale(this)"><i class="fa 
                            <?php if ($search_form) echo 'fa-minus'; else echo 'fa-plus' ?>"></i> Search</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="panel panel-default search-panel" style="display:<?php if ($search_form) echo 'block';else echo 'none' ?>">
                <div class="panel-body">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        //'action' => Yii::$app->request->baseUrl . "/admin/blog",
                        'method' => 'GET',
                        'options' => ['class' => ''],
                        'validateOnBlur' => true,
                        'enableClientValidation' => true,
                        'fieldConfig' => [
                            'template' => "<div class=\"col-sm-4\">{label}{input}</div>",
                        ],
                    ]);
                    echo $form->field($search,'date_range')->label('Select Date Range');
                    echo $form->field($search,'title')->label('Event Name');
                    echo $form->field($search,'name')->label('Payment App Name');
                    
                    //echo $form->field($search, 'role')->dropDownList(ArrayHelper::map(Roles::find()->asArray()->all(), 'id', 'name'),['prompt'=>'-- Select User Type --']);

                    echo '<div class="col-sm-12 text-right">';
                    echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-right:5px;']);
                    echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record', 'onclick' => 'clearSearch(this)']);
                    echo '</div>';
                    ActiveForm::end();
                    ?>
                </div>
            </div>
            <?= \app\widgets\Alert::widget() ?>
            <div class="box box-info">
                <div class="box-body table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?= $sort->link('email'); ?></th>
                                <th>Phone</th>
                                <th><?= $sort->link('event'); ?></th>
                                <th><?= $sort->link('amount'); ?></th>
                                <th>Payment App</th>
                                <th>Received At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (isset($pages->page) && !empty($pages->page)) {
                                $i = (($pages->page + 1) - 1) * $pages->defaultPageSize;
                            } else {
                                $i = 0;
                            }
                            if ($records) {
                                foreach ($records as $record) { ?>
                                    <tr>
                                        <td><?= ++$i ?>.</td>
                                        <td><?= ($record->email) ? $record->email : "--"; ?></td>
                                        <td><?= ($record->phone) ? $record->phone : "--"; ?></td>
                                        <td><?= ($record->event) ? $record->event->title : "--"; ?></td>
                                        <td><?= ($record->txn_amount) ? '$'.$record->txn_amount : "--"; ?></td>
                                        <td><?= ($record->service) ? $record->service->name : "--"; ?></td>
                                        <td ><?=date('M d, Y',strtotime($record->received_at));?></td>	
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="8" style="padding:2px;">
                                        <div class="pull-right" style="border-top:none;height:50px;">
                                            <?php
                                            if (isset($pages)) {
                                                echo \yii\widgets\LinkPager::widget(['pagination' => $pages,]);
                                            }
                                            ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4" class="text-center" style="color:red">Result not found!</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

<?php
$this->registerJs('
var start = moment();
var end = moment();
$("#searchform-date_range").daterangepicker({
ranges: {
    "Today": [moment(), moment()],
    "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
    "Last 7 Days": [moment().subtract(6, "days"), moment()],
    "Last 30 Days": [moment().subtract(29, "days"), moment()],
    "This Month": [moment().startOf("month"), moment().endOf("month")],
    "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
},
"locale": {
  "format": "YYYY-MM-DD",
  "separator": " - "
},
"startDate": start,
"endDate": end,
"opens": "left"
}, function(start, end, label) {
console.log("New date range selected: " + start.format("YYYY-MM-DD") + " to " + end.format("YYYY-MM-DD") + " (predefined range: " + label + ")");
});
');
Pjax::end();
