<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use dosamigos\chartjs\ChartJs;

/* @var $this yii\web\View */
$this->title = 'Admin Dashboard';

$this->registerCss('
.small-box h3 {
    font-size: 28px;
}
.small-box .icon {
    font-size: 80px;
}
.small-box {
    height: 100px;}
.small-box:hover .icon{font-size:82px}
');
$totalCollectedAmount = 0;
if(isset($fund)){
  $labelx = [];
  $labely = [];
  foreach($fund as $fund_data){
    $labelx[] = $fund_data['added_date'];
    $labely[] = $fund_data['total'];
    $totalCollectedAmount = $totalCollectedAmount + $fund_data['total'];
  }
}

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Admin Dashboard
        <small>Control panel</small>
      </h1>
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i>&nbsp; &nbsp; &nbsp; Admin Dashboard</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!--
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <?=\app\widgets\Alert::widget()?>
        </div>
    </div>
    -->
<div class="container-fluid">
<div class="row">
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3><?=$total_campaign?></h3>

                <p>No. of Campaigns</p>
              </div>
              <div class="icon">
                <i class="fa fa-handshake-o"></i>
              </div>
             
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary" style="background-color: seagreen;">
              <div class="inner">
                <h3><?=$funded_campaign?></h3>

                <p>No. of Funded Campaigns</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
             
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary" style="background-color: #00c0ef;">
              <div class="inner">
                <h3><?=$active_campaign?></h3>

                <p>No. of Active Campaigns</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
           
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary" style="background-color:#dd4b39;">
              <div class="inner">
                <h3><i class="fa fa-dollar"></i><?=$total_money?></h3>

                <p>Total Money Raised</p>
              </div>
              <div class="icon">
                <i class="fa fa-dollar"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
        </div>
        <div class="row">
           <div class="col-lg-3 col-6">
            <div class="small-box bg-primary" style="background-color: #436a5a;">
              <div class="inner">
                <h3><?=$ticketed_events?></h3>
                <p>Number of total ticketed events</p>
              </div>
              <div class="icon">
                <i class="fa fa-handshake-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-primary" style="background-color: #4d5e42;">
              <div class="inner">
                <h3><?=$countTicketSold?></h3>
                <p>Number of tickets sold for all events</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-primary" style="background-color: #5e8089;">
              <div class="inner">
                <h3>$<?=$totalTicketPrice?></h3>
                <p>Money raised on platform via ticketed events</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-primary" style="background-color:#7c4e48;">
              <div class="inner">
                <h3><i class="fa fa-dollar"></i><?=0?></h3>
                <p>Services fees raised from ticketed events</p>
              </div>
              <div class="icon">
                <i class="fa fa-dollar"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="row">

        <?php 			
                        echo '<div class="col-sm-5">';
                        $form = ActiveForm::begin([
                            'id' => 'search-form',
                            'options' => ['class' => ''],
                            'fieldConfig' => [
                                'template' => "<div class=\"col-sm-12\">{label}{input}</div>",            
                            ],			
                        ]); 
                        
                        echo $form->field($transactions,'added_on')->Input(['class'=>'date-range'])->label('Select Date Range');

                        ActiveForm::end();
                    ?> 
       

    </div>
    <div class="col-lg-3 col-7">
            <!-- small box -->
            <div class="small-box bg-primary" style="background-color:#665a66;">
              <div class="inner">
                <h3 id="total-collected-amount"><i class="fa fa-dollar"></i><?=$totalCollectedAmount?></h3>

                <p>Total Money Raised</p>
              </div>
              <div class="icon">
                <i class="fa fa-dollar"></i>
              </div>
              
            </div>
          </div>
    <div class="row" id="chartid">

        <?php
        

    echo ChartJs::widget([
    'type' => 'line',
    'options' => [
        'height' => 100,
        'width' => 400
    ],
    'data' => [
        //'labels' => ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"],
        'labels' => $labelx,
        'datasets' => [
            [
                'label' => "Amount Funded(in USD)",
                'backgroundColor' => "rgba(255,99,132,0.2)",
                'borderColor' => "rgba(255,99,132,1)",
                'pointBackgroundColor' => "rgba(255,99,132,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                'data' => $labely
                //'data' => [28, 48, 40, 19, 96, 27, 100,25,10,0,8,46,27]
            ]
        ]
    ]
]);
?>


        </div>

</div>

</section>
<!-- /.content -->
<?php


$this->registerJs('
  //var start = moment().subtract(29, "days");
  var start = moment().startOf("month");
  var end = moment();
  $("#transactions-added_on").daterangepicker({
  ranges: {
      "Today": [moment(), moment()],
      "Yesterday": [moment().subtract(1, "days"), moment().subtract(1, "days")],
      "Last 7 Days": [moment().subtract(6, "days"), moment()],
      "Last 30 Days": [moment().subtract(29, "days"), moment()],
      "This Month": [moment().startOf("month"), moment().endOf("month")],
      "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
  },
  "locale": {
    "format": "YYYY-MM-DD",
    "separator": " - "
  },
  "startDate": start,
  "endDate": end,
  "opens": "left"
}, function(start, end, label) {
console.log("New date range selected: " + start.format("YYYY-MM-DD") + " to " + end.format("YYYY-MM-DD") + " (predefined range: " + label + ")");
});
');


$this->registerJs('
$("#transactions-added_on").on("change",function(){
        var url = "'.Yii::$app->request->baseUrl.'/admin/default/ajax-update";
        var date_range = $("#transactions-added_on").val();
        //var data = $(this).serializeArray();
        //alert(data);
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: {date_range:date_range},
            //headers: {Accept : "application/json;charset=utf-8","Content-Type":"application/json;charset=utf-8"},
            success:function(response){
              $("#total-collected-amount").html("<i class=\"fa fa-dollar\"></i>"+response.totalCollectedAmount.toFixed(2));
              //var data1 = JSON.stringify(response);   
              //var jsonData = JSON.parse(response);
              var jsonData = response;
              
              var x = [];
              var y = [];
              for(var i=0; i<jsonData.model.length;i++){
                var newdata = jsonData.model[i];
                x.push([newdata.added_date]);
                y.push([newdata.total][0]);
              }
              
              chartJS_w1.data.datasets[0].data = y;
              chartJS_w1.data.labels = x;
              chartJS_w1.update();  
            },
            error:function(){
                toastr.warning("Something Went Wrong, Please Try again");
            }

        });
});

');

	
	$this->registerJs('

	', \yii\web\VIEW::POS_READY);


?>
