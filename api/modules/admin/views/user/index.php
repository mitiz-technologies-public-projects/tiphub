<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TicketPositions;
use app\models\Cities;
use \app\models\Roles;
//use yii\helpers\Url;


/* @var $this yii\web\View */
$this->title = 'Users';
Pjax::begin();

?>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?=Yii::$app->request->baseUrl?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-12 col-sm-12">
                            <div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?=$this->title?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-12 col-sm-12 text-right" style="padding:8px 10px;">
                            <!-- <button name="add-user-button" class="btn btn-primary add-user-button ladda-button" type="button" data-style="expand-left" data-parent-id="0">Add New User</button> -->
                            <button name="search-button" class="btn btn-warning search-button" type="button" onclick="expandSearchPanale(this)"><i class="fa <?php if($search_form) echo 'fa-minus';else echo 'fa-plus'?>"></i> Search</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="panel panel-default search-panel" style="display:<?php if($search_form) echo 'block';else echo 'none'?>">
                <div class="panel-body">
                    <?php 			
                        $form = ActiveForm::begin([
                            'id' => 'search-form',
                            'action' => Yii::$app->request->baseUrl."/admin/user",
                            'method' => 'GET',				
                            'options' => ['class' => ''],
                            'validateOnBlur'=>true,
                            'enableClientValidation'=>true,	
                            'fieldConfig' => [
                                'template' => "<div class=\"col-sm-4\">{label}{input}</div>",            
                            ],			
                        ]); 
                        echo $form->field($search, 'name')->label("Name or Email or Username");
                        //echo $form->field($search, 'role')->dropDownList(ArrayHelper::map(Roles::find()->asArray()->all(), 'id', 'name'),['prompt'=>'-- Select User Type --']);

                        echo '<div class="col-sm-12 text-right">';
                        echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button','style'=>'margin-right:5px;']);            
                        echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record','onclick'=>'clearSearch(this)']);
                        echo '</div>';
                        ActiveForm::end();
                    ?> 		                                     
                </div>
            </div> 
            <?=\app\widgets\Alert::widget()?>           
            <div class="box box-info coustum-box">
                <div class="box-body table-responsive">
                    <table class="table table-hover users" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?=$sort->link('name');?></th>
                                <th><?=$sort->link('username');?></th>
                                <th><?=$sort->link('email');?></th>
                                <!-- <th>Role</th> -->
                                <th width="10%"><?=$sort->link('added_on');?></th>
                                <th>Mark as Featured</th>
                                <!--
                                <th width="10%"><?=$sort->link('phone');?></th>
                                -->
                                <!--
                                <th>Status</th>
                                -->
                                <th class="text-center">Action</th>
                                <!-- <?php if((!empty($check_roles) && in_array(Yii::$app->params['roles']['admin'],Yii::$app->user->identity->getRoleIds(Yii::$app->user->identity->id)))){ ?>
                                    <th width="7%" class="text-center">Clock</th>
                                <?php } ?> -->
                            </tr>
                        </thead>
                        <tbody>
						<?php  if(isset($pages->page) && !empty($pages->page)) {$i = (($pages->page + 1)-1)*$pages->defaultPageSize; } else { $i = 0; } if($records){foreach($records as $record){?>
                            <tr>                                
                                <td><?=++$i?>.</td>
                                <td><?=($record->name)?$record->name:"N/A";?></td>
                                <td><?php $url = Yii::$app->params['siteUrl'];
                                    $username2 = $record->username;
                                ?><a href="<?=$url."/".$username2?>"><?=($record->username)?$record->username:"N/A";?></a></td>
                                <td><?=$record->email?></td>
                                <!-- <td>
                                    <?php 
                                    if(!empty($record->roles)){
                                        foreach($record->roles as $role){
                                            if(isset($role->role['name']) && !empty($role->role['name']))
                                              echo $role->role['name'].", ";
                                        }
                                    }
                                    ?>
                                </td> -->
                                <td ><?=date('d M, Y',strtotime($record->added_on));?></td>
                                <td>
                                    <label class="switch" title="Change Status" rel="tooltip">
                                        <input type="checkbox" class="change-status" data-this-id="<?=$record->id?>" <?=($record->featured=='1')?"checked":"";?>>
                                            <span class="slider round"></span>
                                    </label>
                                </td>
                                <!--
                                <td><?=$record->phone?></td>
                                -->
                                <!--
                                <td ><span class="label label-<?=($record->status=="Y")?'success':'danger'?>"><?=($record->status=="Y")?'Active':'In-Active'?></span></td>
                                -->
                                <td class="text-center">
                                <button class="btn btn-primary btn-sm ladda-button edit-user-button" data-id="<?=$record->id?>" data-size="xs" data-style="expand-left"><i class="fa fa-edit"></i> Edit</button>
								</td>
								<?php 
								$name =  ($record->name)?$record->name:"N/A";
								$message  = "Are you sure you want to log in as ".$name."? You will be logged out from current user."
								?>
								<td class="text-center">
                                	<a href="<?=Yii::$app->request->baseUrl?>/admin/user/shadow-user?id=<?=$record->id?>" onclick="return confirm('<?=$message ?>')">
                                	<button class="btn btn-primary btn-sm" style="background: #bd2771;"><i class="fa fa-child"></i> Login As</button></a>
								</td>
								
                                <td class="text-center">

                                <a  href="<?=Yii::$app->request->baseUrl?>/admin/user/delete-user?id=<?=$record->id?>" onclick="return confirm('Are you sure to delete this user?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button></a>

                                    <!-- <div class="btn-group">
                                        <button class="btn btn-primary dropdown-toggle btn-xs ladda-button user-action_<?=$record->id?>" type="button" id="user-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-style="expand-left">
                                            Action
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu animated zoomIn pull-right" role="menu" aria-labelledby="user-action">
                                            <li><a href="javascript:void(0);" class="edit-user-button" data-id="<?=$record->id?>"><i class="fa fa-edit"></i> Edit</a></li>
                                            <?php
                                                //$check_roles = Yii::$app->user->identity->getRoleIds(Yii::$app->user->identity->id); if ((!empty($check_roles) && in_array(Yii::$app->params['roles']['admin'], $check_roles))){ if(Yii::$app->common->checkModuleActionPermission("Payroll", "add-new-template")){ ?>
                                            <li><a href="javascript:void(0);" class="add-template-button" data-id="<?=$record->id?>"><i class="fa fa-inr"></i> Salary Settings </a></li>
                                            <?php //} } ?>
                                            <?php   //if(\app\models\User::checkAdmin($record->id) !== true){?>
									        <li><a  href="<?=Yii::$app->request->baseUrl?>/admin/user/delete-user?id=<?=$record->id?>" onclick="return confirm('Are you sure to delete this user?')"><i class="fa fa-trash"></i> Delete</a></li>
                                            <?php  //} ?>

                                        </ul>
                                    </div>-->
								</td> 
                            </tr>                            
                        <?php } ?>
                            <tr>
                                <td colspan="8" style="padding:2px;">
                                    <div class="pull-right" style="border-top:none;height:50px;">
                                        <?php
                                            if (isset($pages)) {
                                                echo \yii\widgets\LinkPager::widget([
                                                    'pagination' => $pages, 
                                                    'firstPageLabel'=>'First',
                                                    'lastPageLabel'=>'Last'
                                                ]);
                                            }
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <?php }else{?>
                            <tr>
                                <td colspan="11" class="text-center" style="color:red">Result not found!</td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>      
</section>

<?php
$this->registerJs(
    '
    $(document).off("click", "button.add-user-button, button.edit-user-button").on("click", "button.add-user-button, button.edit-user-button", function () {
        
        if($(this).hasAttr("data-id")){
            var url = "'.Yii::$app->request->baseUrl.'/admin/user/add?id="+$(this).attr("data-id");
        }
        else{
            var url = "' . Yii::$app->request->baseUrl . '/admin/user/add";
        }
            var loading_btn = Ladda.create(this);	
        
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    }); ', 
\yii\web\VIEW::POS_END);

$this->registerJs('
    $(document).off("click", "button.add-user-button, .users .edit-user-button").on("click", "button.add-user-button, .users .edit-user-button", function () {
        if($(this).hasAttr("data-id")){
            var url = "'.Yii::$app->request->baseUrl.'/admin/user/add?id="+$(this).attr("data-id");
            var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
        }
        else{
            var url = "'.Yii::$app->request->baseUrl.'/admin/user/add";
            var loading_btn = Ladda.create(this);	
        }
			
		$.ajax({
			type: "POST",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });  

    $(document).off("click", ".users .view-user-button").on("click", ".users .view-user-button", function () {
        var url = "'.Yii::$app->request->baseUrl.'/admin/user/view-user-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });   
    
    $(document).off("click", ".users .edit-personal-button").on("click", ".users .edit-personal-button", function () {
        var url = "'.Yii::$app->request->baseUrl.'/admin/user/edit-personal-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
	}); 
',
\yii\web\VIEW::POS_END);

$this->registerJs('
    //called on click print user details 
    $(document).on("click", "#btn_print, .print-user-details-button", function()
    {
        var id = $(this).attr("data-id");
        if($(this).attr("id") == "btn_print"){
            printUserDetails(id, false);
        } else {
            printUserDetails(id, true);
        }
    });

    function printUserDetails(id, ladda)
    {
        if(ladda){
            var loading_btn = Ladda.create(document.querySelector(".user-action_" + id));
        }
        var print_url = FULL_PATH + "/admin/user/print-user-details?id="+id+"&print=true";
        $.ajax({
            type: "GET",
            dataType: "json",
            url: print_url,
            beforeSend: function () {
                if(ladda){
                    loading_btn.start();
                }
            },
            complete: function () {
                if(ladda){
                    loading_btn.stop();
                }
            },
            success: function (response) {
                if (response.print) {
                    var content = response.print_html; //document.getElementById(divId).innerHTML;
                    var mywindow = window.open(print_url, "Print", "height=600, width=900");
                    if(mywindow){
                        //mywindow.document.write("<html><head><title>User Details</title>");
                        //mywindow.document.write("</head><body >");
                        mywindow.document.write(content);
                        //mywindow.document.write("</body></html>");
    
                        mywindow.document.close();
                        mywindow.focus()
                        mywindow.print();
                        mywindow.close();
                    }
                }
            },
        });
    }
');

$this->registerJs('
    $(".change-status").on("change",function(){
        var featured = 0;
        var url = "'.Yii::$app->request->baseUrl.'/admin/user/update";
        id = $(this).data("this-id");
        if($(this).is(":checked")){
            featured = 1;
        }else{
            featured = 2;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {featured:featured,id:id},
            success:function(){
                toastr.success("Changes Sussessfully saved");
            },
            error:function(){
                toastr.warning("Something Went Wrong, Please Try again");
            }

        });
    });
');

$this->registerJs('
    //add salary details
    $(".add-template-button").on("click",function()
    {
        if($(this).hasAttr("data-id")){
            var url = "'.Yii::$app->request->baseUrl.'/admin/payroll/add-new-template?id="+$(this).attr("data-id");
        }else{
            var url = "'.Yii::$app->request->baseUrl.'/admin/payroll/add-new-template";
        }

        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function(){
                loading_btn.start();
            },
            complete: function(){
                loading_btn.stop();
            },
            success:function(response){
                $("div#add-edit-window").remove();
                $(response).filter("div#add-edit-window").modal({backdrop:"static", show:true});
            }
        });
    });

', \yii\web\VIEW::POS_END);

Pjax::end();