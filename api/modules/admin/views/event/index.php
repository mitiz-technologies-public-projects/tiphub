<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TicketPositions;
use app\models\Cities;
use app\models\Transactions;
use \app\models\Roles;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Events';
Pjax::begin();



?>
<link href="/api/web/css/animation.css" rel="stylesheet">

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?= Yii::$app->request->baseUrl ?>/web/images/page-loader.gif" width="80"
                style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-12 col-sm-12">
                            <div class="navbar-header"><a href="javascript:void(0)"
                                    class="navbar-brand"><?= $this->title ?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-12 col-sm-12 text-right" style="padding:8px 10px;">
                            <button name="search-button" class="btn btn-warning search-button" type="button"
                                onclick="expandSearchPanale(this)"><i
                                    class="fa <?php if ($search_form) echo 'fa-minus';
                                                                                                                                                                else echo 'fa-plus' ?>"></i>
                                Search</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="panel panel-default search-panel" style="display:<?php if ($search_form) echo 'block';
                                                                            else echo 'none' ?>">
                <div class="panel-body">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'action' => Yii::$app->request->baseUrl . "/admin/event",
                        'method' => 'GET',
                        'options' => ['class' => ''],
                        'validateOnBlur' => true,
                        'enableClientValidation' => true,
                        'fieldConfig' => [
                            'template' => "<div class=\"col-sm-4\">{label}{input}</div>",
                        ],
                    ]);
                    echo $form->field($search, 'name')->label("Title");
                    echo $form->field($search, 'category')->dropdownList($categories, [])->label("Select Category");

                    echo '<div class="col-sm-12 text-right">';
                    echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-right:5px;']);
                    echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record', 'onclick' => 'clearSearch(this)']);
                    echo '</div>';
                    ActiveForm::end();
                    ?>
                </div>
            </div>
            <?= \app\widgets\Alert::widget() ?>
            <div class="box box-info coustum-box">
                <div class="box-body table-responsive">
                    <table class="table table-hover events" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?=$sort->link('title');?></th>
                                <th><?=$sort->link('target_amount');?></th>
                                <th>Acheived</th>
                                <!--
                                <th>Image</th>
                                -->
                                <th><?= $sort->link('start_date'); ?></th>
                                <th><?= $sort->link('end_date'); ?></th>
                                <th><?= $sort->link('added_on'); ?></th>
                                <th>Featured</th>
                                <th>Discovered</th>
                                <th colspan="2" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($pages->page) && !empty($pages->page)) {
                                $i = (($pages->page + 1) - 1) * $pages->defaultPageSize;
                            } else {
                                $i = 0;
                            }
                            if ($records) {
                                foreach ($records as $record) { ?>
                            <tr>
                                <td><?= ++$i ?>.</td>
                                <td><a class="newtab-open" style="cursor:pointer;"
                                        data-url="<?= Yii::$app->params['siteUrl'] . "/" . $record->url . "/" . $record->id ?>"><?= !empty($record->title) ? $record->title : "N/A"; ?></a><?php if($record->approved == 1) echo ' <sup class="label label-success">Approved</sup>'; else echo ' <sup class="label label-danger">Pending approval</sup>';?>
                                </td>
                                <td><?= !empty($record->target_amount) ? "$" . $record->target_amount : "0.00"; ?></td>
                                <td><?php $achieved_amount = Transactions::find()->where(['event_id' => $record->id])->sum('amount');
                                            echo (!empty($achieved_amount)) ? "$" . $achieved_amount : "0.00"; ?></td>
                                <!--
                                <td>
                                    <?php
                                    if (!empty($record->image) && file_exists(\Yii::getAlias('@webroot') . "/web/events/" . $record->image)) {
                                        $image = Url::base(true) . '/web/events/' . $record->image;
                                        echo Html::img($image, ['alt' => '', 'style' => 'width:100px;']);
                                    }
                                    ?>
                                </td>
                                -->
                                <td><?= !empty($record->title) ? date('d M, Y', strtotime($record->start_date)) : "N/A"; ?>
                                </td>
                                <td><?= !empty($record->title) ? date('d M, Y', strtotime($record->end_date)) : "N/A"; ?>
                                </td>
                                <td><?= !empty($record->title) ? date('d M, Y', strtotime($record->added_on)) : "N/A"; ?>
                                </td>
                                <td>
                                    <label class="switch" title="Change Status" rel="tooltip">
                                        <input type="checkbox" class="change-status" data-this-id="<?= $record->id ?>"
                                            <?= ($record->featured == '1') ? "checked" : ""; ?>>
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td>
                                    <label class="switch" title="Change Status" rel="tooltip">
                                        <input type="checkbox" class="change-discovered-status"
                                            data-this-id="<?= $record->id ?>"
                                            <?= ($record->discovered == '1') ? "checked" : ""; ?>>
                                        <span class="slider round"></span>
                                    </label>
                                </td>


                                <td align="center">
                                    <div class="btn-group">
                                        <button
                                            class="btn btn-primary dropdown-toggle btn-xs ladda-button customer_report-action_<?=!empty($record->user['id'])?$record->user['id']:""?> event-action_<?= $record->id ?>"
                                            type="button" id="customer_report-action-<?=!empty($record->user['id'])?$record->user['id']:""?>"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            data-style="expand-left">
                                            <span class="ladda-label">Action</span>
                                        </button>
                                        <ul class="dropdown-menu animated zoomIn pull-right" role="menu"
                                            aria-labelledby="customer_report-action-<?=!empty($record->user['id'])?$record->user['id']:""?>">
                                            <li><a href="<?= Yii::$app->request->baseUrl ?>/admin/event/add?id=<?= $record->id ?>" class="change-customer-password"><i class="fa fa-edit"></i> Edit</a></li>
                                            <li><a href="<?= Yii::$app->request->baseUrl ?>/admin/event/delete?id=<?= $record->id ?>" onclick="return confirm('Are you sure to delete this event?')" class="change-customer-password"><i class="fa fa-trash"></i> Delete</a></li>
                                            <?php 
                                            if (in_array($record->category, [59,71]) ) {
                                            ?>
                                            <li><a href="javascript:void(0);" event-id ="<?= $record->id ?>" data-id="<?=!empty($record->user['id'])?$record->user['id']:""?>" class="event-detail"><i class="fa fa-exchange"></i>Manage Invoice</a></li>
                                            <?php }?>
                                            <!-- <button class="btn btn-primary btn-sm ladda-button edit-user-button" data-id="</?=$record->id?>" data-size="xs" data-style="expand-left"><i class="fa fa-edit"></i> Send invoice</button> -->
                                        </ul>
                                    
                           
                                    </div>
                                </td>




                                <!-- <td class="text-center">
                                            <a href="</?= Yii::$app->request->baseUrl ?>/admin/event/add?id=</?= $record->id ?>"><button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</button></a>
                                        </td>
                                        <td class="text-center">
                                            <a href="</?= Yii::$app->request->baseUrl ?>/admin/event/delete?id=</?= $record->id ?>" onclick="return confirm('Are you sure to delete this event?')"><button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</button></a>
                                        </td> -->
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="11" style="padding:2px;">
                                    <div class="pull-right" style="border-top:none;height:50px;">
                                        <?php
                                                if (isset($pages)) {
                                                    echo \yii\widgets\LinkPager::widget([
                                                        'pagination' => $pages, 
                                                        'firstPageLabel'=>'First',
                                                        'lastPageLabel'=>'Last'
                                                    ]);
                                                }
                                            ?>
                                    </div>
                                </td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td colspan="11" class="text-center" style="color:red">Result not found!</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

<?php
$this->registerJs(
    '
    $(document).off("click", "button.add-user-button, .users .edit-user-button").on("click", "button.add-user-button, .users .edit-user-button", function () {
        if($(this).hasAttr("data-id")){
            var url = "' . Yii::$app->request->baseUrl . '/admin/user/add?id="+$(this).attr("data-id");
            var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
        }
        else{
            var url = "' . Yii::$app->request->baseUrl . '/admin/user/add";
            var loading_btn = Ladda.create(this);	
        }
			
		$.ajax({
			type: "POST",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });  
    $(".event-detail").click(function(e){
        var event_id =  $(this).attr("event-id");
        var url = "' . Yii::$app->request->baseUrl . '/admin/event/show-invoice?id="+event_id;
        var loading_btn = Ladda.create(document.querySelector(".event-action_"+event_id));	
		$.ajax({
			type: "POST",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#send-invoice").remove();
				$(response).filter("div#send-invoice").modal({backdrop:"static",show:true});				   
			}
		});	
    });

    $(document).off("click", ".users .view-user-button").on("click", ".users .view-user-button", function () {
        var url = "' . Yii::$app->request->baseUrl . '/admin/user/view-user-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });   
    
    $(document).off("click", ".users .edit-personal-button").on("click", ".users .edit-personal-button", function () {
        var url = "' . Yii::$app->request->baseUrl . '/admin/user/edit-personal-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
	}); 
',
    \yii\web\VIEW::POS_END
);

$this->registerJs('
    //called on click print user details 
    $(document).on("click", "#btn_print, .print-user-details-button", function()
    {
        var id = $(this).attr("data-id");
        if($(this).attr("id") == "btn_print"){
            printUserDetails(id, false);
        } else {
            printUserDetails(id, true);
        }
    });

    function printUserDetails(id, ladda)
    {
        if(ladda){
            var loading_btn = Ladda.create(document.querySelector(".user-action_" + id));
        }
        var print_url = FULL_PATH + "/admin/user/print-user-details?id="+id+"&print=true";
        $.ajax({
            type: "GET",
            dataType: "json",
            url: print_url,
            beforeSend: function () {
                if(ladda){
                    loading_btn.start();
                }
            },
            complete: function () {
                if(ladda){
                    loading_btn.stop();
                }
            },
            success: function (response) {
                if (response.print) {
                    var content = response.print_html; //document.getElementById(divId).innerHTML;
                    var mywindow = window.open(print_url, "Print", "height=600, width=900");
                    if(mywindow){
                        //mywindow.document.write("<html><head><title>User Details</title>");
                        //mywindow.document.write("</head><body >");
                        mywindow.document.write(content);
                        //mywindow.document.write("</body></html>");
    
                        mywindow.document.close();
                        mywindow.focus()
                        mywindow.print();
                        mywindow.close();
                    }
                }
            },
        });
    }
');

$this->registerJs('
    $(".change-status").on("change",function(){
        var featured = 0;
        var url = "' . Yii::$app->request->baseUrl . '/admin/event/update";
        id = $(this).data("this-id");
        if($(this).is(":checked")){
            featured = 1;
        }
        $.ajax({
            type: "POST",
            url: url,
            data: {featured:featured,id:id},
            success:function(){
                toastr.success("Changes Successfully saved");
            },
            error:function(){
                toastr.warning("Something Went Wrong, Please Try again");
            }

        });
    });
');

$this->registerJs('
    $(".change-discovered-status").on("change",function(){

        var discovered = 0;
        var url = "' . Yii::$app->request->baseUrl . '/admin/event/update-discovered";

        id = $(this).data("this-id");
        if($(this).is(":checked")){
            discovered = 1;
        }
        
        $.ajax({
            type: "POST",
            url: url,
            data: {discovered:discovered,id:id},
            success:function(){
                toastr.success("Changes Successfully saved");
            },
            error:function(){
                toastr.warning("Something Went Wrong, Please Try again");
            }

        });
    });
');

$this->registerJs('
$(".newtab-open").on("click",function(){
    var url = $(this).data("url");
    window.open(url, "_blank").focus();
});
');

$this->registerJs('
    //add salary details
    $(".add-template-button").on("click",function()
    {
        if($(this).hasAttr("data-id")){
            var url = "' . Yii::$app->request->baseUrl . '/admin/payroll/add-new-template?id="+$(this).attr("data-id");
        }else{
            var url = "' . Yii::$app->request->baseUrl . '/admin/payroll/add-new-template";
        }

        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function(){
                loading_btn.start();
            },
            complete: function(){
                loading_btn.stop();
            },
            success:function(response){
                $("div#add-edit-window").remove();
                $(response).filter("div#add-edit-window").modal({backdrop:"static", show:true});
            }
        });
    });

', \yii\web\VIEW::POS_END);

Pjax::end();