<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
use app\models\SubCategory;
/* @var $this yii\web\View */

$this->title = ($model->id) ? 'Update Event' : 'Add Event';
?>
<style>
.field-events-child_category{
    display:none;
}  
</style>
<!-- content goes here -->

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-lg-12">
			<img src="<?= Yii::$app->request->baseUrl ?>/web/images/page-loader.gif" width="80" style="left: 45%;position: absolute;top: 54%;" class="hide">
			<nav class="navbar navbar-default" role="navigation">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-7 col-xs-6 col-sm-6">
							<div class="navbar-header"><a href="javascript:void(0)" class="navbar-brand"><?= $this->title ?></a></div>
						</div>
						<div class="col-md-5 col-xs-6 col-sm-6 text-right" style="padding:8px 10px;">
							<a class="btn btn-primary" href="<?= Yii::$app->request->baseUrl ?>/admin/event"><i class="fa fa-undo"></i> Back</a>
						</div>
					</div>
				</div>
			</nav>
			<?= \app\widgets\Alert::widget() ?>
			<div class="box box-info coustum-box">
				<div class="box-body">
					<?php
					$form = ActiveForm::begin([
						'id' => 'add-static-content-form',
						'options' => ['class' => 'form','enctype' => 'multipart/form-data'],
						'validateOnBlur' => true,
						'enableClientValidation' => true,
						'fieldConfig' => [
							'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
						],
					]);

					echo $form->field($model, 'id')->hiddenInput(['value' => $model->id])->label(false)->error(false);
					
						// print_r( $model->image);
						// die;
					//set fundraising default value
					$model->fundraising_for==null ? $model->fundraising_for=0:$model->fundraising_for;

					$fundraisinglist = [0 => 'Yourself or another person (These donations go to a person instead of an organization)', 1 => 'A nonprofit or charity (Donations will go to an organization that has an EIN number.)'];
					echo $form->field($model, 'fundraising_for')->label('Who are you fundraising for?')->radioList($fundraisinglist);

					//hide fields
					?>
					<div class="row" id="fundraisinghide">
						<div class="col-md-6">
							<?php

							echo $form->field($model, 'non_profit_name')->label('Non Profit Name');
							echo $form->field($model, 'city')->label('City');
							?>
						</div>
						<div class="col-md-6">
							<?php
								echo $form->field($model, 'state')->label('State');
								echo $form->field($model, 'ein_number')->label('EIN Number');
							?>
						</div>
					</div>

					<?php

					//set category default value
					$model->category==null ? $model->category=0:$model->category;

					echo $form->field($model, 'category')
						->label('What are you fundraising for?')
						->dropDownList(
							$categorylist, // Flat array ('id'=>'label')
							['prompt' => 'Choose a category'] // options
						);
						$subCategory = $model->getSubCategory();
					echo $form->field($model, 'sub_category')
					->label('Sub Category')
					->dropDownList(
					    $subCategory, // Flat array ('id'=>'label')
					    ['prompt' => 'Choose sub category'] // options
					    );
					echo $form->field($model, 'sub_sub_category')
					->label('Sub Sub Category')
					->dropDownList(
					    ArrayHelper::map(SubCategory::find()->all(), "id", "name"), // Flat array ('id'=>'label')
					    ['prompt' => 'Choose sub sub category'] // options
					    );
					//set existing_gofundme_campaign default value
					$model->existing_gofundme_campaign==null ? $model->existing_gofundme_campaign=1:$model->existing_gofundme_campaign;
					
					$existinggofundmecampaignlist = [1 => 'Yes', 0 => 'No'];
					echo $form->field($model, 'existing_gofundme_campaign')->inline(true)->label("Does your event have an existing gofundme campaign?")->radioList($existinggofundmecampaignlist);

					//set recurring default value
					$model->recurring==null ? $model->recurring=0:$model->recurring;
					
					$eventTypelist = [0 => 'One time event', 1 => 'Recurring', 2 => 'Ongoing'];
					echo $form->field($model, 'recurring')->inline(true)->label("Event type")->radioList($eventTypelist);

					//set show_name default value
					$model->show_name==null ? $model->show_name=0:$model->show_name;

					$eventTypelist = [0 => 'Anonymous', 1 => 'First Name', 2 => 'Full Name', 3 => 'Initials'];
					echo $form->field($model, 'show_name')->inline(true)->label("How would you like public supporters names to show up?")->radioList($eventTypelist);

					?>
					<div class="row">
						<div class="col-md-6">
							<?php
							echo $form->field($model, 'title')->label('Event Title');

							if($model->start_date != null)
							{
								$startdateformated = date("Y-m-d\TH:i", strtotime($model->start_date));
								$enddateformated = date("Y-m-d\TH:i", strtotime($model->end_date));
							}else{
								$startdateformated = "";
								$enddateformated = "";
							}

							echo $form->field($model, 'start_date')->label('Start Date')->textInput(['type' => 'datetime-local', 'value' => $startdateformated]);

							?>
						</div>
						<div class="col-md-6">
							<?php

							echo $form->field($model, 'target_amount')->label('Targeted amount')->textInput(['type' => 'number']);

							echo $form->field($model, 'end_date')->label('End Date')->textInput(['type' => 'datetime-local', 'value' => $enddateformated]);
							?>
						</div>
					</div>
					<?php
					//echo $form->field($model, 'image')->label('Event Image')->fileInput();

					echo $form->field($model, 'description')->label('Event Description')->widget(CKEditor::className(), [
						'options' => ['rows' => 10],
						'preset' => 'custom',
						'clientOptions' => [
							'allowedContent' => true,
							'height' => "300",
							'toolbarGroups' => [
								['name' => 'basicstyles', 'groups' => ['basicstyles']],
								['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align']],
								['name' => 'styles',],

							]
						]
					]);
					echo $form->field($model, 'meta_title')->textInput()->label('Meta Title');
					echo $form->field($model, 'meta_key')->textInput()->label('Meta Keyword');
					echo $form->field($model, 'meta_description')->textarea()->label('Meta Description');
					echo '<div class="row"><div class="col-md-6">';
					echo $form->field($model, 'service_fee')->textInput(['type' => 'number'])->label('Service Fee(%)');
					echo $form->field($model, 'approved')->inline(true)->radioList([1=>'Approve',0=>'Disapprove'])->label(false);
					echo '</div></div>';
					?>
					<?php
					echo Html::submitButton(($model->id) ? 'Update' : 'Save', ['class' => 'btn btn-success pull-right', 'name' => 'login-button']) . ' <a class="btn btn-primary pull-right" href="' . Yii::$app->request->baseUrl . '/admin/event" style="margin-right:5px;"><i class="fa fa-undo"></i> Back</a>';
					ActiveForm::end();
					?>
				</div>
			</div>

		</div>
	</div>

</section>
<?php
if (empty($subCategory)) {
    $this->registerJs('
        $(".field-events-sub_category").hide();
',\yii\web\VIEW::POS_END);
}

$script = <<< JS

//hide/show on page load
if ($model->fundraising_for == 1) {

	$("#fundraisinghide").show();

}else{
	$("#fundraisinghide").hide();
}
    

//hide/show on radio button change
$("input[name='Events[fundraising_for]']").on("change", function() {

    if ($(this).val() == "1") {

        $("#fundraisinghide").show();

    }

    if ($(this).val() == "0") {

        $("#fundraisinghide").hide();

    }


});
JS;
$this->registerJs(
    '
   $("#events-category").change(function(e){
    var category_id = $(this).val();
    if(category_id == 5 || category_id == 59 || category_id == 71){
        $(".field-events-sub_category").show();
    }else{
        $(".field-events-sub_category").hide();
    }
    if(category_id !=""){
        var url = "' . Yii::$app->request->baseUrl . '/admin/event/get-sub-cateory?id="+category_id;
    	$.ajax({
    		url: url,
    		type: "GET",
    		dataType: "json",
    		success: function(response) {
                options = "";
                if (response.status == "OK") {
                    $(".field-events-sub_category").show();
                    var type = response.data;
                    $.each(type, function(index, value) {
					   options += "<option value=" + index + ">" + value + "</option>";
				    });
                }
                $("#events-sub_category").empty();
			    $(options).appendTo("#events-sub_category");
    		}
    	});
    }
});
$(document).ready(function () {
    var category_id = $("#events-category").val();
    if(category_id == 5 || category_id == 59 || category_id == 71){
        $(".field-events-sub_category").show();
    }else{
        $(".field-events-sub_category").hide();
    }
});
    
',\yii\web\VIEW::POS_END);
$this->registerJs($script);

$this->registerJs(
	\yii\web\VIEW::POS_END
);
