<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */

?>
<style>
.modal-body {
    padding: 0px 20px 20px 20px !important;
}
</style>
<!-- content goes here -->

<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">
		<div class="col-lg-12">
			
			<div>
				<div class="box-body">
					<?php
					$form = ActiveForm::begin([
						'id' => 'add-payment-form',
						'options' => ['class' => 'form','enctype' => 'multipart/form-data'],
						'validateOnBlur' => true,
						'enableClientValidation' => true,
						'fieldConfig' => [
							'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
						],
					]);
					?>
					<div class="row">
						<div class="col-md-6">
							<?php
							echo $form->field($model, 'invoice_id')->textInput(["readonly" => $model->invoice_id?true:false])->label('Invoice  Id:');
							echo $form->field($model, 'amount')->textInput()->label('Amount:');
							?>
						</div>
						<div class="col-md-6">
							<?= $form->field($model, 'payment_service')->label('Payment Service:')->dropdownList($categories, []);?>
							<?= $form->field($model, 'payment_date')->label('Payment Date')->textInput(['type' => 'datetime-local']);?>
						</div>
					</div>
					<?php
					echo '<div class="col-sm-12 text-right" style="margin-top: 20px;">';
					echo Html::Button('Cancel', ['class' => 'btn btn-danger', 'data-dismiss'=>'modal','style'=>"margin-right:5px;"]);
					echo Html::submitButton( 'Save', ['class' => 'btn btn-success pull-right', 'name' => 'login-button','style'=>'margin-right: -15px;']) ;
					echo '</div>';
					ActiveForm::end();
					?>
				</div>
			</div>

		</div>
	</div>

</section>