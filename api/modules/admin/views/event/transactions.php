<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TicketPositions;
use app\models\Cities;
use app\models\Transactions;
use \app\models\Roles;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'All Transactions';
Pjax::begin();



?>
<link href="/api/web/css/animation.css" rel="stylesheet">

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-12">
            <img src="<?= Yii::$app->request->baseUrl ?>/web/images/page-loader.gif" width="80"
                style="left: 45%;position: absolute;top: 54%;" class="hide">
            <nav class="navbar navbar-default" role="navigation">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-7 col-xs-12 col-sm-12">
                            <div class="navbar-header"><a href="javascript:void(0)"
                                    class="navbar-brand"><?= $this->title ?></a></div>
                        </div>
                        <div class="col-md-5 col-xs-12 col-sm-12 text-right" style="padding:8px 10px;">
                            <button name="search-button" class="btn btn-warning search-button" type="button"
                                onclick="expandSearchPanale(this)"><i
                                    class="fa <?php if ($search_form) echo 'fa-minus';else echo 'fa-plus' ?>"></i>
                                Search</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="panel panel-default search-panel" style="display:<?php if ($search_form) echo 'block';
                                                                            else echo 'none' ?>">
                <div class="panel-body">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'search-form',
                        'action' => Yii::$app->request->baseUrl . "/admin/event/all-transactions",
                        'method' => 'GET',
                        'options' => ['class' => ''],
                        'validateOnBlur' => true,
                        'enableClientValidation' => true,
                        'fieldConfig' => [
                            'template' => "<div class=\"col-sm-4\">{label}{input}</div>",
                        ],
                    ]);
                    echo $form->field($search, 'name')->label("Name");
                    echo $form->field($search, 'event_id')->dropdownList($categories, [])->label("Select Event");
                    echo '<div class="col-sm-12 text-right">';
                    echo Html::submitButton('Submit', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'margin-right:5px;']);
                    echo Html::Button('Clear', ['class' => 'btn btn-danger clear_record', 'onclick' => 'clearSearch(this)']);
                    echo '</div>';
                    ActiveForm::end();
                    ?>
                </div>
            </div>
            <?= \app\widgets\Alert::widget() ?>
            <div class="box box-info coustum-box">
                <div class="box-body table-responsive">
                    <table class="table table-hover events" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th><?=$sort->link('name');?></th>
                                <th><?= $sort->link('event_id'); ?></th>
                                <th><?=$sort->link('amount');?></th>
                                <th><?= $sort->link('added_on'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($pages->page) && !empty($pages->page)) {
                                $i = (($pages->page + 1) - 1) * $pages->defaultPageSize;
                            } else {
                                $i = 0;
                            }
                            if ($records) {
                                foreach ($records as $record) { ?>
                            <tr>
                                <td><?= ++$i ?>.</td>
                                <td><?= !empty($record->name) ? $record->name : "N/A"; ?></td>
                                <td><?= !empty($record['event']['title']) ? $record['event']['title'] : "N/A"; ?></td>
                                <td><?= !empty($record->amount) ? "$" . $record->amount : "0.00"; ?></td>
                                <td><?= !empty($record->added_on) ? date('d M, Y', strtotime($record->added_on)) : "N/A"; ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="11" style="padding:2px;">
                                    <div class="pull-right" style="border-top:none;height:50px;">
                                        <?php
                                                if (isset($pages)) {
                                                    echo \yii\widgets\LinkPager::widget([
                                                        'pagination' => $pages, 
                                                        'firstPageLabel'=>'First',
                                                        'lastPageLabel'=>'Last'
                                                    ]);
                                                }
                                            ?>
                                    </div>
                                </td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td colspan="11" class="text-center" style="color:red">Result not found!</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

<?php
$this->registerJs(
    '
    $(document).off("click", "button.add-user-button, .users .edit-user-button").on("click", "button.add-user-button, .users .edit-user-button", function () {
        if($(this).hasAttr("data-id")){
            var url = "' . Yii::$app->request->baseUrl . '/admin/user/add?id="+$(this).attr("data-id");
            var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
        }
        else{
            var url = "' . Yii::$app->request->baseUrl . '/admin/user/add";
            var loading_btn = Ladda.create(this);	
        }
			
		$.ajax({
			type: "POST",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });  
   

    $(document).off("click", ".users .view-user-button").on("click", ".users .view-user-button", function () {
        var url = "' . Yii::$app->request->baseUrl . '/admin/user/view-user-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
    });   
    
    $(document).off("click", ".users .edit-personal-button").on("click", ".users .edit-personal-button", function () {
        var url = "' . Yii::$app->request->baseUrl . '/admin/user/edit-personal-details?id="+$(this).attr("data-id");
        var loading_btn = Ladda.create(document.querySelector(".user-action_" + $(this).attr("data-id")));
			
		$.ajax({
			type: "GET",
			url: url,								
			beforeSend: function(){			
				loading_btn.start();											
			},
			complete: function(){			
				loading_btn.stop();					
			},
			success:function(response){			
				$("div#add-edit-window").remove();
				$(response).filter("div#add-edit-window").modal({backdrop:"static",show:true});				   
			}
		});		
	}); 
',
    \yii\web\VIEW::POS_END
);

Pjax::end();