<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\EventTicketBookings;
use app\models\EventInvoice;
use app\models\InvoicePayment;
?>
<?php 
$event_details =  EventTicketBookings::findOne(['event_id'=>$id]);
?>
<style>
#total-amount-paid {
    position: fixed;
    background-color: #b76797;
    color: #fff;
    padding: 10px;
    font-size: 16px;
    border-radius: 13px;
}

.modal-body {
    padding: 20px 20px 20px 20px !important;
}
</style>
<div class="container-fluid">
	<div class="row">
	<div class="col-lg-4 col-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3 style="font-size:25px"><?= !empty($total_tickets_sold)?$total_tickets_sold:0 ?></h3>
            <p>Total Tickets Sold</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
      </div>
       <div class="col-lg-4 col-6">
        <!-- small box -->
        <div class="small-box" style="background-color: #8ecf6d;">
          <div class="inner">
            <h3 style="font-size:25px">$<?= !empty($total_amount)?$total_amount:0 ?></h3>
            <p>Total Amount</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-6">
        <!-- small box -->
        <div class="small-box" style="background-color: #00c0ef;">
          <div class="inner">
            <h3 style="font-size:25px">$<?=  number_format($total_service_fee, 2) ?></h3>
            <p>Service Charge(<?=!empty($event_details->service_fee)?$event_details->service_fee:0 ?>%)</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
      </div>
    </div>
    <?php 
    $eventInvoice =  EventInvoice::find()->where(["event_id"=>$id])->one();
    if (!empty($eventInvoice)) {
        $total_payment= InvoicePayment::find()->where(['event_id'=>$id])->sum('amount');
        $total_payment = ! empty($total_payment)?$total_payment:0;
        echo "<div  id='total-amount-paid'> Total amount Paid : <span class='amount-paid-value'style='font-size: 21px;'>$" . $total_payment."</span></div>" ;
        echo '<div style=" color:red;text-align: left;font-size: 18px;margin-top: 60px;">Invoice has already been sent for this event.</div>';
    }
    ?>
    
    <div class="frm-btn">
	<div class="popup-message pull-left"></div>
	<div class="text-right "style="margin-top: 15px;">						
		<?php			
		$form = ActiveForm::begin([
		    'id' => 'send-invoice-form',
		    'action' =>[ 
		        "send-invoice",
		        'id'=>$id,
		    ],
		    'method' => 'GET',
		]);
		echo '<div class="col-sm-12 text-right">';
		echo Html::Button('Cancel', ['class' => 'btn btn-danger', 'data-dismiss'=>'modal','style'=>"margin-right:5px;"]);
		if (empty($eventInvoice)) {
		    echo Html::submitButton('Send Invoice', ['disabled' => empty($total_service_fee) || $model->end_date < date("Y-m-d H:i:s")?'disabled':false,'class' => 'btn btn-success send-invoice-button', 'name'=>"send-invoice-button","style"=>"margin-right: -15px;"]);
		}else{
		    echo Html::submitButton('Re-Send Invoice', ['class' => 'btn btn-success send-invoice-button', 'name'=>"send-invoice-button","style"=>"margin-right: 5px; background-color: #2f6a5f;"]);
		    echo Html::Button('Add payment', ['class' => 'btn btn-success add-payment-button','event-id'=>$id, 'style'=>"margin-right:-15px;"]);
		}
		echo '</div>';
		ActiveForm::end();
		?>
	</div>
</div>
 </div>
 <?php
$this->registerJs(
    '
    $(".add-payment-button").click(function(e){
        var event_id =  $(this).attr("event-id");
        var url = "' . Yii::$app->request->baseUrl . '/admin/event/add-payment?id="+event_id;
		$.ajax({
			type: "POST",
			url: url,								
			success:function(response){			
				$("div#add-payment").remove();
				$(response).filter("div#add-payment").modal({backdrop:"static",show:true});				   
			}
		});	
// ".Yii::$app->request->baseUrl ."/admin/event/add-payment?id=".$id."
    });
    
',\yii\web\VIEW::POS_END);?>    
        