<!DOCTYPE html>
<html>
	<body>
		<div class="invoice-box">
			<table cellpadding="0" cellspacing="0">
				<tr class="top">
					<td colspan="2">
						<table>
							<tr>
								<td  align="center">
									<div class="company-logo"><img src="/assets/tiphub-app.png" width="150"/></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="information">
					<td colspan="2">
						<table>
							<tr>
								<td>
									<?=!empty($user)?$user->first_name. " ".$user->last_name:"N/A" ?><br />
									<?= !empty($user)?$user->email:"N/A" ?><br />
									<?= !empty($user)?$user->phone:"N/A" ?>
								</td>
								<td>
									Event Title: <b><?= !empty($event)?$event->title:"N/A" ?></b><br />
									Invoice No: <b><?= !empty($eventInvoice->invoice_id)?$eventInvoice->invoice_id:"N/A" ?></b><br />
									Date: <b><?= date("d-m-Y")?></b>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="heading">
					<td>Invoice Details</td>
					<td>Price</td>
				</tr>
				<tr class="item">
					<td>Total Tickets:</td>
					<td><b><?=$sold_ticket ?></b></td>
				</tr>
				<tr class="item">
					<td>Total Amount($):</td>
					<td><b>$<?= !empty($eventInvoice->total_sold_amount)?$eventInvoice->total_sold_amount:0 ?></b></td>
				</tr>
				<tr class="item">
					<td>Service Charges(<?=!empty($eventInvoice->service_charge)?$eventInvoice->service_charge:0 ?>%):</td>
					<td><b>$<?= !empty($eventInvoice->amount)?$eventInvoice->amount:0 ?></b></td>
				</tr>
				<tr class="item last">
					<td><b>Total Amount To Be Paid($): </b></td>
					<td><b><h2>$<?= !empty($eventInvoice->amount)?$eventInvoice->amount:0 ?></h2></b></td>
				</tr>
			</table><br />
			<b>Please make payment within 30 days to <a href="<?=Yii::$app->params['siteUrl'] ?>">tiphub.co</a>.You can find payment options here: <a href="https://tiphub.co/tiphub.co?invoice=<?=$eventInvoice->id ?>">https://tiphub.co/tiphub.co?invoice=<?=$eventInvoice->id ?></a></b>
			<div><h2 align="center">Thank You<h2></div>
		</div>
	</body>
</html>