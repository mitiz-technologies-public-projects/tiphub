$(document).ready(function(){
    toastr.options = {
        "closeButton": false,
		"closeDuration": false,
		"closeEasing": false,
		"closeClass": "toast-close-button",
		"closeOnHover": true,
		"containerId": "toast-container",
		"debug": false,
		"escapeHtml": false,
		"extendedTimeOut": 1000,
		"hideDuration": 1000,
		"hideEasing": "linear",
		"hideMethod": "fadeOut",
		"iconClass": "toast-info",
		"iconClasses": {
			"error": "toast-error",
			"info": "toast-info",
			"success": "toast-success",
			"warning": "toast-warning"
		},
		"messageClass": "toast-message",
		"newestOnTop": false,
		"onclick": null,
		"positionClass": "toast-top-right",
		"preventDuplicates": true,
		"progressBar": true,
		"progressClass": "toast-progress",
		"rtl": false,
		"showDuration": 300,
		"showEasing": "swing",
		"showMethod": "fadeIn",
		"tapToDismiss": true,
		"target": "body",
		"timeOut": 5000,
		"titleClass": "toast-title",
		"toastClass": "toast",
    }
});
$.fn.hasAttr = function(name) {
  return this.attr(name) !== undefined;
};

function expandSearchPanale(t) {
  if (
    $(t)
      .find("i")
      .hasClass("fa-plus")
  ) {
    $(t)
      .find("i")
      .removeClass("fa-plus")
      .addClass("fa-minus");
    $(".search-panel").slideDown();
  } else {
    $(t)
      .find("i")
      .removeClass("fa-minus")
      .addClass("fa-plus");
    $(".search-panel").slideUp();
  }
}

function clearSearch(t) {
  var frm = $(t).closest("form");
  frm.find("input[type=text], textarea, select").val("");
  window.location.href = frm.attr("action");
}

var current_tr = "";
function toggleExpand(t) {
  var target_tr = $(t)
    .parent()
    .next()
    .prop("id");
  if (current_tr != "" && current_tr != target_tr) {
    $("#" + current_tr).hide();
  }
  if (current_tr == target_tr) {
    $("#" + target_tr).toggle();
  } else {
    $("#" + target_tr).show();
  }
  current_tr = target_tr;
}

function toggleClosed(t) {
  $("#" + t).toggle();
}

setInterval(function(){
    var now = new Date();
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();

    if(hour.toString().length == 1) {
         hour = "0"+hour;
    }
    if(minute.toString().length == 1) {
         minute = "0"+minute;
    }
    if(second.toString().length == 1) {
         second = "0"+second;
    }
    $("#digital-clock #txt").text(hour+":"+minute+":"+second);

    $(".dashboard-clock").text(hour + ":" + minute);

}, 1000);

$("#clock_in, #clock_out").on("click", function(e)
{
    var user_id = $(this).attr("data-id");
    e.preventDefault();
    var url = "";
    var loading_btn = Ladda.create(this);
    if($(this).attr("id") == "clock_in"){
        action = "clock-in";
    }else{
        action = "clock-out";
    }

    $.ajax({
        type: "POST",
        url: FULL_PATH + "/" + MODULE_NAME + "/user/"+action+"?user_id="+user_id,
        dataType:"json",
        beforeSend: function(){
            loading_btn.start();
        },
        complete: function(){
            loading_btn.stop();
        },
        success:function(response){
          location.reload();
        }
    });
});

//View salary details
$(".view-template-button").on("click", function()
{
    var element = this;
    if($(this).attr("id") != "developer"){var loading_btn = Ladda.create(this)}
    $.ajax({
        type: "POST",
        url: "view-salary-details?user_id="+$(this).attr("data-user_id")+"&template_id="+$(this).attr("data-template_id"),
        beforeSend: function(){
            if($(element).attr("id") != "developer"){loading_btn.start()}
        },
        complete: function(){
            if($(element).attr("id") != "developer"){loading_btn.stop()}
        },
        success:function(response){
            $(response).filter("div#view-salary-details").modal({backdrop:"static", show:true});
        }
    });
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

    function getStates(val, id){
      var url = FULL_PATH + "/" + MODULE_NAME + "/page/get-states";
      $("#settings-city_id").html("<option value=''>--Select City--</option>")
      $.ajax({
          type: "POST",
          url: url,
          dataType:"json",
          data: { "country_id": val },
          cache: false,
          beforeSend: function() {
              //$("#tab-loader").show();
          },
          complete: function() {
              //$("#tab-loader").hide();
          },
          success: function(response) {
              $(id).html(response.html);
          }
      });
    }

    function getCities(val, id){
      var url = FULL_PATH + "/" + MODULE_NAME + "/page/get-cities";
      $.ajax({
          type: "POST",
          url: url,
          dataType:"json",
          data: { "state_id": val },
          cache: false,
          beforeSend: function() {
              //$("#tab-loader").show();
          },
          complete: function() {
              //$("#tab-loader").hide();
          },
          success: function(response) {
              $(id).html(response.html);
          }
      });
    }

    